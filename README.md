OPA DB Connector
================

Version 1.1.0
-------------

The OPA DB Connector (Connector) is a platform for integrating Oracle Policy Automation (OPA) with Relational Databases.

To build the whole project run,

```
mvn clean install
```

The Connector consists of,

* Shared - All code which is required across modules
* Utility - This is the CLI package for the Connector, containing all
            utility functions
* Model Layer - This contains the database model layer for the Connector
* Service Model Layer - This contains all models used by the service layer
* Service Layer - This contains all REST services and processing code
                  the Connector uses to implement the OPA Connector Framework
* Management Layer - This is the GUI layer for the Connector
* Documentation - This contains the documentation for the Connector
* Web Layer - This is the full deployment, pulling together all modules

To run the project,

```
cd web-layer
mvn jetty:run
```

This will expose the Connector on localhost:9999.