/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.docs;

import com.google.common.io.ByteStreams;
import lombok.val;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Serves the asciidoc html documentation.
 */
public class DocumentationServlet extends HttpServlet {

    /**
     * Documentation.
     */
    private byte[] documentation;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (documentation == null) {
            val is = getClass().getResourceAsStream("/index.html");
            documentation = ByteStreams.toByteArray(is);
        }

        // return documentation
        resp.setContentType("text/html");
        ByteStreams.copy(new ByteArrayInputStream(documentation), resp.getOutputStream());
    }
}
