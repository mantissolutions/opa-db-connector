=== Checkpoints
Checkpoint requests either persist or load checkpoint data. Checkpoint data is base64 encoded OPA session data.
The Connector will use a 128-bit UID value as the key. All data is stored in a single table, defined in the Mapping.