=== How Mappings Work
Once a Mapping is deployed it is exposed using a unique service name with a link for each supported version of the Connector Framework.

Mappings utilise the Datasource mapping defined for that mapping. It then handles SOAP requests as defined in the <<_opa_webservice_connector_framework>>.

For example a mapping called "Test Mapping" might be exposed at, /services/test_mapping_1/{OPA Version}