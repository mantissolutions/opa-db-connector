=== How Datasources Work
A Datasource mapping once defined is instantiated in a lazy manner on first use. No datasource
is created until an operation which needs database access is called.

The Connector then builds a connection pool based on the passed user credentials.
This is done once and from this point the datasource is active for that user.
To close the datasource un-deploy it.

[NOTE]
====
The underlying connection pool implementation is https://github.com/brettwooldridge/HikariCP[HikariCP, window="_blank"].
This is a robust and high performance implementation, for more information on what makes it so great see the following links,

* https://github.com/brettwooldridge/HikariCP/wiki/Down-the-Rabbit-Hole[/HikariCP/wiki/Down-the-Rabbit-Hole, window="_blank"]
* https://github.com/brettwooldridge/HikariCP-benchmark[/HikariCP-benchmark, window="_blank"]
* http://blog.wix.engineering/2015/04/28/how-does-hikaricp-compare-to-other-connection-pools[/how-does-hikaricp-compare-to-other-connection-pools, window="_blank"]
* https://github.com/brettwooldridge/HikariCP/wiki/About-Pool-Sizing[/HikariCP/wiki/About-Pool-Sizing, window="_blank"]
====