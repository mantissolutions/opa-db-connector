=== Creating a Datasource
To create a Datasource navigate to the Datasources page

[[img-datasource-screen]]
image::datasource-screen.png[title="Datasource Screen"]

Then select the Add Datasource button or right click on the table and select Add.
This will display the create datasource form.

[[img-datasource-form]]
image::datasource-form.png[title="Create Datasource Form"]

[[img-datasource-connection-pool-form]]
image::datasource-connection-pool-form.png[title="Create Datasource Form Connection Pool Settings"]

The following parameters are exposed.

.Datasource Mapping Fields
[format="csv", options="header"]
|===
Field,Description,Default Value
Name,The unique name of the Datasource mapping,
Driver,The database driver to use. See the <<_supported_databases>> section for more information,
JDBC URL,The JDBC connection string,
Connection Timeout,"This property controls the maximum number of milliseconds that a client
                    will wait for a connection from the pool. If this time is exceeded without a
                    connection becoming available, a SQLException will be thrown.
                    1000ms is the minimum value",30000 (30 seconds)
Idle Timeout,"This property controls the maximum amount of time that a connection is allowed to sit idle in the pool.
              Whether a connection is retired as idle or not is subject to a maximum variation of +30 seconds,
              and average variation of +15 seconds. A connection will never be retired as idle before this timeout.
              A value of 0 means that idle connections are never removed from the pool",600000 (10 minutes)
Max Lifetime,"This property controls the maximum lifetime of a connection in the pool.
              When a connection reaches this timeout it will be retired from the pool,
              subject to a maximum variation of +30 seconds. An in-use connection will never be retired,
              only when it is closed will it then be removed. We strongly recommend setting this value,
              and it should be at least 30 seconds less than any database-level connection timeout.
              A value of 0 indicates no maximum lifetime (infinite lifetime), subject of course to the
              idleTimeout setting",1800000 (30 minutes)
Maximum PoolSize,"This property controls the maximum size that the pool is allowed to reach, including both
                  idle and in-use connections. Basically this value will determine the maximum number of actual
                  connections to the database backend. A reasonable value for this is best determined by your execution
                  environment. When the pool reaches this size, and no idle connections are available, calls
                  to getConnection() will block for up to connectionTimeout milliseconds before timing out",10
Minimum Idle,"This property controls the minimum number of idle connections that the pool tries to maintain
              in the pool. If the idle connections dip below this value, the pool will make a best effort to
              add additional connections quickly and efficiently. However, for maximum performance and
              responsiveness to spike demands, we recommend not setting this value and instead allowing the pool
              to act as a fixed size connection pool",same as maximumPoolSize
Leak Detection Threshold,"This property controls the amount of time that a connection can be out of the pool
                          before a message is logged indicating a possible connection leak.  A value of 0 means leak detection
                          is disabled. Lowest acceptable value for enabling leak detection is 2000 (2 secs)",0
|===
