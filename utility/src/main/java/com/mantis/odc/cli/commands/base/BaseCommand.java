/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.cli.commands.base;

/**
 * Base command interface.
 */
public interface BaseCommand {

    /**
     * @return command name
     */
    String getCommand();

    /**
     * Runs the given command.
     */
    void runCommand() throws Exception;
}
