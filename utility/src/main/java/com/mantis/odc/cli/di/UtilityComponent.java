/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.cli.di;

import com.mantis.odc.di.SharedModule;
import com.mantis.odc.di.SharedSubComponent;
import dagger.Component;

import javax.inject.Singleton;

/**
 * Utility component.
 */
@Singleton
@Component(modules = {SharedModule.class, UtilityModule.class})
public interface UtilityComponent {

    // SUB-COMPONENTS

    SharedSubComponent getSharedSubComponent();

    CommandFactory getCommandFactory();
}
