/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.cli.commands;

import com.beust.jcommander.Parameters;
import com.mantis.odc.cli.commands.base.BaseCommand;
import com.mantis.odc.config.AppConfig;
import lombok.Getter;
import lombok.val;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.Console;
import java.util.Arrays;

import static java.lang.System.out;
import static java.text.MessageFormat.format;

/**
 * Starts an interactive installer for ODC.
 */
@Singleton
@Parameters(commandDescription = "Interactive installer for the OPA DB Connector.")
public class Installer implements BaseCommand {

    private final AppConfig config;
    private final UpdateDatabaseSchema updateDatabaseSchema;
    private final SetAdminPassword setAdminPassword;

    /**
     * Command name.
     */
    @Getter
    private final String command = "install";

    @Inject
    Installer(AppConfig config, UpdateDatabaseSchema updateDatabaseSchema, SetAdminPassword setAdminPassword) {
        this.config = config;
        this.updateDatabaseSchema = updateDatabaseSchema;
        this.setAdminPassword = setAdminPassword;
    }

    @Override
    public void runCommand() throws Exception {
        val console = System.console();
        out.println(format("This will install the {0} version of the OPA DB Connector.", config.getApplicationVersion()));

        if (readBoolean(console, "Would you like to proceed? (Y/n): ", true)) {
            val clean = readBoolean(console, "Would you like to drop the current database? (y/N): ", false);
            updateDatabaseSchema.setClean(clean);
            updateDatabaseSchema.runCommand();
            val password = console.readPassword("What would you like the admin password to be?: ");
            char[] repeatPassword = console.readPassword("Confirm password: ");
            while (!Arrays.equals(password, repeatPassword))
                repeatPassword = console.readPassword("Confirm password: ");
            setAdminPassword.setPassword(new String(password));
            setAdminPassword.runCommand();
            out.println("Installation Complete.");
        }
    }

    private static boolean readBoolean(Console console, String message, boolean defaultValue) {
        val b = console.readLine(message).toUpperCase();
        return b.isEmpty() ? defaultValue : b.equals("Y");
    }
}
