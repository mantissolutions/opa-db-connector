/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.cli.di;

import com.mantis.odc.config.AppConfig;
import dagger.Module;
import dagger.Provides;
import org.apache.shiro.crypto.hash.HashRequest;

/**
 * Utility module.
 */
@Module
public class UtilityModule {

    /**
     * Returns the hash function to run.
     *
     * @param config app config
     * @return hash
     */
    @Provides
    public static HashRequest.Builder getHash(AppConfig config) {
        return new HashRequest.Builder()
                .setAlgorithmName(config.getSecurity().getHashType().getName())
                .setIterations(config.getSecurity().getIterations());
    }
}
