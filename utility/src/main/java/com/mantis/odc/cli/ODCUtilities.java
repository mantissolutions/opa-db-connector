/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.cli;

import com.beust.jcommander.JCommander;
import com.mantis.odc.cli.di.DaggerUtilityComponent;
import com.mantis.odc.cli.di.UtilityComponent;
import lombok.Getter;
import lombok.val;

import static java.lang.System.out;

/**
 * Core ODC utilities.
 */
public class ODCUtilities {

    @Getter
    private final static UtilityComponent injector = DaggerUtilityComponent.create();

    /**
     * @param args command line arguments
     */
    public static void main(String[] args) {
        val jc = new JCommander();
        val factory = injector.getCommandFactory();

        // add commands
        val installerCommand = factory.getInstaller();
        val setAdminCommand = factory.getSetAdminPassword();
        val updateDatabaseCommand = factory.getUpdateDatabaseSchema();

        // add commands
        jc.addCommand(installerCommand.getCommand(), installerCommand);
        jc.addCommand(setAdminCommand.getCommand(), setAdminCommand);
        jc.addCommand(updateDatabaseCommand.getCommand(), updateDatabaseCommand);

        // parse commands
        try {
            jc.parse(args);

            // get the command
            val command = jc.getParsedCommand();

            // installer command
            if (installerCommand.getCommand().equals(command)) installerCommand.runCommand();

                // parse set admin password command
            else if (setAdminCommand.getCommand().equals(command)) setAdminCommand.runCommand();

                // update database schema
            else if (updateDatabaseCommand.getCommand().equals(command)) updateDatabaseCommand.runCommand();

                // else print command usages
            else printUsage(jc);

        } catch (Exception e) {
            out.println("Invalid operation. " + e.getMessage() + " Please run help for more information.");
        } finally {
            injector.getSharedSubComponent().getDatabase().close();
        }
    }

    /**
     * Prints the application footer.
     */
    private static void printFooter() {
        printLine();
        out.println("Author: Ben Mazzarol - ben.mazzarol@mantissolutions.com.au\n");
        printLine();
    }

    /**
     * prints the application header.
     */
    private static void printHeader() {
        out.println("Copyright (c) 2016. Mantis Innovations");
    }

    /**
     * Prints a line
     */
    private static void printLine() {
        out.println("###############################################################################");
    }

    /**
     * Prints out the utilities usages.
     *
     * @param jc commander
     */
    private static void printUsage(JCommander jc) {
        printHeader();
        printLine();
        out.println("The following operations are permitted.\n");
        jc.getCommands().keySet().forEach(jc::usage);
        printFooter();
    }
}
