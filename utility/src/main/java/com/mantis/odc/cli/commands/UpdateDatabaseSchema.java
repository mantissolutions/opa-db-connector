/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.mantis.odc.cli.commands.base.BaseCommand;
import com.mantis.odc.database.AppDB;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import org.flywaydb.core.Flyway;

import javax.inject.Inject;
import javax.inject.Singleton;

import static java.lang.System.out;

/**
 * Runs all the valid database migrations for the current version of odc.
 */
@Singleton
@Parameters(commandDescription = "Updates the database schema for the current version of the OPA DB Connector.")
public class UpdateDatabaseSchema implements BaseCommand {

    /**
     * Application database.
     */
    private final AppDB db;

    /**
     * Command name.
     */
    @Getter
    private final String command = "updateDatabase";

    @Parameter(names = {"-c", "-clean"},
            description = "Clean database flag, this will drop the current schema before rebuilding it.")
    @Getter
    @Setter
    private boolean clean = false;

    @Inject
    UpdateDatabaseSchema(AppDB db) {
        this.db = db;
    }

    @Override
    public void runCommand() {
        out.println("Migrating database...");

        val flyway = new Flyway();
        flyway.setDataSource(db.getConnectionPool());
        flyway.setLocations(db.getConfig().getType().getMigrationPath());

        if (clean) {
            out.println("Cleaning database...");
            flyway.clean();
            out.print("Complete.");
        }

        out.println("Migrating schema...");
        flyway.migrate();
        out.println("Complete.");
    }
}
