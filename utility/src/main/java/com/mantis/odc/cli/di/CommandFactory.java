/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.cli.di;

import com.mantis.odc.cli.commands.Installer;
import com.mantis.odc.cli.commands.SetAdminPassword;
import com.mantis.odc.cli.commands.UpdateDatabaseSchema;
import com.mantis.odc.di.SharedModule;
import dagger.Subcomponent;

/**
 * Creates commands.
 */
@Subcomponent(modules = {SharedModule.class, UtilityModule.class})
public interface CommandFactory {

    /**
     * @return set admin password command
     */
    SetAdminPassword getSetAdminPassword();

    /**
     * @return update database schema command
     */
    UpdateDatabaseSchema getUpdateDatabaseSchema();

    /**
     * @return application installer command
     */
    Installer getInstaller();
}
