/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.mantis.odc.cli.commands.base.BaseCommand;
import com.mantis.odc.database.AppDB;
import com.mantis.odc.database.base.DAO;
import com.mantis.odc.models.User;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.HashRequest;
import org.apache.shiro.crypto.hash.SimpleHash;

import javax.inject.Inject;
import javax.inject.Singleton;

import static java.lang.System.out;

/**
 * Sets the admin password.
 */
@Singleton
@Parameters(commandDescription = "Sets the admin password used for the OPA DB Connector.")
public class SetAdminPassword implements BaseCommand {

    /**
     * Hash to use.
     */
    private final HashRequest.Builder hashRequestBuilder;

    private final DAO<String, User> userDAO;

    /**
     * Command name.
     */
    @Getter
    private final String command = "setAdminPassword";

    @Parameter(names = {"-np", "-passwordToSet"}, description = "New password value", required = true, hidden = true)
    @Setter
    @Getter
    private String password;

    @Inject
    SetAdminPassword(HashRequest.Builder hashBuilder, AppDB db) {
        this.hashRequestBuilder = hashBuilder;
        userDAO = db.getDAO(User.class);
    }

    @Override
    public void runCommand() {
        out.println("Loading admin user...");

        // get admin user
        val adminOpt = userDAO.get("admin");
        val admin = adminOpt.orElse(User.builder().username("admin").build());

        out.println("Admin user loaded.");
        out.println("Changing password...");

        val salt = new SecureRandomNumberGenerator().nextBytes();

        // get hash
        hashRequestBuilder.setSalt(salt);
        hashRequestBuilder.setSource(password);
        val hashRequest = hashRequestBuilder.build();
        val hash = new SimpleHash(hashRequest.getAlgorithmName(), hashRequest.getSource(), hashRequest.getSalt(), hashRequest.getIterations());

        // set encoded password
        admin.setPassword(hash.toHex());
        admin.setPasswordSalt(salt.toHex());

        // save changes
        userDAO.update(admin);

        out.println("Password changed.");
    }

}
