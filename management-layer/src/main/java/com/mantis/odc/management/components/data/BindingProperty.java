/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.data;

import com.vaadin.data.Property;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Property based on a getter and setting interface.
 */
@Data
@AllArgsConstructor
public class BindingProperty<MODEL, T> implements Property<T> {

    private final String name;
    private MODEL model;
    private final Class<T> type;
    private boolean readOnly;
    private final Function<MODEL, T> getter;
    private final BiConsumer<MODEL, T> setter;

    @Override
    public T getValue() {
        return getter.apply(model);
    }

    @Override
    public void setValue(T newValue) {
        setter.accept(model, newValue);
    }
}
