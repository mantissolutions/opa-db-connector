/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings;

import com.mantis.odc.models.DatasourceMapping;
import com.vaadin.ui.ComboBox;
import lombok.Data;

import java.util.List;

import static com.vaadin.server.FontAwesome.DATABASE;
import static com.vaadin.server.Sizeable.Unit.PIXELS;

/**
 * Datasource mapping select field.
 */
@Data
class DatasourceSelectField extends ComboBox {

    private List<DatasourceMapping> datasourceMappings;

    DatasourceSelectField() {
        setId("datasource");
        setCaption("Datasource Mapping");
        setInputPrompt("datasource...");
        setWidth(500, PIXELS);
        setIcon(DATABASE);
        setNullSelectionAllowed(false);
        setRequired(true);
        setRequiredError("Datasource Mapping is required.");
        setDescription("Datasource to map against.");
    }

    /**
     * @param datasourceMappings data source mappings to add
     */
    void setDatasourceMappings(List<DatasourceMapping> datasourceMappings) {
        this.datasourceMappings = datasourceMappings;
        removeAllItems();
        datasourceMappings.forEach(mapping -> {
            addItem(mapping);
            setItemCaption(mapping, mapping.getName());
        });
    }
}
