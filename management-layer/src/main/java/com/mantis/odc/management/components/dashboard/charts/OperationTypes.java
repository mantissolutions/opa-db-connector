package com.mantis.odc.management.components.dashboard.charts;

import lombok.Getter;

@Getter
public enum OperationTypes {
    CHECK_ALIVE("Check Alive", ChartColors.LIGHT_GREEN),
    GET_METADATA("Get Metadata", ChartColors.DARK_GREEN),
    LOAD("Load", ChartColors.TEAL),
    SAVE("Save", ChartColors.DARK_BLUE),
    GET_CHECKPOINT("Get Checkpoint", ChartColors.LIGHT_BLUE),
    SET_CHECKPOINT("Set Checkpoint", ChartColors.LIGHT_GREY),
    EXECUTE_QUERY("Execute Query", ChartColors.DARK_GREY);

    private final String label;
    private final String color;

    OperationTypes(String label, String color) {
        this.label = label;
        this.color = color;
    }
}
