/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings;

import com.mantis.odc.data.ReferenceEqualityArray;
import com.mantis.odc.database.dao.DatasourceMappingDAO;
import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.management.components.mappings.bindmappings.BindMappingListComponent;
import com.mantis.odc.management.components.mappings.enumerationmappings.EnumerationMappingListComponent;
import com.mantis.odc.management.components.mappings.junctiontablemappings.JunctionTableMappingListComponent;
import com.mantis.odc.management.components.mappings.tablemappings.TableMappingListComponent;
import com.mantis.odc.models.*;
import com.vaadin.ui.*;
import lombok.Data;
import lombok.val;
import org.vaadin.aceeditor.AceEditor;
import org.vaadin.viritin.ListContainer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mantis.odc.management.components.utilities.BindingUtils.bind;
import static com.mantis.odc.management.components.utilities.ValidationUtils.*;
import static com.vaadin.event.ShortcutAction.KeyCode.ENTER;
import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.server.Sizeable.Unit.PIXELS;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;
import static com.vaadin.ui.themes.ValoTheme.PANEL_BORDERLESS;
import static org.vaadin.aceeditor.AceMode.sql;

/**
 * Mapping form.
 */
@Data
class MappingFormComponent extends BaseCRUDFormComponent<Mapping> {

    private final TextField nameField;
    private final TextField bindTokenField;
    private final DatasourceSelectField datasourceField;
    private final CheckBox deployOnStartupField;
    private final CheckBox supportsAttachmentsField;
    private final CheckBox supportsCheckpointsField;
    private final CheckBox debugModeField;
    private final AceEditor preLoadScript;
    private final AceEditor postLoadScript;
    private final AceEditor preSaveScript;
    private final AceEditor postSaveScript;
    private TableMappingListComponent tableMappingsList;
    private JunctionTableMappingListComponent junctionTableMappingList;
    private final AttachmentTableMappingFormComponent attachmentTableMappingForm = new AttachmentTableMappingFormComponent();
    private final CheckpointTableMappingFormComponent checkpointTableMappingForm = new CheckpointTableMappingFormComponent();
    private BindMappingListComponent bindMappingList;
    private EnumerationMappingListComponent enumerationMappingList;
    private final Button saveButton;
    private final TabSheet tabs;
    private final VerticalLayout layout;


    MappingFormComponent(DatasourceMappingDAO datasourceMappingDAO, List<Mapping> siblings) {

        nameField = new TextField() {{
            setId("name");
            setCaption("Name");
            setInputPrompt("name...");
            setNullRepresentation("");
            setWidth(500, PIXELS);
            setRequired(true);
            setRequiredError("Mapping name is required.");
            maxLength(this, 250);
            setDescription("Unique name of this mapping.");
            unique(this, "Mapping name must be unique.", () -> siblings, Mapping::getName);
        }};

        bindTokenField = new TextField() {{
            setId("bindToken");
            setCaption("URL Bind Token Name");
            setInputPrompt("token...");
            setNullRepresentation("");
            setWidth(500, PIXELS);
            setRequired(true);
            setRequiredError("Bind token name is required.");
            maxLength(this, 250);
            setDescription("Name of the get parameter used in load requests.");
        }};

        datasourceField = new DatasourceSelectField() {{
            setDatasourceMappings(datasourceMappingDAO.list());
        }};

        deployOnStartupField = new CheckBox() {{
            setId("deployOnStartup");
            setCaption("Deploy On Startup");
            setDescription("The mapping will be deployed on server start-up.");
        }};

        supportsAttachmentsField = new CheckBox() {{
            setId("supportsAttachments");
            setCaption("Supports Attachments");
            setDescription("The mapping will support saving attachments.");
        }};

        supportsCheckpointsField = new CheckBox() {{
            setId("supportsCheckpoints");
            setCaption("Supports Checkpoints");
            setDescription("The mapping will support setting and getting checkpoints.");
        }};

        debugModeField = new CheckBox() {{
            setId("debugMode");
            setCaption("Debug Mode");
            setDescription("Capture operation information for debugging deployed mappings. It is recommended that this be" +
                    " disabled for mappings running in production.");
        }};

        preLoadScript = new AceEditor() {{
            setId("preLoadScript");
            setWidth(1000, PIXELS);
            setHeight(500, PIXELS);
            setMode(sql);
            setWordWrap(true);
            setDescription("Script to run before each load operation.");
        }};

        postLoadScript = new AceEditor() {{
            setId("postLoadScript");
            setWidth(1000, PIXELS);
            setHeight(500, PIXELS);
            setMode(sql);
            setWordWrap(true);
            setDescription("Script to run after each load operation.");
        }};

        preSaveScript = new AceEditor() {{
            setId("preSaveScript");
            setWidth(1000, PIXELS);
            setHeight(500, PIXELS);
            setMode(sql);
            setWordWrap(true);
            setDescription("Script to run before each save operation.");
        }};

        postSaveScript = new AceEditor() {{
            setId("postSaveScript");
            setWidth(1000, PIXELS);
            setHeight(500, PIXELS);
            setMode(sql);
            setWordWrap(true);
            setDescription("Script to run after each save operation.");
        }};

        saveButton = new Button() {{
            setId("saveButton");
            setCaption("Save Mapping");
            setIcon(SAVE);
            setStyleName(BUTTON_PRIMARY);
            setClickShortcut(ENTER);
            dependOnWithFilter(this, field -> supportsField(field), nameField, bindTokenField, datasourceField, supportsAttachmentsField, supportsCheckpointsField, debugModeField, attachmentTableMappingForm, checkpointTableMappingForm);
        }};

        tabs = new TabSheet() {{
            setId("tabs");
            setSizeUndefined();
            addSelectedTabChangeListener(event -> {
                // if form in a window, recenter the window on tab change
                Component parent = MappingFormComponent.this.getParent();
                if (parent instanceof Window) ((Window) parent).center();
            });
            addTab(new FormLayout() {{
                setSizeUndefined();
                setSpacing(true);
                addComponents(nameField, bindTokenField, datasourceField, deployOnStartupField, supportsAttachmentsField, supportsCheckpointsField, debugModeField);
            }}, "Details", ASTERISK);
        }};

        layout = new VerticalLayout() {{
            setSizeUndefined();
            setSpacing(true);
            setMargin(true);
            addComponent(new Panel() {{
                setSizeUndefined();
                setStyleName(PANEL_BORDERLESS);
                setContent(tabs);
            }});
            addComponent(new HorizontalLayout() {{
                setSizeUndefined();
                setSpacing(true);
                addComponents(saveButton);
            }});
        }};
    }

    /**
     * Returns true if the validation supports the given field.
     *
     * @param field field to validate
     * @return true if it should be included
     */
    private boolean supportsField(AbstractField field) {
        if (field == attachmentTableMappingForm) return supportsAttachmentsField.getValue();
        else if (field == checkpointTableMappingForm) return supportsCheckpointsField.getValue();
        else return true;
    }

    @Override
    protected void bindToModel(Mapping model) {
        bind(nameField, model, String.class, Mapping::getName, Mapping::setName);
        bind(bindTokenField, model, String.class, Mapping::getBindToken, Mapping::setBindToken);
        bind(datasourceField, model, DatasourceMapping.class, Mapping::getDatasource, Mapping::setDatasource);
        bind(deployOnStartupField, model, Boolean.class, Mapping::isDeployOnStartup, Mapping::setDeployOnStartup);
        bind(supportsAttachmentsField, model, Boolean.class, Mapping::isSupportsAttachments, Mapping::setSupportsAttachments);
        bind(supportsCheckpointsField, model, Boolean.class, Mapping::isSupportsCheckpoints, Mapping::setSupportsCheckpoints);
        bind(debugModeField, model, Boolean.class, Mapping::isDebugMode, Mapping::setDebugMode);
        bind(attachmentTableMappingForm, model, AttachmentMapping.class, Mapping::getAttachment, Mapping::setAttachment);
        bind(checkpointTableMappingForm, model, CheckpointMapping.class, Mapping::getCheckpoint, Mapping::setCheckpoint);
        bind(preLoadScript, model, String.class, Mapping::getPreLoadScript, Mapping::setPreLoadScript);
        bind(postLoadScript, model, String.class, Mapping::getPostLoadScript, Mapping::setPostLoadScript);
        bind(preSaveScript, model, String.class, Mapping::getPreSaveScript, Mapping::setPreSaveScript);
        bind(postSaveScript, model, String.class, Mapping::getPostSaveScript, Mapping::setPostSaveScript);
    }

    @Override
    public void setValue(Mapping newFieldValue) {
        // shared array for tables
        val tables = new ReferenceEqualityArray<TableMapping>(newFieldValue.getTables());
        val tablesContainer = new ListContainer<TableMapping>(TableMapping.class, tables);

        // enumeration mapping table
        enumerationMappingList = new EnumerationMappingListComponent(newFieldValue);

        // containment tables
        tableMappingsList = new TableMappingListComponent(newFieldValue, tables, tablesContainer, enumerationMappingList.getItems());
        tabs.addTab(new VerticalLayout(tableMappingsList), "Table Mappings", TABLE);

        // junction tables
        junctionTableMappingList = new JunctionTableMappingListComponent(newFieldValue, tables);
        val junctionTableTab = tabs.addTab(new VerticalLayout(junctionTableMappingList), "Junction Table Mappings", EXTERNAL_LINK);
        junctionTableTab.setEnabled(!tables.isEmpty());
        tablesContainer.addItemSetChangeListener(event -> junctionTableTab.setEnabled(!tables.isEmpty()));

        // attachment table
        val attachmentTab = tabs.addTab(new VerticalLayout(attachmentTableMappingForm), "Attachments", UPLOAD);
        attachmentTab.setEnabled(supportsAttachmentsField.getValue());
        supportsAttachmentsField.addValueChangeListener(event -> attachmentTab.setEnabled(supportsAttachmentsField.getValue()));

        // checkpoint table
        val checkpointTab = tabs.addTab(new VerticalLayout(checkpointTableMappingForm), "Checkpoints", FLAG_CHECKERED);
        checkpointTab.setEnabled(supportsCheckpointsField.getValue());
        supportsCheckpointsField.addValueChangeListener(event -> checkpointTab.setEnabled(supportsCheckpointsField.getValue()));

        // bind mapping table
        bindMappingList = new BindMappingListComponent(newFieldValue, tables);
        val bindMappingTab = tabs.addTab(new VerticalLayout(bindMappingList), "Bind Mappings", SEARCH_PLUS);
        bindMappingTab.setEnabled(!tables.isEmpty());
        tablesContainer.addItemSetChangeListener(event -> bindMappingTab.setEnabled(!tables.isEmpty()));

        // add to tabs
        tabs.addTab(new VerticalLayout(enumerationMappingList), "Enumeration Mappings", LIST);

        // scripts
        tabs.addTab(new TabSheet() {{
            setWidth(1000, PIXELS);
            setHeight(500, PIXELS);
            addTab(preLoadScript, "Pre-Load Script");
            addTab(postLoadScript, "Post-Load Script");
            addTab(preSaveScript, "Pre-Save Script");
            addTab(postSaveScript, "Post-Save Script");
        }}, "Scripts", FILE_CODE_O);

        super.setValue(newFieldValue);
    }

    @Override
    public boolean isModified() {
        return tableMappingsList.isModified()
                || junctionTableMappingList.isModified()
                || bindMappingList.isModified()
                || enumerationMappingList.isModified()
                || super.isModified();
    }

    @Override
    public void setEnabled(boolean enabled) {
        tableMappingsList.setEnabled(enabled);
        junctionTableMappingList.setEnabled(enabled);
        bindMappingList.setEnabled(enabled);
        enumerationMappingList.setEnabled(enabled);
        super.setEnabled(enabled);
    }

    @Override
    public void saveModel(Mapping model) {
        tableMappingsList.commit();
        junctionTableMappingList.commit();
        bindMappingList.commit();
        enumerationMappingList.commit();
        super.saveModel(model);
    }

    @Override
    protected List<Field<?>> getFields() {
        val fields = new ArrayList<Field<?>>(Arrays.asList(nameField, bindTokenField, datasourceField, deployOnStartupField, supportsAttachmentsField, supportsCheckpointsField, debugModeField));
        if (supportsAttachmentsField.getValue()) fields.add(attachmentTableMappingForm);
        if (supportsCheckpointsField.getValue()) fields.add(checkpointTableMappingForm);
        return fields;
    }

    @Override
    public Class<? extends Mapping> getType() {
        return Mapping.class;
    }
}
