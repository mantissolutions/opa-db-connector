/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings.tablemappings;

import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.management.components.extentions.EnumeratedComboBox;
import com.mantis.odc.management.components.mappings.tablemappings.fieldmappings.FieldMappingListComponent;
import com.mantis.odc.models.DataType;
import com.mantis.odc.models.EnumerationMapping;
import com.mantis.odc.models.FieldMapping;
import com.mantis.odc.models.TableMapping;
import com.vaadin.ui.*;
import lombok.Getter;
import lombok.val;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static com.mantis.odc.management.components.utilities.BindingUtils.bind;
import static com.mantis.odc.management.components.utilities.ValidationUtils.*;
import static com.vaadin.event.ShortcutAction.KeyCode.ENTER;
import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;
import static com.vaadin.ui.themes.ValoTheme.PANEL_BORDERLESS;

/**
 * Table Mapping form.
 */
@Getter
public class TableMappingFormComponent extends BaseCRUDFormComponent<TableMapping> {

    private final List<TableMapping> siblings;
    private final List<EnumerationMapping> enumerationMappings;
    private final TextField sourceField;
    private final TextField opaNameField;
    private final CheckBox inferredField;
    private final EnumeratedComboBox parentTableField;
    private final TextField linkNameField;
    private final CheckBox oneToOneField;
    private final Button saveButton;
    private final VerticalLayout layout;
    private FieldMappingListComponent fieldMappingList;
    private final TabSheet tabs;

    /**
     * @param siblings sibling table mappings
     */
    TableMappingFormComponent(List<TableMapping> siblings, List<EnumerationMapping> enumerationMappings) {
        this.siblings = siblings;
        this.enumerationMappings = enumerationMappings;

        sourceField = new TextField() {{
            setId("source");
            setCaption("Table Source");
            setIcon(DATABASE);
            setInputPrompt("source...");
            setNullRepresentation("");
            setRequired(true);
            setRequiredError("Table source is required.");
            maxLength(this, 250);
            setDescription("Table source name.");
        }};

        opaNameField = new TextField() {{
            setId("opaName");
            setCaption("OPA Entity Name");
            setIcon(USER);
            setInputPrompt("opa name...");
            setNullRepresentation("");
            maxLength(this, 250);
            setDescription("Name of the entity in the rulebase data model.");
            unique(this, "The opa name must be unique.", () -> siblings, tableMapping -> tableMapping.getTableId().getName());
        }};

        inferredField = new CheckBox() {{
            setId("inferred");
            setCaption("Inferred Entity");
            setDescription("If checked the table mapping is to be mapped to an inferred entity. " +
                    "Inferred entities are re-populated on each save request.");
        }};

        parentTableField = new EnumeratedComboBox() {{
            setId("parentTableId");
            setCaption("Parent Table");
            setDescription("Parent table. A table can have no parent only if it is a root containment entity.");
            for (val tm : siblings) {
                addItem(tm);
                setItemCaption(tm, tm.getTableId().getName());
            }
        }};

        linkNameField = new TextField() {{
            setId("linkName");
            setCaption("Link Name");
            setIcon(LINK);
            setNullRepresentation("");
            setDescription("Name of the link between the child table and its parent table.");
            setVisible(false);
            maxLength(this, 250);
            parentTableField.addValueChangeListener(event -> setVisible(event.getProperty().getValue() != null));
        }};

        oneToOneField = new CheckBox() {{
            setId("oneToOne");
            setCaption("One To One");
            setDescription("Set to true if the relationship with the parent table is one to one, " +
                    "there is only ever on child entity to parent entity.");
            setVisible(false);
            parentTableField.addValueChangeListener(event -> setVisible(event.getProperty().getValue() != null));
        }};

        saveButton = new Button() {{
            setId("saveButton");
            setCaption("Save Table Mapping");
            setIcon(SAVE);
            setStyleName(BUTTON_PRIMARY);
            setClickShortcut(ENTER);
            dependOn(this, sourceField, opaNameField, linkNameField);
        }};

        tabs = new TabSheet() {{
            setId("tabs");
            setSizeUndefined();
            addSelectedTabChangeListener(event -> {
                // if form in a window, recenter the window on tab change
                Component parent = TableMappingFormComponent.this.getParent();
                if (parent instanceof Window) ((Window) parent).center();
            });
            addTab(new FormLayout() {{
                setSizeUndefined();
                setSpacing(true);
                addComponents(sourceField, opaNameField, inferredField, parentTableField, linkNameField, oneToOneField);
            }}, "Details", ASTERISK);
        }};

        layout = new VerticalLayout() {{
            setSpacing(true);
            setMargin(true);
            addComponent(new Panel() {{
                setSizeUndefined();
                setStyleName(PANEL_BORDERLESS);
                setContent(tabs);
            }});
            addComponent(new HorizontalLayout() {{
                setSizeUndefined();
                setSpacing(true);
                addComponents(saveButton);
            }});
        }};
    }

    @Override
    protected void bindToModel(TableMapping model) {
        bind(sourceField, model, String.class, t -> t.getTableId().getSource(), (t, s) -> t.getTableId().setSource(s));
        bind(opaNameField, model, String.class, t -> t.getTableId().getOpaName(), (t, s) -> t.getTableId().setOpaName(s));
        bind(inferredField, model, Boolean.class, TableMapping::isInferred, TableMapping::setInferred);
        bind(parentTableField, model, TableMapping.class, TableMapping::getParent, TableMapping::setParent);
        bind(linkNameField, model, String.class, TableMapping::getLinkNameNoDefault, TableMapping::setLinkName);
        bind(oneToOneField, model, Boolean.class, TableMapping::isOneToOne, TableMapping::setOneToOne);
    }

    @Override
    public void setValue(TableMapping newFieldValue) {
        fieldMappingList = new FieldMappingListComponent(newFieldValue, newFieldValue.getFields(), enumerationMappings, this);
        tabs.addTab(new VerticalLayout(fieldMappingList), "Field Mappings", COLUMNS);
        super.setValue(newFieldValue);
    }

    @Override
    public boolean isModified() {
        return fieldMappingList.isModified() || super.isModified();
    }

    @Override
    public void setEnabled(boolean enabled) {
        fieldMappingList.setEnabled(enabled);
        super.setEnabled(enabled);
    }

    @Override
    public void saveModel(TableMapping model) {
        fieldMappingList.commit();
        super.saveModel(model);
    }

    @Override
    protected List<Field<?>> getFields() {
        return Arrays.asList(sourceField, opaNameField, inferredField, parentTableField, linkNameField, oneToOneField);
    }

    @Override
    public Class<? extends TableMapping> getType() {
        return TableMapping.class;
    }

    /**
     * @return true if the table has a link to parent table
     */
    public boolean hasParentTable() {
        return parentTableField.getValue() != null;
    }

    /**
     * @return parent table primary key data type.
     */
    public Optional<DataType> getParentPrimaryKeyType() {
        return siblings.stream()
                .filter(tableMapping -> tableMapping.getTableId().equals(parentTableField.getValue()))
                .flatMap(tableMapping -> tableMapping.getPrimaryKeyOpt()
                        .map(Stream::of).orElse(Stream.empty()))
                .map(FieldMapping::getType)
                .findFirst();
    }
}
