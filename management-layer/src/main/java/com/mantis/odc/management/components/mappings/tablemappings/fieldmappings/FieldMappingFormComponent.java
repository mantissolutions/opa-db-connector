/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings.tablemappings.fieldmappings;

import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.models.DataType;
import com.mantis.odc.models.EnumerationMapping;
import com.mantis.odc.models.FieldMapping;
import com.vaadin.ui.*;
import lombok.Data;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.mantis.odc.management.components.utilities.BindingUtils.bind;
import static com.mantis.odc.management.components.utilities.ValidationUtils.*;
import static com.vaadin.event.ShortcutAction.KeyCode.ENTER;
import static com.vaadin.event.ShortcutAction.ModifierKey.SHIFT;
import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;

/**
 * Field mapping form.
 */
@Data
class FieldMappingFormComponent extends BaseCRUDFormComponent<FieldMapping> {

    private final TextField sourceField;
    private final TextField opaNameField;
    private final CheckBox primaryKeyField;
    private final CheckBox autoIncrementField;
    private final TextField sequenceField;
    private final CheckBox foreignKeyField;
    private final Optional<DataType> parentPrimaryKeyType;
    private final DataTypeSelectField dataTypeField;
    private final CheckBox requiredField;
    private final CheckBox inputField;
    private final CheckBox outputField;
    private final Button saveButton;
    private final Button saveAndAddButton;
    private final VerticalLayout layout;

    FieldMappingFormComponent(FieldMappingListComponent parentList, List<FieldMapping> siblings, boolean isChildTable, Optional<DataType> parentPrimaryKeyType, List<EnumerationMapping> enumerationMappings) {
        this.parentPrimaryKeyType = parentPrimaryKeyType;
        setCloseOnSave(true);

        sourceField = new TextField() {{
            setId("source");
            setCaption("Field Source");
            setIcon(DATABASE);
            setInputPrompt("source...");
            setNullRepresentation("");
            setRequired(true);
            setRequiredError("Field source is required.");
            maxLength(this, 250);
            setDescription("Field source name.");
        }};

        opaNameField = new TextField() {{
            setId("opaName");
            setCaption("OPA attribute Name");
            setIcon(USER);
            setInputPrompt("opa name...");
            setNullRepresentation("");
            maxLength(this, 250);
            setDescription("Name of the attribute in the rulebase data model.");
            unique(this, "The opa name must be unique.", () -> siblings, (fieldMapping) -> fieldMapping.getFieldId().getName());
        }};

        primaryKeyField = new CheckBox() {{
            setId("primaryKey");
            setCaption("Primary Key");
            setDescription("Flag to indicate this field is the tables primary key.");
            setVisible(siblings.stream().noneMatch(FieldMapping::isPrimaryKey));
        }};

        autoIncrementField = new CheckBox() {{
            setId("autoIncrement");
            setCaption("Auto Increment");
            setDescription("Flag to indicate this field has an auto incrementing key.");
            setVisible(false);
            primaryKeyField.addValueChangeListener(event -> setVisible(primaryKeyField.getValue()));
        }};

        sequenceField = new TextField() {{
            setId("sequence");
            setCaption("Sequence Value");
            setIcon(TABLE);
            setInputPrompt("sequence...");
            setNullRepresentation("");
            maxLength(this, 250);
            setDescription("Fragment used in a sequence select call for the primary key. For example SELECT <sequence> FROM DUAL;");
            setVisible(false);
            primaryKeyField.addValueChangeListener(event -> setVisible(primaryKeyField.getValue() && !autoIncrementField.getValue()));
            autoIncrementField.addValueChangeListener(event -> setVisible(primaryKeyField.getValue() && !autoIncrementField.getValue()));
        }};

        foreignKeyField = new CheckBox() {{
            setId("foreignKey");
            setCaption("Foreign Key");
            setDescription("Flag to indicate this field is a foreign key.");
            setVisible(isChildTable && siblings.stream().noneMatch(FieldMapping::isForeignKey));
        }};

        dataTypeField = new DataTypeSelectField(enumerationMappings) {{
            setId("dataType");
            setCaption("Data Type");
            setNullSelectionAllowed(false);
            setRequired(true);
            setRequiredError("A type is required.");
            // dependencies on types that are supported
            primaryKeyField.addValueChangeListener(event -> setKey(primaryKeyField.getValue() || foreignKeyField.getValue()));
            foreignKeyField.addValueChangeListener(event -> setKey(primaryKeyField.getValue() || foreignKeyField.getValue()));
            // add validation on foreign key type
            parentPrimaryKeyType.ifPresent(type -> addValidation(this, "The selected type does not match the parent table primary key type " + type.getDisplayName(),
                    value -> !(foreignKeyField.getValue() && value != type)));
        }};

        requiredField = new CheckBox() {{
            setId("required");
            setCaption("Required Field");
            setDescription("Flag to indicate this field must be mapped by OPA.");
        }};

        inputField = new CheckBox() {{
            setId("input");
            setCaption("Input Field");
            setDescription("Flag to indicate this field can be input mapped in OPA. Data can be loaded which includes this field.");
        }};

        outputField = new CheckBox() {{
            setId("output");
            setCaption("Output Field");
            setDescription("Flag to indicate this field can be output mapped in OPA. Data can be saved which includes this field.");
        }};

        saveButton = new Button() {{
            setId("saveButton");
            setCaption("Save");
            setIcon(SAVE);
            setStyleName(BUTTON_PRIMARY);
            setClickShortcut(ENTER);
            dependOn(this, sourceField, opaNameField, sequenceField, dataTypeField);
        }};

        saveAndAddButton = new Button() {{
            setId("saveButton");
            setCaption("Save/Create");
            setIcon(ARROW_RIGHT);
            setClickShortcut(ENTER, SHIFT);
            addClickListener(clickEvent -> {
                // save current model
                saveModel(getValue());
                // close window
                closeWindow();
                // create new
                parentList.showCreateNewField();
            });
            dependOn(this, sourceField, opaNameField, sequenceField, dataTypeField);
        }};

        layout = new VerticalLayout() {{
            setSizeUndefined();
            setSpacing(true);
            setMargin(true);
            addComponent(new FormLayout() {{
                setSizeUndefined();
                setSpacing(true);
                addComponents(sourceField, opaNameField, primaryKeyField, autoIncrementField, sequenceField, foreignKeyField, dataTypeField, requiredField, inputField, outputField);
            }});
            addComponent(new HorizontalLayout() {{
                setSpacing(true);
                addComponents(saveButton, saveAndAddButton);
            }});
        }};
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
        saveAndAddButton.setVisible(!readOnly);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        saveAndAddButton.setVisible(enabled);
    }

    @Override
    protected void bindToModel(FieldMapping model) {
        bind(sourceField, model, String.class, fm -> fm.getFieldId().getSource(), (fm, s) -> fm.getFieldId().setSource(s));
        bind(opaNameField, model, String.class, fm -> fm.getFieldId().getOpaName(), (fm, s) -> fm.getFieldId().setOpaName(s));
        bind(primaryKeyField, model, Boolean.class, FieldMapping::isPrimaryKey, FieldMapping::setPrimaryKey);
        bind(autoIncrementField, model, Boolean.class, FieldMapping::isAutoIncrement, FieldMapping::setAutoIncrement);
        bind(sequenceField, model, String.class, FieldMapping::getSequence, FieldMapping::setSequence);
        bind(foreignKeyField, model, Boolean.class, FieldMapping::isForeignKey, FieldMapping::setForeignKey);
        bind(dataTypeField, model, Object.class, FieldMapping::getTypeOrEnum, FieldMapping::setTypeOrEnum);
        bind(dataTypeField, model, Object.class, FieldMapping::getTypeOrEnum, FieldMapping::setTypeOrEnum);
        bind(requiredField, model, Boolean.class, FieldMapping::isRequired, FieldMapping::setRequired);
        bind(inputField, model, Boolean.class, FieldMapping::isInput, FieldMapping::setInput);
        bind(outputField, model, Boolean.class, FieldMapping::isOutput, FieldMapping::setOutput);
    }

    @Override
    protected List<Field<?>> getFields() {
        return Arrays.asList(sourceField, opaNameField, primaryKeyField, autoIncrementField, sequenceField, foreignKeyField, dataTypeField, requiredField, inputField, outputField);
    }

    @Override
    public Class<? extends FieldMapping> getType() {
        return FieldMapping.class;
    }
}