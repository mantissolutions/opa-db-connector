/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.di;

import com.mantis.odc.management.views.*;
import com.mantis.odc.services.di.ServiceModule;
import dagger.Subcomponent;

/**
 * Factory for all views used in this application.
 */
@Subcomponent(modules = ServiceModule.class)
public interface ViewFactory {

    /**
     * @return login view
     */
    LoginView getLoginView();

    /**
     * @return root dashboard view
     */
    HomeView getHomeView();

    /**
     * @return datasource mapping view
     */
    DatasourceView getDatasourceView();

    /**
     * @return mapping view
     */
    MappingsView getMappingsView();

    /**
     * @return documentation view
     */
    DocumentationView getDocumentationView();
}
