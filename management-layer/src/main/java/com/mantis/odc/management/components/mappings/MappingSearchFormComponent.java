/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings;

import com.mantis.odc.management.components.base.BaseSearchFormComponent;
import com.vaadin.data.Container;
import com.vaadin.data.util.filter.Like;
import com.vaadin.ui.*;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

import static com.vaadin.server.FontAwesome.SEARCH;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;

/**
 * Mapping Search form.
 */
@Data
class MappingSearchFormComponent extends BaseSearchFormComponent {

    private final Container.Filterable container;
    private final TextField nameField;
    private final Button searchButton;
    private final Button clearButton;

    MappingSearchFormComponent(Container.Filterable container) {
        this.container = container;

        nameField = new TextField() {{
            setId("name");
            setCaption("Name");
            setInputPrompt("name...");
            setNullRepresentation("");
            setImmediate(true);
            setDescription("Mapping name.");
        }};

        searchButton = new Button() {{
            setId("searchButton");
            setCaption("Search");
            setIcon(SEARCH);
            setStyleName(BUTTON_PRIMARY);
            addClickListener(event -> search());
        }};

        clearButton = new Button() {{
            setId("saveButton");
            setCaption("Clear");
            addClickListener(event -> clearSearch());
        }};

        setCompositionRoot(new FormLayout() {{
            setSpacing(true);
            setMargin(true);
            addComponents(
                    new HorizontalLayout() {{
                        setSpacing(true);
                        addComponents(nameField);
                    }},
                    new HorizontalLayout() {{
                        setSpacing(true);
                        addComponents(searchButton, clearButton);
                    }}
            );
        }});
    }

    @Override
    public Map<Field, Container.Filter> getFilters() {
        Map<Field, Container.Filter> filters = new HashMap<>();
        filters.put(nameField, new Like("name", nameField.getValue()));
        return filters;
    }
}
