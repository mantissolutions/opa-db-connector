/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.dashboard.widgets.base;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import lombok.Data;
import lombok.SneakyThrows;

import java.lang.reflect.Method;
import java.util.Map;

import static com.vaadin.server.FontAwesome.CLOSE;
import static com.vaadin.ui.Alignment.MIDDLE_CENTER;
import static com.vaadin.ui.Alignment.MIDDLE_RIGHT;
import static com.vaadin.ui.themes.ValoTheme.*;

/**
 * Container for widgets.
 */
@Data
public abstract class AbstractWidget extends VerticalLayout implements Widget {

    private HorizontalLayout header;
    private Label titleLabel;
    private Button closeButton;
    private Panel content;

    private int widgetWidth;
    private int widgetHeight;
    private int widgetX = 0;
    private int widgetY = 0;

    public void init() {
        setSizeFull();

        titleLabel = new Label(title()) {{
            setStyleName(LABEL_BOLD);
        }};

        closeButton = new Button(null, CLOSE) {{
            setStyleName(BUTTON_BORDERLESS);
        }};

        header = new HorizontalLayout() {{
            setSizeFull();
            setMargin(new MarginInfo(false, false, false, true));
            setSpacing(true);
            addComponents(titleLabel, closeButton);
            setComponentAlignment(titleLabel, MIDDLE_CENTER);
            setComponentAlignment(closeButton, MIDDLE_RIGHT);
        }};

        content = new Panel() {{
            setSizeFull();
            setStyleName(PANEL_BORDERLESS);
            setContent(getComponent());
        }};

        addComponents(header, getComponent());
    }

    @Override
    public String id() {
        return getClass().getName();
    }

    /**
     * Call to inform listeners that the config has been updated.
     */
    protected void updateConfig() {
        fireEvent(new ConfigUpdatedEvent(this, config()));
    }

    /**
     * @param listener form listener to add
     */
    @SneakyThrows
    public void addConfigUpdatedListener(ConfigUpdatedListener listener) {
        Method method = ConfigUpdatedListener.class.getDeclaredMethod("configUpdated", ConfigUpdatedEvent.class);
        addListener(ConfigUpdatedEvent.class, listener, method);
    }

    /**
     * @param listener listener to remove
     */
    public void removeConfigUpdatedListener(ConfigUpdatedListener listener) {
        removeListener(ConfigUpdatedListener.class, listener);
    }

    /**
     * Listens on widget config update events.
     */
    public interface ConfigUpdatedListener {
        /**
         * @param event config updated event
         */
        void configUpdated(ConfigUpdatedEvent event);
    }

    /**
     * Created on an updated widget config.
     */
    @Data
    public static class ConfigUpdatedEvent extends Event {

        private final Map<String, String> config;

        /**
         * Constructs a new event with the specified source component.
         *
         * @param source the source component of the event
         * @param config updated config
         */
        public ConfigUpdatedEvent(Component source, Map<String, String> config) {
            super(source);
            this.config = config;
        }
    }
}
