/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.base;

import com.mantis.odc.management.components.models.ActionModel;
import com.vaadin.ui.MenuBar;
import lombok.val;

import java.util.List;

import static com.vaadin.server.FontAwesome.COGS;

/**
 * Provides access to table actions column and enforces that actions be provided.
 */
public interface TableActions<MODEL> {

    /**
     * @return returns all the context actions this list supports.
     */
    List<ActionModel<MODEL>> getActions();

    /**
     * @param model model to return operations for
     * @return table/grid operations.
     */
    default MenuBar getActionsColumn(MODEL model) {
        return new MenuBar() {{
            val actions = addItem("", COGS, null);
            actions.setDescription("Operations which can be performed on the current row.");
            getActions().stream()
                    .filter(ActionModel::isRequiresModel)
                    .forEach(action -> actions.addItem(action.getCaption(), action.getIcon(), selectedItem -> action.getConsumer().accept(model)));
        }};
    }
}
