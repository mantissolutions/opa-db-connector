/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings.bindmappings;

import com.mantis.odc.data.ReferenceEqualityArray;
import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.management.components.base.BaseSubFormListComponent;
import com.mantis.odc.management.components.extentions.AdvancedTable;
import com.mantis.odc.management.components.extentions.BooleanField;
import com.mantis.odc.models.BindMapping;
import com.mantis.odc.models.Mapping;
import com.mantis.odc.models.TableMapping;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import lombok.Data;
import lombok.val;
import org.vaadin.viritin.ListContainer;

import java.util.List;
import java.util.Optional;

import static com.vaadin.server.FontAwesome.SEARCH_PLUS;
import static com.vaadin.server.Sizeable.Unit.PIXELS;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;

/**
 * Bind mapping list.
 */
@Data
public class BindMappingListComponent extends BaseSubFormListComponent<BindMapping, Mapping> {

    private boolean isViewMode;
    private final String modelName = "Bind Mapping";
    private final Mapping parentModel;
    private final List<TableMapping> tables;
    private final List<BindMapping> items = new ReferenceEqualityArray<>();
    private final ListContainer<BindMapping> container;
    private final Button addBindMappingButton;
    private final AdvancedTable<BindMapping, BindMapping> table;
    private final VerticalLayout layout;

    public BindMappingListComponent(Mapping parentModel, List<TableMapping> tables) {
        this.parentModel = parentModel;
        this.tables = tables;
        this.items.addAll(parentModel.getBinds());

        container = new ListContainer<>(BindMapping.class, this.items);

        table = new AdvancedTable<BindMapping, BindMapping>() {{
            setId("bindMappingList");
            setWidth(1100, PIXELS);
            setHeight(450, PIXELS);
            setActionProvider(Optional.of(mapping -> getActions()));
            setContainerDataSource(container);
            addColumn("actions", mapping -> getActionsColumn(mapping));
            addColumn("requiredBoolean", mapping -> new BooleanField(mapping.isRequired()));
            addColumn("tableSource", mapping -> mapping.getTable() != null ? mapping.getTable().getTableId().getName() : null);
            addColumn("fieldSource", mapping -> mapping.getField() != null ? mapping.getField().getFieldId().getName() : null);
            setVisibleColumns("actions", "tokenName", "requiredBoolean", "condition", "operator", "tableSource", "fieldSource");
            setColumnHeaders("", "Bind Token", "Required", "Condition", "Operator", "Table", "Field");
            setColumnWidth("actions", 130);
        }};

        addBindMappingButton = new Button() {{
            setId("addBindMapping");
            setCaption("Add Bind Mapping");
            setIcon(SEARCH_PLUS);
            setStyleName(BUTTON_PRIMARY);
            addClickListener((event) -> showCreateModelWindow());
        }};

        layout = new VerticalLayout() {{
            setSizeFull();
            setSpacing(true);
            setMargin(true);
            addComponents(addBindMappingButton, table);
        }};

        setCompositionRoot(layout);
    }

    @Override
    public void setEnabled(boolean enabled) {
        addBindMappingButton.setVisible(enabled);
        isViewMode = !enabled;
    }

    @Override
    public BindMapping createNewModelInstance() {
        return new BindMapping();
    }

    @Override
    public BaseCRUDFormComponent<BindMapping> getForm(BindMapping model) {
        val form = new BindMappingFormComponent(tables);
        form.setValue(model);
        return form;
    }

    @Override
    public List<BindMapping> getItemsFromParent(Mapping mapping) {
        return mapping.getBinds();
    }

    @Override
    protected void saveItemsToParent(Mapping mapping, List<BindMapping> items) {
        items.forEach(bindMapping -> bindMapping.setMapping(mapping));
        mapping.setBinds(items);
    }

    @Override
    protected void removeModel(BindMapping mapping) {
        // remove the links to table and field
        mapping.removeTable();
        mapping.removeField();
        super.removeModel(mapping);
    }
}
