/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings.tablemappings.fieldmappings;

import com.mantis.odc.models.DataType;
import com.mantis.odc.models.EnumerationMapping;
import com.vaadin.ui.ComboBox;
import lombok.Data;
import lombok.val;

import java.util.List;
import java.util.Optional;

import static com.mantis.odc.models.DataType.DECIMAL_TYPE;

/**
 * Select field for data types.
 */
@Data
class DataTypeSelectField extends ComboBox {

    private final List<EnumerationMapping> enumerationMappings;
    private boolean isKey = false;

    DataTypeSelectField(List<EnumerationMapping> enumerationMappings) {
        this.enumerationMappings = enumerationMappings;
        populateValues();
        addValueChangeListener(event -> setDescription(event.getProperty().getValue()));
    }

    /**
     * Adds items.
     */
    private void populateValues() {
        removeAllItems();
        for (val d : DataType.values()) {
            if ((!isKey || d.isKeyType()) && d.isMappableType()) {
                addItem(d);
                setItemCaption(d, d.getDisplayName());
            }
        }

        // add enumerations to the end of the
        if (!isKey) {
            for (val e : enumerationMappings) {
                addItem(e);
                setItemCaption(e, e.getName());
            }
        }
    }

    /**
     * @param isKey true if key types are to be shown
     */
    public void setKey(boolean isKey) {
        this.isKey = isKey;
        populateValues();
        setValue(DECIMAL_TYPE);
        setComponentError(null);
    }

    /**
     * Sets the description.
     *
     * @param value driver
     */
    private void setDescription(Object value) {
        super.setDescription(Optional.ofNullable(value)
                .map((dataType) -> {
                    if (dataType instanceof DataType)
                        return ((DataType) dataType).getDetailedDescription();
                    else if (dataType instanceof EnumerationMapping)
                        return ((EnumerationMapping) dataType).getDetailedDescription();
                    else return null;
                })
                .orElse("Select a Data type."));
    }
}
