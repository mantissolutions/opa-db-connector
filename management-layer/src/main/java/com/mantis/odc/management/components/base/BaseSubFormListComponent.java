/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.base;

import com.mantis.odc.database.base.DAO;
import com.mantis.odc.management.components.models.ActionModel;
import com.mantis.odc.models.base.Duplicatable;
import com.vaadin.ui.UI;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import org.vaadin.dialogs.ConfirmDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static com.vaadin.server.FontAwesome.*;

/**
 * Base list used in a form for child model items without a database backend.
 */
public abstract class BaseSubFormListComponent<MODEL extends Duplicatable<MODEL>, PARENT> extends BaseCRUDListComponent<MODEL, MODEL> {

    /**
     * Flag to indicate that changes have occurred to the list since the paren form was last saved.
     */
    @Getter
    @Setter
    private boolean isModified;

    /**
     * default resolver.
     */
    @Getter
    private final Function<MODEL, MODEL> keyToModelResolver = key -> key;

    /**
     * @return flag to indicate in view mode.
     */
    public abstract boolean isViewMode();

    /**
     * @return parent model
     */
    public abstract PARENT getParentModel();

    /**
     * @param parent parent model
     * @return list of models
     */
    public abstract List<MODEL> getItemsFromParent(PARENT parent);

    /**
     * @return list of models
     */
    public abstract List<MODEL> getItems();

    /**
     * @return true if the list has been modified.
     */
    public boolean isModified() {
        val parent = getParentModel();
        return isModified || !getItems().equals(getItemsFromParent(parent));
    }

    /**
     * Commits the form.
     */
    public void commit() {
        isModified = false;
        val parent = getParentModel();
        saveItemsToParent(parent, getItems());
    }

    /**
     * Saves the items to the parent.
     *
     * @param parent parent model
     * @param items  item to save
     */
    protected abstract void saveItemsToParent(PARENT parent, List<MODEL> items);

    @Override
    protected void createModel(MODEL model) {
        withUserFeedback(getModelName() + " Added!", () -> {
            getContainer().addItem(model);
            isModified = true;
        }, null);
    }

    @Override
    protected void updateModel(MODEL model) {
        withUserFeedback(getModelName() + " Updated!", () -> {
            replaceModel(model);
            isModified = true;
        }, null);
    }

    @Override
    protected void removeModel(MODEL model) {
        withUserFeedback(getModelName() + " Removed!", () -> {
            getContainer().removeItem(model);
            isModified = true;
        }, null);
    }

    @Override
    public DAO<MODEL, MODEL> getDao() {
        return null;
    }

    @Override
    protected MODEL getModelKey(MODEL model) {
        return model;
    }

    /**
     * @return returns all the context actions this list supports.
     */
    public List<ActionModel<MODEL>> getActions() {
        val actions = new ArrayList<ActionModel<MODEL>>();
        if (!isViewMode()) {
            actions.add(new ActionModel<>("Add", PLUS, false, m -> showCreateModelWindow()));
            actions.add(new ActionModel<>("View", EYE, true, this::showViewModelWindow));
            actions.add(new ActionModel<>("Edit", PENCIL, true, this::showEditModelWindow));
            actions.add(new ActionModel<>("Remove", TRASH, true, this::showRemoveModelDialog));
        } else {
            actions.add(new ActionModel<>("View", EYE, true, this::showViewModelWindow));
        }
        return actions;
    }

    /**
     * Shows an error message dialog on removal.
     *
     * @param message error message
     */
    protected void showErrorMessage(String message) {
        val dialog = ConfirmDialog.show(UI.getCurrent(), " Cannot Remove Table",
                message, "Ok", "No", d -> {
                });
        dialog.setIcon(TRASH);
        dialog.getCancelButton().setVisible(false);
    }
}
