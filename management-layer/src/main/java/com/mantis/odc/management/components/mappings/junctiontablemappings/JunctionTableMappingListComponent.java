/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings.junctiontablemappings;

import com.mantis.odc.data.ReferenceEqualityArray;
import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.management.components.base.BaseSubFormListComponent;
import com.mantis.odc.management.components.extentions.AdvancedTable;
import com.mantis.odc.models.JunctionTableMapping;
import com.mantis.odc.models.Mapping;
import com.mantis.odc.models.OPAMappingId;
import com.mantis.odc.models.TableMapping;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import lombok.Data;
import lombok.val;
import org.vaadin.viritin.ListContainer;

import java.util.List;
import java.util.Optional;

import static com.vaadin.server.FontAwesome.EXTERNAL_LINK;
import static com.vaadin.server.Sizeable.Unit.PIXELS;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;

/**
 * Table mapping list.
 */
@Data
public class JunctionTableMappingListComponent extends BaseSubFormListComponent<JunctionTableMapping, Mapping> {

    private boolean isViewMode;
    private final String modelName = "Junction Table Mapping";
    private final Mapping parentModel;
    private final List<TableMapping> tables;
    private final List<JunctionTableMapping> items = new ReferenceEqualityArray<>();
    private final ListContainer<JunctionTableMapping> container;
    private final Button addTableMappingButton;
    private final AdvancedTable<JunctionTableMapping, JunctionTableMapping> table;
    private final VerticalLayout layout;

    public JunctionTableMappingListComponent(Mapping parentModel, List<TableMapping> tables) {
        this.parentModel = parentModel;
        this.tables = tables;
        this.items.addAll(parentModel.getJunctionTables());

        container = new ListContainer<>(JunctionTableMapping.class, this.items);

        table = new AdvancedTable<JunctionTableMapping, JunctionTableMapping>() {{
            setId("junctionTableMappingList");
            setWidth(1100, PIXELS);
            setHeight(450, PIXELS);
            setActionProvider(Optional.of(mapping -> getActions()));
            setContainerDataSource(container);
            addColumn("actions", mapping -> getActionsColumn(mapping));
            addColumn("source", mapping -> mapping.getTableId().getSource());
            addColumn("opaName", mapping -> mapping.getTableId().getOpaName());
            addColumn("linkTypeDisplay", mapping -> mapping.getLinkType().getDisplay());
            addColumn("fromTableSource", mapping -> mapping.getFromTableId().map(OPAMappingId::getName).orElse(null));
            addColumn("toTableSource", mapping -> mapping.getToTableId().map(OPAMappingId::getName).orElse(null));
            setVisibleColumns("actions", "source", "linkName", "linkTypeDisplay", "fromKeyName", "fromTableSource", "toKeyName", "toTableSource");
            setColumnHeaders("", "Source", "Link Name", "Link Type", "From Key", "From Table", "To Key", "To Table");
            setColumnWidth("actions", 130);
        }};

        addTableMappingButton = new Button() {{
            setId("addJunctionTableMapping");
            setCaption("Add Junction Table Mapping");
            setIcon(EXTERNAL_LINK);
            setStyleName(BUTTON_PRIMARY);
            addClickListener((event) -> showCreateModelWindow());
        }};

        layout = new VerticalLayout() {{
            setSizeFull();
            setSpacing(true);
            setMargin(true);
            addComponents(addTableMappingButton, table);
        }};

        setCompositionRoot(layout);
    }

    @Override
    public void setEnabled(boolean enabled) {
        addTableMappingButton.setVisible(enabled);
        isViewMode = !enabled;
    }

    @Override
    public JunctionTableMapping createNewModelInstance() {
        return new JunctionTableMapping();
    }

    @Override
    public BaseCRUDFormComponent<JunctionTableMapping> getForm(JunctionTableMapping model) {
        val form = new JunctionTableMappingFormComponent(tables, getSiblings(model));
        form.setValue(model);
        return form;
    }

    @Override
    public List<JunctionTableMapping> getItemsFromParent(Mapping mapping) {
        return mapping.getJunctionTables();
    }

    @Override
    protected void saveItemsToParent(Mapping mapping, List<JunctionTableMapping> items) {
        items.forEach(junctionTableMapping -> junctionTableMapping.setMapping(mapping));
        mapping.setJunctionTables(items);
    }

    @Override
    protected void removeModel(JunctionTableMapping mapping) {
        // clear the table links
        mapping.removeFromTable();
        mapping.removeToTable();
        super.removeModel(mapping);
    }
}
