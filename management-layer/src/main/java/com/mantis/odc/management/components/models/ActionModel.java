/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.models;


import com.vaadin.event.Action;
import com.vaadin.server.Resource;
import lombok.Data;

import java.util.function.Consumer;

/**
 * List action. Used in context menu and action menu.
 */
@Data
public class ActionModel<MODEL> extends Action {

    /**
     * Flag if a model is required.
     */
    private final boolean requiresModel;

    /**
     * ActionModel consumer.
     */
    private final Consumer<MODEL> consumer;

    public ActionModel(String caption, Resource icon, boolean requiresModel, Consumer<MODEL> consumer) {
        super(caption, icon);
        this.requiresModel = requiresModel;
        this.consumer = consumer;
    }
}
