/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.dashboard.charts;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.config.BarChartConfig;
import com.byteowls.vaadin.chartjs.data.BarDataset;
import com.byteowls.vaadin.chartjs.options.InteractionMode;
import com.byteowls.vaadin.chartjs.options.Position;
import com.byteowls.vaadin.chartjs.options.scale.Axis;
import com.byteowls.vaadin.chartjs.options.scale.DefaultScale;
import com.mantis.odc.database.AppDB;
import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.val;

import javax.inject.Inject;
import java.util.ArrayList;

import static com.mantis.odc.management.components.dashboard.charts.OperationTypes.*;


/**
 * Stacked column chart showing operations by mappings.
 */
public class OperationsByMappingsChart extends ChartJs {

    private final AppDB db;

    @Inject
    public OperationsByMappingsChart(AppDB db) {
        this.db = db;
        setSizeFull();
        populateAndConfigureChart();
    }

    @SneakyThrows
    public void populateAndConfigureChart() {
        @Cleanup val conn = db.getConnectionPool().getConnection();
        @Cleanup val ps = conn.prepareStatement("SELECT m.m_name,"
                + "SUM(CASE WHEN op.op_type = 0 THEN 1 ELSE 0 END) AS CheckAliveCount,"
                + "SUM(CASE WHEN op.op_type = 1 THEN 1 ELSE 0 END) AS GetMetadataCount,"
                + "SUM(CASE WHEN op.op_type = 2 THEN 1 ELSE 0 END) AS LoadCount,"
                + "SUM(CASE WHEN op.op_type = 3 THEN 1 ELSE 0 END) AS SaveCount,"
                + "SUM(CASE WHEN op.op_type = 4 THEN 1 ELSE 0 END) AS GetCheckpointCount,"
                + "SUM(CASE WHEN op.op_type = 5 THEN 1 ELSE 0 END) AS SetCheckpointCount, "
                + "SUM(CASE WHEN op.op_type = 6 THEN 1 ELSE 0 END) AS ExecuteQueryCount "
                + "FROM odc_operations op "
                + "INNER JOIN odc_mappings m ON op.m_id = m.m_id "
                + "GROUP BY m.m_name");

        // configure
        BarChartConfig config = new BarChartConfig();
        config.data()
                .addDataset(new BarDataset().label(CHECK_ALIVE.getLabel()).backgroundColor(CHECK_ALIVE.getColor()))
                .addDataset(new BarDataset().label(GET_METADATA.getLabel()).backgroundColor(GET_METADATA.getColor()))
                .addDataset(new BarDataset().label(LOAD.getLabel()).backgroundColor(LOAD.getColor()))
                .addDataset(new BarDataset().label(SAVE.getLabel()).backgroundColor(SAVE.getColor()))
                .addDataset(new BarDataset().label(GET_CHECKPOINT.getLabel()).backgroundColor(GET_CHECKPOINT.getColor()))
                .addDataset(new BarDataset().label(SET_CHECKPOINT.getLabel()).backgroundColor(SET_CHECKPOINT.getColor()))
                .addDataset(new BarDataset().label(EXECUTE_QUERY.getLabel()).backgroundColor(EXECUTE_QUERY.getColor()))
                .and()
                .options()
                .responsive(true)
                .title()
                .display(false)
                .and()
                .legend()
                .position(Position.BOTTOM)
                .and()
                .tooltips()
                .mode(InteractionMode.INDEX)
                .intersect(false)
                .and()
                .scales()
                .add(Axis.X, new DefaultScale()
                        .scaleLabel()
                        .labelString("Mappings")
                        .and()
                        .stacked(true))
                .add(Axis.Y, new DefaultScale()
                        .scaleLabel()
                        .labelString("Requests")
                        .and()
                        .stacked(true))
                .and()
                .done();

        // populate data and labels
        val labels = new ArrayList<String>();
        val rs = ps.executeQuery();
        while (rs.next()) {
            val mappingName = rs.getString(1);
            val checkAliveCount = rs.getInt(2);
            val GetMetadataCount = rs.getInt(3);
            val LoadCount = rs.getInt(4);
            val SaveCount = rs.getInt(5);
            val GetCheckpointCount = rs.getInt(6);
            val SetCheckpointCount = rs.getInt(7);
            val ExecuteQueryCount = rs.getInt(8);

            // Add a label
            labels.add(mappingName);
            // Add to series
            ((BarDataset) config.data().getDatasetAtIndex(0)).addData(checkAliveCount);
            ((BarDataset) config.data().getDatasetAtIndex(1)).addData(GetMetadataCount);
            ((BarDataset) config.data().getDatasetAtIndex(2)).addData(LoadCount);
            ((BarDataset) config.data().getDatasetAtIndex(3)).addData(SaveCount);
            ((BarDataset) config.data().getDatasetAtIndex(4)).addData(GetCheckpointCount);
            ((BarDataset) config.data().getDatasetAtIndex(5)).addData(SetCheckpointCount);
            ((BarDataset) config.data().getDatasetAtIndex(6)).addData(ExecuteQueryCount);
        }

        // Add labels
        config.data().labelsAsList(labels);

        // configure chart
        configure(config);
        refreshData();
    }
}
