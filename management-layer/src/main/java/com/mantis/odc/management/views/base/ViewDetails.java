/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.views.base;

import com.vaadin.server.FontAwesome;

import java.util.List;

/**
 * Information about a view/page in this application.
 */
public interface ViewDetails {

    /**
     * Names of the view.
     *
     * @return view names
     */
    List<String> names();

    /**
     * Title of the view.
     *
     * @return view title
     */
    String title();

    /**
     * Icon for the view.
     *
     * @return view icon
     */
    FontAwesome icon();

    /**
     * View description.
     *
     * @return description
     */
    String description();

    /**
     * Flag to indicate a menu item should be created for it.
     *
     * @return menu item flag
     */
    boolean hasMenuItem();

    /**
     * @return position in the menu.
     */
    int menuPosition();
}
