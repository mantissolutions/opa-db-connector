package com.mantis.odc.management.layouts;

import lombok.experimental.UtilityClass;

@UtilityClass
public class MantisTheme {
    String PANEL = "mantis-blue";
    String FOOTER = "footer";
    String LOGIN_LOGO = "mantis-logo-login";
    String LOGO = "mantis-logo";
    String EXTRA_TOP_PADDING = "extra-top-padding";
    String COPYRIGHT = "© Copyright 2018 Mantis Solutions";
}
