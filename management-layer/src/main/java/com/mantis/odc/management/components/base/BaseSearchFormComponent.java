/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.base;

import com.vaadin.data.Container;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Field;

import java.util.Map;

/**
 * Base Search form.
 */
public abstract class BaseSearchFormComponent extends CustomComponent {

    /**
     * Clears the search conditions.
     */
    public void clearSearch() {
        getContainer().removeAllContainerFilters();
        getFilters().forEach((field, filter) -> field.setValue(null));
    }

    /**
     * @return container to filter.
     */
    public abstract Container.Filterable getContainer();


    /**
     * Returns all filters implemented by this form.
     *
     * @return all form filter fields
     */
    public abstract Map<Field, Container.Filter> getFilters();

    /**
     * Searches the container.
     */
    public void search() {
        // remove existing filters
        getContainer().removeAllContainerFilters();

        // add filters to container
        getFilters().forEach((field, filter) -> {
            Object value = field.getValue();
            if (value != null && !value.equals(""))
                getContainer().addContainerFilter(filter);
        });
    }
}
