/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.views;

import com.mantis.odc.management.components.datasources.DatasourceListComponent;
import com.mantis.odc.management.layouts.MainLayout;
import com.mantis.odc.management.support.SecureNavigator;
import com.mantis.odc.management.views.base.ComponentView;
import com.mantis.odc.management.views.model.URLParameters;
import com.vaadin.server.FontAwesome;
import lombok.Data;
import lombok.val;

import javax.inject.Inject;
import java.util.List;

import static com.vaadin.server.FontAwesome.DATABASE;
import static java.util.Collections.singletonList;

/**
 * Data source view.
 */
@Data
public class DatasourceView extends ComponentView {

    private final DatasourceListComponent component;

    @Inject
    public DatasourceView(DatasourceListComponent component) {
        super(new MainLayout(component, true, true));
        setSizeFull();

        this.component = component;
    }

    @Override
    protected void enter(List<String> parts, URLParameters parameters) {
        super.enter(parts, parameters);
        component.updateFromDatabase();
    }

    @Override
    public void setNavigator(SecureNavigator navigator) {
        val layout = (MainLayout) getCompositionRoot();
        layout.setNavigator(navigator);
        super.setNavigator(navigator);
    }

    @Override
    public List<String> names() {
        return singletonList("datasources");
    }

    @Override
    public String title() {
        return "Datasources";
    }

    @Override
    public FontAwesome icon() {
        return DATABASE;
    }

    @Override
    public String description() {
        return "Manage datasource mappings.";
    }

    @Override
    public boolean hasMenuItem() {
        return true;
    }

    @Override
    public int menuPosition() {
        return 2;
    }
}
