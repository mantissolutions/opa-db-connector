/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.datasources;

import com.mantis.odc.models.DatasourceMapping;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import lombok.Data;
import org.vaadin.aceeditor.AceEditor;
import org.vaadin.aceeditor.AceMode;

import java.sql.SQLException;

import static com.mantis.odc.utilities.ErrorUtil.errorToString;
import static com.vaadin.event.ShortcutAction.KeyCode.ENTER;
import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;
import static com.vaadin.server.Sizeable.Unit.PIXELS;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_DANGER;

/**
 * Tests a given data source.
 */
@Data
class TestDatasourceDialog extends Window {

    private final DatasourceMapping mapping;
    private final TextField usernameField;
    private final PasswordField passwordField;
    private final Button testButton;
    private final AceEditor detailLabel;
    private final Panel messagePanel;
    private final HorizontalLayout buttonLayout;
    private final FormLayout formLayout;

    TestDatasourceDialog(DatasourceMapping mapping) {
        this.mapping = mapping;

        setId("testDatasourceDialog");
        setSizeUndefined();
        setCaption(" Test Datasource");
        setIcon(DATABASE);
        setDraggable(true);
        setResizable(false);
        setModal(true);
        center();

        usernameField = new TextField() {{
            setId("user");
            setCaption("Database User");
            setIcon(USER);
            setInputPrompt("user...");
            setNullRepresentation("");
            setImmediate(true);
            setDescription("Username for the database user");
        }};

        passwordField = new PasswordField() {{
            setId("password");
            setCaption("Database Password");
            setIcon(LOCK);
            setNullRepresentation("");
            setImmediate(true);
            setDescription("Password for the database user.");
        }};

        testButton = new Button() {{
            setId("test");
            setCaption("Test Connection");
            setIcon(DATABASE);
            setStyleName(BUTTON_DANGER);
            setClickShortcut(ENTER);
            addClickListener(TestDatasourceDialog.this::testDatasource);
        }};

        detailLabel = new AceEditor() {{
            setId("detail");
            setWidth(100, PERCENTAGE);
            setHeight(187, PIXELS);
            setWordWrap(true);
            setMode(AceMode.plain_text);
        }};

        messagePanel = new Panel() {{
            setId("message");
            setVisible(false);
            setHeight(250, PIXELS);
            setWidth(500, PIXELS);
            setContent(new VerticalLayout() {{
                setSpacing(true);
                setMargin(true);
                addComponent(detailLabel);
            }});
        }};

        buttonLayout = new HorizontalLayout() {{
            setSizeUndefined();
            setSpacing(true);
            addComponents(testButton);
        }};

        formLayout = new FormLayout() {{
            setSizeUndefined();
            setSpacing(true);
            addComponents(usernameField, passwordField, buttonLayout);
        }};

        setContent(new VerticalLayout() {{
            setSpacing(true);
            setMargin(true);
            setSizeUndefined();
            addComponents(formLayout, messagePanel);
        }});

        UI.getCurrent().addWindow(this);
    }

    /**
     * Test the given data source.
     *
     * @param clickEvent click event
     */
    protected void testDatasource(Button.ClickEvent clickEvent) {
        try {
            mapping.test(usernameField.getValue(), passwordField.getValue());
            setMessage("Connection Successful", null, CHECK_CIRCLE);
        } catch (ClassNotFoundException | SQLException e) {
            setMessage("Connection Failed: " + e.getMessage(), errorToString(e), FontAwesome.EXCLAMATION);
        }
    }

    /**
     * Sets the message.
     *
     * @param message message to set
     * @param detail  detail to user
     * @param icon    icon to user
     */
    void setMessage(String message, String detail, FontAwesome icon) {
        messagePanel.setCaption(message);
        messagePanel.setIcon(icon);
        detailLabel.setReadOnly(false);
        detailLabel.setValue((detail != null) ? detail : "");
        detailLabel.setReadOnly(true);
        messagePanel.setVisible(true);
    }
}
