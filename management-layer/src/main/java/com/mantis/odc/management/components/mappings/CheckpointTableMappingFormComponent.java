/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings;

import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.models.CheckpointMapping;
import com.vaadin.ui.*;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

import static com.mantis.odc.management.components.utilities.BindingUtils.bind;
import static com.mantis.odc.management.components.utilities.ValidationUtils.maxLength;
import static com.vaadin.server.FontAwesome.COLUMNS;
import static com.vaadin.server.FontAwesome.DATABASE;

/**
 * Attachment table mapping from component.
 */
@Data
class CheckpointTableMappingFormComponent extends BaseCRUDFormComponent<CheckpointMapping> {

    private final TextField sourceField = new TextField() {{
        setId("source");
        setCaption("Table Source");
        setIcon(DATABASE);
        setInputPrompt("source...");
        setNullRepresentation("");
        setRequired(true);
        setRequiredError("Table source is required.");
        maxLength(this, 250);
        setDescription("Table source name.");
    }};

    private final TextField bindTokenField = new TextField() {{
        setId("bindToken");
        setCaption("URL Bind Token Name");
        setInputPrompt("token...");
        setNullRepresentation("");
        setRequired(true);
        setRequiredError("Bind token name is required.");
        maxLength(this, 250);
        setDescription("Name of the get parameter used in load requests.");
    }};

    private final TextField idFieldField = new TextField() {{
        setId("idField");
        setCaption("Id Field Name");
        setIcon(COLUMNS);
        setInputPrompt("field name...");
        setNullRepresentation("");
        setRequired(true);
        setRequiredError("Id field name is required.");
        maxLength(this, 250);
        setDescription("Name of the id field name in the checkpoint table.");
    }};

    private final TextField dataFieldField = new TextField() {{
        setId("dataField");
        setCaption("Data Field Name");
        setIcon(COLUMNS);
        setInputPrompt("field name...");
        setNullRepresentation("");
        setRequired(true);
        setRequiredError("Data field name is required.");
        maxLength(this, 250);
        setDescription("Name of the data field name in the checkpoint table.");
    }};

    // not used in this form
    private final Button saveButton = new Button();

    private final VerticalLayout layout = new VerticalLayout() {{
        setSpacing(true);
        setMargin(true);
        addComponent(new FormLayout() {{
            setSizeUndefined();
            setSpacing(true);
            addComponents(sourceField, bindTokenField, idFieldField, dataFieldField);
        }});
    }};

    @Override
    protected void bindToModel(CheckpointMapping model) {
        bind(sourceField, model, String.class, CheckpointMapping::getTableName, CheckpointMapping::setTableName);
        bind(bindTokenField, model, String.class, CheckpointMapping::getBindToken, CheckpointMapping::setBindToken);
        bind(idFieldField, model, String.class, CheckpointMapping::getIdField, CheckpointMapping::setIdField);
        bind(dataFieldField, model, String.class, CheckpointMapping::getDataField, CheckpointMapping::setDataField);
    }

    @Override
    protected List<Field<?>> getFields() {
        return Arrays.asList(sourceField, bindTokenField, idFieldField, dataFieldField);
    }

    @Override
    public Class<? extends CheckpointMapping> getType() {
        return CheckpointMapping.class;
    }
}