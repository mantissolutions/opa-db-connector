/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.layouts;

import com.mantis.odc.management.support.SecureNavigator;
import com.mantis.odc.management.views.base.ViewDetails;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ClassResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Panel;
import lombok.Data;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.mantis.odc.management.support.SecureNavigator.LOGOUT;
import static com.vaadin.server.FontAwesome.SIGN_OUT;
import static com.vaadin.ui.Alignment.MIDDLE_LEFT;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_BORDERLESS;
import static com.vaadin.ui.themes.ValoTheme.PANEL_BORDERLESS;

/**
 * Main menu component.
 */
@Data
class MainMenu extends Panel implements ViewChangeListener {

    private Map<ViewDetails, Button> menuItems;

    private Button logoutButton;

    MainMenu() {
        setSizeFull();
        addStyleName(PANEL_BORDERLESS);
        addStyleName(MantisTheme.PANEL);
    }

    void setNavigator(SecureNavigator navigator) {
        if (menuItems == null) {
            menuItems = new LinkedHashMap<>();
            navigator.addViewChangeListener(this);

            setContent(new HorizontalLayout() {{
                setSpacing(true);
                setMargin(new MarginInfo(false, true));

                // logo
                addComponent(new Image(null, new ClassResource("/mantis_icon.png")) {{
                    setStyleName(MantisTheme.LOGO);
                    setDescription("Mantis Solutions");
                }});

                // add buttons
                navigator.getViews().stream()
                        .map(component -> (ViewDetails) component)
                        .filter(ViewDetails::hasMenuItem)
                        .sorted(Comparator.comparingInt(ViewDetails::menuPosition))
                        .forEach(viewDetails -> menuItems.put(viewDetails, new Button() {{
                            setCaption(viewDetails.title());
                            setIcon(viewDetails.icon());
                            setStyleName(BUTTON_BORDERLESS);
                            addClickListener(event -> navigator.navigateTo(viewDetails.names().get(0)));
                            addComponent(this);
                            setComponentAlignment(this, MIDDLE_LEFT);
                        }}));

                // add logout button
                logoutButton = new Button() {{
                    setCaption("Logout");
                    setIcon(SIGN_OUT);
                    setStyleName(BUTTON_BORDERLESS);
                    addClickListener(event -> navigator.navigateTo(LOGOUT));
                    addComponent(this);
                    setComponentAlignment(this, MIDDLE_LEFT);
                }};
            }});
        }
    }

    @Override
    public void afterViewChange(ViewChangeEvent event) {
        menuItems.forEach((details, button) -> button.setEnabled(!details.names().contains(event.getViewName())));
    }

    @Override
    public boolean beforeViewChange(ViewChangeEvent event) {
        return true;
    }
}
