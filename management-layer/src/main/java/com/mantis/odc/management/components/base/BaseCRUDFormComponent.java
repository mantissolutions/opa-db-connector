/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.base;

import com.vaadin.data.Buffered;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Window;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.val;

import java.lang.reflect.Method;

/**
 * Base CRUD from component
 */
@Data
public abstract class BaseCRUDFormComponent<MODEL> extends BaseModelCustomComponent<MODEL> {

    /**
     * Flag to indicate the form should close on save.
     */
    private boolean closeOnSave = true;

    /**
     * @return save button
     */
    public abstract Button getSaveButton();

    @Override
    public void setReadOnly(boolean readOnly) {
        getSaveButton().setVisible(!readOnly);
        super.setReadOnly(readOnly);
    }

    @Override
    public void setEnabled(boolean enabled) {
        getSaveButton().setVisible(enabled);
        super.setEnabled(enabled);
    }

    @Override
    protected Component initContent() {
        // commit all fields on a save button press
        getSaveButton().addClickListener(event -> {
            saveModel(getValue());
            if (closeOnSave) closeWindow();
        });
        return super.initContent();
    }

    /**
     * Closes the current window if the form is in one.
     */
    protected void closeWindow() {
        val parent = getParent();
        if (parent instanceof Window) {
            ((Window) parent).close();
        }
    }

    /**
     * Saves the model.
     *
     * @param model model
     */
    public void saveModel(MODEL model) {
        // commit data
        getFields().forEach(Buffered::commit);
        // call form saved
        fireEvent(new FormSavedEvent<>(this, getValue()));
    }

    /**
     * @param listener form listener to add
     */
    @SneakyThrows
    public void addFormSavedListener(FormSavedListener<MODEL> listener) {
        Method method = FormSavedListener.class.getDeclaredMethod("formSaved", FormSavedEvent.class);
        addListener(FormSavedEvent.class, listener, method);
    }

    /**
     * @param listener listener to remove
     */
    public void removeFormSavedListener(FormSavedListener<MODEL> listener) {
        removeListener(FormSavedListener.class, listener);
    }

    /**
     * Listens on form save events.
     *
     * @param <MODEL> model type
     */
    public interface FormSavedListener<MODEL> {
        /**
         * @param event saved event
         */
        void formSaved(FormSavedEvent<MODEL> event);
    }

    /**
     * Created on a save.
     *
     * @param <MODEL> model type
     */
    @Data
    public static class FormSavedEvent<MODEL> extends Event {

        private final MODEL model;

        /**
         * Constructs a new event with the specified source component.
         *
         * @param source the source component of the event
         * @param model  saved model
         */
        public FormSavedEvent(Component source, MODEL model) {
            super(source);
            this.model = model;
        }
    }
}
