/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.utilities;

import com.mantis.odc.management.components.data.BindingProperty;
import com.vaadin.ui.Field;

import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Utilities for bind fields to objects.
 */
public class BindingUtils {

    /**
     * Binds a field to an object field. This is a one way binding, changes to model
     * by other fields will not automatically propagate to this field.
     *
     * @param field   field to bind
     * @param object  object to bind
     * @param type    parameter type
     * @param getter  getter function
     * @param setter  setter function
     * @param <MODEL> model type
     * @param <T>     field value type
     */
    public static <MODEL, T> void bind(Field field,
                                       MODEL object,
                                       Class type,
                                       Function<MODEL, T> getter,
                                       BiConsumer<MODEL, T> setter) {
        field.setBuffered(true);
        field.setPropertyDataSource(new BindingProperty<>(null, object, type, field.isReadOnly(), getter, setter));
    }
}
