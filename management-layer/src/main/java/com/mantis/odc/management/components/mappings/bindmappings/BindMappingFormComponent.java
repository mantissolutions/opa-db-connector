/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings.bindmappings;

import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.models.*;
import com.vaadin.ui.*;
import lombok.Getter;
import lombok.val;

import java.util.Arrays;
import java.util.List;

import static com.mantis.odc.management.components.utilities.BindingUtils.bind;
import static com.mantis.odc.management.components.utilities.ValidationUtils.dependOn;
import static com.mantis.odc.management.components.utilities.ValidationUtils.maxLength;
import static com.vaadin.event.ShortcutAction.KeyCode.ENTER;
import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;

/**
 * Bind Mapping form.
 */
@Getter
class BindMappingFormComponent extends BaseCRUDFormComponent<BindMapping> {

    private final List<TableMapping> tableMappings;
    private final TextField bindTokenField;
    private final CheckBox requiredField;
    private final ComboBox conditionField;
    private final ComboBox operatorField;
    private final ComboBox tableField;
    private final ComboBox fieldField;
    private final Button saveButton;
    private final VerticalLayout layout;

    BindMappingFormComponent(List<TableMapping> tableMappings) {
        this.tableMappings = tableMappings;

        bindTokenField = new TextField() {{
            setId("bindToken");
            setCaption("URL Bind Token Name");
            setInputPrompt("token...");
            setNullRepresentation("");
            setRequired(true);
            setRequiredError("Bind token name is required.");
            maxLength(this, 250);
            setDescription("Name of the get parameter used in load requests.");
        }};

        requiredField = new CheckBox() {{
            setId("required");
            setCaption("Required Field");
            setDescription("Flag to indicate this condition must be passed on all load requests.");
        }};

        conditionField = new ComboBox() {{
            setId("condition");
            setCaption("Condition");
            setNullSelectionAllowed(false);
            setRequired(true);
            setRequiredError("A condition is required.");
            for (val condition : ConditionalJoin.values()) {
                addItem(condition);
                setItemCaption(condition, condition.getName());
            }
        }};

        operatorField = new ComboBox() {{
            setId("operator");
            setCaption("Operator");
            setNullSelectionAllowed(false);
            setRequired(true);
            setRequiredError("An operator is required.");
            for (val operator : Operator.values()) {
                addItem(operator);
                setItemCaption(operator, operator.getName());
            }
        }};

        tableField = new ComboBox() {{
            setId("table");
            setCaption("Table");
            setIcon(TABLE);
            setRequired(true);
            setRequiredError("A table is required.");
            for (val t : tableMappings) {
                addItem(t);
                setItemCaption(t, t.getTableId().getName());
            }
        }};

        fieldField = new ComboBox() {{
            setId("field");
            setCaption("Field");
            setIcon(COLUMNS);
            setRequired(true);
            setRequiredError("A field is required.");
            tableField.addValueChangeListener(event -> {
                List<FieldMapping> fields = getFieldMappings();
                removeAllItems();
                if (fields != null) {
                    for (FieldMapping t : fields) {
                        addItem(t);
                        setItemCaption(t, t.getFieldId().getName());
                    }
                }
            });
        }};

        saveButton = new Button() {{
            setId("saveButton");
            setCaption("Save Bind Mapping");
            setIcon(SAVE);
            setStyleName(BUTTON_PRIMARY);
            setClickShortcut(ENTER);
            dependOn(this, bindTokenField, requiredField, conditionField, operatorField, tableField, fieldField);
        }};

        layout = new VerticalLayout() {{
            setSpacing(true);
            setMargin(true);
            addComponent(new FormLayout() {{
                setSizeUndefined();
                setSpacing(true);
                addComponents(bindTokenField, requiredField, conditionField, operatorField, tableField, fieldField);
            }});
            addComponent(new HorizontalLayout() {{
                setSizeUndefined();
                setSpacing(true);
                addComponents(saveButton);
            }});
        }};
    }

    /**
     * @return field mappings
     */
    private List<FieldMapping> getFieldMappings() {
        return tableMappings.stream()
                .filter(tableMapping -> tableMapping.equals(tableField.getValue()))
                .findFirst()
                .map(TableMapping::getFields)
                .orElse(null);
    }

    @Override
    protected void bindToModel(BindMapping model) {
        bind(bindTokenField, model, String.class, BindMapping::getTokenName, BindMapping::setTokenName);
        bind(requiredField, model, Boolean.class, BindMapping::isRequired, BindMapping::setRequired);
        bind(conditionField, model, ConditionalJoin.class, BindMapping::getCondition, BindMapping::setCondition);
        bind(operatorField, model, Operator.class, BindMapping::getOperator, BindMapping::setOperator);
        bind(tableField, model, TableMapping.class, BindMapping::getTable, BindMapping::setTable);
        bind(fieldField, model, FieldMapping.class, BindMapping::getField, BindMapping::setField);
    }

    @Override
    protected List<Field<?>> getFields() {
        return Arrays.asList(bindTokenField, requiredField, conditionField, operatorField, tableField, fieldField);
    }

    @Override
    public Class<? extends BindMapping> getType() {
        return BindMapping.class;
    }
}
