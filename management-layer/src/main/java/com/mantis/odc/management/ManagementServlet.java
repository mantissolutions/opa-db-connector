/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;

/**
 * Management entry point.
 */
@VaadinServletConfiguration(productionMode = false, ui = MainUI.class)
public class ManagementServlet extends VaadinServlet {
}