/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.views.base;

import com.mantis.odc.management.support.SecureNavigator;
import com.mantis.odc.management.views.model.URLParameters;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.val;

import java.util.Arrays;
import java.util.List;

/**
 * View based on a root component. The forces component logic out of views.
 * Views are the equivalent of presenters.
 */
public abstract class ComponentView extends CustomComponent implements View, ViewDetails {

    @Setter
    @Getter
    protected SecureNavigator navigator;

    /**
     * @param component root component
     */
    public ComponentView(Component component) {
        super(component);
    }

    @Override
    @SneakyThrows
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        this.navigator = (SecureNavigator) event.getNavigator();
        val parts = event.getParameters().split("\\?");
        val paths = parts[0];
        val query = (parts.length > 1) ? parts[1] : "";
        enter(Arrays.asList(paths.split("/")), new URLParameters(query));
    }

    /**
     * @param parts      url parts after the view name
     * @param parameters url parameters
     */
    protected void enter(List<String> parts, URLParameters parameters) {
    }

}
