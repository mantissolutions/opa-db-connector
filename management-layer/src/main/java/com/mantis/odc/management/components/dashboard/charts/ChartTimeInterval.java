/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.dashboard.charts;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;

import java.sql.Date;
import java.util.Calendar;

import static java.util.Calendar.*;

/**
 * Supported Chart intervals.
 */
@RequiredArgsConstructor
public enum ChartTimeInterval {
    OneDay("One Day", removeFromToday(-1, DATE)),
    FiveDays("Five Days", removeFromToday(-5, DATE)),
    OneMonth("One Month", removeFromToday(-1, MONTH)),
    ThreeMonths("Three Months", removeFromToday(-3, MONTH)),
    SixMonths("Six Months", removeFromToday(-6, MONTH)),
    OneYear("One Year", removeFromToday(-1, YEAR));

    @Getter
    private final String caption;
    @Getter
    private final Date date;

    public static Date removeFromToday(int amount, int unit) {
        val cal = Calendar.getInstance();
        cal.setTime(new java.util.Date());
        cal.add(unit, amount);
        return new Date(cal.getTimeInMillis());
    }
}
