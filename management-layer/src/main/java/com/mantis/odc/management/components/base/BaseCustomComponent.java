/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.base;

import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;

/**
 * Base component class.
 */
public abstract class BaseCustomComponent extends CustomComponent {

    public BaseCustomComponent() {
        setCompositionRoot(getLayout());
    }

    /**
     * @return component layout
     */
    protected abstract Component getLayout();
}
