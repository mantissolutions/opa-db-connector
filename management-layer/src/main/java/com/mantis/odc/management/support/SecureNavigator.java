/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.support;

import com.mantis.odc.management.MainUI;
import com.mantis.odc.management.di.ViewFactory;
import com.mantis.odc.management.views.base.ComponentView;
import com.mantis.odc.management.views.base.ViewDetails;
import com.mantis.odc.management.views.model.URLParameter;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.UI;
import lombok.Data;
import lombok.val;
import org.jooq.lambda.Unchecked;

import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.mantis.odc.management.views.LoginView.REDIRECT_VIEW;

/**
 * Implements a secure view provider and navigator.
 * All view implemented in the factory are exposed in this navigator.
 * A default view is used in place of a requested view. All views must implement
 * ComponentView or some sub class.
 */
@Data
public class SecureNavigator extends Navigator implements ViewChangeListener {

    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String DEFAULT_VIEW = "";

    private final List<ComponentView> views;
    private final Map<String, ComponentView> viewByName;

    /**
     * @param ui          root UI
     * @param viewFactory view factory
     */
    public SecureNavigator(MainUI ui, ViewFactory viewFactory) {
        super(ui, ui.getContentPanel());

        // add views
        views = createViews(viewFactory);
        views.forEach(components -> components.setNavigator(this));
        viewByName = groupByName(views);
        views.forEach(view -> view.names().forEach(name -> addView(name, view)));

        // default page title
        Page.getCurrent().setTitle("OPA DB Connector");
    }

    /**
     * Groups views by valid paths/names for those views.
     *
     * @param views list of views
     * @return map of paths/names to views
     */
    private Map<String, ComponentView> groupByName(List<ComponentView> views) {
        val map = new HashMap<String, ComponentView>();
        views.forEach(view -> view.names().forEach(name -> map.put(name, view)));
        return map;
    }

    /**
     * Adds each view the factory can produce to the navigator.
     *
     * @param viewFactory view factory
     */
    private List<ComponentView> createViews(ViewFactory viewFactory) {
        return Arrays.stream(ViewFactory.class.getMethods())
                .map(Unchecked.function(method -> (ComponentView) method.invoke(viewFactory)))
                .collect(Collectors.toList());
    }

    /**
     * Sets the page title.
     *
     * @param event view change event
     */
    @Override
    public void afterViewChange(ViewChangeEvent event) {
        Optional.ofNullable((ViewDetails) event.getNewView())
                .ifPresent(viewDetails -> Page.getCurrent()
                        .setTitle("OPA DB Connector | " + viewDetails.title()));
    }

    /**
     * Checks the user is authenticated. If they are proceed to the requested view,
     * Otherwise redirect them to the login page. If a logout request is made, the user is
     * logged out and the login view is displayed.
     *
     * @param event view change event
     * @return true to proceed
     */
    @Override
    public boolean beforeViewChange(ViewChangeEvent event) {
        val subject = ((MainUI) UI.getCurrent()).getSubject();
        val viewName = event.getViewName();
        val redirectView = viewName + event.getParameters();

        // log subject out
        if (LOGOUT.equals(viewName)) subject.logout();

        if (subject.isAuthenticated() || LOGIN.equals(viewName))
            return true;
        else
            navigateTo(LOGIN, null, new URLParameter(REDIRECT_VIEW, LOGOUT.equals(viewName) ? DEFAULT_VIEW : redirectView));

        return false;
    }

    /**
     * @param viewName      name of the view
     * @param paths         navigation state, any additional parts
     * @param urlParameters query parameters
     */
    private void navigateTo(String viewName, String paths, URLParameter... urlParameters) {
        val view = viewByName.get(viewName);
        navigateTo(view, viewName, MessageFormat.format("{0}?{1}", (paths != null) ? paths : "", Arrays.stream(urlParameters)
                .map(URLParameter::encodeToString)
                .collect(Collectors.joining("&"))));
    }
}
