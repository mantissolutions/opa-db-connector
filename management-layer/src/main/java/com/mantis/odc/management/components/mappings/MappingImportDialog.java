/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings;

import com.mantis.odc.models.Mapping;
import com.mantis.odc.services.validators.MappingValidator;
import com.mantis.odc.services.validators.ValidationResult;
import com.mantis.odc.xml.MappingXMLTransport;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import lombok.Data;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Method;

import static com.vaadin.ui.themes.ValoTheme.LABEL_FAILURE;

/**
 * Popup used to import mappings.
 */
@Getter
@Slf4j
class MappingImportDialog extends Window {

    private final MappingXMLTransport transport;
    private final MappingValidator validator;
    private ByteArrayOutputStream outputStream;
    private final Upload uploadField;
    private final Label errorLabel;
    private final VerticalLayout layout;

    /**
     * @param transport mapping XML transport
     * @param validator mapping validator
     */
    MappingImportDialog(MappingXMLTransport transport, MappingValidator validator) {
        this.transport = transport;
        this.validator = validator;

        setSizeUndefined();
        setCaption(" Import Mapping");
        setIcon(FontAwesome.UPLOAD);
        center();

        uploadField = new Upload() {{
            setCaption("Select mapping XML file to import");
            setButtonCaption("Import");
            setReceiver((filename, mimeType) -> outputStream = new ByteArrayOutputStream());
            addSucceededListener(event -> {
                try {
                    if (event.getLength() != 0) {
                        Mapping mapping = transport.load(new ByteArrayInputStream(outputStream.toByteArray()));
                        ValidationResult result = validator.validate(mapping);
                        if (result.isSuccess()) {
                            MappingImportDialog.this.fireEvent(new ImportedMappingEvent(this, mapping));
                            close();
                        } else new MappingValidationDialog(result);
                    } else setFailedMessage("No file selected.");
                } catch (Exception e) {
                    log.error("failed to import mapping {}.", e);
                    setFailedMessage(e.getMessage());
                }
            });
        }};

        errorLabel = new Label() {{
            setId("errorLabel");
            setStyleName(LABEL_FAILURE);
            setVisible(false);
        }};

        layout = new VerticalLayout() {{
            setSizeUndefined();
            setSpacing(true);
            setMargin(true);
            addComponents(errorLabel, uploadField);
        }};

        UI.getCurrent().addWindow(this);
        setContent(layout);
    }

    /**
     * Updates the errorLabel message.
     *
     * @param message errorLabel message to show
     */
    private void setFailedMessage(String message) {
        val m = (message != null && !message.isEmpty()) ? message : "Failed to import mapping.";
        errorLabel.setValue(m);
        errorLabel.setVisible(true);
    }

    /**
     * @param listener form listener to add
     */
    @SneakyThrows
    public void addImportedMappingListener(ImportedMappingListener listener) {
        Method method = ImportedMappingListener.class.getDeclaredMethod("formSaved", ImportedMappingEvent.class);
        addListener(ImportedMappingEvent.class, listener, method);
    }

    /**
     * @param listener listener to remove
     */
    public void removeImportedMappingListener(ImportedMappingListener listener) {
        removeListener(ImportedMappingEvent.class, listener);
    }

    /**
     * Listens on active connection events.
     */
    public interface ImportedMappingListener {
        /**
         * @param event active imported mapping event
         */
        void formSaved(ImportedMappingEvent event);
    }

    /**
     * Created on an imported mapping.
     */
    @Data
    public static class ImportedMappingEvent extends Event {

        private final Mapping mapping;

        /**
         * Constructs a new event with the specified source component.
         *
         * @param source  the source component of the event
         * @param mapping imported mapping
         */
        @SneakyThrows
        public ImportedMappingEvent(Component source, Mapping mapping) {
            super(source);
            this.mapping = mapping;
        }
    }
}