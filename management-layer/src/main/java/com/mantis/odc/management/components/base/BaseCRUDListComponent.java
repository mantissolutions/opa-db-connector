/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.base;

import com.mantis.odc.database.base.DAO;
import com.mantis.odc.management.components.models.ActionModel;
import com.mantis.odc.models.base.Duplicatable;
import com.vaadin.data.Container;
import com.vaadin.data.util.AbstractContainer;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.vaadin.dialogs.ConfirmDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.vaadin.event.ShortcutAction.KeyCode.ENTER;
import static com.vaadin.event.ShortcutAction.KeyCode.ESCAPE;
import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.ui.Notification.Type.TRAY_NOTIFICATION;
import static com.vaadin.ui.Notification.show;

/**
 * Base CRUD component.
 */
@Slf4j
public abstract class BaseCRUDListComponent<ID, MODEL extends Duplicatable<MODEL>> extends BaseCustomComponent implements TableActions<MODEL> {

    /**
     * Model resolver. This can be overridden for containers which have alternate identifying methods.
     */
    protected abstract Function<ID, MODEL> getKeyToModelResolver();

    /**
     * @return name of the given model
     */
    public abstract String getModelName();

    /**
     * @return returns the data access object for the model.
     */
    public abstract DAO<ID, MODEL> getDao();

    /**
     * @param model model
     * @return key for the model
     */
    protected abstract ID getModelKey(MODEL model);

    /**
     * @return returns the underlying list container
     */
    public abstract Container getContainer();

    /**
     * @return model form
     */
    public abstract BaseCRUDFormComponent<MODEL> getForm(MODEL model);

    /**
     * @return new instance of the given model
     */
    public abstract MODEL createNewModelInstance();

    /**
     * Creates a new form window.
     *
     * @param title window title
     * @param icon  window icon
     * @param form  form
     * @return window
     */
    protected Window createFormWindow(String title, FontAwesome icon, BaseCRUDFormComponent<?> form) {
        return new Window() {
            {
                setCaption(" " + title);
                setIcon(icon);
                setContent(form);
                setClosable(true);
                setResizable(false);
                setModal(true);
                addCloseShortcut(ESCAPE);
                center();
                UI.getCurrent().addWindow(this);
            }

            // Add a confirm button on close if the data has not been saved.
            @Override
            public void close() {
                if (form.isModified()) {
                    val dialog = ConfirmDialog.show(UI.getCurrent(), " Unsaved Data",
                            "This form has unsaved data. Are you sure you want to close it?",
                            "Yes", "No",
                            d -> {
                                if (d.isConfirmed()) super.close();
                            });
                    dialog.getOkButton().setClickShortcut(ENTER);
                    dialog.setIcon(WARNING);
                } else super.close();
            }
        };
    }

    /**
     * @param model model to view
     */
    protected void showViewModelWindow(MODEL model) {
        val form = getForm(model);
        form.setEnabled(false);
        createFormWindow(getModelName(), EYE, form);
    }

    /**
     * Creates a new model and displays the form.
     */
    protected void showCreateModelWindow() {
        val model = createNewModelInstance();
        val form = getForm(model);
        form.addFormSavedListener(event -> createOrUpdateModel(event.getModel()));
        createFormWindow("Create " + getModelName(), PLUS, form);
    }

    /**
     * Creates a new model.
     *
     * @param model model to create
     */
    protected void createModel(MODEL model) {
        withUserFeedback(getModelName() + " Saved!",
                () -> {
                    getDao().create(model);
                    // add new item to the container
                    Container container = getContainer();
                    if (container instanceof BeanContainer)
                        ((BeanContainer) container).addItem(getModelKey(model), model);
                    else container.addItem(model);
                }, null);
    }

    /**
     * Updates the given model.
     *
     * @param model model to update
     */
    protected void showEditModelWindow(MODEL model) {
        val form = getForm(model);
        form.addFormSavedListener(event -> updateModel(event.getModel()));
        createFormWindow("Update " + getModelName(), PENCIL, form);
    }

    /**
     * Updates a existing model.
     *
     * @param model model to update
     */
    protected void updateModel(MODEL model) {
        withUserFeedback(getModelName() + " Updated!",
                () -> {
                    getDao().update(model);
                    replaceModel(model);
                },
                () -> refreshModelFromDatabase(model));
    }

    /**
     * Refreshes the model from the database.
     *
     * @param model model to refresh
     */
    protected void refreshModelFromDatabase(MODEL model) {
        getContainer().removeItem(getModelKey(model));
        getDao().get(getModelKey(model)).ifPresent(this::replaceModel);
    }

    /**
     * Replaces the old model with the new model.
     * This can be used to refresh the given row the model relates to.
     *
     * @param model new model
     */
    @SneakyThrows
    protected void replaceModel(MODEL model) {
        val fireItemSetChangeMethod = AbstractContainer.class.getDeclaredMethod("fireItemSetChange");
        fireItemSetChangeMethod.setAccessible(true);
        fireItemSetChangeMethod.invoke(getContainer());
    }

    /**
     * Copies the model.
     *
     * @param model model to copy
     */
    protected void showCopyModelWindow(MODEL model) {
        val copy = model.copy();
        val form = getForm(copy);
        form.addFormSavedListener(event -> createOrUpdateModel(event.getModel()));
        createFormWindow("Copy " + getModelName(), COPY, form);
    }

    /**
     * Copies a model.
     *
     * @param model model to copy
     */
    protected void createOrUpdateModel(MODEL model) {
        if (getContainer().containsId(getModelKey(model))) updateModel(model);
        else createModel(model);
    }

    /**
     * Removes the given model.
     *
     * @param model model to remove
     */
    protected void showRemoveModelDialog(MODEL model) {
        val dialog = ConfirmDialog.show(UI.getCurrent(), " Remove " + getModelName(),
                "Are you sure?", "Yes", "No", d -> {
                    if (d.isConfirmed()) removeModel(getModelKey(model));
                });
        dialog.setIcon(TRASH);
    }

    /**
     * Removes the given model.
     *
     * @param id model id to remove.
     */
    protected void removeModel(ID id) {
        withUserFeedback(getModelName() + " Removed!", () -> {
            getDao().delete(id);
            getContainer().removeItem(id);
        }, null);
    }

    /**
     * Runs the given operation with user notifications on success and failure.
     *
     * @param successMessage success message
     * @param operation      operation to run
     */
    protected void withUserFeedback(String successMessage, Runnable operation, Runnable errorOperation) {
        try {
            operation.run();
            show(successMessage, TRAY_NOTIFICATION);
        } catch (Exception e) {
            log.error("operation failed", e);
            errorOperation.run();
            show(e.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
    }

    /**
     * Returns all the siblings of the model.
     *
     * @param model model
     * @return siblings
     */
    protected List<MODEL> getSiblings(MODEL model) {
        return getContainer().getItemIds().stream()
                .map(o -> getKeyToModelResolver().apply((ID) o))
                .filter(m -> m != model)
                .collect(Collectors.toList());
    }

    /**
     * @return returns all the context actions this list supports.
     */
    public List<ActionModel<MODEL>> getActions() {
        val actions = new ArrayList<ActionModel<MODEL>>();
        actions.add(new ActionModel<>("Add", PLUS, false, m -> showCreateModelWindow()));
        actions.add(new ActionModel<>("View", EYE, true, this::showViewModelWindow));
        actions.add(new ActionModel<>("Edit", PENCIL, true, this::showEditModelWindow));
        actions.add(new ActionModel<>("Copy", COPY, true, this::showCopyModelWindow));
        actions.add(new ActionModel<>("Remove", TRASH, true, this::showRemoveModelDialog));
        return actions;
    }

}
