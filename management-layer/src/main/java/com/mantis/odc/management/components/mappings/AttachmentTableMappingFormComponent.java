/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings;

import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.models.AttachmentMapping;
import com.vaadin.ui.*;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

import static com.mantis.odc.management.components.utilities.BindingUtils.bind;
import static com.mantis.odc.management.components.utilities.ValidationUtils.maxLength;
import static com.vaadin.server.FontAwesome.COLUMNS;
import static com.vaadin.server.FontAwesome.DATABASE;

/**
 * Attachment table mapping from component.
 */
@Data
class AttachmentTableMappingFormComponent extends BaseCRUDFormComponent<AttachmentMapping> {

    private final TextField sourceField = new TextField() {{
        setId("source");
        setCaption("Table Source");
        setIcon(DATABASE);
        setInputPrompt("source...");
        setNullRepresentation("");
        setRequired(true);
        setRequiredError("Table source is required.");
        maxLength(this, 250);
        setDescription("Table source name.");
    }};

    private final TextField tableFieldField = new TextField() {{
        setId("tableField");
        setCaption("Table Field Name");
        setIcon(COLUMNS);
        setInputPrompt("field name...");
        setNullRepresentation("");
        setRequired(true);
        setRequiredError("Table field name is required.");
        maxLength(this, 250);
        setDescription("Name of the table field name in the attachment table.");
    }};

    private final TextField keyFieldField = new TextField() {{
        setId("keyField");
        setCaption("Key Field Name");
        setIcon(COLUMNS);
        setInputPrompt("field name...");
        setNullRepresentation("");
        setRequired(true);
        setRequiredError("Key field name is required.");
        maxLength(this, 250);
        setDescription("Name of the key field name in the attachment table.");
    }};

    private final TextField nameFieldField = new TextField() {{
        setId("nameField");
        setCaption("Name Field Name");
        setIcon(COLUMNS);
        setInputPrompt("field name...");
        setNullRepresentation("");
        setRequired(true);
        setRequiredError("Name field name is required.");
        maxLength(this, 250);
        setDescription("Name of the name field name in the attachment table.");
    }};

    private final TextField fileNameFieldField = new TextField() {{
        setId("fileNameField");
        setCaption("File Name Field Name");
        setIcon(COLUMNS);
        setInputPrompt("field name...");
        setNullRepresentation("");
        setRequired(true);
        setRequiredError("File name field name is required.");
        maxLength(this, 250);
        setDescription("Name of the file name field name in the attachment table.");
    }};

    private final TextField fileFieldField = new TextField() {{
        setId("fileField");
        setCaption("File Field Name");
        setIcon(COLUMNS);
        setInputPrompt("field name...");
        setNullRepresentation("");
        setRequired(true);
        setRequiredError("File field name is required.");
        maxLength(this, 250);
        setDescription("Name of the file field name in the attachment table.");
    }};

    private final TextField descriptionFieldField = new TextField() {{
        setId("descriptionField");
        setCaption("Description Field Name");
        setIcon(COLUMNS);
        setInputPrompt("field name...");
        setNullRepresentation("");
        setRequired(true);
        setRequiredError("Description field name is required.");
        maxLength(this, 250);
        setDescription("Name of the description field name in the attachment table.");
    }};

    // not used in this form
    private final Button saveButton = new Button();

    private final VerticalLayout layout = new VerticalLayout() {{
        setSpacing(true);
        setMargin(true);
        addComponent(new FormLayout() {{
            setSizeUndefined();
            setSpacing(true);
            addComponents(sourceField, tableFieldField, keyFieldField, nameFieldField, fileNameFieldField, fileFieldField, descriptionFieldField);
        }});
    }};

    @Override
    protected void bindToModel(AttachmentMapping model) {
        bind(sourceField, model, String.class, AttachmentMapping::getTableName, AttachmentMapping::setTableName);
        bind(tableFieldField, model, String.class, AttachmentMapping::getTableField, AttachmentMapping::setTableField);
        bind(keyFieldField, model, String.class, AttachmentMapping::getKeyField, AttachmentMapping::setKeyField);
        bind(nameFieldField, model, String.class, AttachmentMapping::getNameField, AttachmentMapping::setNameField);
        bind(fileNameFieldField, model, String.class, AttachmentMapping::getFileNameField, AttachmentMapping::setFileNameField);
        bind(fileFieldField, model, String.class, AttachmentMapping::getFileField, AttachmentMapping::setFileField);
        bind(descriptionFieldField, model, String.class, AttachmentMapping::getDescriptionField, AttachmentMapping::setDescriptionField);
    }

    @Override
    protected List<Field<?>> getFields() {
        return Arrays.asList(sourceField, tableFieldField, keyFieldField, nameFieldField, fileNameFieldField, fileFieldField, descriptionFieldField);
    }

    @Override
    public Class<? extends AttachmentMapping> getType() {
        return AttachmentMapping.class;
    }
}