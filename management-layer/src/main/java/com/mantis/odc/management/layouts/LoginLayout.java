/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.layouts;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import lombok.val;

import static com.vaadin.ui.Alignment.MIDDLE_CENTER;
import static com.vaadin.ui.themes.ValoTheme.PANEL_BORDERLESS;

/**
 * Blue background with no menu to be used with the login view.
 */
public class LoginLayout extends Panel {

    public LoginLayout(Component content) {
        setSizeFull();
        addStyleName(PANEL_BORDERLESS);
        addStyleName(MantisTheme.PANEL);

        val mantisLogo = new Image(null, new ThemeResource("logo.png")) {{
            setStyleName(MantisTheme.LOGIN_LOGO);
        }};

        val copyright = new Label(MantisTheme.COPYRIGHT);

        val layout = new VerticalLayout() {{
            setSizeFull();
            setMargin(true);
            addComponents(mantisLogo, content, copyright);
            setComponentAlignment(mantisLogo, MIDDLE_CENTER);
            setComponentAlignment(content, MIDDLE_CENTER);
            setExpandRatio(content, 4);
            setExpandRatio(copyright, (float) 0.25);
        }};

        setContent(layout);
    }
}
