/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.layouts;

import com.mantis.odc.management.support.SecureNavigator;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import lombok.val;

import static com.vaadin.ui.themes.ValoTheme.PANEL_BORDERLESS;

/**
 * Main Application layout.
 */
public class MainLayout extends Panel {

    private final MainMenu mainMenu = new MainMenu();
    private final Label footer;

    public MainLayout(Component content, boolean spacing, boolean extraTopPadding) {
        setSizeFull();
        addStyleName(PANEL_BORDERLESS);

        footer = new Label() {{
            setSizeFull();
            setStyleName(MantisTheme.FOOTER);
            setValue(MantisTheme.COPYRIGHT);
        }};

        val layout = new VerticalLayout() {{
            setSizeFull();
            setSpacing(spacing);
            val panel = new Panel() {{
                setSizeFull();
                setStyleName(PANEL_BORDERLESS);
                if (extraTopPadding) addStyleName(MantisTheme.EXTRA_TOP_PADDING);
                setContent(content);
            }};
            addComponents(mainMenu, panel, footer);
            setExpandRatio(mainMenu, (float) 0.18);
            setExpandRatio(panel, (float) 3.72);
            setExpandRatio(footer, (float) 0.10);
        }};

        setContent(layout);
    }

    public void setNavigator(SecureNavigator navigator) {
        mainMenu.setNavigator(navigator);
    }
}
