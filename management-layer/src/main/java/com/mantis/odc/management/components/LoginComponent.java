/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components;

import com.mantis.odc.management.components.base.BaseCustomComponent;
import com.vaadin.ui.*;
import lombok.Data;

import javax.inject.Inject;

import static com.mantis.odc.management.components.utilities.ValidationUtils.dependOn;
import static com.vaadin.event.ShortcutAction.KeyCode.ENTER;
import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;
import static com.vaadin.ui.themes.ValoTheme.LABEL_FAILURE;

/**
 * Login View.
 */
@Data
public class LoginComponent extends BaseCustomComponent {

    private final Label errorLabel;
    private final TextField usernameField;
    private final PasswordField passwordField;
    private final Button loginButton;
    private final FormLayout layout;

    @Inject
    public LoginComponent() {
        setSizeUndefined();

        errorLabel = new Label() {{
            setId("error");
            setVisible(false);
            setStyleName(LABEL_FAILURE);
            setValue("Invalid login attempt!");
        }};

        usernameField = new TextField() {{
            setId("username");
            setCaption("Username");
            setIcon(USER);
            setNullRepresentation("");
            setInputPrompt("username...");
            setRequired(true);
            setRequiredError("Username is required.");
            setImmediate(true);
            setDescription("Username.");
            addValueChangeListener(event -> errorLabel.setVisible(false));
        }};

        passwordField = new PasswordField() {{
            setId("password");
            setCaption("Password");
            setIcon(LOCK);
            setNullRepresentation("");
            setRequired(true);
            setRequiredError("Password is required.");
            setImmediate(true);
            setDescription("Password for the user.");
            addValueChangeListener(event -> errorLabel.setVisible(false));
        }};

        loginButton = new Button() {{
            setId("login");
            setCaption("Login");
            setStyleName(BUTTON_PRIMARY);
            setIcon(SIGN_IN);
            setClickShortcut(ENTER);
            dependOn(this, usernameField, passwordField);
        }};

        layout = new FormLayout() {{
            setSizeUndefined();
            setSpacing(true);
            setMargin(true);
            addComponents(errorLabel, usernameField, passwordField, loginButton);
        }};

        setCompositionRoot(layout);
    }
}
