/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.views;

import com.mantis.odc.management.components.dashboard.DashboardComponent;
import com.mantis.odc.management.layouts.MainLayout;
import com.mantis.odc.management.support.SecureNavigator;
import com.mantis.odc.management.views.base.ComponentView;
import com.mantis.odc.management.views.model.URLParameters;
import com.vaadin.server.FontAwesome;
import lombok.Data;
import lombok.val;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

import static com.vaadin.server.FontAwesome.DASHBOARD;

/**
 * Home dashboard view.
 */
@Data
public class HomeView extends ComponentView {

    private final DashboardComponent component;

    @Inject
    public HomeView(DashboardComponent component) {
        super(new MainLayout(component, true, true));
        setSizeFull();
        this.component = component;
    }

    @Override
    protected void enter(List<String> parts, URLParameters parameters) {
        super.enter(parts, parameters);
        component.updateData();
    }

    @Override
    public void setNavigator(SecureNavigator navigator) {
        val layout = (MainLayout) getCompositionRoot();
        layout.setNavigator(navigator);
        super.setNavigator(navigator);
    }

    @Override
    public List<String> names() {
        return Arrays.asList("", "/");
    }

    @Override
    public String title() {
        return "Dashboard";
    }

    @Override
    public FontAwesome icon() {
        return DASHBOARD;
    }

    @Override
    public String description() {
        return "Displays all statics relevant to the deployed services.";
    }

    @Override
    public boolean hasMenuItem() {
        return true;
    }

    @Override
    public int menuPosition() {
        return 1;
    }
}
