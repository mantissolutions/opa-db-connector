/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.dashboard.widgets;

import com.mantis.odc.management.components.dashboard.charts.OperationsByMappingsChart;
import com.mantis.odc.management.components.dashboard.widgets.base.AbstractWidget;
import com.vaadin.ui.Component;
import lombok.Data;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

/**
 * Stacked column chart showing operations by mappings.
 */
@Data
public class OperationsByMappingsWidget extends AbstractWidget {

    private final OperationsByMappingsChart chart;

    @Inject
    public OperationsByMappingsWidget(OperationsByMappingsChart chart) {
        this.chart = chart;
        init();
    }

    @Override
    public String title() {
        return "Operations By Mappings";
    }

    @Override
    public int minWidth() {
        return 2;
    }

    @Override
    public int maxWidth() {
        return 4;
    }

    @Override
    public int minHeight() {
        return 6;
    }

    @Override
    public int maxHeight() {
        return 6;
    }

    @Override
    public Component getComponent() {
        return chart;
    }

    @Override
    public void refreshOnSizeChange() {
    }

    @Override
    public void updateData() {
        chart.populateAndConfigureChart();
    }

    @Override
    public void configure(Map<String, String> settings) {
    }

    @Override
    public Map<String, String> config() {
        return new HashMap<>();
    }


}
