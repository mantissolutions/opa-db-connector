package com.mantis.odc.management.components.dashboard.charts;

/**
 * Nice Chart Colours.
 */
public class ChartColors {
    public static final String LIGHT_GREEN = "#3baa34";
    public static final String DARK_GREEN = "#0c8e36";
    public static final String TEAL = "#12a19b";
    public static final String DARK_BLUE = "#0f70b7";
    public static final String LIGHT_BLUE = "#2daae2";
    public static final String DARK_GREY = "#575656";
    public static final String LIGHT_GREY = "#A8A9A9";
}
