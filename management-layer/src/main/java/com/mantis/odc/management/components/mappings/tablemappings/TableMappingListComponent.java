/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings.tablemappings;

import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.management.components.base.BaseSubFormListComponent;
import com.mantis.odc.management.components.extentions.AdvancedTable;
import com.mantis.odc.management.components.extentions.BooleanField;
import com.mantis.odc.models.EnumerationMapping;
import com.mantis.odc.models.Mapping;
import com.mantis.odc.models.TableMapping;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import lombok.Data;
import lombok.val;
import org.vaadin.viritin.ListContainer;

import java.util.List;
import java.util.Optional;

import static com.vaadin.server.FontAwesome.TABLE;
import static com.vaadin.server.Sizeable.Unit.PIXELS;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;

/**
 * Table mapping list.
 */
@Data
public class TableMappingListComponent extends BaseSubFormListComponent<TableMapping, Mapping> {

    private boolean isViewMode;
    private final List<EnumerationMapping> enumerationMappings;
    private final String modelName = "Table Mapping";
    private final Mapping parentModel;
    private final List<TableMapping> items;
    private final ListContainer<TableMapping> container;
    private final Button addTableMappingButton;
    private final AdvancedTable<TableMapping, TableMapping> table;
    private final VerticalLayout layout;

    /**
     * @param parentModel   parent model
     * @param tableMappings table mappings
     * @param container     container
     */
    public TableMappingListComponent(Mapping parentModel, List<TableMapping> tableMappings, ListContainer<TableMapping> container, List<EnumerationMapping> enumerationMappings) {
        this.parentModel = parentModel;
        this.enumerationMappings = enumerationMappings;
        items = tableMappings;

        this.container = container;

        table = new AdvancedTable<TableMapping, TableMapping>() {{
            setId("tableMappingList");
            setWidth(1100, PIXELS);
            setHeight(450, PIXELS);
            setActionProvider(Optional.of(mapping -> getActions()));
            setContainerDataSource(container);
            addColumn("actions", mapping -> getActionsColumn(mapping));
            addColumn("source", mapping -> mapping.getTableId().getSource());
            addColumn("opaName", mapping -> mapping.getTableId().getOpaName());
            addColumn("parentName", mapping -> mapping.getParent() != null ? mapping.getParent().getTableId().getName() : null);
            addColumn("inferredBoolean", mapping -> new BooleanField(mapping.isInferred()));
            addColumn("oneToOneBoolean", mapping -> new BooleanField(mapping.isOneToOne()));
            setVisibleColumns("actions", "source", "opaName", "inferredBoolean", "parentName", "linkName", "oneToOneBoolean");
            setColumnHeaders("", "Source", "OPA Name", "Inferred", "Parent", "Link Name", "One To One");
            setColumnWidth("actions", 130);
        }};

        addTableMappingButton = new Button() {{
            setId("addTableMapping");
            setCaption("Add Table Mapping");
            setIcon(TABLE);
            setStyleName(BUTTON_PRIMARY);
            addClickListener((event) -> showCreateModelWindow());
        }};

        layout = new VerticalLayout() {{
            setSizeFull();
            setSpacing(true);
            setMargin(true);
            addComponents(addTableMappingButton, table);
        }};

        setCompositionRoot(layout);
    }

    @Override
    public void setEnabled(boolean enabled) {
        addTableMappingButton.setVisible(enabled);
        isViewMode = !enabled;
    }

    @Override
    public TableMapping createNewModelInstance() {
        return new TableMapping();
    }

    @Override
    public BaseCRUDFormComponent<TableMapping> getForm(TableMapping model) {
        val form = new TableMappingFormComponent(getSiblings(model), enumerationMappings);
        form.setValue(model);
        return form;
    }

    @Override
    public List<TableMapping> getItemsFromParent(Mapping mapping) {
        return mapping.getTables();
    }

    @Override
    protected void saveItemsToParent(Mapping mapping, List<TableMapping> items) {
        items.forEach(tableMapping -> tableMapping.setMapping(mapping));
        mapping.setTables(items);
    }

    @Override
    protected void removeModel(TableMapping mapping) {
        // inform the user that the table has children
        if (!mapping.getChildren().isEmpty())
            showErrorMessage("The table has children, remove the table from all child tables before removing.");
            // else inform the user that the table is linked to a junction table
        else if (!mapping.getJunctionTables().isEmpty())
            showErrorMessage("The table is linked to a junction table, remove the table from all junction tables before removing.");
            // else if the table is used by a bind mapping
        else if (!mapping.getBinds().isEmpty())
            showErrorMessage("The table is linked to a bind mapping, remove the table from all bind mappings before removing.");
        else {
            // remove the table from the parent
            if (mapping.getParentOpt().isPresent()) mapping.getParent().removeChildMapping(mapping);
            super.removeModel(mapping);
        }
    }
}
