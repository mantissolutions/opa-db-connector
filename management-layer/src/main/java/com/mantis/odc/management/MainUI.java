/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management;

import com.mantis.odc.management.di.ManagementSubComponent;
import com.mantis.odc.management.support.SecureNavigator;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import lombok.Data;
import lombok.val;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import static com.vaadin.ui.themes.ValoTheme.PANEL_BORDERLESS;
import static java.text.MessageFormat.format;
import static org.atmosphere.cpr.FrameworkConfig.SECURITY_SUBJECT;

/**
 * Main UI class.
 */
@Data
@Push()
@Theme("mantis-theme")
@Widgetset("com.mantis.odc.management.MantisWidgetset")
@DesignRoot
public class MainUI extends UI {

    public static final String MANAGEMENT_INJECTOR = "management-injector";

    /**
     * DI injector.
     */
    private ManagementSubComponent injector;

    private final Panel contentPanel = new Panel() {{
        setSizeFull();
        setStyleName(PANEL_BORDERLESS);
    }};

    @Override
    protected void init(VaadinRequest request) {
        injector = (ManagementSubComponent) VaadinServlet.getCurrent()
                .getServletContext()
                .getAttribute(MANAGEMENT_INJECTOR);

        // set root panel
        setContent(contentPanel);

        // create navigator
        val navigator = new SecureNavigator(this, injector.getViewFactory());
        setNavigator(navigator);
    }

    /**
     * @return returns the current subject, either from a http request or websocket context
     */
    public Subject getSubject() {
        val subject = (Subject) VaadinService.getCurrentRequest().getAttribute(SECURITY_SUBJECT);
        return subject == null ? SecurityUtils.getSubject() : subject;
    }

    /**
     * @param path relative path
     * @return return the absolute path
     */
    public String getRelPath(String path) {
        val location = getPage().getLocation();
        val uri = VaadinService.getCurrentRequest().getContextPath();
        return format("{0}://{1}{2}{3}{4}", location.getScheme(), location.getHost(),
                (location.getPort() > 0) ? ":" + Integer.toString(location.getPort()) : "", uri, path);
    }
}