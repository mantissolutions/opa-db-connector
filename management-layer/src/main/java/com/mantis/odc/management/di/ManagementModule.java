/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.di;

import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Management module.
 */
@Module
public class ManagementModule {

    /**
     * Shared executor for the application.
     *
     * @return executor
     */
    @Provides
    @Singleton
    public static ScheduledExecutorService getScheduledExecutorService() {
        return Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
    }
}
