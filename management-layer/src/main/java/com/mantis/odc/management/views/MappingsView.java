/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.views;

import com.mantis.odc.management.components.mappings.MappingListComponent;
import com.mantis.odc.management.layouts.MainLayout;
import com.mantis.odc.management.support.SecureNavigator;
import com.mantis.odc.management.views.base.ComponentView;
import com.mantis.odc.management.views.model.URLParameters;
import com.vaadin.server.FontAwesome;
import lombok.Data;
import lombok.val;

import javax.inject.Inject;
import java.util.List;

import static com.vaadin.server.FontAwesome.CHAIN;
import static java.util.Collections.singletonList;

/**
 * Mapping View.
 */
@Data
public class MappingsView extends ComponentView {

    private final MappingListComponent component;

    @Inject
    public MappingsView(MappingListComponent component) {
        super(new MainLayout(component, true, true));
        setSizeFull();
        this.component = component;
    }

    @Override
    protected void enter(List<String> parts, URLParameters parameters) {
        super.enter(parts, parameters);
        component.updateFromDatabase();
    }

    @Override
    public void setNavigator(SecureNavigator navigator) {
        val layout = (MainLayout) getCompositionRoot();
        layout.setNavigator(navigator);
        super.setNavigator(navigator);
    }

    @Override
    public List<String> names() {
        return singletonList("mappings");
    }

    @Override
    public String title() {
        return "Mappings";
    }

    @Override
    public FontAwesome icon() {
        return CHAIN;
    }

    @Override
    public String description() {
        return "Displays all mappings.";
    }

    @Override
    public boolean hasMenuItem() {
        return true;
    }

    @Override
    public int menuPosition() {
        return 3;
    }
}
