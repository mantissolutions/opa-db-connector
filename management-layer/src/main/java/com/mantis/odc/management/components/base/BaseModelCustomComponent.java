/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.base;

import com.vaadin.data.Validator;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Field;

import java.util.List;

/**
 * Base Model component.
 *
 * @param <MODEL> model type
 */
public abstract class BaseModelCustomComponent<MODEL> extends CustomField<MODEL> {

    /**
     * Binds the model to the fields in the component.
     *
     * @param model model
     */
    protected abstract void bindToModel(MODEL model);

    @Override
    protected void setInternalValue(MODEL newValue) {
        if (newValue != null) bindToModel(newValue);
        super.setInternalValue(newValue);
    }

    /**
     * @return list of fields that are to set to read-only
     */
    protected abstract List<Field<?>> getFields();

    @Override
    public void setReadOnly(boolean readOnly) {
        getFields().forEach(field -> field.setReadOnly(readOnly));
    }

    @Override
    public void setEnabled(boolean enabled) {
        getFields().forEach(field -> field.setEnabled(enabled));
    }

    @Override
    public boolean isModified() {
        return getFields().stream().anyMatch(Field::isModified);
    }

    @Override
    public boolean isValid() {
        return getFields().stream().allMatch(Field::isValid);
    }

    @Override
    public void commit() throws SourceException, Validator.InvalidValueException {
        getFields().forEach(Field::commit);
        super.commit();
    }

    @Override
    protected Component initContent() {
        // fire value change on form if fields fire one
        getFields().forEach(field -> field.addValueChangeListener(event -> fireValueChange(false)));
        return getLayout();
    }

    /**
     * @return field layout.
     */
    protected abstract Component getLayout();
}
