/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.views;

import com.mantis.odc.management.MainUI;
import com.mantis.odc.management.components.LoginComponent;
import com.mantis.odc.management.layouts.LoginLayout;
import com.mantis.odc.management.views.base.ComponentView;
import com.mantis.odc.management.views.model.URLParameters;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.UI;
import lombok.Data;
import lombok.val;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

import static com.mantis.odc.management.support.SecureNavigator.*;
import static com.vaadin.server.FontAwesome.SIGN_IN;

/**
 * Login View.
 */
@Data
public class LoginView extends ComponentView {

    public static final String REDIRECT_VIEW = "redirectView";

    private final LoginComponent component;

    @Inject
    public LoginView(LoginComponent loginComponent) {
        super(new LoginLayout(loginComponent));
        setSizeFull();
        this.component = loginComponent;
    }

    @Override
    protected void enter(List<String> parts, URLParameters parameters) {
        // on login check credentials, if they match redirect
        // redirect user to requested page, otherwise show errors to the user.
        component.getLoginButton().addClickListener(event -> {
            Subject subject = ((MainUI) UI.getCurrent()).getSubject();
            try {
                subject.login(createToken());
                navigator.navigateTo(parameters.getWithDefault(REDIRECT_VIEW, DEFAULT_VIEW));
            } catch (Exception e) {
                component.getErrorLabel().setVisible(true);
            }
        });
    }

    /**
     * @return login token
     */
    private UsernamePasswordToken createToken() {
        val username = component.getUsernameField().getValue();
        val password = component.getPasswordField().getValue();
        return new UsernamePasswordToken(username, password);
    }

    @Override
    public List<String> names() {
        return Arrays.asList(LOGIN, LOGOUT);
    }

    @Override
    public String title() {
        return "Login";
    }

    @Override
    public FontAwesome icon() {
        return SIGN_IN;
    }

    @Override
    public String description() {
        return "Login";
    }

    @Override
    public boolean hasMenuItem() {
        return false;
    }

    @Override
    public int menuPosition() {
        return 0;
    }
}
