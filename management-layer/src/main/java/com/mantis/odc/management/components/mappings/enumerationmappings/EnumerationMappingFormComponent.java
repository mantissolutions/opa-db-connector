/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings.enumerationmappings;

import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.management.components.extentions.EnumeratedComboBox;
import com.mantis.odc.models.EnumerationMapping;
import com.mantis.odc.models.EnumerationType;
import com.vaadin.ui.*;
import lombok.Getter;
import lombok.val;
import org.vaadin.aceeditor.AceEditor;

import java.util.Arrays;
import java.util.List;

import static com.mantis.odc.management.components.utilities.BindingUtils.bind;
import static com.mantis.odc.management.components.utilities.ValidationUtils.*;
import static com.vaadin.server.FontAwesome.SAVE;
import static com.vaadin.server.Sizeable.Unit.PIXELS;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;
import static org.vaadin.aceeditor.AceMode.sql;

/**
 * Enumeration Mapping form.
 */
@Getter
class EnumerationMappingFormComponent extends BaseCRUDFormComponent<EnumerationMapping> {

    private final TextField nameField;
    private final ComboBox typeField;
    private final EnumeratedComboBox childField;
    private final TextArea descriptionField;
    private final AceEditor sqlField;
    private final Button saveButton;
    private final VerticalLayout layout;

    EnumerationMappingFormComponent(List<EnumerationMapping> enumerationMappings) {

        nameField = new TextField() {{
            setId("name");
            setCaption("Enumeration Name");
            setInputPrompt("name...");
            setNullRepresentation("");
            setRequired(true);
            setRequiredError("Enumeration name is required.");
            maxLength(this, 250);
            setDescription("Name of the enumeration.");
            unique(this, "Enumeration name must be unique.", () -> enumerationMappings, EnumerationMapping::getName);
        }};

        typeField = new ComboBox() {{
            setId("type");
            setCaption("Enumeration Type");
            setNullSelectionAllowed(false);
            setRequired(true);
            setRequiredError("Enumeration type is required.");
            for (val enumType : EnumerationType.values()) {
                addItem(enumType);
                setItemCaption(enumType, enumType.getDisplay());
            }
            setDescription("Enumeration type. This specifies the types in the enumeration.");
        }};

        childField = new EnumeratedComboBox() {{
            setId("child");
            setCaption("Child Enumeration");
            setDescription("Child enumeration to return child values from.");
            for (val em : enumerationMappings) {
                addItem(em);
                setItemCaption(em, em.getName());
            }
        }};

        descriptionField = new TextArea() {{
            setId("description");
            setCaption("Description");
            setDescription("Enumeration description.");
            setNullRepresentation("");
            maxLength(this, 500);
        }};

        sqlField = new AceEditor() {{
            setId("sql");
            setCaption("SQL Statement");
            setMode(sql);
            setWordWrap(true);
            setRequired(true);
            setRequiredError("SQL statement is required.");
            setWidth(500, PIXELS);
            maxLength(this, 250);
            setDescription("SQL statement to return the enumeration data.");
        }};

        saveButton = new Button() {{
            setId("saveButton");
            setCaption("Save Enumeration Mapping");
            setIcon(SAVE);
            setStyleName(BUTTON_PRIMARY);
            dependOn(this, nameField, typeField, descriptionField, sqlField);
        }};

        layout = new VerticalLayout() {{
            setSpacing(true);
            setMargin(true);
            addComponent(new FormLayout() {{
                setSizeUndefined();
                setSpacing(true);
                addComponents(nameField, typeField, childField, descriptionField, sqlField);
            }});
            addComponent(new HorizontalLayout() {{
                setSizeUndefined();
                setSpacing(true);
                addComponents(saveButton);
            }});
        }};
    }

    @Override
    protected void bindToModel(EnumerationMapping model) {
        bind(nameField, model, String.class, EnumerationMapping::getName, EnumerationMapping::setName);
        bind(typeField, model, EnumerationType.class, EnumerationMapping::getType, EnumerationMapping::setType);
        bind(childField, model, EnumerationMapping.class, EnumerationMapping::getChild, EnumerationMapping::setChild);
        bind(descriptionField, model, String.class, EnumerationMapping::getDescription, EnumerationMapping::setDescription);
        bind(sqlField, model, String.class, EnumerationMapping::getSql, EnumerationMapping::setSql);
    }

    @Override
    protected List<Field<?>> getFields() {
        return Arrays.asList(nameField, typeField, childField, descriptionField, sqlField);
    }

    @Override
    public Class<? extends EnumerationMapping> getType() {
        return EnumerationMapping.class;
    }
}
