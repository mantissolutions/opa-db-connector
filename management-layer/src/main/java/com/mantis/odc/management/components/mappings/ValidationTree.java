/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings;

import com.mantis.odc.services.validators.ValidationResult;
import com.vaadin.ui.Tree;
import lombok.Data;

import static com.vaadin.server.FontAwesome.*;

/**
 * Displays validations in a tree.
 */
@Data
public class ValidationTree extends Tree {

    /**
     * Show only failed.
     */
    private boolean showOnlyFailed = true;

    /**
     * Validation result.
     */
    private final ValidationResult result;

    /**
     * @param result result to view
     */
    public ValidationTree(ValidationResult result) {
        this.result = result;
        setSelectable(false);
        addResult(result, null);
    }

    /**
     * @param showOnlyFailed show only failed flag
     */
    public void setShowOnlyFailed(boolean showOnlyFailed) {
        this.showOnlyFailed = showOnlyFailed;
        removeAllItems();
        addResult(result, null);
    }

    /**
     * Adds a result to the tree.
     *
     * @param result   result to add
     * @param parentId parent id
     */
    private void addResult(ValidationResult result, Object parentId) {
        // don't show successful validations
        if (showOnlyFailed && result.isSuccess()) return;

        // add validation
        Object id = addItem();
        setItemCaption(id, result.getMessage());
        setItemIcon(id, (result.isSuccess()) ? (result.isWarning() ? WARNING : CHECK) : EXCLAMATION);
        expandItem(id);
        if (parentId != null) setParent(id, parentId);
        result.getChildren().forEach(child -> addResult(child, id));
    }
}
