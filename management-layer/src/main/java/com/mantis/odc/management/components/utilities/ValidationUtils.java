/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.utilities;

import com.vaadin.data.Validator;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Button;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.text.MessageFormat.format;

/**
 * Utilities for validating Vaadin fields.
 */
public class ValidationUtils {

    /**
     * Simple non null validation.
     *
     * @param field field
     */
    public static void notNull(AbstractField field) {
        addValidation(field, "This value is required.", Objects::nonNull);
    }

    /**
     * Checks the size of the number.
     *
     * @param field   field to validate
     * @param message message to return
     * @param test    test
     */
    public static void size(AbstractField field, String message, Function<Integer, Boolean> test) {
        addValidation(field, message, o -> {
            if (o == null) return true;
            else if (o instanceof Number) return test.apply(((Number) o).intValue());
            else if (o instanceof String) return test.apply(Integer.valueOf((String) o));
            else throw new UnsupportedOperationException(format("Cannot validate size of type {0}",
                        o.getClass().getName()));
        });
    }

    /**
     * Adds a maxLength validation to the field.
     *
     * @param field field to validate
     * @param max   maxLength amount
     */
    public static void maxSize(AbstractField field, int max) {
        length(field, format("Field must be no bigger than {0} size", max), i -> i <= max);
    }

    /**
     * Adds a minLength validation to the field.
     *
     * @param field field to validate.
     * @param min   minLength amount
     */
    public static void minSize(AbstractField field, int min) {
        length(field, format("Field must be no smaller than {0} size", min), i -> i >= min);
    }

    /**
     * Tests a fields length.
     *
     * @param field   field to validate
     * @param message message to return
     * @param test    test
     */
    public static void length(AbstractField field, String message, Function<Integer, Boolean> test) {
        addValidation(field, message, o -> {
            if (o == null) return true;
            else if (o instanceof Number) return test.apply(((Number) o).intValue());
            else if (o instanceof String) return test.apply(((String) o).length());
            else if (o instanceof Collection) return test.apply(((Collection) o).size());
            else throw new UnsupportedOperationException(format("Cannot validate length of type {0}",
                        o.getClass().getName()));
        });
    }

    /**
     * Adds a maxLength validation to the field.
     *
     * @param field field to validate
     * @param max   maxLength amount
     */
    public static void maxLength(AbstractField field, int max) {
        length(field, format("Field must be no longer than {0} length", max), i -> i <= max);
    }

    /**
     * Adds a minLength validation to the field.
     *
     * @param field field to validate.
     * @param min   minLength amount
     */
    public static void minLength(AbstractField field, int min) {
        length(field, format("Field must be no shorter than {0} length", min), i -> i >= min);
    }

    /**
     * Adds a between validation to the field.
     *
     * @param field field to validate.
     * @param min   minLength amount
     * @param max   maxLength amount
     */
    public static void between(AbstractField field, int min, int max) {
        minLength(field, min);
        maxLength(field, max);
    }

    /**
     * Tests for uniqueness within a set domain. This is against string based fields.
     *
     * @param field          field to validate
     * @param errorMessage   error message
     * @param existingValues domain to test
     * @param getter         returns the string to test
     * @param <T>            value type
     */
    public static <T> void unique(AbstractField field,
                                  String errorMessage,
                                  Supplier<List<T>> existingValues,
                                  Function<T, String> getter) {
        addValidation(field, errorMessage, v -> existingValues.get().stream()
                .map(getter)
                .noneMatch(ev -> ev != null && ev.equals(v)));
    }

    /**
     * Adds a validation to the field. This is a real time operation and on commit.
     *
     * @param field        field to validate
     * @param errorMessage message to display on validation error.
     * @param test         validation test to perform.
     * @param <T>          value type
     */
    public static <T> void addValidation(AbstractField field, String errorMessage, Function<T, Boolean> test) {
        field.addValidator(value -> {
            if (!test.apply((T) value)) throw new Validator.InvalidValueException(errorMessage);
        });
    }

    /**
     * Adds a dependency on the given validatable fields.
     *
     * @param button button to disable or enable
     * @param fields fields to depend on
     */
    public static void dependOn(Button button, AbstractField<?>... fields) {
        button.setEnabled(false);
        Arrays.stream(fields).forEach(field -> field.addValueChangeListener(event ->
                button.setEnabled(Arrays.stream(fields).allMatch(AbstractField::isValid))));
    }

    /**
     * Adds a dependency on the given validatable fields.
     *
     * @param button button to disable or enable
     * @param filter to apply to exclude field based on changes to data
     * @param fields fields to depend on
     */
    public static void dependOnWithFilter(Button button, Predicate<AbstractField> filter, AbstractField<?>... fields) {
        button.setEnabled(false);
        Arrays.stream(fields).forEach(field -> field.addValueChangeListener(event ->
                button.setEnabled(Arrays.stream(fields).filter(filter).allMatch(AbstractField::isValid))));
    }
}
