/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.datasources;

import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.models.DatasourceMapping;
import com.mantis.odc.models.Driver;
import com.vaadin.ui.*;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

import static com.mantis.odc.management.components.utilities.BindingUtils.bind;
import static com.mantis.odc.management.components.utilities.ValidationUtils.*;
import static com.vaadin.event.ShortcutAction.KeyCode.ENTER;
import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.server.Sizeable.Unit.PIXELS;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;

/**
 * Data source mapping form.
 */
@Data
public class DatasourceFormComponent extends BaseCRUDFormComponent<DatasourceMapping> {

    private final TextField nameField;
    private final DriverSelectField driverField;
    private final TextField urlField;
    private final TextField connectionTimeoutMsField;
    private final TextField idleTimeoutMsField;
    private final TextField maxLifetimeMsField;
    private final TextField maxPoolSizeField;
    private final TextField minIdleField;
    private final TextField leakDetectionThresholdField;
    private final Button saveButton;
    private final VerticalLayout layout;

    /**
     * @param siblings sibling data source mappings.
     */
    protected DatasourceFormComponent(List<DatasourceMapping> siblings) {

        nameField = new TextField() {{
            setId("name");
            setCaption("Name");
            setInputPrompt("name...");
            setNullRepresentation("");
            setWidth(500, PIXELS);
            setRequired(true);
            setRequiredError("Datasource name is required.");
            setDescription("Unique name of this data source.");
            maxLength(this, 250);
            unique(this, "The Datasource name must be unique.", () -> siblings, DatasourceMapping::getName);
        }};

        driverField = new DriverSelectField() {{
            setId("Driver");
            setCaption("Database Driver");
            setIcon(DATABASE);
            setInputPrompt("driver...");
            setNullSelectionAllowed(false);
            setWidth(500, PIXELS);
            setRequired(true);
            setRequiredError("Database driver type is required.");
            setDescription("Driver for connecting to the database.");
        }};

        urlField = new TextField() {{
            setId("url");
            setCaption("JDBC URL");
            setInputPrompt("url...");
            setNullRepresentation("");
            setWidth(500, PIXELS);
            setRequired(true);
            setRequiredError("JDBC URL is required.");
            setDescription("JDBC connection string. For Example jdbc:mysql://somehost:4487/somedatabase");
            maxLength(this, 250);
        }};

        connectionTimeoutMsField = new TextField() {{
            setId("connectionTimeoutMs");
            setCaption("Connection Timeout");
            setNullRepresentation("");
            setConversionError("A valid number is required.");
            setDescription("This property controls the maximum number of milliseconds that a client (that's you) " +
                    "will wait for a connection from the pool. If this time is exceeded without a connection becoming " +
                    "available, a SQLException will be thrown. 1000ms is the minimum value. Default: 30000 (30 seconds)");
            minSize(this, 1000);
        }};

        idleTimeoutMsField = new TextField() {{
            setId("idleTimeoutMs");
            setCaption("Idle Timeout");
            setNullRepresentation("");
            setConversionError("A valid number is required.");
            setDescription("This property controls the maximum amount of time that a connection is allowed to sit idle " +
                    "in the pool. Whether a connection is retired as idle or not is subject to a maximum variation " +
                    "of +30 seconds, and average variation of +15 seconds. A connection will never be retired as idle " +
                    "before this timeout. A value of 0 means that idle connections are never removed from the pool. " +
                    "Default: 600000 (10 minutes)");
            minSize(this, 0);
        }};

        maxLifetimeMsField = new TextField() {{
            setId("maxLifetimeMs");
            setCaption("Max Lifetime");
            setNullRepresentation("");
            setConversionError("A valid number is required.");
            setDescription("This property controls the maximum lifetime of a connection in the pool. When a connection " +
                    "reaches this timeout it will be retired from the pool, subject to a maximum variation of +30 seconds. " +
                    "An in-use connection will never be retired, only when it is closed will it then be removed. " +
                    "We strongly recommend setting this value, and it should be at least 30 seconds less than any " +
                    "database-level connection timeout. A value of 0 indicates no maximum lifetime (infinite lifetime), " +
                    "subject of course to the idleTimeout setting. Default: 1800000 (30 minutes)");
            minSize(this, 0);
        }};

        maxPoolSizeField = new TextField() {{
            setId("maxPoolSize");
            setCaption("Maximum PoolSize");
            setNullRepresentation("");
            setConversionError("A valid number is required.");
            setDescription("This property controls the maximum size that the pool is allowed to reach, including both " +
                    "idle and in-use connections. Basically this value will determine the maximum number of actual " +
                    "connections to the database backend. A reasonable value for this is best determined by your execution " +
                    "environment. When the pool reaches this size, and no idle connections are available, calls " +
                    "to getConnection() will block for up to connectionTimeout milliseconds before timing out. " +
                    "Default: 10");
            minSize(this, 1);
        }};

        minIdleField = new TextField() {{
            setId("minIdle");
            setCaption("Minimum Idle");
            setNullRepresentation("");
            setConversionError("A valid number is required.");
            setDescription("This property controls the minimum number of idle connections that the pool tries to maintain" +
                    " in the pool. If the idle connections dip below this value, the pool will make a best effort to " +
                    "add additional connections quickly and efficiently. However, for maximum performance and " +
                    "responsiveness to spike demands, we recommend not setting this value and instead allowing the pool " +
                    "to act as a fixed size connection pool. Default: same as maximumPoolSize");
            minSize(this, 1);
        }};

        leakDetectionThresholdField = new TextField() {{
            setId("leakDetectionThreshold");
            setCaption("Leak Detection Threshold");
            setNullRepresentation("");
            setConversionError("A valid number is required.");
            setDescription("This property controls the amount of time that a connection can be out of the pool " +
                    "before a message is logged indicating a possible connection leak.  A value of 0 means leak detection " +
                    "is disabled. Lowest acceptable value for enabling leak detection is 2000 (2 secs). Default: 0");
            minSize(this, 0);
        }};

        saveButton = new Button() {{
            setId("saveButton");
            setCaption("Save Datasource");
            setIcon(SAVE);
            setStyleName(BUTTON_PRIMARY);
            setClickShortcut(ENTER);
            dependOn(this, nameField, driverField, urlField, connectionTimeoutMsField, idleTimeoutMsField, maxLifetimeMsField, maxPoolSizeField, minIdleField, leakDetectionThresholdField);
        }};

        layout = new VerticalLayout() {{
            setSizeUndefined();
            setSpacing(true);
            setMargin(true);
            addComponent(new TabSheet() {{
                setId("tabs");
                setSizeUndefined();
                addSelectedTabChangeListener(event -> {
                    // if form in a window, recenter the window on tab change
                    Component parent = DatasourceFormComponent.this.getParent();
                    if (parent instanceof Window) ((Window) parent).center();
                });
                addTab(new FormLayout() {{
                    setSizeUndefined();
                    setSpacing(true);
                    addComponents(nameField, driverField, urlField);
                }}, "Datasource Detail", ASTERISK);
                addTab(new FormLayout() {{
                    setSizeUndefined();
                    setSpacing(true);
                    addComponents(connectionTimeoutMsField, idleTimeoutMsField, maxLifetimeMsField, maxPoolSizeField, minIdleField, leakDetectionThresholdField);
                }}, "Connection Pool Settings", COG);
            }});
            addComponent(new HorizontalLayout() {{
                setSizeUndefined();
                setSpacing(true);
                addComponents(saveButton);
            }});
        }};
    }

    @Override
    protected void bindToModel(DatasourceMapping model) {
        bind(nameField, model, String.class, DatasourceMapping::getName, DatasourceMapping::setName);
        bind(driverField, model, Driver.class, DatasourceMapping::getDriver, DatasourceMapping::setDriver);
        bind(urlField, model, String.class, DatasourceMapping::getJdbc, DatasourceMapping::setJdbc);
        bind(connectionTimeoutMsField, model, Long.class, DatasourceMapping::getConnectionTimeoutMs, DatasourceMapping::setConnectionTimeoutMs);
        bind(idleTimeoutMsField, model, Long.class, DatasourceMapping::getIdleTimeoutMs, DatasourceMapping::setIdleTimeoutMs);
        bind(maxLifetimeMsField, model, Long.class, DatasourceMapping::getMaxLifetimeMs, DatasourceMapping::setMaxLifetimeMs);
        bind(maxPoolSizeField, model, Integer.class, DatasourceMapping::getMaxPoolSize, DatasourceMapping::setMaxPoolSize);
        bind(minIdleField, model, Integer.class, DatasourceMapping::getMinIdle, DatasourceMapping::setMinIdle);
        bind(leakDetectionThresholdField, model, Long.class, DatasourceMapping::getLeakDetectionThreshold, DatasourceMapping::setLeakDetectionThreshold);
    }

    @Override
    public Class<? extends DatasourceMapping> getType() {
        return DatasourceMapping.class;
    }

    @Override
    protected List<Field<?>> getFields() {
        return Arrays.asList(nameField, driverField, urlField, connectionTimeoutMsField, idleTimeoutMsField, maxLifetimeMsField, maxPoolSizeField, minIdleField, leakDetectionThresholdField);
    }
}
