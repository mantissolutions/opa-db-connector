/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.di;

import com.mantis.odc.services.di.ServiceModule;
import com.mantis.odc.services.di.ServiceSubComponent;
import dagger.Subcomponent;

import javax.inject.Singleton;

/**
 * Management sub component.
 */
@Singleton
@Subcomponent(modules = {ServiceModule.class, ManagementModule.class})
public interface ManagementSubComponent {

    // SUB-COMPONENTS

    ServiceSubComponent getServiceSubComponent();

    /**
     * @return application view factory
     */
    ViewFactory getViewFactory();
}
