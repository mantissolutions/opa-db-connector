/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings.junctiontablemappings;

import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.management.components.extentions.EnumeratedComboBox;
import com.mantis.odc.models.JunctionTableMapping;
import com.mantis.odc.models.LinkType;
import com.mantis.odc.models.TableMapping;
import com.vaadin.ui.*;
import lombok.Getter;
import lombok.val;

import java.util.Arrays;
import java.util.List;

import static com.mantis.odc.management.components.utilities.BindingUtils.bind;
import static com.mantis.odc.management.components.utilities.ValidationUtils.*;
import static com.vaadin.event.ShortcutAction.KeyCode.ENTER;
import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;

/**
 * Table Mapping form.
 */
@Getter
class JunctionTableMappingFormComponent extends BaseCRUDFormComponent<JunctionTableMapping> {

    private final TextField sourceField;
    private final TextField linkNameField;
    private final ComboBox linkTypeField;
    private final TextField fromKeyNameField;
    private final EnumeratedComboBox fromTableField;
    private final TextField toKeyNameField;
    private final EnumeratedComboBox toTableField;
    private final Button saveButton;
    private final VerticalLayout layout;

    JunctionTableMappingFormComponent(List<TableMapping> tableMappings, List<JunctionTableMapping> siblings) {

        sourceField = new TextField() {{
            setId("source");
            setCaption("Table Source");
            setIcon(DATABASE);
            setInputPrompt("source...");
            setNullRepresentation("");
            setRequired(true);
            setRequiredError("Table source is required.");
            maxLength(this, 250);
            setDescription("Table source name.");
            unique(this, "The table source name must be unique.", () -> siblings, t -> t.getTableId().getSource());
        }};

        linkNameField = new TextField() {{
            setId("linkName");
            setCaption("Link Name");
            setIcon(LINK);
            setNullRepresentation("");
            setDescription("Name of the link between the child table and its parent table.");
            maxLength(this, 250);
        }};

        linkTypeField = new ComboBox() {{
            setId("linkType");
            setCaption("Link Type");
            setIcon(LINK);
            setNullSelectionAllowed(false);
            setRequired(true);
            setRequiredError("A link type is required.");
            setDescription("Type of relationship in OPA.");
            for (val type : LinkType.values()) {
                addItem(type);
                setItemCaption(type, type.getDisplay());
            }
        }};

        fromKeyNameField = new TextField() {{
            setId("fromKeyName");
            setCaption("From Field Source");
            setIcon(COLUMNS);
            setInputPrompt("field source...");
            setNullRepresentation("");
            setRequired(true);
            setRequiredError("A From key source is required.");
            maxLength(this, 250);
            setDescription("From key source name. The key on one side of the junction table.");
        }};

        fromTableField = new EnumeratedComboBox() {{
            setId("fromTable");
            setCaption("From Table");
            setRequired(true);
            setRequiredError("A From table is required.");
            for (val tm : tableMappings) {
                addItem(tm);
                setItemCaption(tm, tm.getTableId().getName());
            }
        }};

        toKeyNameField = new TextField() {{
            setId("toKeyName");
            setCaption("To Field Source");
            setIcon(COLUMNS);
            setInputPrompt("field source...");
            setNullRepresentation("");
            setRequired(true);
            setRequiredError("A To key source is required.");
            maxLength(this, 250);
            setDescription("To key source name. The opposite key to the from key.");
            addValidation(this, "The key names must be unique within the junction table.", v -> v == null || !v.equals(fromKeyNameField.getValue()));
            addValidation(fromKeyNameField, "The key names must be unique within the junction table.", v -> v == null || !v.equals(this.getValue()));
        }};

        toTableField = new EnumeratedComboBox() {{
            setId("toTable");
            setCaption("To Table");
            setRequired(true);
            setRequiredError("A To table is required.");
            for (val tm : tableMappings) {
                addItem(tm);
                setItemCaption(tm, tm.getTableId().getName());
            }
        }};

        saveButton = new Button() {{
            setId("saveButton");
            setCaption("Save Table Mapping");
            setIcon(SAVE);
            setStyleName(BUTTON_PRIMARY);
            setClickShortcut(ENTER);
            dependOn(this, sourceField, linkNameField, linkTypeField, fromKeyNameField, fromTableField, toKeyNameField, toTableField);
        }};

        layout = new VerticalLayout() {{
            setSpacing(true);
            setMargin(true);
            addComponent(new FormLayout() {{
                setSizeUndefined();
                setSpacing(true);
                addComponents(sourceField, linkNameField, linkTypeField, fromKeyNameField, fromTableField, toKeyNameField, toTableField);
            }});
            addComponent(new HorizontalLayout() {{
                setSizeUndefined();
                setSpacing(true);
                addComponents(saveButton);
            }});
        }};
    }

    /**
     * Returns the current key names.
     *
     * @return key names
     */
    private List<String> getKeyNames() {
        return Arrays.asList(toKeyNameField.getValue(), fromKeyNameField.getValue());
    }

    @Override
    protected void bindToModel(JunctionTableMapping model) {
        bind(sourceField, model, String.class, t -> t.getTableId().getSource(), (t, s) -> t.getTableId().setSource(s));
        bind(linkNameField, model, String.class, JunctionTableMapping::getLinkNameNoDefault, JunctionTableMapping::setLinkName);
        bind(linkTypeField, model, LinkType.class, JunctionTableMapping::getLinkType, JunctionTableMapping::setLinkType);
        bind(fromKeyNameField, model, String.class, JunctionTableMapping::getFromKeyName, JunctionTableMapping::setFromKeyName);
        bind(fromTableField, model, TableMapping.class, JunctionTableMapping::getFromTable, JunctionTableMapping::setFromTable);
        bind(toKeyNameField, model, String.class, JunctionTableMapping::getToKeyName, JunctionTableMapping::setToKeyName);
        bind(toTableField, model, TableMapping.class, JunctionTableMapping::getToTable, JunctionTableMapping::setToTable);
    }

    @Override
    protected List<Field<?>> getFields() {
        return Arrays.asList(sourceField, linkNameField, linkTypeField, fromKeyNameField, fromTableField, toKeyNameField, toTableField);
    }

    @Override
    public Class<? extends JunctionTableMapping> getType() {
        return JunctionTableMapping.class;
    }
}
