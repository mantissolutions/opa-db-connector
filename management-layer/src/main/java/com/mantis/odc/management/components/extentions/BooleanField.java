/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.extentions;

import com.vaadin.ui.Label;

import static com.vaadin.server.FontAwesome.CHECK;
import static com.vaadin.server.FontAwesome.TIMES;
import static com.vaadin.shared.ui.label.ContentMode.HTML;

/**
 * Friendly boolean label.
 */
public class BooleanField extends Label {
    public BooleanField(boolean bool) {
        setContentMode(HTML);
        setValue(bool ? CHECK.getHtml() : TIMES.getHtml());
    }
}
