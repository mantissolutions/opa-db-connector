/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings;

import com.mantis.odc.management.components.datasources.DatasourceConnectionDialog;
import com.mantis.odc.models.Mapping;
import com.mantis.odc.services.validators.MappingValidator;
import com.mantis.odc.services.validators.MetaDataValidator;
import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.val;

/**
 * Validates a mapping against a schema.
 */
public class SchemaValidationDialog {

    @SneakyThrows
    public SchemaValidationDialog(Mapping mapping) {
        val mappingValidator = new MappingValidator();
        val mappingResult = mappingValidator.validate(mapping);

        if (mappingResult.isSuccess()) {
            // get an active connection
            val dialog = new DatasourceConnectionDialog(mapping.getDatasource());
            dialog.addActiveConnectionListener((event) -> openDialog(event, mapping));
        } else new MappingValidationDialog(mappingResult);
    }

    /**
     * @param event   active connection event
     * @param mapping mapping
     */
    @SneakyThrows
    private void openDialog(DatasourceConnectionDialog.ActiveConnectionEvent event, Mapping mapping) {
        @Cleanup val connection = event.getConnection();
        val metaData = connection.getMetaData();
        val validator = new MetaDataValidator(mapping.getDatasource().getDriver(), metaData, new MappingValidator());
        val result = validator.validate(mapping);
        new MappingValidationDialog(result);
    }


}
