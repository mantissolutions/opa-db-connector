/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.extentions;

import com.vaadin.ui.ComboBox;

/**
 * Combo box which can only have values which exist in its items.
 */
public class EnumeratedComboBox extends ComboBox {

    @Override
    public void setValue(Object newValue) throws ReadOnlyException {
        if (getItemIds().contains(newValue)) super.setValue(newValue);
    }
}
