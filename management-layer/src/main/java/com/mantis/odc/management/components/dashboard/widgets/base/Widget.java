/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.dashboard.widgets.base;

import com.vaadin.ui.Component;

import java.util.Map;

/**
 * Widget interface.
 */
public interface Widget {

    /**
     * @return widget id
     */
    String id();

    /**
     * @return title of the widget
     */
    String title();

    /**
     * @return min width in columns
     */
    int minWidth();

    /**
     * @return max width in columns
     */
    int maxWidth();

    /**
     * @return min height in rows
     */
    int minHeight();

    /**
     * @return max height in rows
     */
    int maxHeight();

    /**
     * @return widget body
     */
    Component getComponent();

    /**
     * Called on a size change. This handle can be used to
     * resize a chart or other explicitly sized components.
     */
    void refreshOnSizeChange();

    /**
     * Updates the widget data.
     */
    void updateData();

    /**
     * Configures the widget.
     *
     * @param settings settings to configure
     */
    void configure(Map<String, String> settings);

    /**
     * @return widget config
     */
    Map<String, String> config();
}
