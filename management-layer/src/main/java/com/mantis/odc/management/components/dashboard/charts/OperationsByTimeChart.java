/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.dashboard.charts;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.config.LineChartConfig;
import com.byteowls.vaadin.chartjs.data.LineDataset;
import com.byteowls.vaadin.chartjs.options.InteractionMode;
import com.byteowls.vaadin.chartjs.options.Position;
import com.byteowls.vaadin.chartjs.options.scale.Axis;
import com.byteowls.vaadin.chartjs.options.scale.CategoryScale;
import com.byteowls.vaadin.chartjs.options.scale.LinearScale;
import com.mantis.odc.config.database.DatabaseType;
import com.mantis.odc.database.AppDB;
import lombok.Cleanup;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.val;

import javax.inject.Inject;
import java.util.ArrayList;

import static com.mantis.odc.management.components.dashboard.charts.ChartTimeInterval.SixMonths;
import static com.mantis.odc.management.components.dashboard.charts.OperationTypes.*;


/**
 * Line chart showing operations by time.
 */
public class OperationsByTimeChart extends ChartJs {

    private final AppDB db;
    @Getter
    private ChartTimeInterval interval;

    @Inject
    public OperationsByTimeChart(AppDB db) {
        this.db = db;
        setSizeFull();
        populateAndConfigureChart();
    }

    /**
     * @param interval interval to set
     */
    public void setInterval(ChartTimeInterval interval) {
        this.interval = interval;
        populateAndConfigureChart();
    }

    /**
     * Returns the SQL for the chart.
     *
     * @return sql
     */
    private String getSql(DatabaseType type) {
        switch (type) {
            case MySQL:
                return "SELECT DATE_FORMAT(op_date, '%Y-%m-%d') AS DateFormatted,\n" +
                        "SUM(CASE WHEN op_type = 0 THEN 1 ELSE 0 END) AS CheckAliveCount,\n" +
                        "SUM(CASE WHEN op_type = 1 THEN 1 ELSE 0 END) AS GetMetadataCount,\n" +
                        "SUM(CASE WHEN op_type = 2 THEN 1 ELSE 0 END) AS LoadCount,\n" +
                        "SUM(CASE WHEN op_type = 3 THEN 1 ELSE 0 END) AS SaveCount,\n" +
                        "SUM(CASE WHEN op_type = 4 THEN 1 ELSE 0 END) AS GetCheckpoint,\n" +
                        "SUM(CASE WHEN op_type = 5 THEN 1 ELSE 0 END) AS SetCheckpoint,\n" +
                        "SUM(CASE WHEN op_type = 6 THEN 1 ELSE 0 END) AS ExecuteQuery,\n" +
                        "COUNT(op_id) AS TotalCount \n" +
                        "FROM odc_operations \n" +
                        "WHERE op_date > ? \n" +
                        "GROUP BY DATE_FORMAT(op_date, '%Y-%m-%d') \n" +
                        "ORDER BY 1 ASC";
            case Oracle:
                return "SELECT TO_CHAR(op_date, 'YYYY-MM-DD') AS DateFormatted,\n" +
                        "SUM(CASE WHEN op_type = 0 THEN 1 ELSE 0 END) AS CheckAliveCount,\n" +
                        "SUM(CASE WHEN op_type = 1 THEN 1 ELSE 0 END) AS GetMetadataCount,\n" +
                        "SUM(CASE WHEN op_type = 2 THEN 1 ELSE 0 END) AS LoadCount,\n" +
                        "SUM(CASE WHEN op_type = 3 THEN 1 ELSE 0 END) AS SaveCount,\n" +
                        "SUM(CASE WHEN op_type = 4 THEN 1 ELSE 0 END) AS GetCheckpoint,\n" +
                        "SUM(CASE WHEN op_type = 5 THEN 1 ELSE 0 END) AS SetCheckpoint,\n" +
                        "SUM(CASE WHEN op_type = 6 THEN 1 ELSE 0 END) AS ExecuteQuery,\n" +
                        "COUNT(op_id) AS TotalCount \n" +
                        "FROM odc_operations \n" +
                        "WHERE op_date > ? \n" +
                        "GROUP BY TO_CHAR(op_date, 'YYYY-MM-DD') \n" +
                        "ORDER BY 1 ASC";
            default:
                return "";
        }
    }

    @SneakyThrows
    public void populateAndConfigureChart() {
        @Cleanup val conn = db.getConnectionPool().getConnection();
        @Cleanup val ps = conn.prepareStatement(getSql(db.getConfig().getType()));
        ps.setDate(1, (interval != null) ? interval.getDate() : SixMonths.getDate());

        LineChartConfig config = new LineChartConfig();
        config.data()
                .addDataset(new LineDataset().label(CHECK_ALIVE.getLabel()).backgroundColor(CHECK_ALIVE.getColor()))
                .addDataset(new LineDataset().label(GET_METADATA.getLabel()).backgroundColor(GET_METADATA.getColor()))
                .addDataset(new LineDataset().label(LOAD.getLabel()).backgroundColor(LOAD.getColor()))
                .addDataset(new LineDataset().label(SAVE.getLabel()).backgroundColor(SAVE.getColor()))
                .addDataset(new LineDataset().label(GET_CHECKPOINT.getLabel()).backgroundColor(GET_CHECKPOINT.getColor()))
                .addDataset(new LineDataset().label(SET_CHECKPOINT.getLabel()).backgroundColor(SET_CHECKPOINT.getColor()))
                .addDataset(new LineDataset().label(EXECUTE_QUERY.getLabel()).backgroundColor(EXECUTE_QUERY.getColor()))
                .and()
                .options()
                .responsive(true)
                .title()
                .display(false)
                .and()
                .tooltips()
                .mode(InteractionMode.INDEX)
                .and()
                .legend()
                .position(Position.BOTTOM)
                .and()
                .hover()
                .mode(InteractionMode.INDEX)
                .and()
                .scales()
                .add(Axis.X, new CategoryScale()
                        .scaleLabel()
                        .display(true)
                        .labelString("Date")
                        .and())
                .add(Axis.Y, new LinearScale()
                        .stacked(true)
                        .scaleLabel()
                        .display(true)
                        .labelString("Requests")
                        .and())
                .and()
                .done();

        // populate data and labels
        val labels = new ArrayList<String>();
        val rs = ps.executeQuery();
        while (rs.next()) {
            val dateName = rs.getString(1);
            val checkAliveCount = rs.getInt(2);
            val GetMetadataCount = rs.getInt(3);
            val LoadCount = rs.getInt(4);
            val SaveCount = rs.getInt(5);
            val GetCheckpointCount = rs.getInt(6);
            val SetCheckpointCount = rs.getInt(7);
            val ExecuteQueryCount = rs.getInt(8);

            // Add a label
            labels.add(dateName);
            // Add to series
            ((LineDataset) config.data().getDatasetAtIndex(0)).addData(checkAliveCount);
            ((LineDataset) config.data().getDatasetAtIndex(1)).addData(GetMetadataCount);
            ((LineDataset) config.data().getDatasetAtIndex(2)).addData(LoadCount);
            ((LineDataset) config.data().getDatasetAtIndex(3)).addData(SaveCount);
            ((LineDataset) config.data().getDatasetAtIndex(4)).addData(GetCheckpointCount);
            ((LineDataset) config.data().getDatasetAtIndex(5)).addData(SetCheckpointCount);
            ((LineDataset) config.data().getDatasetAtIndex(6)).addData(ExecuteQueryCount);
        }

        // Add labels
        config.data().labelsAsList(labels);

        // configure chart
        configure(config);
        refreshData();
    }
}
