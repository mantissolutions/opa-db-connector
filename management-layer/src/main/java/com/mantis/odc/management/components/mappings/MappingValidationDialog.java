/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings;

import com.mantis.odc.services.validators.ValidationResult;
import com.vaadin.ui.*;

import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.server.Sizeable.Unit.PIXELS;
import static com.vaadin.ui.themes.ValoTheme.PANEL_BORDERLESS;

/**
 * Mapping validation dialog.
 */
class MappingValidationDialog extends Window {

    private final CheckBox showOnlyFailedField;
    private final ValidationTree resultTree;

    MappingValidationDialog(ValidationResult result) {

        showOnlyFailedField = new CheckBox() {{
            setId("showOnlyFailed");
            setCaption("Display Failed Only");
            setDescription("Show only the failed validations");
            setValue(!result.isSuccess());
        }};

        resultTree = new ValidationTree(result);
        resultTree.setShowOnlyFailed(showOnlyFailedField.getValue());
        showOnlyFailedField.addValueChangeListener(event -> resultTree.setShowOnlyFailed(showOnlyFailedField.getValue()));

        setCaption(" " + result.getMessage());
        setIcon(result.isSuccess() ? (result.isWarning() ? WARNING : CHECK) : EXCLAMATION);
        setSizeUndefined();
        setResizable(false);
        setModal(true);
        center();

        setContent(new VerticalLayout() {{
            setSizeUndefined();
            setSpacing(true);
            setMargin(true);
            addComponent(showOnlyFailedField);
            addComponent(new Panel() {{
                setSizeUndefined();
                setStyleName(PANEL_BORDERLESS);
                setContent(resultTree);
                setHeight(400, PIXELS);
            }});
        }});

        UI.getCurrent().addWindow(this);
    }
}
