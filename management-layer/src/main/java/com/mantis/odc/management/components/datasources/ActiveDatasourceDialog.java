/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.datasources;

import com.mantis.odc.services.ServiceDatasource;
import com.vaadin.ui.*;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static com.vaadin.server.FontAwesome.LINE_CHART;
import static com.vaadin.server.FontAwesome.USER;
import static com.vaadin.server.Sizeable.Unit.PIXELS;
import static com.vaadin.ui.themes.ValoTheme.TEXTFIELD_BORDERLESS;
import static java.lang.String.valueOf;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Displays information about active service data sources.
 */
@Slf4j
@Getter
class ActiveDatasourceDialog extends Window {

    private final ServiceDatasource serviceDatasource;
    private final TextField name;
    private final TextField jdbc;
    private final TextField driver;
    private final TabSheet users;
    private final Map<ServiceDatasource.ServiceUser, ConnectionPoolDetailsForm> activeTabs;
    private final ScheduledFuture<?> job;

    ActiveDatasourceDialog(ScheduledExecutorService executorService, ServiceDatasource serviceDatasource) {
        this.serviceDatasource = serviceDatasource;
        setSizeUndefined();
        setCaption(" " + serviceDatasource.getMapping().getName() + " Details");
        setIcon(LINE_CHART);
        setModal(true);
        setResizable(false);
        setClosable(true);

        name = new TextField() {{
            setId("name");
            setCaption("Name");
            setNullRepresentation("");
            setWidth(500, PIXELS);
            setDescription("Unique name of this service data source.");
            setValue(serviceDatasource.getName());
            setStyleName(TEXTFIELD_BORDERLESS);
            setReadOnly(false);
        }};

        jdbc = new TextField() {{
            setId("jdbc");
            setCaption("JDBC URL");
            setNullRepresentation("");
            setWidth(500, PIXELS);
            setDescription("JDBC URL string to this datasource.");
            setValue(serviceDatasource.getMapping().getJdbc());
            setStyleName(TEXTFIELD_BORDERLESS);
            setReadOnly(false);
        }};

        driver = new TextField() {{
            setId("driver");
            setCaption("Driver");
            setNullRepresentation("");
            setWidth(500, PIXELS);
            setDescription(serviceDatasource.getMapping().getDriver().getDescription());
            setValue(serviceDatasource.getMapping().getDriver().getName());
            setStyleName(TEXTFIELD_BORDERLESS);
            setReadOnly(false);
        }};

        // sheet of user connections
        users = new TabSheet();
        users.setSizeUndefined();

        setContent(new VerticalLayout() {{
            setSpacing(true);
            setMargin(true);
            addComponents(new FormLayout() {{
                setSizeUndefined();
                setSpacing(true);
                setMargin(true);
                addComponents(name, jdbc, driver);
            }}, users);
        }});

        UI.getCurrent().addWindow(this);
        activeTabs = new HashMap<>();

        // update every second
        job = executorService.scheduleWithFixedDelay(this::populateConnectedUsersTabs, 0, 1, SECONDS);
    }

    @Override
    public void close() {
        job.cancel(true);
        super.close();
    }

    /**
     * Creates a metrics tab for each connected user.
     */
    private void populateConnectedUsersTabs() {
        // these updates can occur in the background
        try {
            val ui = getUI();
            if (ui != null) ui.access(() -> {
                Map<ServiceDatasource.ServiceUser, ServiceDatasource.DatabaseAndPool> pools = serviceDatasource.getConnectionPools().asMap();

                // add and update existing service data sources
                pools.forEach((serviceUser, pool) -> {
                    // update existing tab
                    if (activeTabs.containsKey(serviceUser)) {
                        activeTabs.get(serviceUser).updateForm();
                    } else { // create a new tab
                        ConnectionPoolDetailsForm form = new ConnectionPoolDetailsForm(pool);
                        users.addTab(form, pool.getUsername(), USER);
                        activeTabs.put(serviceUser, form);
                    }
                });

                // remove any orphaned tabs
                activeTabs.entrySet().stream()
                        .filter(entry -> !pools.containsKey(entry.getKey()))
                        .forEach(entry -> users.removeTab(users.getTab(entry.getValue())));
            });
        } catch (Throwable e) {
            log.error("Failed to update service datasource information.", e);
        }
    }

    /**
     * Form for connection pools.
     */
    @Getter
    private class ConnectionPoolDetailsForm extends FormLayout {

        private final ServiceDatasource.DatabaseAndPool pool;
        private final TextField timeActive;
        private final TextField waitTime;
        private final TextField usage;
        private final TextField totalConnections;
        private final TextField idleConnections;
        private final TextField activeConnections;
        private final TextField pendingConnections;
        private final DecimalFormat decimalFormat = new DecimalFormat("#.0000");

        private ConnectionPoolDetailsForm(ServiceDatasource.DatabaseAndPool pool) {
            this.pool = pool;
            setSizeUndefined();
            setSpacing(true);
            setMargin(true);

            timeActive = new TextField() {{
                setId("timeActive");
                setCaption("Up Time");
                setNullRepresentation("");
                setDescription("How the pool has been running. (H:MM:SS)");
                setStyleName(TEXTFIELD_BORDERLESS);
                setReadOnly(false);
            }};

            waitTime = new TextField() {{
                setId("waitTime");
                setCaption("Wait Time");
                setNullRepresentation("");
                setDescription("How long requesting threads to getConnection() are waiting for" +
                        " a connection (or timeout exception) from the pool.");
                setStyleName(TEXTFIELD_BORDERLESS);
                setReadOnly(false);
            }};

            usage = new TextField() {{
                setId("usage");
                setCaption("Usage Time");
                setNullRepresentation("");
                setDescription("How long each connection is used before being returned to the pool.");
                setStyleName(TEXTFIELD_BORDERLESS);
                setReadOnly(false);
            }};

            totalConnections = new TextField() {{
                setId("totalConnections");
                setCaption("Total Connections");
                setNullRepresentation("");
                setDescription("Total number of connections in the pool.");
                setStyleName(TEXTFIELD_BORDERLESS);
                setReadOnly(false);
            }};

            idleConnections = new TextField() {{
                setId("idleConnections");
                setCaption("Idle Connections");
                setNullRepresentation("");
                setDescription("Number of idle connections in the pool.");
                setStyleName(TEXTFIELD_BORDERLESS);
                setReadOnly(false);
            }};

            activeConnections = new TextField() {{
                setId("activeConnections");
                setCaption("Active Connections");
                setNullRepresentation("");
                setDescription("Number of active connections in the pool.");
                setStyleName(TEXTFIELD_BORDERLESS);
                setReadOnly(false);
            }};

            pendingConnections = new TextField() {{
                setId("pendingConnections");
                setCaption("Pending Connections");
                setNullRepresentation("");
                setDescription("Number of pending connections in the pool.");
                setStyleName(TEXTFIELD_BORDERLESS);
                setReadOnly(false);
            }};

            updateForm();
            addComponents(timeActive, waitTime, usage, totalConnections, idleConnections, activeConnections, pendingConnections);
        }

        /**
         * Updates the form.
         */
        private void updateForm() {
            timeActive.setValue(pool.getUpTime());
            waitTime.setValue(valueOf(decimalFormat.format(pool.getPoolWaitTime().getMeanRate())));
            usage.setValue(valueOf(decimalFormat.format(pool.getPoolUsage().getSnapshot().getMedian())));
            totalConnections.setValue(valueOf(pool.getTotalConnections().getValue()));
            idleConnections.setValue(valueOf(pool.getIdleConnections().getValue()));
            activeConnections.setValue(valueOf(pool.getActiveConnections().getValue()));
            pendingConnections.setValue(valueOf(pool.getPendingConnections().getValue()));
        }
    }
}
