/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.dashboard.widgets;

import com.mantis.odc.management.components.dashboard.charts.ChartTimeInterval;
import com.mantis.odc.management.components.dashboard.charts.OperationsByTimeChart;
import com.mantis.odc.management.components.dashboard.widgets.base.AbstractWidget;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import lombok.Data;
import lombok.val;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

import static com.mantis.odc.management.components.dashboard.charts.ChartTimeInterval.SixMonths;
import static com.vaadin.server.FontAwesome.CLOCK_O;
import static com.vaadin.ui.themes.ValoTheme.COMBOBOX_TINY;

/**
 * Line chart showing operations by time.
 */
@Data
public class OperationsByTimeWidget extends AbstractWidget {

    private final ComboBox intervals;
    private final OperationsByTimeChart chart;

    @Inject
    public OperationsByTimeWidget(OperationsByTimeChart chart) {
        this.chart = chart;

        intervals = new ComboBox() {{
            setCaption("Intervals");
            setIcon(CLOCK_O);
            setStyleName(COMBOBOX_TINY);
            setNullSelectionAllowed(false);
            for (val interval : ChartTimeInterval.values()) {
                addItem(interval);
                setItemCaption(interval, interval.getCaption());
            }
            setValue(SixMonths);
            addValueChangeListener(e -> {
                chart.setInterval((ChartTimeInterval) getValue());
                // update config
                updateConfig();
            });
        }};

        init();

        getHeader().addComponent(new FormLayout(intervals) {{
            setMargin(new MarginInfo(false, true));
        }}, 1);
    }

    @Override
    public String title() {
        return "Operations By Time";
    }

    @Override
    public int minWidth() {
        return 2;
    }

    @Override
    public int maxWidth() {
        return 4;
    }

    @Override
    public int minHeight() {
        return 6;
    }

    @Override
    public int maxHeight() {
        return 6;
    }

    @Override
    public Component getComponent() {
        return chart;
    }

    @Override
    public void refreshOnSizeChange() {
    }

    @Override
    public void updateData() {
        chart.populateAndConfigureChart();
    }

    @Override
    public void configure(Map<String, String> settings) {
        intervals.setValue(ChartTimeInterval.valueOf(settings.get("interval")));
    }

    @Override
    public Map<String, String> config() {
        val map = new HashMap<String, String>();
        map.put("interval", ((ChartTimeInterval) intervals.getValue()).name());
        return map;
    }
}
