/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings;

import com.mantis.odc.database.dao.DatasourceMappingDAO;
import com.mantis.odc.database.dao.MappingDAO;
import com.mantis.odc.management.MainUI;
import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.management.components.base.BaseServiceCRUDListComponent;
import com.mantis.odc.management.components.datasources.DatasourceFormComponent;
import com.mantis.odc.management.components.extentions.AdvancedTable;
import com.mantis.odc.management.components.models.ActionModel;
import com.mantis.odc.models.Mapping;
import com.mantis.odc.models.OPAVersion;
import com.mantis.odc.services.Service;
import com.mantis.odc.services.storage.DebugStorage;
import com.mantis.odc.services.storage.ServiceStorage;
import com.mantis.odc.services.validators.MappingValidator;
import com.mantis.odc.services.validators.ValidationResult;
import com.mantis.odc.xml.MappingXMLTransport;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import lombok.Data;
import lombok.val;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Function;

import static com.mantis.odc.utilities.ErrorUtil.errorToString;
import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.ui.Notification.Type.ERROR_MESSAGE;
import static com.vaadin.ui.Notification.Type.TRAY_NOTIFICATION;
import static com.vaadin.ui.Notification.show;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_LINK;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;
import static java.text.MessageFormat.format;
import static java.util.Collections.EMPTY_LIST;

/**
 * Mapping list view.
 */
@Data
public class MappingListComponent extends BaseServiceCRUDListComponent<BigDecimal, Mapping, String, Service> {

    private final String modelName = "Mapping";
    private final ScheduledExecutorService executorService;
    private final MappingDAO dao;
    private final DatasourceMappingDAO datasourceMappingDAO;
    private final MappingValidator validator;
    private final MappingXMLTransport transport;
    private final ServiceStorage storage;
    private final DebugStorage debugStorage;
    private final BeanContainer<BigDecimal, Mapping> container;
    private final Function<BigDecimal, Mapping> keyToModelResolver;
    private final Button addMappingButton;
    private final Button importMappingButton;
    private final MappingSearchFormComponent searchForm;
    private final AdvancedTable<BigDecimal, Mapping> table;
    private final VerticalLayout layout;

    @Inject
    MappingListComponent(ScheduledExecutorService executorService, MappingDAO dao,
                         DatasourceMappingDAO datasourceMappingDAO,
                         MappingValidator validator,
                         MappingXMLTransport transport,
                         ServiceStorage storage,
                         DebugStorage debugStorage) {
        this.executorService = executorService;
        this.dao = dao;
        this.datasourceMappingDAO = datasourceMappingDAO;
        this.validator = validator;
        this.transport = transport;
        this.storage = storage;
        this.debugStorage = debugStorage;

        container = new BeanContainer<BigDecimal, Mapping>(Mapping.class) {{
            setBeanIdResolver(Mapping::getId);
            addAll(dao.list());
        }};

        keyToModelResolver = key -> (key != null) ? container.getItem(key).getBean() : null;

        searchForm = new MappingSearchFormComponent(container);

        table = new AdvancedTable<BigDecimal, Mapping>() {{
            setId("mappingList");
            setSizeFull();
            setKeyToModelResolver(keyToModelResolver);
            setActionProvider(Optional.of(mapping -> getServiceActions(mapping)));
            setContainerDataSource(container);
            addColumn("status", mapping -> getStatusColumn(mapping));
            addColumn("actions", mapping -> getActionsColumn(mapping));
            addColumn("datasourceLink", mapping -> getDatasourceLink(mapping));
            addColumn("deployedLinks", mapping -> getDeployedLinks(mapping));
            setVisibleColumns("status", "actions", "name", "datasourceLink", "deployedLinks");
            setColumnHeaders("", "", "Name", "Datasource", "Links");
            setColumnWidth("status", 40);
            setColumnWidth("actions", 130);
        }};

        addMappingButton = new Button() {{
            setId("addMapping");
            setCaption("Add Mapping");
            setIcon(PLUS);
            setStyleName(BUTTON_PRIMARY);
            addClickListener((event) -> showCreateModelWindow());
        }};

        importMappingButton = new Button() {{
            setId("importMapping");
            setCaption("Import Mapping");
            setIcon(UPLOAD);
            addClickListener((event) -> importMapping());
        }};

        layout = new VerticalLayout() {{
            setSizeFull();
            setSpacing(true);
            setMargin(new MarginInfo(false, true));

            val topSection = new HorizontalLayout() {{
                setSpacing(true);
                addComponent(new HorizontalLayout() {{
                    setSpacing(true);
                    addComponents(addMappingButton, importMappingButton);
                }});
                addComponent(new Panel(searchForm));
            }};
            addComponents(topSection, table);
            setExpandRatio(topSection, (float) 1.5);
            setExpandRatio(table, (float) 3.5);
        }};
        setCompositionRoot(layout);
    }

    /**
     * Updates all records from the database.
     */
    public void updateFromDatabase() {
        container.removeAllItems();
        dao.list().forEach(datasourceMapping -> container.addItem(datasourceMapping.getId(), datasourceMapping));
    }

    /**
     * Creates a datasource view model.
     *
     * @param mapping mapping
     * @return datasource viewer model
     */
    private Button getDatasourceLink(Mapping mapping) {
        return mapping.getDatasource() != null ?
                new Button() {{
                    setId("datasourceLink" + mapping.getName());
                    setCaption(mapping.getDatasource().getName());
                    setIcon(DATABASE);
                    setStyleName(BUTTON_LINK);
                    setDescription("View datasource");
                    addClickListener(event -> createFormWindow(
                            "View " + mapping.getName(), DATABASE,
                            new DatasourceFormComponent(EMPTY_LIST) {{
                                setValue(mapping.getDatasource());
                                setEnabled(false);
                            }}));
                }} : null;
    }

    /**
     * Returns list of all deployed links.
     *
     * @param mapping mapping
     * @return list of deployed links
     */
    private MenuBar getDeployedLinks(Mapping mapping) {
        return storage.contains(mapping.getServiceName()) ? new MenuBar() {{
            val mainUI = (MainUI) UI.getCurrent().getUI();
            val mainItem = addItem("", LINK, null);
            Arrays.stream(OPAVersion.values())
                    .forEach(mv -> {
                        String link = mainUI.getRelPath(format("/services/{0}/{1}", mapping.getServiceName(), mv.name()));
                        val item = mainItem.addItem(mv.getVersion(), LINK, (a) -> getUI().getPage().open(link, "_blank"));
                        item.setDescription(format("Mapping deployment for the OPA {0} connector version.", mv.getVersion()));
                    });
        }} : null;
    }

    @Override
    public List<ActionModel<Mapping>> getActions() {
        val actions = super.getActions();
        actions.add(new ActionModel<>("Validate", CHECK, true, mapping -> {
            ValidationResult result = validator.validate(mapping);
            new MappingValidationDialog(result);
        }));
        actions.add(new ActionModel<>("Validate Schema", DATABASE, true, SchemaValidationDialog::new));
        actions.add(new ActionModel<>("Export", DOWNLOAD, true, this::exportMapping));
        return actions;
    }

    @Override
    protected List<ActionModel<Mapping>> getServiceActions(Mapping mapping) {
        val serviceActions = super.getServiceActions(mapping);
        if (mapping != null && mapping.isDebugMode() && storage.contains(mapping.getServiceName()))
            serviceActions.add(new ActionModel<>("Debug", LIST, true, key ->
                    new MappingDebugViewComponent(executorService, key.getServiceName(), debugStorage)));
        return serviceActions;
    }

    /**
     * Imports a mapping
     */
    private void importMapping() {
        val importMappingDialog = new MappingImportDialog(transport, validator);
        importMappingDialog.addImportedMappingListener(event -> {
            try {
                Mapping mapping = event.getMapping();
                dao.create(mapping);
                container.addItem(mapping.getId(), mapping);
                show(format("Mapping {0} has been Imported!", event.getMapping().getName()), TRAY_NOTIFICATION);
            } catch (Exception e) {
                show(format("Failed to import mapping: {0}", errorToString(e)), ERROR_MESSAGE);
            }
        });
    }

    /**
     * Exports the current mapping if it is valid.
     *
     * @param model mapping to export
     */
    private void exportMapping(Mapping model) {
        val result = validator.validate(model);
        // check mapping is valid
        if (result.isSuccess()) {
            val mainUi = (MainUI) UI.getCurrent();
            UI.getCurrent().getPage().open(mainUi.getRelPath("/mappings/" + model.getName()), "_blank");
        } else new MappingValidationDialog(result);
    }

    @Override
    protected void undeployModel(Mapping mapping) {
        storage.undeployService(mapping.getServiceName());
        show(format("Service {0} has been un-deployed!", mapping.getServiceName()), TRAY_NOTIFICATION);
        replaceModel(mapping);
    }

    @Override
    protected void deployModel(Mapping mapping) {
        val result = validator.validate(mapping);
        if (result.isSuccess()) {
            storage.deployMapping(mapping);
            show(format("Mapping {0} has been deployed as service {1}!", mapping.getName(), mapping.getServiceName()), TRAY_NOTIFICATION);
            replaceModel(mapping);
        } else new MappingValidationDialog(result);
    }


    @Override
    public Mapping createNewModelInstance() {
        return new Mapping();
    }

    @Override
    protected BigDecimal getModelKey(Mapping mapping) {
        return mapping.getId();
    }

    @Override
    public BaseCRUDFormComponent<Mapping> getForm(Mapping model) {
        val form = new MappingFormComponent(datasourceMappingDAO, getSiblings(model));
        form.setValue(model);
        return form;
    }

    @Override
    protected String getServiceKey(Mapping mapping) {
        return mapping.getServiceName();
    }

}
