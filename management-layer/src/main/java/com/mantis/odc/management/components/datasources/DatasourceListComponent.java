/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.datasources;

import com.mantis.odc.database.dao.DatasourceMappingDAO;
import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.management.components.base.BaseServiceCRUDListComponent;
import com.mantis.odc.management.components.extentions.AdvancedTable;
import com.mantis.odc.management.components.models.ActionModel;
import com.mantis.odc.models.DatasourceMapping;
import com.mantis.odc.services.Service;
import com.mantis.odc.services.ServiceDatasource;
import com.mantis.odc.services.storage.DatasourceStorage;
import com.mantis.odc.services.storage.ServiceStorage;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import lombok.Data;
import lombok.val;
import org.vaadin.dialogs.ConfirmDialog;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.ui.Notification.Type.TRAY_NOTIFICATION;
import static com.vaadin.ui.Notification.show;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;
import static java.text.MessageFormat.format;

/**
 * Data source mapping list.
 */
@Data
public class DatasourceListComponent extends BaseServiceCRUDListComponent<BigDecimal, DatasourceMapping, DatasourceMapping, ServiceDatasource> {

    private final String modelName = "Datasource Mapping";
    private final ScheduledExecutorService executorService;
    private final DatasourceMappingDAO dao;
    private final DatasourceStorage storage;
    private final ServiceStorage serviceStorage;
    private final BeanContainer<BigDecimal, DatasourceMapping> container;
    private final Function<BigDecimal, DatasourceMapping> keyToModelResolver;
    private final Button addDatasourceButton;
    private final DatasourceSearchFormComponent searchForm;
    private final AdvancedTable<BigDecimal, DatasourceMapping> table;
    private final VerticalLayout layout;

    @Inject
    public DatasourceListComponent(ScheduledExecutorService executorService,
                                   DatasourceMappingDAO dao,
                                   DatasourceStorage storage,
                                   ServiceStorage serviceStorage) {
        this.executorService = executorService;
        this.dao = dao;
        this.storage = storage;
        this.serviceStorage = serviceStorage;

        container = new BeanContainer<BigDecimal, DatasourceMapping>(DatasourceMapping.class) {{
            setBeanIdResolver(DatasourceMapping::getId);
            addAll(dao.list());
        }};

        keyToModelResolver = key -> (key != null) ? container.getItem(key).getBean() : null;

        searchForm = new DatasourceSearchFormComponent(container);

        table = new AdvancedTable<BigDecimal, DatasourceMapping>() {{
            setId("datasourceList");
            setSizeFull();
            setActionProvider(Optional.of(mapping -> getServiceActions(mapping)));
            setContainerDataSource(container);
            setKeyToModelResolver(keyToModelResolver);
            addColumn("status", mapping -> getStatusColumn(mapping));
            addColumn("actions", mapping -> getActionsColumn(mapping));
            setVisibleColumns("status", "actions", "name", "jdbc", "driver");
            setColumnHeaders("", "", "Name", "JDBC URL", "Driver");
            setColumnWidth("status", 40);
            setColumnWidth("actions", 130);
        }};

        addDatasourceButton = new Button() {{
            setId("addDatasource");
            setCaption("Add Datasource");
            setIcon(PLUS);
            setStyleName(BUTTON_PRIMARY);
            addClickListener((event) -> showCreateModelWindow());
        }};

        layout = new VerticalLayout() {{
            setSizeFull();
            setSpacing(true);
            setMargin(new MarginInfo(false, true));

            val topSection = new HorizontalLayout() {{
                setSpacing(true);
                addComponent(addDatasourceButton);
                addComponent(new Panel(searchForm));
            }};
            addComponents(topSection, table);
            setExpandRatio(topSection, (float) 1.5);
            setExpandRatio(table, (float) 3.5);
        }};

        setCompositionRoot(layout);
    }

    /**
     * Updates all records from the database.
     */
    public void updateFromDatabase() {
        container.removeAllItems();
        dao.list().forEach(datasourceMapping -> container.addItem(datasourceMapping.getId(), datasourceMapping));
    }

    @Override
    public List<ActionModel<DatasourceMapping>> getActions() {
        val actions = super.getActions();
        actions.add(new ActionModel<>("Test", DATABASE, true, TestDatasourceDialog::new));
        return actions;
    }

    @Override
    protected void undeployModel(DatasourceMapping mapping) {
        val services = serviceStorage.dependentServices(mapping);
        // safe to un-deployMapping
        if (services.isEmpty()) {
            storage.undeployServiceDatasource(mapping);
            show(format("Data source {0} has been un-deployed!", mapping.getName()), TRAY_NOTIFICATION);
            replaceModel(mapping);
        } else { // request un-deployMapping of related services, then un-deployMapping
            ConfirmDialog.show(UI.getCurrent(), "Un-deployMapping Related Mappings",
                    format("To un-deployMapping this data source the following mappings need to be un-deployed\n\n{0}\n\nWould you like to proceed?",
                            services.stream().map(Service::getName).collect(Collectors.joining("\n"))), "Yes", "No",
                    dialog -> {
                        if (dialog.isConfirmed()) {
                            services.forEach(service -> serviceStorage.undeployService(service.getName()));
                            undeployModel(mapping);
                        }
                    });
        }
    }

    @Override
    protected List<ActionModel<DatasourceMapping>> getServiceActions(DatasourceMapping datasourceMapping) {
        val serviceActions = super.getServiceActions(datasourceMapping);

        if (storage.contains(datasourceMapping))
            serviceActions.add(new ActionModel<>("Statistics", LINE_CHART, true, key -> new ActiveDatasourceDialog(executorService, storage.get(key))));

        return serviceActions;
    }

    @Override
    protected void deployModel(DatasourceMapping mapping) {
        storage.deployMapping(mapping);
        show(format("Data source {0} has been deployed!", mapping.getName()), TRAY_NOTIFICATION);
        replaceModel(mapping);
    }

    @Override
    public DatasourceMapping createNewModelInstance() {
        return new DatasourceMapping();
    }

    @Override
    protected BigDecimal getModelKey(DatasourceMapping model) {
        return model.getId();
    }

    @Override
    public BaseCRUDFormComponent<DatasourceMapping> getForm(DatasourceMapping model) {
        val form = new DatasourceFormComponent(getSiblings(model));
        form.setValue(model);
        return form;
    }

    @Override
    protected DatasourceMapping getServiceKey(DatasourceMapping model) {
        return model;
    }
}
