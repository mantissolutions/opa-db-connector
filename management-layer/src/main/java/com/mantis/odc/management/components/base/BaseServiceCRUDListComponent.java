/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.base;

import com.mantis.odc.management.components.models.ActionModel;
import com.mantis.odc.models.base.Duplicatable;
import com.mantis.odc.services.storage.base.BaseStorage;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import lombok.val;

import java.util.List;
import java.util.stream.Collectors;

import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.shared.ui.label.ContentMode.HTML;
import static java.text.MessageFormat.format;

/**
 * Base service model type list component.
 */
public abstract class BaseServiceCRUDListComponent<ID, MODEL extends Duplicatable<MODEL>, SERVICE_KEY, SERVICE_MODEL> extends BaseCRUDListComponent<ID, MODEL> {

    /**
     * @return returns the model service storage.
     */
    protected abstract BaseStorage<SERVICE_KEY, SERVICE_MODEL> getStorage();

    /**
     * @param model model
     * @return key used to identify it when deployed
     */
    protected abstract SERVICE_KEY getServiceKey(MODEL model);

    /**
     * @return returns the service status for the model. This is either deployed or not deployed
     */
    protected Label getStatusColumn(MODEL model) {
        return new Label() {{
            setContentMode(HTML);
            if (getStorage().contains(getServiceKey(model))) {
                setValue(LOCK.getHtml());
                setDescription(format("The {0} is deployed. To make changes un-deploy the {0}.", getModelName()));
            } else {
                setValue(UNLOCK.getHtml());
                setDescription(format("The {0} is not deployed.", getModelName()));
            }
        }};
    }

    @Override
    public List<ActionModel<MODEL>> getActions() {
        val actions = super.getActions();
        actions.add(new ActionModel<>("Deploy", CLOUD_UPLOAD, true, this::deployModel));
        actions.add(new ActionModel<>("Un-deploy", CLOUD_DOWNLOAD, true, this::undeployModel));
        return actions;
    }

    @Override
    public MenuBar getActionsColumn(MODEL model) {
        return new MenuBar() {{
            val actions = addItem("", COGS, null);
            actions.setDescription("Operations which can be performed on the current row.");
            getServiceActions(model).stream()
                    .filter(ActionModel::isRequiresModel)
                    .forEach(action -> actions.addItem(action.getCaption(), action.getIcon(), selectedItem -> action.getConsumer().accept(model)));
        }};
    }

    /**
     * Returns all the valid service actions.
     *
     * @param model model.
     * @return service actions
     */
    protected List<ActionModel<MODEL>> getServiceActions(MODEL model) {
        return getActions().stream()
                .filter(action -> {
                    boolean isDeployed = model != null && getStorage().contains(getServiceKey(model));
                    // if deployed and
                    return (isDeployed
                            // not equal to edit, remove or deployMapping
                            && !("Edit".equals(action.getCaption())
                            || "Remove".equals(action.getCaption())
                            || "Deploy".equals(action.getCaption())))
                            // or if not deployed remove un-deployMapping action
                            || (!isDeployed && !"Un-deploy".equals(action.getCaption()));
                })
                .collect(Collectors.toList());
    }

    /**
     * @param model un-deploys the given model
     */
    protected abstract void undeployModel(MODEL model);

    /**
     * @param model deploys the given model
     */
    protected abstract void deployModel(MODEL model);
}
