/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.views.model;

import lombok.Data;
import lombok.SneakyThrows;

import static java.net.URLDecoder.decode;
import static java.net.URLEncoder.encode;
import static java.text.MessageFormat.format;

/**
 * URL parameter.
 */
@Data
public class URLParameter {

    private static final String UTF_8 = "UTF-8";

    private final String key;
    private final String value;

    @SneakyThrows
    public URLParameter(String pair) {
        int idx = pair.indexOf("=");
        if (idx > 0) {
            this.key = decode(pair.substring(0, idx), UTF_8);
            this.value = decode(pair.substring(idx + 1), UTF_8);
        } else {
            key = null;
            value = null;
        }
    }

    public URLParameter(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @SneakyThrows
    public String encodeToString() {
        return format("{0}={1}", encode(key, UTF_8), encode(value, UTF_8));
    }
}
