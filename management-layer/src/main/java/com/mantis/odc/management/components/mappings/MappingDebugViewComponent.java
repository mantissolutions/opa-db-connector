/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings;

import com.google.common.collect.Lists;
import com.mantis.odc.management.components.base.BaseSearchFormComponent;
import com.mantis.odc.management.components.base.TableActions;
import com.mantis.odc.management.components.extentions.AdvancedTable;
import com.mantis.odc.management.components.models.ActionModel;
import com.mantis.odc.models.OPAVersion;
import com.mantis.odc.models.ServiceAction;
import com.mantis.odc.services.models.debug.DebugEventType;
import com.mantis.odc.services.models.debug.base.DebugEvent;
import com.mantis.odc.services.storage.DebugStorage;
import com.vaadin.data.Container;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.data.util.filter.Like;
import com.vaadin.ui.*;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.vaadin.aceeditor.AceEditor;
import org.vaadin.aceeditor.AceMode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static com.mantis.odc.services.models.debug.DebugEventType.SQL;
import static com.vaadin.server.FontAwesome.*;
import static com.vaadin.server.Sizeable.Unit.PIXELS;
import static com.vaadin.shared.ui.label.ContentMode.HTML;
import static com.vaadin.ui.themes.ValoTheme.*;
import static java.util.Collections.singletonList;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Displays all mapping debug information.
 */
@Slf4j
@Getter
class MappingDebugViewComponent extends Window implements TableActions<DebugEvent> {

    private final String serviceName;
    private final DebugStorage debugStorage;
    private final BeanItemContainer<DebugEvent> container;
    private final AdvancedTable<DebugEvent, DebugEvent> table;
    private final Button clearButton;
    private final MappingDebugSearchFormComponent searchForm;
    private final VerticalLayout layout;
    private final ScheduledFuture<?> job;

    MappingDebugViewComponent(ScheduledExecutorService executorService, String serviceName, DebugStorage debugStorage) {
        this.serviceName = serviceName;
        this.debugStorage = debugStorage;

        container = new BeanItemContainer<>(DebugEvent.class);

        setSizeUndefined();
        setCaption(" " + serviceName + " Debug Events");
        setIcon(LIST);
        setModal(true);
        setResizable(false);
        setClosable(true);

        table = new AdvancedTable<DebugEvent, DebugEvent>() {{
            setId("debugList");
            setWidth(900, PIXELS);
            setHeight(450, PIXELS);
            setActionProvider(Optional.of(mapping -> getActions()));
            setContainerDataSource(container);
            addColumn("actions", event -> getActionsColumn(event));
            addColumn("typeIcon", event -> getTypeIcon(event.getType()));
            addColumn("shortMessage", event -> (event.getMessage().length() > 150) ? event.getMessage().substring(0, 150) + "..." : event.getMessage());
            setVisibleColumns("actions", "typeIcon", "formattedCreateTime", "versionString", "serviceActionString", "shortMessage");
            setColumnHeaders("", "", "Created", "Version", "Action", "Message");
            setColumnWidth("actions", 75);
            setColumnWidth("typeIcon", 40);
        }};

        clearButton = new Button() {{
            setId("clearDebug");
            setCaption("Clear Debug Messages");
            setIcon(TRASH);
            setStyleName(BUTTON_DANGER);
            addClickListener((event) -> {
                debugStorage.clearMessages(serviceName);
                container.removeAllItems();
            });
        }};

        searchForm = new MappingDebugSearchFormComponent();

        layout = new VerticalLayout() {{
            setSpacing(true);
            setMargin(true);

            val topSection = new HorizontalLayout() {{
                setSpacing(true);
                addComponents(clearButton, new Panel(searchForm));
            }};

            addComponents(topSection, table);
            setExpandRatio(topSection, (float) 1.5);
            setExpandRatio(table, (float) 3.5);
        }};

        setContent(layout);
        UI.getCurrent().addWindow(this);

        // populate debug messages
        job = executorService.scheduleWithFixedDelay(this::populateDebugMessages, 0, 1, SECONDS);
    }

    private void populateDebugMessages() {
        UI ui = getUI();
        if (ui != null) ui.access(() -> {
            try {
                // load any new message from this message
                debugStorage.getOpt(serviceName)
                        .ifPresent(debugEvents -> {
                            // add new messages to the container
                            List<DebugEvent> reverseList = Lists.reverse(debugEvents);
                            int itemsToAdd = debugEvents.size() - container.size();
                            for (int i = itemsToAdd - 1; i >= 0; i--)
                                container.addItemAfter(null, reverseList.get(i));
                        });
            } catch (Throwable e) {
                log.error("Failed to update debug list.", e);
            }
        });
    }

    @Override
    public void close() {
        super.close();
        job.cancel(true);
    }

    private Label getTypeIcon(DebugEventType type) {
        switch (type) {
            case Error:
                return new Label(EXCLAMATION.getHtml(), HTML);
            case Info:
                return new Label(INFO.getHtml(), HTML);
            case SQL:
                return new Label(DATABASE.getHtml(), HTML);
            case Message:
                return new Label(CLOUD_UPLOAD.getHtml(), HTML);
            default:
                return null;
        }
    }

    @Override
    public List<ActionModel<DebugEvent>> getActions() {
        return singletonList(new ActionModel<>("View", EYE, true, event -> new Window() {{
            setSizeUndefined();
            setIcon(EYE);
            setModal(true);
            setResizable(false);
            setClosable(true);
            setContent(new VerticalLayout() {{
                setMargin(true);
                addComponent(new Panel() {{
                    setWidth(800, PIXELS);
                    setHeight(600, PIXELS);
                    setStyleName(PANEL_BORDERLESS);

                    switch (event.getType()) {
                        case Error:
                        case Info:
                            setContent(new AceEditor() {{
                                setValue(event.getMessage());
                                setSizeFull();
                                setWordWrap(true);
                                setReadOnly(true);
                                setMode(AceMode.plain_text);
                            }});
                            break;
                        case SQL:
                        case Message:
                            setContent(new AceEditor() {{
                                setValue(event.getMessage());
                                setSizeFull();
                                setWordWrap(true);
                                setReadOnly(true);
                                setMode(event.getType() == SQL ? AceMode.sql : AceMode.xml);
                            }});
                            break;
                    }
                }});
            }});
            UI.getCurrent().addWindow(this);
        }}));
    }

    /**
     * Search form.
     */
    @Data
    private class MappingDebugSearchFormComponent extends BaseSearchFormComponent {

        private final TextField formattedCreateTimeField;
        private final ComboBox opaVersionField;
        private final ComboBox serviceActionField;
        private final TextField messageField;
        private final Button searchButton;
        private final Button clearButton;

        MappingDebugSearchFormComponent() {

            formattedCreateTimeField = new TextField() {{
                setId("formattedCreateTime");
                setCaption("Create Time");
                setIcon(CLOCK_O);
                setInputPrompt("time...");
                setNullRepresentation("");
                setImmediate(true);
                setDescription("Event create time.");
            }};

            opaVersionField = new ComboBox() {{
                setId("opaVersionString");
                setCaption("OPA Version");
                setImmediate(true);
                setDescription("OPA version.");
                for (val version : OPAVersion.values()) addItem(version.getVersion());
            }};

            serviceActionField = new ComboBox() {{
                setId("serviceActionString");
                setCaption("Service Action");
                setImmediate(true);
                setDescription("Service action.");
                for (val action : ServiceAction.values()) addItem(action.getActionName());
            }};

            messageField = new TextField() {{
                setId("message");
                setCaption("Message");
                setInputPrompt("message...");
                setNullRepresentation("");
                setImmediate(true);
                setDescription("Debug message.");
            }};

            searchButton = new Button() {{
                setId("searchButton");
                setCaption("Search");
                setIcon(SEARCH);
                setStyleName(BUTTON_PRIMARY);
                addClickListener(event -> search());
            }};

            clearButton = new Button() {{
                setId("saveButton");
                setCaption("Clear");
                addClickListener(event -> clearSearch());
            }};

            setCompositionRoot(new FormLayout() {{
                setSpacing(true);
                setMargin(true);
                addComponents(
                        new HorizontalLayout() {{
                            setSpacing(true);
                            addComponents(formattedCreateTimeField, opaVersionField);
                        }},
                        new HorizontalLayout() {{
                            setSpacing(true);
                            addComponents(serviceActionField, messageField);
                        }},
                        new HorizontalLayout() {{
                            setSpacing(true);
                            addComponents(searchButton, clearButton);
                        }}
                );
            }});
        }

        @Override
        public Container.Filterable getContainer() {
            return container;
        }

        @Override
        public Map<Field, Container.Filter> getFilters() {
            Map<Field, Container.Filter> filters = new HashMap<>();
            filters.put(formattedCreateTimeField, new Like("formattedCreateTime", formattedCreateTimeField.getValue()));
            filters.put(opaVersionField, new Compare.Equal("versionString", opaVersionField.getValue()));
            filters.put(serviceActionField, new Compare.Equal("serviceActionString", serviceActionField.getValue()));
            filters.put(messageField, new Like("message", messageField.getValue()));
            return filters;
        }
    }
}
