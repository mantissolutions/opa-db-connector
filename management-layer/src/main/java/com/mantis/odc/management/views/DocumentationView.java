/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.views;

import com.mantis.odc.management.MainUI;
import com.mantis.odc.management.layouts.MainLayout;
import com.mantis.odc.management.support.SecureNavigator;
import com.mantis.odc.management.views.base.ComponentView;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.UI;
import lombok.Data;
import lombok.val;

import javax.inject.Inject;
import java.util.List;

import static com.vaadin.server.FontAwesome.BOOK;
import static java.util.Collections.singletonList;

/**
 * Home dashboard view.
 */
@Data
public class DocumentationView extends ComponentView {

    @Inject
    public DocumentationView() {
        super(new MainLayout(new BrowserFrame("", new ExternalResource(((MainUI) UI.getCurrent()).getRelPath("/documentation"))) {{
            setSizeFull();
        }}, false, false));
        setSizeFull();
    }

    @Override
    public void setNavigator(SecureNavigator navigator) {
        val layout = (MainLayout) getCompositionRoot();
        layout.setNavigator(navigator);
        super.setNavigator(navigator);
    }

    @Override
    public List<String> names() {
        return singletonList("documentation");
    }

    @Override
    public String title() {
        return "Documentation";
    }

    @Override
    public FontAwesome icon() {
        return BOOK;
    }

    @Override
    public String description() {
        return "Displays the OPA DB Connector Documentation.";
    }

    @Override
    public boolean hasMenuItem() {
        return true;
    }

    @Override
    public int menuPosition() {
        return 5;
    }
}
