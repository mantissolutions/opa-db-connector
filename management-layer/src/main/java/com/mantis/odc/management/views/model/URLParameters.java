/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.views.model;

import lombok.NonNull;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Encapsulates the URL parameters passed to the view.
 */
public class URLParameters {

    private final List<URLParameter> items;

    public URLParameters(@NonNull String parameters) {
        items = Arrays.stream(parameters.split("&"))
                .map(URLParameter::new)
                .collect(Collectors.toList());
    }

    /**
     * Returns the parameter value if it exists.
     *
     * @param key key value
     * @return value or none
     */
    public Optional<String> get(String key) {
        return items.stream()
                .filter(urlParameter -> urlParameter.getKey().equals(key))
                .map(URLParameter::getValue)
                .findFirst();
    }

    /**
     * Returns the parameter value or the given default.
     *
     * @param key          key value
     * @param defaultValue default value
     * @return value or default
     */
    public String getWithDefault(String key, String defaultValue) {
        return get(key).orElse(defaultValue);
    }

    /**
     * Returns the converted parameter value if it exists.
     *
     * @param key       key value
     * @param converter converter to T
     * @param <T>       type to return
     * @return value or none
     */
    public <T> Optional<T> getAs(String key, Function<String, T> converter) {
        return get(key).map(converter);
    }

    /**
     * Returns the converted parameter value or the default.
     *
     * @param key          key value
     * @param converter    converter to T
     * @param defaultValue default value
     * @param <T>          type to return
     * @return value or default
     */
    public <T> T getWithDefaultAs(String key, Function<String, T> converter, T defaultValue) {
        return getAs(key, converter).orElse(defaultValue);
    }
}
