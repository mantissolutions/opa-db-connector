/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.datasources;

import com.mantis.odc.models.Driver;
import com.vaadin.ui.ComboBox;
import lombok.val;

import java.util.Optional;

import static java.text.MessageFormat.format;

/**
 * Field for selecting database drivers.
 */
class DriverSelectField extends ComboBox {

    DriverSelectField() {
        for (val d : Driver.values()) {
            addItem(d);
            setItemCaption(d, d.getName());
        }
        addValueChangeListener(event -> setDescription((Driver) event.getProperty().getValue()));
    }

    /**
     * Sets the description.
     *
     * @param value driver
     */
    private void setDescription(Driver value) {
        setDescription(Optional.ofNullable(value)
                .map(driver -> format("<b>{0}</b><p><b>Version:</b> {1}</p><p><b>Class:</b> {2}</p><p><b>Description:</b> {3}</p>",
                        driver.getName(), driver.getVersion(), driver.getClassName(), driver.getDescription()))
                .orElse("Select a database driver."));
    }
}
