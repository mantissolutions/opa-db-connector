/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings.enumerationmappings;

import com.mantis.odc.data.ReferenceEqualityArray;
import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.management.components.base.BaseSubFormListComponent;
import com.mantis.odc.management.components.extentions.AdvancedTable;
import com.mantis.odc.models.EnumerationMapping;
import com.mantis.odc.models.Mapping;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import lombok.Data;
import lombok.val;
import org.vaadin.viritin.ListContainer;

import java.util.List;
import java.util.Optional;

import static com.vaadin.server.FontAwesome.LIST;
import static com.vaadin.server.Sizeable.Unit.PIXELS;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;

/**
 * Bind mapping list.
 */
@Data
public class EnumerationMappingListComponent extends BaseSubFormListComponent<EnumerationMapping, Mapping> {

    private boolean isViewMode;
    private final String modelName = "Enumeration Mapping";
    private final Mapping parentModel;
    private final List<EnumerationMapping> items = new ReferenceEqualityArray<>();
    private final ListContainer<EnumerationMapping> container;
    private final Button addEnumerationMappingButton;
    private final AdvancedTable<EnumerationMapping, EnumerationMapping> table;
    private final VerticalLayout layout;

    public EnumerationMappingListComponent(Mapping parentModel) {
        this.parentModel = parentModel;
        items.addAll(parentModel.getEnumerations());

        container = new ListContainer<>(EnumerationMapping.class, items);

        table = new AdvancedTable<EnumerationMapping, EnumerationMapping>() {{
            setId("enumerationMappingList");
            setWidth(1100, PIXELS);
            setHeight(450, PIXELS);
            setActionProvider(Optional.of(mapping -> getActions()));
            setContainerDataSource(container);
            addColumn("actions", mapping -> getActionsColumn(mapping));
            addColumn("typeDisplay", mapping -> mapping.getType().getDisplay());
            addColumn("childName", mapping -> mapping.getChild() != null ? mapping.getChild().getName() : null);
            setVisibleColumns("actions", "name", "typeDisplay", "childName", "description");
            setColumnHeaders("", "Name", "Type", "Child Name", "Description");
            setColumnWidth("actions", 130);
        }};

        addEnumerationMappingButton = new Button() {{
            setId("addEnumerationMapping");
            setCaption("Add Enumeration Mapping");
            setIcon(LIST);
            setStyleName(BUTTON_PRIMARY);
            addClickListener((event) -> showCreateModelWindow());
        }};

        layout = new VerticalLayout() {{
            setSizeFull();
            setSpacing(true);
            setMargin(true);
            addComponents(addEnumerationMappingButton, table);
        }};

        setCompositionRoot(layout);
    }

    @Override
    public void setEnabled(boolean enabled) {
        addEnumerationMappingButton.setVisible(enabled);
        isViewMode = !enabled;
    }

    @Override
    public EnumerationMapping createNewModelInstance() {
        return new EnumerationMapping();
    }

    @Override
    public BaseCRUDFormComponent<EnumerationMapping> getForm(EnumerationMapping model) {
        val form = new EnumerationMappingFormComponent(getSiblings(model));
        form.setValue(model);
        return form;
    }

    @Override
    public List<EnumerationMapping> getItemsFromParent(Mapping mapping) {
        return mapping.getEnumerations();
    }

    @Override
    protected void saveItemsToParent(Mapping mapping, List<EnumerationMapping> items) {
        items.forEach(enumerationMapping -> enumerationMapping.setMapping(mapping));
        mapping.setEnumerations(items);
    }

    @Override
    protected void removeModel(EnumerationMapping mapping) {
        // inform the user that the enumeration has parents
        if (!mapping.getParents().isEmpty()) {
            showErrorMessage("The enumeration is in use, remove the enumeration from the using enumerations before removing.");
            return;
        }
        // else inform the user that the table is linked to a junction table
        if (!mapping.getFields().isEmpty()) {
            // replace the used enums with underlying types
            mapping.unlinkFromFields();
        }

        if (mapping.hasChildEnumeration()) mapping.getChild().removeParent(mapping);
        super.removeModel(mapping);
    }
}
