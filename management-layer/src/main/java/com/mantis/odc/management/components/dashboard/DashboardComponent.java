/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.dashboard;

import com.mantis.odc.database.AppDB;
import com.mantis.odc.database.base.DAO;
import com.mantis.odc.management.components.base.BaseCustomComponent;
import com.mantis.odc.management.components.dashboard.widgets.OperationsByMappingsWidget;
import com.mantis.odc.management.components.dashboard.widgets.OperationsByTimeWidget;
import com.mantis.odc.management.components.dashboard.widgets.base.AbstractWidget;
import com.mantis.odc.models.User;
import com.mantis.odc.models.WidgetConfig;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.VerticalLayout;
import lombok.Data;
import lombok.val;
import org.vaadin.alump.gridstack.GridStackLayout;
import org.vaadin.alump.gridstack.GridStackMoveEvent;

import javax.inject.Inject;
import java.util.*;

import static com.vaadin.server.FontAwesome.DASHBOARD;
import static com.vaadin.ui.themes.ValoTheme.COMBOBOX_TINY;

/**
 * Primary dashboard component.
 */
@Data
public class DashboardComponent extends BaseCustomComponent {

    private final DAO<String, User> userDao;
    private final User adminUser;
    private final OperationsByMappingsWidget operationsByMappingsWidget;
    private final OperationsByTimeWidget operationsByTimeWidget;
    private final ComboBox widgetList;
    private final GridStackLayout layout;
    private final List<AbstractWidget> widgets;

    @Inject
    public DashboardComponent(AppDB db, OperationsByMappingsWidget operationsByMappingsWidget, OperationsByTimeWidget operationsByTimeWidget) {
        this.userDao = db.getDAO(User.class);
        this.adminUser = userDao.get("admin").get();
        this.operationsByMappingsWidget = operationsByMappingsWidget;
        this.operationsByTimeWidget = operationsByTimeWidget;

        widgets = Arrays.asList(operationsByMappingsWidget, operationsByTimeWidget);

        widgetList = new ComboBox() {{
            setCaption("Widgets");
            setIcon(DASHBOARD);
            setStyleName(COMBOBOX_TINY);
            widgets.forEach(w -> {
                addItem(w);
                setItemCaption(w, w.title());
            });
            addValueChangeListener(event -> {
                selectWidget((AbstractWidget) getValue());
                setValue(null);
            });
        }};

        layout = new GridStackLayout(4) {{
            setSizeFull();

            // resize widgets.
            addGridStackMoveListener(events -> {
                // save config
                saveConfigs(events);

                events.forEach(e -> {
                    // refresh widget size if the height or width has changed.
                    if (AbstractWidget.class.isAssignableFrom(e.getMovedChild().getClass())
                            && (e.getOld().getHeight() != e.getNew().getHeight() || e.getOld().getWidth() != e.getNew().getWidth())) {
                        ((AbstractWidget) e.getMovedChild()).refreshOnSizeChange();
                    }
                });
            });
        }};

        // add widgets
        adminUser.getConfigList().forEach(wc -> getWidget(wc.getId()).ifPresent(w -> {
            w.setWidgetWidth(wc.getWidth());
            w.setWidgetHeight(wc.getHeight());
            w.setWidgetX(wc.getX());
            w.setWidgetY(wc.getY());
            w.configure(wc.getConfig());
            w.addConfigUpdatedListener(event ->
                    saveConfig((AbstractWidget) event.getComponent(),
                            w.getWidgetHeight(),
                            w.getWidgetWidth(),
                            w.getWidgetX(),
                            w.getWidgetY(),
                            event.getConfig()));
            selectWidget(w);
        }));

        setCompositionRoot(new VerticalLayout() {{
            setSizeFull();
            setSpacing(true);
            addComponents(new FormLayout(widgetList) {{
                setMargin(new MarginInfo(false, true));
            }}, layout);
        }});
    }

    /**
     * Saves the current config
     *
     * @param events widget events to save
     */
    private void saveConfigs(Collection<GridStackMoveEvent> events) {
        for (val e : events) {
            val widget = (AbstractWidget) e.getMovedChild();
            val nc = e.getNew();
            saveConfig(widget, nc.getHeight(), nc.getWidth(), nc.getX(), nc.getY(), widget.config());
        }
    }

    private void saveConfig(AbstractWidget widget, int height, int width, int x, int y, Map<String, String> widgetConfig) {
        val configOpt = getWidgetConfig(widget.id());
        WidgetConfig config;
        if (!configOpt.isPresent()) {
            config = new WidgetConfig();
            adminUser.getConfigList().add(config);
        } else {
            config = configOpt.get();
        }
        config.setId(widget.id());
        config.setHeight(height);
        config.setWidth(width);
        config.setUser(adminUser);
        config.setX(x);
        config.setY(y);
        config.setConfig(widgetConfig);
        userDao.update(adminUser);
    }

    /**
     * Returns the given widget.
     *
     * @param id widget id.
     * @return widget
     */
    private Optional<AbstractWidget> getWidget(String id) {
        for (val widget : widgets) {
            if (widget.id().equals(id)) return Optional.of(widget);
        }
        return Optional.empty();
    }

    /**
     * Returns the given widget config.
     *
     * @param id widget id.
     * @return widget config
     */
    private Optional<WidgetConfig> getWidgetConfig(String id) {
        for (val wc : adminUser.getConfigList()) {
            if (wc.getId().equals(id)) return Optional.of(wc);
        }
        return Optional.empty();
    }

    /**
     * Selects a given widget.
     *
     * @param widget widget to select
     */
    private void selectWidget(AbstractWidget widget) {
        if (widget != null) {
            for (val w : layout) {
                if (w == widget) {
                    layout.removeComponent(widget);
                    adminUser.removeConfig(widget.id());
                    userDao.update(adminUser);
                    return;
                }
            }
            layout.addComponent(widget, widget.getWidgetX(), widget.getWidgetY(), widget.getWidgetWidth(), widget.getWidgetHeight(), true);
            layout.setComponentSizeLimits(widget, widget.minWidth(), widget.maxWidth(), widget.minHeight(), widget.maxHeight());
            widget.getCloseButton().addClickListener(event -> selectWidget(widget));
        }
    }

    public void updateData() {
        for (val w : layout) ((AbstractWidget) w).updateData();
    }
}
