/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.mappings.tablemappings.fieldmappings;

import com.mantis.odc.data.ReferenceEqualityArray;
import com.mantis.odc.management.components.base.BaseCRUDFormComponent;
import com.mantis.odc.management.components.base.BaseSubFormListComponent;
import com.mantis.odc.management.components.extentions.AdvancedTable;
import com.mantis.odc.management.components.extentions.BooleanField;
import com.mantis.odc.management.components.mappings.tablemappings.TableMappingFormComponent;
import com.mantis.odc.models.EnumerationMapping;
import com.mantis.odc.models.FieldMapping;
import com.mantis.odc.models.TableMapping;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import lombok.Data;
import lombok.val;
import org.vaadin.viritin.ListContainer;

import java.util.List;
import java.util.Optional;

import static com.vaadin.server.FontAwesome.COLUMNS;
import static com.vaadin.server.Sizeable.Unit.PIXELS;
import static com.vaadin.ui.themes.ValoTheme.BUTTON_PRIMARY;

/**
 * Field mapping list.
 */
@Data
public class FieldMappingListComponent extends BaseSubFormListComponent<FieldMapping, TableMapping> {

    private boolean isViewMode;
    private final List<EnumerationMapping> enumerationMappings;
    private final TableMapping parentModel;
    private final TableMappingFormComponent parentForm;
    private final String modelName = "Field Mapping";
    private final List<FieldMapping> items = new ReferenceEqualityArray<>();
    private final ListContainer<FieldMapping> container;
    private final Button addFieldMappingButton;
    private final AdvancedTable<FieldMapping, FieldMapping> table;
    private final VerticalLayout layout;

    public FieldMappingListComponent(TableMapping parentModel, List<FieldMapping> fieldMappings, List<EnumerationMapping> enumerationMappings, TableMappingFormComponent parentForm) {
        this.enumerationMappings = enumerationMappings;
        this.parentModel = parentModel;
        this.parentForm = parentForm;
        items.addAll(fieldMappings);

        container = new ListContainer<>(FieldMapping.class, items);

        table = new AdvancedTable<FieldMapping, FieldMapping>() {{
            setId("fieldMappingList");
            setWidth(1100, PIXELS);
            setHeight(450, PIXELS);
            setActionProvider(Optional.of(mapping -> getActions()));
            setContainerDataSource(container);
            addColumn("actions", mapping -> getActionsColumn(mapping));
            addColumn("source", mapping -> mapping.getFieldId().getSource());
            addColumn("opaName", mapping -> mapping.getFieldId().getOpaName());
            addColumn("typeDisplay", mapping -> mapping.getType().getDisplayName());
            addColumn("enumerationName", mapping -> mapping.getEnumeration() != null ? mapping.getEnumeration().getName() : null);
            addColumn("primaryKeyBoolean", mapping -> new BooleanField(mapping.isPrimaryKey()));
            addColumn("autoIncrementBoolean", mapping -> new BooleanField(mapping.isAutoIncrement()));
            addColumn("foreignKeyBoolean", mapping -> new BooleanField(mapping.isForeignKey()));
            addColumn("requiredBoolean", mapping -> new BooleanField(mapping.isRequired()));
            addColumn("inputBoolean", mapping -> new BooleanField(mapping.isInput()));
            addColumn("outputBoolean", mapping -> new BooleanField(mapping.isOutput()));
            setVisibleColumns("actions", "source", "opaName", "primaryKeyBoolean", "autoIncrementBoolean", "sequence", "foreignKeyBoolean", "typeDisplay", "enumerationName", "requiredBoolean", "inputBoolean", "outputBoolean");
            setColumnHeaders("", "Source", "OPA Name", "Primary Key", "Auto Increment", "Sequence", "Foreign Key", "Data Type", "Enumeration", "Required", "Input", "Output");
            setColumnWidth("actions", 130);
        }};

        addFieldMappingButton = new Button() {{
            setId("addFieldMapping");
            setCaption("Add Field Mapping");
            setIcon(COLUMNS);
            setStyleName(BUTTON_PRIMARY);
            addClickListener((event) -> showCreateModelWindow());
        }};

        layout = new VerticalLayout() {{
            setSizeFull();
            setSpacing(true);
            setMargin(true);
            addComponents(addFieldMappingButton, table);
        }};

        setCompositionRoot(layout);
    }

    @Override
    public void setEnabled(boolean enabled) {
        addFieldMappingButton.setVisible(enabled);
        isViewMode = !enabled;
    }

    @Override
    public FieldMapping createNewModelInstance() {
        return new FieldMapping();
    }

    @Override
    public BaseCRUDFormComponent<FieldMapping> getForm(FieldMapping model) {
        val form = new FieldMappingFormComponent(
                this,
                getSiblings(model),
                parentForm.hasParentTable(),
                parentForm.getParentPrimaryKeyType(),
                enumerationMappings);
        form.setValue(model);
        return form;
    }

    @Override
    public List<FieldMapping> getItemsFromParent(TableMapping tableMapping) {
        return tableMapping.getFields();
    }

    @Override
    protected void saveItemsToParent(TableMapping tableMapping, List<FieldMapping> items) {
        items.forEach(fieldMapping -> fieldMapping.setTable(tableMapping));
        tableMapping.setFields(items);
    }

    /**
     * Shows a new field dialog.
     */
    void showCreateNewField() {
        showCreateModelWindow();
    }

    @Override
    protected void removeModel(FieldMapping mapping) {
        // if the field is used by a bind mapping
        if (!mapping.getBinds().isEmpty())
            showErrorMessage("The field is linked to a bind mapping, remove the field from all bind mappings before removing.");
        else {
            // remove the field from the parent
            if (mapping.getTable() != null)
                mapping.getTable().removeFieldMapping(mapping);
            super.removeModel(mapping);
        }
    }
}
