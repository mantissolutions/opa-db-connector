/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.datasources;

import com.mantis.odc.models.DatasourceMapping;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import lombok.Data;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.val;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;

import static com.mantis.odc.utilities.ErrorUtil.errorToString;
import static com.vaadin.server.FontAwesome.CLOSE;

/**
 * Returns an active connection for a given data source mapping.
 */
@Getter
public class DatasourceConnectionDialog extends TestDatasourceDialog {

    private final Button cancelButton;

    public DatasourceConnectionDialog(DatasourceMapping mapping) {
        super(mapping);
        setCaption(" Connect to Datasource.");
        setModal(true);
        setClosable(false);
        setResizable(false);
        getTestButton().setCaption("Connect");

        cancelButton = new Button() {{
            setId("cancel");
            setCaption("Cancel");
            setIcon(CLOSE);
            addClickListener(event -> close());
        }};

        getButtonLayout().addComponent(cancelButton);
    }

    @Override
    protected void testDatasource(Button.ClickEvent clickEvent) {
        try {
            val connection = getMapping().getConnection(getUsernameField().getValue(), getPasswordField().getValue());
            fireEvent(new ActiveConnectionEvent(this, connection));
            close();
        } catch (ClassNotFoundException | SQLException e) {
            setMessage("Connection Failed: " + e.getMessage(), errorToString(e), FontAwesome.EXCLAMATION);
        }
    }

    /**
     * @param listener form listener to add
     */
    @SneakyThrows
    public void addActiveConnectionListener(ActiveConnectionListener listener) {
        Method method = ActiveConnectionListener.class.getDeclaredMethod("formSaved", ActiveConnectionEvent.class);
        addListener(ActiveConnectionEvent.class, listener, method);
    }

    /**
     * @param listener listener to remove
     */
    public void removeActiveConnectionListener(ActiveConnectionListener listener) {
        removeListener(ActiveConnectionListener.class, listener);
    }

    /**
     * Listens on active connection events.
     */
    public interface ActiveConnectionListener {
        /**
         * @param event active connection event
         */
        void formSaved(ActiveConnectionEvent event);
    }

    /**
     * Created on an active connection.
     */
    @Data
    public static class ActiveConnectionEvent extends Event {

        private final Connection connection;

        /**
         * Constructs a new event with the specified source component.
         *
         * @param source     the source component of the event
         * @param connection active connection
         */
        @SneakyThrows
        public ActiveConnectionEvent(Component source, Connection connection) {
            super(source);
            this.connection = connection;
        }
    }
}
