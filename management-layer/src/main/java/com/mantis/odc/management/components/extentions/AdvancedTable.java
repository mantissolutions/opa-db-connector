/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.management.components.extentions;

import com.mantis.odc.management.components.models.ActionModel;
import com.vaadin.event.Action;
import com.vaadin.ui.Table;
import lombok.Data;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * Advanced table.
 */
@Data
public class AdvancedTable<ID, MODEL> extends Table {

    private Optional<Function<MODEL, List<ActionModel<MODEL>>>> actionProvider = Optional.empty();

    /**
     * Model resolver. This can be overridden for containers which have alternate identifying methods.
     */
    private Function<ID, MODEL> keyToModelResolver = key -> (MODEL) key;

    public AdvancedTable() {
        addActionHandler(new AdvancedActionHandler());
    }

    /**
     * Adds a generated column to the table.
     *
     * @param name   column name
     * @param getter value producer.
     * @param <T>    value type
     */
    public <T> void addColumn(String name, Function<MODEL, T> getter) {
        addGeneratedColumn(name, (source, itemId, columnId) -> getter.apply(keyToModelResolver.apply((ID) itemId)));
    }

    /**
     * ActionModel handler for the advanced table.
     */
    private class AdvancedActionHandler implements Action.Handler {
        @Override
        public Action[] getActions(Object target, Object sender) {
            return actionProvider.map(modelListFunction -> modelListFunction.apply(keyToModelResolver.apply((ID) target)).stream()
                    .filter(a -> !a.isRequiresModel() || target != null)
                    .toArray(Action[]::new))
                    .orElse(new Action[0]);
        }

        @Override
        public void handleAction(Action action, Object sender, Object target) {
            ActionModel<MODEL> actionModel = (ActionModel<MODEL>) action;
            actionModel.getConsumer().accept(keyToModelResolver.apply((ID) target));
        }
    }
}
