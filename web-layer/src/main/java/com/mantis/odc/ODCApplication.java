/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import com.mantis.docs.DocumentationServlet;
import com.mantis.odc.config.AppConfig;
import com.mantis.odc.database.AppDB;
import com.mantis.odc.di.DaggerODCRootComponent;
import com.mantis.odc.di.ODCRootComponent;
import com.mantis.odc.management.ManagementServlet;
import com.mantis.odc.security.AppEnvironment;
import com.mantis.odc.services.jaxrs.ServiceApplication;
import com.mantis.odc.services.storage.DatasourceStorage;
import com.mantis.odc.services.storage.OperationStorage;
import com.mantis.odc.services.storage.ServiceStorage;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.shiro.web.servlet.ShiroFilter;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import static com.mantis.odc.management.MainUI.MANAGEMENT_INJECTOR;
import static com.mantis.odc.services.jaxrs.ServiceApplication.SERVICE_INJECTOR;
import static org.apache.shiro.web.env.EnvironmentLoader.ENVIRONMENT_ATTRIBUTE_KEY;
import static org.glassfish.jersey.servlet.ServletProperties.JAXRS_APPLICATION_CLASS;

/**
 * Root ODC application.
 */
@Slf4j
public class ODCApplication implements ServletContextListener {

    private static final ODCRootComponent INJECTOR = DaggerODCRootComponent.create();

    private AppDB appDB;

    private DatasourceStorage datasourceStorage;
    private ServiceStorage serviceStorage;
    private OperationStorage operationStorage;

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        // setup logger
        setupLogger(INJECTOR.getSharedSubComponent().getConfig());

        log.info("Starting ODC server...");
        val ctx = sce.getServletContext();

        // create sub components
        val sharedSubComponent = INJECTOR.getSharedSubComponent();
        val serviceSubComponent = INJECTOR.getServiceSubComponent();
        val managementSubComponent = INJECTOR.getManagementSubComponent();

        appDB = sharedSubComponent.getDatabase();
        datasourceStorage = serviceSubComponent.getDatasourceStorage();
        serviceStorage = serviceSubComponent.getServiceStorage();
        operationStorage = serviceSubComponent.getOperationStorage();

        // add the security services
        // add environment to context
        setupSecurityEnvironment(sharedSubComponent.getAppEnvironment(), ctx);
        val securityFilter = ctx.addFilter("SecurityFilter", ShiroFilter.class.getCanonicalName());
        securityFilter.setAsyncSupported(true);
        securityFilter.addMappingForUrlPatterns(null, false, "/*");

        // operation statistics
        val operationStatsFilter = ctx.addFilter("OperationStatistics", serviceSubComponent.getOperationStatisticsFilter());
        operationStatsFilter.addMappingForUrlPatterns(null, true, "/services/*");

        // add rest services
        val serviceLayer = ctx.addFilter("ServiceLayer", ServletContainer.class);
        ctx.setAttribute(SERVICE_INJECTOR, serviceSubComponent);
        serviceLayer.setInitParameter(JAXRS_APPLICATION_CLASS, ServiceApplication.class.getCanonicalName());
        serviceLayer.addMappingForUrlPatterns(null, true, "/application.wadl", "/services/*",
                "/service-datasource-management/*", "/service-management/*", "/mappings/*", "/datasources/*", "/support/*");

        // add documentation
        val documentation = ctx.addServlet("Documentation", DocumentationServlet.class);
        documentation.addMapping("/documentation");

        // add management gui
        val managementLayer = ctx.addServlet("ManagementLayer", ManagementServlet.class);
        ctx.setAttribute(MANAGEMENT_INJECTOR, managementSubComponent);
        managementLayer.setInitParameter("org.atmosphere.cpr.AtmosphereInterceptor", "org.atmosphere.interceptor.ShiroInterceptor");
        managementLayer.setAsyncSupported(true);
        managementLayer.addMapping("/management/*", "/VAADIN/*");

        log.info("Startup complete.");
    }

    /**
     * Resets the logback config.
     *
     * @param config application help
     */
    @SneakyThrows
    private void setupLogger(AppConfig config) {
        val file = config.getLogbackConfig();
        if (file.exists()) {
            val loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
            loggerContext.reset();
            val configurator = new JoranConfigurator();
            configurator.setContext(loggerContext);
            configurator.doConfigure(file); // loads logback file
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("Shutting down ODC server...");
        log.info("Closing App database...");
        appDB.close();
        log.info("App database closed.");
        log.info("Closing Deployed Services...");
        datasourceStorage.close();
        serviceStorage.close();
        operationStorage.close();
        log.info("Deployed Services closed.");
        log.info("Shutdown complete.");
    }

    /**
     * Sets the security environment.
     *
     * @param appEnvironment security manager
     * @param ctx            servlet context
     */
    private void setupSecurityEnvironment(AppEnvironment appEnvironment, ServletContext ctx) {
        appEnvironment.setServletContext(ctx);
        ctx.setAttribute(ENVIRONMENT_ATTRIBUTE_KEY, appEnvironment);
    }
}
