/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.di;

import com.mantis.odc.management.di.ManagementSubComponent;
import com.mantis.odc.services.di.ServiceModule;
import com.mantis.odc.services.di.ServiceSubComponent;
import dagger.Component;

import javax.inject.Singleton;

/**
 * Root application injector.
 */
@Singleton
@Component(modules = {SharedModule.class, ServiceModule.class})
public interface ODCRootComponent {

    // SUB-COMPONENTS //

    SharedSubComponent getSharedSubComponent();

    ServiceSubComponent getServiceSubComponent();

    ManagementSubComponent getManagementSubComponent();
}
