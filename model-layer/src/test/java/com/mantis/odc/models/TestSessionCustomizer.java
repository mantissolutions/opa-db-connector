/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models;

import org.eclipse.persistence.config.SessionCustomizer;
import org.eclipse.persistence.sessions.Session;

/**
 * Customises the JPA entity factory for validation on startup.
 */
public class TestSessionCustomizer implements SessionCustomizer {
    @Override
    public void customize(Session session) {
        session.getIntegrityChecker().checkDatabase();
        session.getIntegrityChecker().setShouldCatchExceptions(false);
        session.getPlatform().setShouldForceFieldNamesToUpperCase(false);
    }
}
