/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models;

import lombok.val;
import org.flywaydb.core.Flyway;
import org.junit.Test;

import javax.persistence.Persistence;
import java.util.HashMap;

import static org.eclipse.persistence.config.PersistenceUnitProperties.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the model layer against a mock database and the current database migration.
 */
public class TestSchema {

    private static final String USER = "odc";
    private static final String PASSWORD = "odc";

    @Test
    public void testMysqlSchema() {
        testSchema("jdbc:h2:mem:test;MODE=MySQL;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false", "classpath:/db/migration/mysql");
    }

    @Test
    public void testOracleSchema() {
        testSchema("jdbc:h2:mem:test2;MODE=Oracle;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false", "classpath:/db/migration/oracle");
    }

    private void testSchema(String url, String migrationsPath) {
        // create migration
        val flyway = new Flyway();
        flyway.setDataSource(url, USER, PASSWORD);
        flyway.setLocations(migrationsPath);

        // populate database
        flyway.clean();
        flyway.migrate();

        // test in a new connection if the migration has been added
        val info = flyway.info();

        assertEquals(0, info.pending().length);
        assertEquals(17, info.applied().length); // this needs to be updated on each new migration

        // test JPA against created migration
        val properties = new HashMap<String, String>();
        properties.put(SESSION_CUSTOMIZER, TestSessionCustomizer.class.getName());
        properties.put(JDBC_DRIVER, "org.h2.Driver");
        properties.put(JDBC_URL, url);
        properties.put(JDBC_USER, USER);
        properties.put(JDBC_PASSWORD, PASSWORD);
        val emf = Persistence.createEntityManagerFactory("root-pu", properties);
        val em = emf.createEntityManager();
        assertNotNull(em);
    }

}
