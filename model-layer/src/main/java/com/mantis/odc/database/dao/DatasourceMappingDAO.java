/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.database.dao;

import com.mantis.odc.database.AppDB;
import com.mantis.odc.database.base.DAO;
import com.mantis.odc.models.DatasourceMapping;
import lombok.val;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Optional;

/**
 * DatasourceMapping DAO.
 */
public class DatasourceMappingDAO extends DAO<BigDecimal, DatasourceMapping> {

    @Inject
    public DatasourceMappingDAO(AppDB appDB) {
        super(DatasourceMapping.class, appDB);
    }

    /**
     * Returns the named mapping.
     *
     * @param name mapping name
     * @return mapping
     */
    public Optional<DatasourceMapping> getDatasourceMappingByName(String name) {
        val params = new HashMap<String, Object>();
        params.put("name", name);
        return Optional.ofNullable(scalarQuery("SELECT d FROM DatasourceMapping d WHERE d.name = :name", params));
    }

    /**
     * Creates or loads and existing data source mapping with the given name. Because they are shared between mappings
     * they are never updated here. Updating a data source requires that all mappings that use it be disabled and
     * may require new data sources.
     *
     * @param datasource data source mapping
     * @return existing or created datas ource mapping
     */
    DatasourceMapping createOrReturnDatasource(DatasourceMapping datasource) {
        return getDatasourceMappingByName(datasource.getName())
                .orElseGet(() -> {
                    datasource.getMappings().clear();
                    return create(datasource);
                });
    }
}
