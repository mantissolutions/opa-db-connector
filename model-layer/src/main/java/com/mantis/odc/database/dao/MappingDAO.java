/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.database.dao;

import com.mantis.odc.database.AppDB;
import com.mantis.odc.database.base.DAO;
import com.mantis.odc.models.Mapping;
import lombok.val;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * Mapping DAO class.
 */
public class MappingDAO extends DAO<BigDecimal, Mapping> {

    private final DatasourceMappingDAO datasourceMappingDAO;

    @Inject
    public MappingDAO(AppDB appDB) {
        super(Mapping.class, appDB);
        datasourceMappingDAO = new DatasourceMappingDAO(appDB);
    }

    /**
     * Returns the named mapping.
     *
     * @param name mapping name
     * @return mapping
     */
    public Optional<Mapping> getMappingByName(String name) {
        val params = new HashMap<String, Object>();
        params.put("name", name);
        return Optional.ofNullable(scalarQuery("SELECT m FROM Mapping m WHERE m.name = :name", params));
    }

    /**
     * @return returns mappings to deploy
     */
    public List<Mapping> getDeployableMappings() {
        return query("SELECT m FROM Mapping m WHERE m.deployOnStartup = true", null);
    }

    @Override
    public Mapping create(Mapping mapping) {
        // saveButton or update data source
        mapping.setDatasource(datasourceMappingDAO.createOrReturnDatasource(mapping.getDatasource()));
        return super.create(mapping);
    }
}
