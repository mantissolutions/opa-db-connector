/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models;

import com.mantis.odc.models.base.Duplicatable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Models a checkpoint table mapping.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "odc_checkpoint_mappings")
public class CheckpointMapping implements Duplicatable<CheckpointMapping> {

    @Id
    @SequenceGenerator(name = "checkpoint_mapping_seq", sequenceName = "odc_checkpoint_mappings_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "checkpoint_mapping_seq")
    @Column(name = "ch_id", nullable = false)
    private BigDecimal id;
    @Column(name = "ch_bind_token", nullable = false)
    private String bindToken = "checkpointId";
    @Column(name = "ch_table_name", nullable = false)
    private String tableName = "CHECKPOINTS";
    @Column(name = "ch_id_field", nullable = false)
    private String idField = "CH_ID";
    @Column(name = "ch_data_field", nullable = false)
    private String dataField = "CH_DATA";

    /**
     * @return checkpoint table fields
     */
    public List<String> getFields() {
        return Arrays.asList(idField, dataField);
    }

    @Override
    public CheckpointMapping copy() {
        val cm = new CheckpointMapping();
        cm.setBindToken(bindToken);
        cm.setTableName(tableName);
        cm.setIdField(idField);
        cm.setDataField(dataField);
        return cm;
    }
}
