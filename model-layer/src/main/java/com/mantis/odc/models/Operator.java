/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models;

import lombok.Getter;

/**
 * List of supported ANSI operators.
 */
@Getter
public enum Operator {

    /**
     * Equals operator.
     */
    equals("Equals", "equals", "=", "Equal to", true),

    /**
     * Not equals operator.
     */
    notEquals("Not Equals", "notEquals", "<>", "Not equal to", true),

    /**
     * Greater than operator.
     */
    greaterThan("Greater Than", "greaterThan", ">", "Greater than", true),

    /**
     * Less than operator.
     */
    lessThan("Less Than", "lessThan", "<", "Less than", true),

    /**
     * Greater than or equal operator.
     */
    greaterThanOrEqual("Greater Than or Equal", "greaterThanOrEqual", ">=", "Greater than or equal", true),

    /**
     * Less than or equal operator.
     */
    lessThanOrEqual("Less Than or Equal", "lessThanOrEqual", "<=", "Less than or equal", true),

    /**
     * Like operator.
     */
    like("Like", "like", "LIKE", "Match a character pattern. '%' matches any '_' matches one", true),

    /**
     * In operator.
     */
    in("In", "in", "IN", "Equal to one of multiple possible values", true),

    /**
     * Is null operator.
     */
    isNull("Is Null", "isNull", "IS NULL", "Compare to null values", false),

    /**
     * Is not null operator
     */
    isNotNull("Is Not Null", "isNotNull", "IS NOT NULL", "Compare to not null values", false);

    private final String name;
    private final String XMLName;
    private final String sqlOperator;
    private final String description;
    private final boolean hasValue;

    /**
     * @param name        name of the operator
     * @param xmlName     xml name
     * @param sqlOperator sql operator
     * @param description description of operation
     * @param hasValue    flag to indicate a bind is required for the condition
     */
    Operator(String name, String xmlName, String sqlOperator, String description, boolean hasValue) {
        this.name = name;
        XMLName = xmlName;
        this.sqlOperator = sqlOperator;
        this.description = description;
        this.hasValue = hasValue;
    }
}
