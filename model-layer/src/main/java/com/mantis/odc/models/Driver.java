/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models;

import lombok.Getter;

import static com.mantis.odc.models.SupportedCase.*;
import static java.text.MessageFormat.format;

/**
 * Base Driver type enum.
 */
@Getter
public enum Driver {

    /**
     * DB2 Database driver type.
     */
    DB2_T4("IBM DB2 Universal Driver Type 4",
            "DB2", "4.19.26",
            "For information on the driver see <a target='_blank' href='http://www-01.ibm.com/support/knowledgecenter/SSEPGG_10.5.0/com.ibm.swg.im.dbclient.install.doc/doc/c0022612.html?cp=SSEPGG_10.5.0%2F1-2'>here</a>.",
            "com.ibm.db2.jcc.DB2Driver",
            "SELECT current date FROM sysibm.sysdummy1",
            "sysibm.sysdummy1", UNCHANGED),

    /**
     * Derby database driver type.
     */
    Derby("Apache Derby Client JDBC Driver",
            "Derby", "10.11.1.1",
            "The Derby client JDBC driver, used to connect to a Derby server over a network connection. For more information on the driver see <a target='_blank' href='http://db.apache.org/derby/'>here</a>.",
            "org.apache.derby.jdbc.EmbeddedDriver",
            "SELECT 1 FROM SYSIBM.SYSDUMMY1",
            "SYSIBM.SYSDUMMY1", UNCHANGED),

    /**
     * H2 database driver type.
     */
    H2("H2 Database Driver",
            "h2", "1.4.186",
            "For more information on the driver see <a target='_blank' href='http://www.h2database.com/html/main.html'>here</a>.",
            "org.h2.Driver",
            "SELECT 1",
            "", UPPER),

    /**
     * HSQLDB database driver type.
     */
    HSQLDB("HSQLDB (HyperSQL DataBase) Driver",
            "HSQLDB", "1.8.0.10",
            "For more information on the driver see <a target='_blank' href='http://hsqldb.org/'>here</a>.",
            "org.hsqldb.jdbc.JDBCDriver",
            "SELECT 1 FROM any_existing_table WHERE 1=0",
            "any_existing_table WHERE 1=0", UPPER),

    /**
     * MySQL database driver type.
     */
    MySQL("MySQL Java Connector",
            "MySQL", "5.1.34",
            "For more information on the driver see <a target='_blank' href='https://www.mysql.com/products/connector/'>here</a>.",
            "com.mysql.jdbc.Driver",
            "SELECT 1",
            "", UNCHANGED),

    /**
     * Oracle 12c database driver type.
     */
    Oracle("Oracle Database JDBC Driver",
            "Oracle", "12.1.0.2",
            "For more information on the driver see <a target='_blank' href='https://docs.oracle.com/database/121/JJDBC/toc.htm'>here</a>.",
            "oracle.jdbc.OracleDriver",
            "SELECT 1 FROM DUAL",
            "DUAL", UPPER),

    /**
     * PostgreSQL database driver type.
     */
    PostgreSQL_T4("PostgreSQL Driver JDBC4",
            "PostgreSQL", "9.1-901-1.jdbc4",
            "For more information on the driver see <a target='_blank' href='https://jdbc.postgresql.org/documentation/documentation.html'>here</a>.",
            "org.postgresql.Driver",
            "SELECT 1",
            "", LOWER),

    /**
     * SQLite database driver type
     */
    SQLite("SQLite JDBC library",
            "SQLite", "3.8.10.1",
            "For more information on the driver see <a target='_blank' href='https://bitbucket.org/xerial/sqlite-jdbc'>here</a>.",
            "org.sqlite.JDBC",
            "SELECT 1",
            "", UPPER),

    /**
     * SQLServer database driver type
     */
    SQLServer_T4("Microsoft JDBC Driver for SQL Server",
            "SQLServer", "4.1.5605.100",
            "For more information on the driver see <a target='_blank' href='https://msdn.microsoft.com/en-us/library/ms378749(v=sql.110).aspx'>here</a>.",
            "com.microsoft.sqlserver.jdbc.SQLServerDriver",
            "SELECT 1",
            "", UNCHANGED);

    private final String name;
    private final String XMLName;
    private final String version;
    private final String description;
    private final String className;
    private final String testQuery;
    private final String systemTable;
    private final SupportedCase supportedCase;

    /**
     * @param name                  driver name
     * @param xmlName               XML name
     * @param version               driver version
     * @param description           driver description
     * @param className             driver class name
     * @param testQuery             test query for the driver
     * @param systemTable           system table name
     * @param useUpperMetaDataNames sets the driver case level to use
     */
    Driver(String name, String xmlName, String version, String description,
           String className, String testQuery, String systemTable, SupportedCase useUpperMetaDataNames) {
        this.name = name;
        XMLName = xmlName;
        this.version = version;
        this.description = description;
        this.className = className;
        this.testQuery = testQuery;
        this.systemTable = systemTable;
        this.supportedCase = useUpperMetaDataNames;
    }

    @Override
    public String toString() {
        return format("{0}: {1}", name, version);
    }
}
