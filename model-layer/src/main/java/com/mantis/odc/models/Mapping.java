/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.models;

import com.mantis.odc.models.base.Duplicatable;
import com.mantis.odc.models.base.JPAUtility;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Collections.unmodifiableList;
import static javax.persistence.AccessType.FIELD;
import static javax.persistence.AccessType.PROPERTY;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Base mapping class.
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "odc_mappings")
@Access(value = FIELD)
@ToString
@EqualsAndHashCode
public class Mapping implements Duplicatable<Mapping> {

    @Id
    @SequenceGenerator(name = "mappings_seq", sequenceName = "odc_mappings_seq", allocationSize = 1)
    @GeneratedValue(strategy = IDENTITY, generator = "mappings_seq")
    @Column(name = "m_id", nullable = false)
    @Getter
    @Setter
    private BigDecimal id;
    @Getter
    @Setter
    @Column(name = "m_name", nullable = false)
    private String name;
    @Transient
    private DatasourceMapping datasource;
    @Getter
    @Setter
    @Column(name = "m_bind_value", nullable = false)
    private String bindToken = "rowId";
    @Getter
    @Setter
    @Column(name = "m_deploy_on_startup")
    private boolean deployOnStartup = false;
    @Getter
    @Setter
    @Column(name = "m_supports_attachments")
    private boolean supportsAttachments = false;
    @Getter
    @Setter
    @Column(name = "m_supports_checkpoints")
    private boolean supportsCheckpoints = false;
    @Getter
    @Setter
    @Column(name = "m_debug_mode")
    private boolean debugMode = false;
    @Transient
    @Setter
    private List<TableMapping> tables = new ArrayList<>();
    @Transient
    @Setter
    private List<JunctionTableMapping> junctionTables = new ArrayList<>();
    @JoinColumn(name = "am_id")
    @OneToOne(cascade = ALL, orphanRemoval = true)
    private AttachmentMapping attachment;
    @JoinColumn(name = "ch_id")
    @OneToOne(cascade = ALL, orphanRemoval = true)
    private CheckpointMapping checkpoint;
    @Transient
    @Setter
    private List<BindMapping> binds = new ArrayList<>();
    @Transient
    @Setter
    private List<EnumerationMapping> enumerations = new ArrayList<>();
    @Getter
    @Setter
    @Column(name = "m_pre_load_script")
    private String preLoadScript;
    @Getter
    @Setter
    @Column(name = "m_post_load_script")
    private String postLoadScript;
    @Getter
    @Setter
    @Column(name = "m_pre_save_script")
    private String preSaveScript;
    @Getter
    @Setter
    @Column(name = "m_post_save_script")
    private String postSaveScript;

    /**
     * @return get datasource
     */
    @OneToOne
    @JoinColumn(name = "ds_id")
    @Access(value = PROPERTY)
    public DatasourceMapping getDatasource() {
        return datasource;
    }

    /**
     * Sets the mapping datasource.
     *
     * @param datasource mapping datasource
     */
    public void setDatasource(DatasourceMapping datasource) {
        JPAUtility.mergeIntoParent(this, datasource, Mapping::getDatasource, DatasourceMapping::getMappings);
        this.datasource = datasource;
    }

    /**
     * @return table mappings
     */
    @Access(value = PROPERTY)
    @OneToMany(mappedBy = "mapping", targetEntity = TableMapping.class, cascade = ALL, orphanRemoval = true)
    public List<TableMapping> getTables() {
        return unmodifiableList(tables);
    }

    /**
     * @param tableMapping table mapping to add
     */
    public void addTableMapping(TableMapping tableMapping) {
        JPAUtility.addChild(this, tableMapping, m -> m.tables, TableMapping::setMapping);
    }

    /**
     * @param tableMapping table mapping to remove
     */
    public void removeTableMapping(TableMapping tableMapping) {
        JPAUtility.removeChild(this, tableMapping, m -> m.tables, TableMapping::setMapping);
    }

    /**
     * @return junction table mappings
     */
    @Access(value = PROPERTY)
    @OneToMany(mappedBy = "mapping", targetEntity = JunctionTableMapping.class, cascade = ALL, orphanRemoval = true)
    public List<JunctionTableMapping> getJunctionTables() {
        return unmodifiableList(junctionTables);
    }

    /**
     * @param junctionTableMapping junction table mapping to add
     */
    public void addJunctionTable(JunctionTableMapping junctionTableMapping) {
        JPAUtility.addChild(this, junctionTableMapping, m -> m.junctionTables, JunctionTableMapping::setMapping);
    }

    /**
     * @param junctionTableMapping junction table mapping to remove
     */
    public void removeJunctionTable(JunctionTableMapping junctionTableMapping) {
        JPAUtility.removeChild(this, junctionTableMapping, m -> m.junctionTables, JunctionTableMapping::setMapping);
    }

    /**
     * @return attachment mapping
     */
    public AttachmentMapping getAttachment() {
        return attachment != null ? attachment : new AttachmentMapping();
    }

    /**
     * @param attachment attachment table mapping
     */
    public void setAttachment(AttachmentMapping attachment) {
        this.attachment = attachment;
        this.supportsAttachments = attachment != null;
    }

    /**
     * @return attachment mapping
     */
    public CheckpointMapping getCheckpoint() {
        return checkpoint != null ? checkpoint : new CheckpointMapping();
    }

    /**
     * @param checkpoint checkpoint table mapping
     */
    public void setCheckpoint(CheckpointMapping checkpoint) {
        this.checkpoint = checkpoint;
        this.supportsCheckpoints = checkpoint != null;
    }

    /**
     * @return junction table mappings
     */
    @Access(value = PROPERTY)
    @OneToMany(mappedBy = "mapping", targetEntity = BindMapping.class, cascade = ALL, orphanRemoval = true)
    public List<BindMapping> getBinds() {
        return unmodifiableList(binds);
    }

    /**
     * @param bindMapping bind mapping to add
     */
    public void addBindMapping(BindMapping bindMapping) {
        JPAUtility.addChild(this, bindMapping, m -> m.binds, BindMapping::setMapping);
    }

    /**
     * @param bindMapping bind mapping to remove
     */
    public void removeBindMapping(BindMapping bindMapping) {
        JPAUtility.removeChild(this, bindMapping, m -> m.binds, BindMapping::setMapping);
    }

    /**
     * @return enumerations
     */
    @Access(value = PROPERTY)
    @OneToMany(mappedBy = "mapping", targetEntity = EnumerationMapping.class, cascade = ALL, orphanRemoval = true)
    public List<EnumerationMapping> getEnumerations() {
        return unmodifiableList(enumerations);
    }

    /**
     * @param enumerationMapping enumeration mapping to add
     */
    public void addEnumerationMapping(EnumerationMapping enumerationMapping) {
        JPAUtility.addChild(this, enumerationMapping, m -> m.enumerations, EnumerationMapping::setMapping);
    }

    /**
     * @param enumerationMapping enumeration mapping to remove
     */
    public void removeEnumerationMapping(EnumerationMapping enumerationMapping) {
        JPAUtility.removeChild(this, enumerationMapping, m -> m.enumerations, EnumerationMapping::setMapping);
    }

    /**
     * Returns the service name.
     *
     * @return service name
     */
    public String getServiceName() {
        return name.replace(" ", "_").toLowerCase() + "_" + id;
    }

    /**
     * Returns the mapping for the given id.
     *
     * @param id table id
     * @return table mapping
     */
    public Optional<TableMapping> getTable(OPAMappingId id) {
        return getTables().stream()
                .filter(t -> t.getTableId().equals(id))
                .findFirst();
    }

    /**
     * Returns the mapping for the given name.
     *
     * @param name table name
     * @return table mapping
     */
    public Optional<TableMapping> getTable(String name) {
        return getTables().stream()
                .filter(t -> t.getTableId().getName().equals(name))
                .findFirst();
    }

    /**
     * Returns the junction table for the given link name.
     *
     * @param linkName link name
     * @return junction table
     */
    public Optional<JunctionTableMapping> getJunctionTable(String linkName) {
        return getJunctionTables().stream()
                .filter(t -> t.getLinkName().equals(linkName))
                .findFirst();
    }

    /**
     * Returns all the table ids in the mapping.
     *
     * @return all table id values
     */
    private List<OPAMappingId> getTableIds() {
        return Stream.concat(
                getTables().stream().map(TableMapping::getTableId),
                getJunctionTables().stream().map(JunctionTableMapping::getTableId)
        ).collect(Collectors.toList());
    }

    /**
     * Returns the table alias for the given table id.
     *
     * @param tableId table id
     * @return alias
     */
    String getAlias(@NonNull OPAMappingId tableId, String prefix) {
        val ids = getTableIds();
        return IntStream.range(0, ids.size())
                .filter(i -> tableId.equals(ids.get(i)))
                .mapToObj(i -> prefix + (i + 1))
                .findFirst().orElse(prefix);
    }

    public Optional<EnumerationMapping> getEnumeration(String name) {
        return enumerations.stream()
                .filter(enumerationMapping -> enumerationMapping.getName().equals(name))
                .findFirst();
    }

    @Override
    public Mapping copy() {
        val m = new Mapping();
        m.setName(Mapping.this.name + " - copy");
        m.setDatasource(datasource);
        m.setAttachment(Mapping.this.supportsAttachments ? Mapping.this.attachment.copy() : null);
        m.setCheckpoint(Mapping.this.supportsCheckpoints ? Mapping.this.checkpoint.copy() : null);

        // add table mappings
        for (val t : getTables()) m.addTableMapping(t.copy());

        // update parent references
        m.getTables().stream().filter(t -> t.getParent() != null)
                .forEach(t -> t.setParent(m.getTable(t.getParent().getTableId()).get()));

        // set junction tables
        for (val jt : getJunctionTables()) m.addJunctionTable(jt.copy());

        // update table references
        m.getJunctionTables().forEach(jtm -> {
            jtm.setFromTable(m.getTable(jtm.getFromTable().getTableId()).get());
            jtm.setToTable(m.getTable(jtm.getToTable().getTableId()).get());
        });

        // set binds
        for (val b : getBinds()) m.addBindMapping(b.copy());

        // update table & field references
        m.getBinds().forEach(bm -> {
            bm.setTable(m.getTable(bm.getTable().getTableId()).get());
            bm.setField(bm.getTable().getField(bm.getField().getFieldId()).get());
        });

        // set enumerations
        for (val e : getEnumerations()) m.addEnumerationMapping(e.copy());

        // update all enumeration references
        m.getTables().stream().flatMap(tm -> tm.getFields().stream())
                .filter(fm -> fm.getEnumeration() != null)
                .forEach(fm -> fm.setEnumeration(m.getEnumeration(fm.getEnumeration().getName()).get()));
        m.getEnumerations().stream()
                .filter(EnumerationMapping::hasChildEnumeration)
                .forEach(em -> em.setChild(m.getEnumeration(em.getChild().getName()).get()));
        return m;
    }
}
