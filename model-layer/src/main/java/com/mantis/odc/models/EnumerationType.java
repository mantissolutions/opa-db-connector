/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Defines the supported enumeration types.
 */
@Getter
@RequiredArgsConstructor
public enum EnumerationType {
    StringEnumeration("StringEnumeration", "String", DataType.STRING_TYPE),
    NumberEnumeration("NumberEnumeration", "Number", DataType.DECIMAL_TYPE),
    DateEnumeration("DateEnumeration", "Date", DataType.DATE_TYPE),
    DateTimeEnumeration("DateTimeEnumeration", "Date Time", DataType.DATE_TIME_TYPE),
    TimeEnumeration("TimeEnumeration", "Time", DataType.TIME_TYPE),
    BooleanEnumeration("BooleanEnumeration", "Boolean", DataType.BOOLEAN_TYPE);

    private final String XMLName;
    private final String display;
    private final DataType dataType;
}
