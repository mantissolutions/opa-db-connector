/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models.base;

/**
 * Interface used to denote that a JPA model can be copied.
 *
 * @param <T> model type
 */
public interface Duplicatable<T> {

    /**
     * @return a copy of the current model.
     */
    T copy();
}
