/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.models;

import com.mantis.odc.models.base.Duplicatable;
import com.mantis.odc.models.base.JPAUtility;
import lombok.*;

import javax.persistence.*;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.Optional;

import static com.mantis.odc.models.LinkType.*;
import static javax.persistence.AccessType.FIELD;
import static javax.persistence.AccessType.PROPERTY;

/**
 * Models a junction table.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"mapping"})
@EqualsAndHashCode(of = {"id", "tableId"})
@Table(name = "odc_j_table_mappings")
@Access(value = FIELD)
public class JunctionTableMapping implements Duplicatable<JunctionTableMapping> {

    @Id
    @SequenceGenerator(name = "junction_table_mappings_seq", sequenceName = "odc_j_table_mappings_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "junction_table_mappings_seq")
    @Column(name = "jtm_id", nullable = false)
    private BigDecimal id;
    @Embedded
    private OPAMappingId tableId = new OPAMappingId();
    @Column(name = "jtm_link_name")
    private String linkName;
    @Enumerated(EnumType.STRING)
    @Column(name = "jtm_link_type", nullable = false)
    private LinkType linkType = ManyToMany;
    @Column(name = "jtm_from_key_name", nullable = false)
    private String fromKeyName;
    @Column(name = "jtm_to_key_name", nullable = false)
    private String toKeyName;
    @Transient
    private TableMapping fromTable;
    @Transient
    private TableMapping toTable;
    @ManyToOne
    @JoinColumn(name = "m_id")
    private Mapping mapping;

    /**
     * @return from table mapping
     */
    @ManyToOne
    @JoinColumn(name = "jtm_from_table_id")
    @Access(value = PROPERTY)
    public TableMapping getFromTable() {
        return fromTable;
    }

    /**
     * @return optional of the table mapping
     */
    public Optional<TableMapping> getFromTableOpt() {
        return Optional.ofNullable(fromTable);
    }

    /**
     * Sets the from table.
     *
     * @param fromTable from table
     */
    public void setFromTable(TableMapping fromTable) {
        JPAUtility.mergeIntoParent(this, fromTable, JunctionTableMapping::getFromTable, TableMapping::getFromJunctionTables);
        this.fromTable = fromTable;
    }

    /**
     * Removes the table from the junction.
     */
    public void removeFromTable() {
        if (fromTable != null)
            JPAUtility.removeChild(fromTable, this, TableMapping::getFromJunctionTables, JunctionTableMapping::removeTable);
    }

    /**
     * @return to table mapping
     */
    @ManyToOne
    @JoinColumn(name = "jtm_to_table_id")
    @Access(value = PROPERTY)
    public TableMapping getToTable() {
        return toTable;
    }

    /**
     * @return optional of the table mapping
     */
    public Optional<TableMapping> getToTableOpt() {
        return Optional.ofNullable(toTable);
    }

    /**
     * Sets the to table.
     *
     * @param toTable to table
     */
    public void setToTable(TableMapping toTable) {
        JPAUtility.mergeIntoParent(this, toTable, JunctionTableMapping::getToTable, TableMapping::getToJunctionTables);
        this.toTable = toTable;
    }

    /**
     * Removes the table from the junction.
     */
    public void removeToTable() {
        if (toTable != null)
            JPAUtility.removeChild(toTable, this, TableMapping::getToJunctionTables, JunctionTableMapping::removeTable);
    }

    /**
     * @return from table id
     */
    public Optional<OPAMappingId> getFromTableId() {
        return Optional.ofNullable(fromTable).map(TableMapping::getTableId);
    }

    /**
     * @return to table id
     */
    public Optional<OPAMappingId> getToTableId() {
        return Optional.ofNullable(toTable).map(TableMapping::getTableId);
    }

    /**
     * @return junction table alias
     */
    public String getAlias() {
        return mapping.getAlias(tableId, "jt");
    }

    /**
     * @return the linkName
     */
    public String getLinkName() {
        val fromId = getFromTableId();
        val toId = getToTableId();
        if (linkName == null && fromId.isPresent() && toId.isPresent())
            return (fromId.get().getName() + " to " + toId.get().getName()).toLowerCase();
        else return linkName;
    }

    /**
     * @return link name
     */
    public String getLinkNameNoDefault() {
        return linkName;
    }

    /**
     * Returns the target name for the given table id.
     *
     * @param tableId table id.
     * @return target name
     */
    public String getTargetName(OPAMappingId tableId) {
        return (getFromTableId().filter(id -> id.equals(tableId)).isPresent()) ?
                getToTableId().map(OPAMappingId::getName).orElse(null) :
                getFromTableId().map(OPAMappingId::getName).orElse(null);
    }

    @Override
    public JunctionTableMapping copy() {
        val jtm = new JunctionTableMapping();
        jtm.setTableId(tableId);
        jtm.setLinkName(linkName);
        jtm.setLinkType(linkType);
        jtm.setFromKeyName(fromKeyName);
        jtm.setToKeyName(toKeyName);
        jtm.setFromTable(fromTable);
        jtm.setToTable(toTable);
        return jtm;
    }

    /**
     * Removes the table from the junction table.
     *
     * @param tableMapping table to remove
     */
    void removeTable(TableMapping tableMapping) {
        if (fromTable == tableMapping) fromTable = null;
        if (toTable == tableMapping) toTable = null;
    }

    /**
     * Returns the first linked table source value which matches the given OPA name.
     *
     * @param target opa name
     * @return table source
     */
    public String getMatchingSource(String target) {
        return getFromTableId()
                .filter(id -> id.getOpaName().equals(target))
                .isPresent() ?
                getToTable().getTableId().getSource() :
                getFromTable().getTableId().getSource();
    }

    /**
     * Returns the relevent link type based on the direction of the junction table.
     * By default the link type is based on the from to relationship. This will flip it for
     * to from relationships.
     *
     * @param table relationship side
     * @return link type
     */
    public LinkType getLinkType(TableMapping table) {
        if (linkType == ManyToMany || linkType == OneToOne || table == fromTable) return linkType;
        else return linkType == OneToMany ? ManyToOne : OneToMany;
    }
}
