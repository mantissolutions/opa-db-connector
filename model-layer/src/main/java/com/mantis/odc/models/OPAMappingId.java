/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models;

import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Uniquely identifies a table or field mapping in ODC.
 */
@Data
@Embeddable
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class OPAMappingId {

    @Column(name = "opa_name")
    private String opaName;
    @Column(name = "source_name", nullable = false)
    private String source;

    public OPAMappingId(String source) {
        this.source = source;
    }

    /**
     * @return current identity of this OPA mapping.
     */
    public String getName() {
        return (Strings.isNullOrEmpty(opaName)) ? source : opaName;
    }
}
