/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.models;

import lombok.Getter;
import org.jooq.lambda.Seq;

import java.sql.JDBCType;
import java.util.stream.Stream;

import static java.sql.JDBCType.*;
import static java.text.MessageFormat.format;
import static java.util.stream.Collectors.joining;

/**
 * Base OPA data types.
 */
@Getter
public enum DataType {

    /**
     * String type. Corresponds to OPA Text type.
     */
    STRING_TYPE("StringType", "Text", "Corresponds to the OPA Text type. This typically corresponds to a VARCHAR JDBC type.",
            new JDBCType[]{CHAR, VARCHAR}, // core types
            TINYINT, SMALLINT, INTEGER, BIGINT, REAL, FLOAT, DOUBLE, DECIMAL, NUMERIC, BIT, BOOLEAN, CHAR, VARCHAR,
            LONGNVARCHAR, BINARY, VARBINARY, LONGVARBINARY, DATE, TIME, TIME_WITH_TIMEZONE, TIMESTAMP,
            TIMESTAMP_WITH_TIMEZONE), // all supported types with casts

    /**
     * Date Time type. Corresponds to OPA Number and Currency type.
     */
    DECIMAL_TYPE("DecimalType", "Decimal", "Corresponds to the OPA Number type. This typically corresponds to a DECIMAL or NUMERIC JDBC type.",
            new JDBCType[]{DECIMAL, NUMERIC, BIGINT}, // core types
            TINYINT, SMALLINT, INTEGER, BIGINT, REAL, FLOAT, DOUBLE, DECIMAL, NUMERIC, BIT, BOOLEAN, CHAR, VARCHAR,
            LONGNVARCHAR), // all supported types with casts

    /**
     * Boolean type. Corresponds to the OPA Boolean type.
     */
    BOOLEAN_TYPE("BooleanType", "Boolean", "Corresponds to the OPA Boolean type. This typically corresponds to a BOOLEAN or BIT JDBC type.",
            new JDBCType[]{BIT, BOOLEAN}, // core types
            TINYINT, SMALLINT, INTEGER, BIGINT, REAL, FLOAT, DOUBLE, DECIMAL, NUMERIC, BIT, BOOLEAN, CHAR, VARCHAR,
            LONGNVARCHAR), // all supported types with casts

    /**
     * Date type. Corresponds to OPA Date type.
     */
    DATE_TYPE("DateType", "Date", "Corresponds to the OPA Date type. This typically corresponds to a DATE JDBC type.",
            new JDBCType[]{DATE}, // core types
            CHAR, VARCHAR, LONGVARCHAR, LONGNVARCHAR, DATE, TIMESTAMP), // all supported types with casts

    /**
     * Date Time type. Corresponds to OPA DateTime type.
     */
    DATE_TIME_TYPE("DateTimeType", "Date Time", "Corresponds to the OPA Date Time type. This typically corresponds to a TIMESTAMP JDBC type.",
            new JDBCType[]{TIMESTAMP, TIMESTAMP_WITH_TIMEZONE}, // core types
            CHAR, VARCHAR, LONGVARCHAR, LONGNVARCHAR, DATE, TIME, TIME_WITH_TIMEZONE, TIMESTAMP,
            TIMESTAMP_WITH_TIMEZONE), // all supported types with casts


    /**
     * Time type. Corresponds to OPA Time type.
     */
    TIME_TYPE("TimeType", "Time", "Corresponds to the OPA Time type. This typically corresponds to a TIME JDBC type.",
            new JDBCType[]{TIME, TIME_WITH_TIMEZONE}, // core types
            CHAR, VARCHAR, LONGVARCHAR, LONGNVARCHAR, TIME, TIME_WITH_TIMEZONE, TIMESTAMP,
            TIMESTAMP_WITH_TIMEZONE), // all supported types with casts

    /**
     * Attachment.
     */
    ATTACHMENT_TYPE("AttachmentType", "Attachment", "Corresponds to an OPA attachment. This typically corresponds to a BLOB JDBC type.",
            new JDBCType[]{BLOB, LONGVARBINARY}); // core types

    private final String XMLName;
    private final String displayName;
    private final String description;
    private final JDBCType[] coreTypes;
    private final JDBCType[] supportedTypes;

    DataType(String xmlName, String displayName, String description, JDBCType[] coreTypes, JDBCType... supportedTypes) {
        XMLName = xmlName;
        this.displayName = displayName;
        this.description = description;
        this.coreTypes = coreTypes;
        this.supportedTypes = supportedTypes;
    }

    /**
     * Detailed type description.
     *
     * @return Detailed description of the type
     */
    public String getDetailedDescription() {
        return format("<p>{0}</p>" +
                        "<p><b>Core Supported Types:</b> {1}</p>" +
                        "<p><b>All Supported Types with Casts <i>(Dangerous)</i>:</b> {2}</p>" +
                        "<p>For more information on mappings between JDBC types and the database types, see the driver documentation.</p>",
                description,
                Seq.of(coreTypes).map(JDBCType::getName).toArray(),
                Seq.of(supportedTypes).map(JDBCType::getName).collect(joining(", ")));
    }

    /**
     * True if the given JDBC type is in the list of core types.
     *
     * @param type type to test
     * @return true if in the list of core types
     */
    public boolean isCoreType(JDBCType type) {
        return Stream.of(coreTypes).anyMatch(t -> t == type);
    }

    /**
     * True if the given JDBC type is in the list of supported types.
     *
     * @param type type to test
     * @return true if in the list of supported types
     */
    public boolean isSupportedType(JDBCType type) {
        return Stream.of(supportedTypes).anyMatch(t -> t == type);
    }

    /**
     * Returns true if the current type can by used by a key.
     *
     * @return true if key type
     */
    public boolean isKeyType() {
        return this == DECIMAL_TYPE || this == STRING_TYPE;
    }

    /**
     * Returns true if the type is mappable.
     *
     * @return true if mappable.
     */
    public boolean isMappableType() {
        return this != ATTACHMENT_TYPE;
    }
}
