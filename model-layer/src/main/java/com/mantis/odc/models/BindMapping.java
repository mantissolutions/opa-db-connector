/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.models;

import com.mantis.odc.models.base.Duplicatable;
import com.mantis.odc.models.base.JPAUtility;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

import static javax.persistence.AccessType.FIELD;
import static javax.persistence.AccessType.PROPERTY;

/**
 * Bind Mappings token.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"mapping"})
@EqualsAndHashCode(exclude = {"mapping"})
@Table(name = "odc_bind_mappings")
@Access(value = FIELD)
public class BindMapping implements Duplicatable<BindMapping> {

    @Id
    @SequenceGenerator(name = "bind_mappings_seq", sequenceName = "odc_bind_mappings_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "bind_mappings_seq")
    @Column(name = "bm_id", nullable = false)
    private BigDecimal id;
    @Column(name = "bm_token_name", nullable = false)
    private String tokenName;
    @Column(name = "bm_required", nullable = false)
    private boolean required = false;
    @Enumerated(EnumType.STRING)
    @Column(name = "bm_condition")
    private ConditionalJoin condition;
    @Enumerated(EnumType.STRING)
    @Column(name = "bm_operator")
    private Operator operator;
    @Transient
    private TableMapping table;
    @Transient
    private FieldMapping field;
    @ManyToOne
    @JoinColumn(name = "m_id")
    private Mapping mapping;

    /**
     * @return table mapping
     */
    @ManyToOne
    @JoinColumn(name = "tm_id", nullable = false)
    @Access(value = PROPERTY)
    public TableMapping getTable() {
        return table;
    }

    /**
     * Sets the table.
     *
     * @param table table mapping to set
     */
    public void setTable(TableMapping table) {
        JPAUtility.mergeIntoParent(this, table, BindMapping::getTable, TableMapping::getBinds);
        this.table = table;
    }

    /**
     * Removes the table from the mapping.
     */
    public void removeTable() {
        if (table != null) JPAUtility.removeChild(table, this, TableMapping::getBinds, (b, m) -> table = null);
    }

    /**
     * @return field mapping
     */
    @ManyToOne
    @JoinColumn(name = "fm_id", nullable = false)
    @Access(value = PROPERTY)
    public FieldMapping getField() {
        return field;
    }

    /**
     * Sets the field.
     *
     * @param field field mapping to set
     */
    public void setField(FieldMapping field) {
        JPAUtility.mergeIntoParent(this, field, BindMapping::getField, FieldMapping::getBinds);
        this.field = field;
    }

    /**
     * Removes the field from the mapping.
     */
    public void removeField() {
        if (field != null) JPAUtility.removeChild(field, this, FieldMapping::getBinds, (b, m) -> field = null);
    }

    /**
     * Returns the table alias.
     *
     * @return table alias
     */
    public String getAlias() {
        return table.getAlias();
    }

    @Override
    public BindMapping copy() {
        val bm = new BindMapping();
        bm.setTokenName(tokenName);
        bm.setRequired(required);
        bm.setCondition(condition);
        bm.setOperator(operator);
        bm.setTable(table);
        bm.setField(field);
        return bm;
    }
}
