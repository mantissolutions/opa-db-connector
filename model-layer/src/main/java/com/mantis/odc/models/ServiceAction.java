/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models;

import lombok.Getter;

/**
 * List off all implemented actions.
 */
@Getter
public enum ServiceAction {

    /**
     * Checks the service is up.
     */
    CheckAlive("CheckAlive", "CheckAlive", false),

    /**
     * Returns the services meta data.
     */
    GetMetadata("GetMetadata", "GetMetadata", true),

    /**
     * Loads data from the service.
     */
    Load("Load", "Load", true),

    /**
     * Saves data to the service.
     */
    Save("Save", "Save", true),

    /**
     * Gets an existing checkpoint.
     */
    GetCheckpoint("GetCheckpoint", "GetCheckpoint", true),

    /**
     * Sets a checkpoint value.
     */
    SetCheckpoint("SetCheckpoint", "SetCheckpoint", true),

    /**
     * Adhoc Query.
     */
    ExecuteQuery("ExecuteQuery", "ExecuteQuery", true);

    private final String operationName;
    private final String actionName;
    private final boolean authenticated;

    /**
     * @param operationName action name
     * @param actionName    action
     * @param authenticated if action is authenticated
     */
    ServiceAction(String operationName, String actionName, boolean authenticated) {
        this.operationName = operationName;
        this.actionName = actionName;
        this.authenticated = authenticated;
    }
}
