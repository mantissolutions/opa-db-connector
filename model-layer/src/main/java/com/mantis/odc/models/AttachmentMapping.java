/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.models;

import com.mantis.odc.models.base.Duplicatable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Attachment mapping model.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "odc_attachment_mappings")
public class AttachmentMapping implements Duplicatable<AttachmentMapping> {

    @Id
    @SequenceGenerator(name = "attachment_mapping_seq", sequenceName = "odc_attachment_mappings_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "attachment_mapping_seq")
    @Column(name = "am_id", nullable = false)
    private BigDecimal id;
    @Column(name = "am_table_name", nullable = false)
    private String tableName = "attachments";
    @Column(name = "am_table_field")
    private String tableField = "a_table_name";
    @Column(name = "am_key_field")
    private String keyField;
    @Column(name = "am_name_field", nullable = false)
    private String nameField = "a_name";
    @Column(name = "am_file_name_field", nullable = false)
    private String fileNameField = "a_file_name";
    @Column(name = "am_description_field", nullable = false)
    private String descriptionField = "a_description";
    @Column(name = "am_file_field", nullable = false)
    private String fileField = "a_file";

    @Override
    public AttachmentMapping copy() {
        val am = new AttachmentMapping();
        am.setTableName(tableName);
        am.setTableField(tableField);
        am.setKeyField(keyField);
        am.setNameField(nameField);
        am.setFileField(fileNameField);
        am.setDescriptionField(descriptionField);
        am.setFileField(fileField);
        return am;
    }

    /**
     * Returns all the field names for the given attachment table.
     *
     * @return field names
     */
    public List<String> getFields() {
        return asList(tableField, keyField, nameField, fileNameField, descriptionField, fileField);
    }
}
