/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models.base;

import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Enables patch of the object from another object.
 * Its a field by field update of a given MODEL.
 */
public interface Patchable<MODEL> {

    /**
     * Patches the given entity to the current object.
     *
     * @param entity entity to patch
     */
    void patch(MODEL entity);

    /**
     * Patches the given field.
     *
     * @param current     current model (this)
     * @param replacement replace ment model
     * @param getter      getter function
     * @param setter      setter consumer
     * @param <T>         field type
     */
    default <T> void patch(MODEL current, MODEL replacement, Function<MODEL, T> getter, BiConsumer<MODEL, T> setter) {
        // if fields differ
        if (!getter.apply(current).equals(getter.apply(replacement))) {
            // patch field
            setter.accept(current, getter.apply(replacement));
        }
    }
}
