/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.models;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Operation data element.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@ToString(exclude = {"mapping"})
@EqualsAndHashCode(exclude = {"mapping"})
@Table(name = "odc_operations")
public class Operation {

    @Id
    @SequenceGenerator(name = "operations_seq", sequenceName = "odc_operations_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "operations_seq")
    @Column(name = "op_id", nullable = false)
    private BigDecimal id;
    @Column(name = "op_uid", nullable = false)
    private String uid;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "op_date", nullable = false)
    private Date date = new Date();
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "op_type", nullable = false)
    private ServiceAction type;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "m_id", nullable = false)
    private Mapping mapping;
    @Column(name = "op_inbound_packet_size", nullable = false)
    private BigDecimal inboundPacketSize = new BigDecimal(0);
    @Column(name = "op_outbound_packet_size", nullable = false)
    private BigDecimal outboundPacketSize = new BigDecimal(0);
    @Column(name = "op_start_request", nullable = false)
    private BigDecimal startRequest = new BigDecimal(0);
    @Column(name = "op_end_request", nullable = false)
    private BigDecimal endRequest = new BigDecimal(0);
    @Column(name = "op_start_db", nullable = false)
    private BigDecimal startDB = new BigDecimal(0);
    @Column(name = "op_end_db", nullable = false)
    private BigDecimal endDB = new BigDecimal(0);
}
