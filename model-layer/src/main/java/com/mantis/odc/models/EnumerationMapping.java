/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models;

import com.mantis.odc.models.base.Duplicatable;
import com.mantis.odc.models.base.JPAUtility;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static java.text.MessageFormat.format;
import static javax.persistence.AccessType.FIELD;
import static javax.persistence.AccessType.PROPERTY;
import static javax.persistence.EnumType.STRING;

/**
 * Models an enumeration sourced from the database.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"mapping", "fields", "parents"})
@EqualsAndHashCode(of = {"id", "name"})
@Table(name = "odc_enumeration_mappings")
@Access(value = FIELD)
public class EnumerationMapping implements Duplicatable<EnumerationMapping> {

    @Id
    @SequenceGenerator(name = "enumeration_mapping_seq", sequenceName = "odc_enumeration_mappings_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "enumeration_mapping_seq")
    @Column(name = "em_id", nullable = false)
    private BigDecimal id;
    @Column(name = "em_name", nullable = false)
    private String name;
    @Column(name = "em_description")
    private String description;
    @Enumerated(STRING)
    @Column(name = "em_type", nullable = false)
    private EnumerationType type;
    @Column(name = "em_sql", nullable = false)
    private String sql;
    @Transient
    private EnumerationMapping child;
    @ManyToOne
    @JoinColumn(name = "m_id", nullable = false)
    private Mapping mapping;
    @OneToMany(mappedBy = "enumeration")
    private List<FieldMapping> fields = new ArrayList<>();
    @OneToMany(mappedBy = "child")
    private List<EnumerationMapping> parents = new ArrayList<>();

    /**
     * @return child enumeration
     */
    @ManyToOne
    @JoinColumn(name = "em_child_id")
    @Access(value = PROPERTY)
    public EnumerationMapping getChild() {
        return child;
    }

    /**
     * Sets the enumeration mapping child.
     *
     * @param child set the child
     */
    public void setChild(EnumerationMapping child) {
        JPAUtility.mergeIntoParent(this, child, EnumerationMapping::getChild, EnumerationMapping::getParents);
        this.child = child;
    }

    /**
     * @return true if the enumeration has child enumerations.
     */
    public boolean hasChildEnumeration() {
        return child != null;
    }

    @Override
    public EnumerationMapping copy() {
        val em = new EnumerationMapping();
        em.setName(name);
        em.setType(type);
        em.setSql(sql);
        em.setChild(child);
        em.setDescription(description);
        return em;
    }

    /**
     * Returns the detailed description for the enumeration.
     *
     * @return enumeration
     */
    public String getDetailedDescription() {
        return format("<p>{0} - {1} enumeration</p><p>{2}</p>", name, type.getDisplay(), description);
    }

    /**
     * Removes the parent enumeration.
     *
     * @param mapping parent enumeration
     */
    public void removeParent(EnumerationMapping mapping) {
        val it = getParents().iterator();
        while (it.hasNext()) if (it.next().equals(mapping)) it.remove();
    }

    /**
     * Un-links all linked fields.
     */
    public void unlinkFromFields() {
        val it = getFields().iterator();
        while (it.hasNext()) {
            // change the enum to null
            it.next().setEnumeration(null);
            // remove it from the collection
            it.remove();
        }
    }
}
