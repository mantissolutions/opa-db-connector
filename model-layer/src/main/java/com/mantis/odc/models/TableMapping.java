/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.models;

import com.mantis.odc.models.base.Duplicatable;
import com.mantis.odc.models.base.JPAUtility;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.unmodifiableList;
import static javax.persistence.AccessType.FIELD;
import static javax.persistence.AccessType.PROPERTY;

/**
 * Base table mapping class.
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"parent", "mapping", "children", "fromJunctionTables", "toJunctionTables", "binds"})
@EqualsAndHashCode(of = {"id", "tableId"})
@Table(name = "odc_table_mappings")
@Access(value = FIELD)
public class TableMapping implements Duplicatable<TableMapping> {

    @Id
    @SequenceGenerator(name = "table_mappings_seq", sequenceName = "odc_table_mappings_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "table_mappings_seq")
    @Column(name = "tm_id", nullable = false)
    @Getter
    @Setter
    private BigDecimal id;
    @Embedded
    @Getter
    @Setter
    private OPAMappingId tableId = new OPAMappingId();
    @Column(name = "tm_link_name")
    @Setter
    private String linkName;
    @Column(name = "tm_one_to_one", nullable = false)
    @Getter
    @Setter
    private boolean oneToOne = false;
    @Column(name = "tm_inferred", nullable = false)
    @Getter
    @Setter
    private boolean inferred = false;
    @OneToMany(mappedBy = "table", targetEntity = FieldMapping.class, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FieldMapping> fields = new ArrayList<>();
    @Transient
    private TableMapping parent;
    @Getter
    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    private List<TableMapping> children = new ArrayList<>();
    @Getter
    @Setter
    @OneToMany(mappedBy = "fromTable")
    private List<JunctionTableMapping> fromJunctionTables = new ArrayList<>();
    @Getter
    @Setter
    @OneToMany(mappedBy = "toTable")
    private List<JunctionTableMapping> toJunctionTables = new ArrayList<>();
    @Getter
    @Setter
    @OneToMany(mappedBy = "table")
    private List<BindMapping> binds = new ArrayList<>();
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "m_id")
    private Mapping mapping;

    /**
     * Adds all the fields to the table.
     *
     * @param fieldMappings fields
     */
    public void setFields(List<FieldMapping> fieldMappings) {
        fieldMappings.forEach(this::addFieldMaping);
    }

    /**
     * @return returns all the fields
     */
    public List<FieldMapping> getFields() {
        return unmodifiableList(fields);
    }

    /**
     * @param fieldMapping field mapping to add
     */
    public void addFieldMaping(FieldMapping fieldMapping) {
        JPAUtility.addChild(this, fieldMapping, t -> t.fields, FieldMapping::setTable);
    }

    /**
     * @param fieldMapping field mapping to remove
     */
    public void removeFieldMapping(FieldMapping fieldMapping) {
        JPAUtility.removeChild(this, fieldMapping, t -> t.fields, FieldMapping::setTable);
    }

    /**
     * Sets all the child mappings to the parent.
     *
     * @param tableMappings child mappings.
     */
    public void setChildren(List<TableMapping> tableMappings) {
        tableMappings.forEach(this::addChildMapping);
    }

    /**
     * @param tableMapping child table to add
     */
    public void addChildMapping(TableMapping tableMapping) {
        JPAUtility.addChild(this, tableMapping, t -> t.children, TableMapping::setParent);
    }

    /**
     * @param tableMapping child table to remove
     */
    public void removeChildMapping(TableMapping tableMapping) {
        JPAUtility.removeChild(this, tableMapping, t -> t.children, TableMapping::setParent);
    }

    /**
     * @return true if the current table is a child table
     */
    private boolean isGlobalTable() {
        return parent == null;
    }

    /**
     * @return parent value
     */
    @ManyToOne
    @JoinColumn(name = "tm_parent_id")
    @Access(value = PROPERTY)
    public TableMapping getParent() {
        return parent;
    }

    /**
     * @param parent parent to set
     */
    public void setParent(TableMapping parent) {
        JPAUtility.mergeIntoParent(this, parent, TableMapping::getParent, TableMapping::getChildren);

        // clean up relationships fk and link name if no parent set
        if (this.parent == null && parent == null) {
            linkName = null;
            oneToOne = false;
            getForeignKeyOpt().ifPresent(fieldMapping -> fieldMapping.setForeignKey(false));
        }

        this.parent = parent;
    }

    /**
     * @return parent table
     */
    public Optional<TableMapping> getParentOpt() {
        return Optional.ofNullable(parent);
    }

    /**
     * Returns true if the table mapping contains values that can be inputted.
     *
     * @return true if the table mapping can be inputted
     */
    public boolean canBeInput() {
        return isGlobalTable() || getFields().stream().anyMatch(f -> f.isInput() && !f.isForeignKey());
    }

    /**
     * Returns true if the table mapping contains values that can be outputted.
     *
     * @return true if the table mapping can be outputted
     */
    public boolean canBeOutput() {
        return isGlobalTable() || getFields().stream().anyMatch(f -> f.isOutput() && !f.isForeignKey());
    }

    /**
     * Returns the root table mapping in this tree.
     *
     * @return root table mapping
     */
    public TableMapping getRoot() {
        return getParentOpt()
                .map(TableMapping::getRoot)
                .orElse(this);
    }

    /**
     * Returns the table alias.
     *
     * @return unique table alias
     */
    public String getAlias() {
        return mapping.getAlias(tableId, "t");
    }

    /**
     * Locates all junction tables with a reference to this table.
     *
     * @return junction tables linked to this table
     */
    public List<JunctionTableMapping> getJunctionTables() {
        return Stream.concat(fromJunctionTables.stream(), toJunctionTables.stream())
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * Returns the field mapping by id.
     *
     * @param id field id
     * @return field mapping
     */
    public Optional<FieldMapping> getField(OPAMappingId id) {
        return getFields().stream()
                .filter(f -> f.getFieldId().equals(id))
                .findFirst();
    }

    /**
     * Returns the field mapping by name.
     *
     * @param name field name
     * @return field mapping
     */
    public Optional<FieldMapping> getField(String name) {
        return getFields().stream()
                .filter(f -> f.getFieldId().getName().equals(name))
                .findFirst();
    }

    /**
     * @return the primaryKey
     */
    public Optional<FieldMapping> getPrimaryKeyOpt() {
        return getFields().stream()
                .filter(FieldMapping::isPrimaryKey)
                .findFirst();
    }

    /**
     * @return the primaryKey
     */
    public FieldMapping getPrimaryKey() {
        return getPrimaryKeyOpt().orElse(null);
    }

    /**
     * This will set the passed field as the sole primary key.
     *
     * @param field the field to set
     */
    public void setPrimaryKey(FieldMapping field) {
        if (field != null) {
            field.setPrimaryKey(true);
            // disable existing primary keys
            getFields().stream()
                    .filter(f -> f.isPrimaryKey() && !f.getFieldId().equals(field.getFieldId()))
                    .forEach(f -> f.setPrimaryKey(false));
        }
    }

    /**
     * @return the foreignKey
     */
    public Optional<FieldMapping> getForeignKeyOpt() {
        return getFields().stream()
                .filter(FieldMapping::isForeignKey)
                .findFirst();
    }

    /**
     * @return the foreignKey
     */
    public FieldMapping getForeignKey() {
        return getForeignKeyOpt().orElse(null);
    }

    /**
     * This will set the passed field as the sole foreign key.
     *
     * @param field the field to set
     */
    public void setForeignKey(FieldMapping field) {
        if (field != null) {
            field.setForeignKey(true);
            // disable existing foreign keys
            getFields().stream()
                    .filter(f -> f.isForeignKey() && !f.getFieldId().equals(field.getFieldId()))
                    .forEach(f -> f.setForeignKey(false));
        }
    }

    /**
     * @return the linkName
     */
    public String getLinkName() {
        val pt = getParentOpt();
        return (linkName == null && pt.isPresent()) ?
                (pt.get().getTableId().getName() + " to " + getTableId().getName()).toLowerCase() : linkName;
    }

    @Override
    public TableMapping copy() {
        val tm = new TableMapping();
        tm.setTableId(tableId);
        tm.setLinkName(linkName);
        tm.setOneToOne(oneToOne);
        tm.setInferred(inferred);
        tm.setParent(parent);
        for (val f : fields) tm.addFieldMaping(f.copy());
        return tm;
    }

    /**
     * @return link name
     */
    public String getLinkNameNoDefault() {
        return linkName;
    }
}
