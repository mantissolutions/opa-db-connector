/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models;

import lombok.Getter;

/**
 * Valid ANSI conditional joins.
 */
@Getter
public enum ConditionalJoin {

    /**
     * And condition.
     */
    and("And", "and", "AND", "And condition"),

    /**
     * Or condition.
     */
    or("Or", "or", "OR", "Or condition");

    private final String name;
    private final String XMLName;
    private final String sqlOperator;
    private final String description;

    ConditionalJoin(String name, String xmlName, String sqlOperator, String description) {
        this.name = name;
        XMLName = xmlName;
        this.sqlOperator = sqlOperator;
        this.description = description;
    }
}
