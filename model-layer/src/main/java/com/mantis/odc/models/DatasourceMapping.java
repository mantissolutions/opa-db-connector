/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.models;

import com.codahale.metrics.MetricRegistry;
import com.mantis.odc.models.base.Duplicatable;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.*;

import javax.persistence.*;
import javax.sql.DataSource;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.REFRESH;

/**
 * Base DatasourceMapping class.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "mappings")
@EqualsAndHashCode(exclude = "mappings")
@Table(name = "odc_datasources")
public class DatasourceMapping implements Duplicatable<DatasourceMapping> {

    /**
     * Time out value to apply on connecting to databases.
     */
    @Transient
    private static final int TIME_OUT = 5;

    @Id
    @SequenceGenerator(name = "datasources_seq", sequenceName = "odc_datasources_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "datasources_seq")
    @Column(name = "ds_id", nullable = false)
    private BigDecimal id;
    @Size(min = 2, max = 250)
    @Column(name = "ds_name", nullable = false)
    private String name;
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "ds_jdbc", nullable = false)
    private String jdbc;
    @Enumerated(EnumType.STRING)
    @Column(name = "ds_driver", nullable = false)
    private Driver driver;
    /**
     * This property controls the maximum number of milliseconds that a client
     * (that's you) will wait for a connection from the pool. If this time is
     * exceeded without a connection becoming available, a SQLException will be
     * thrown. 1000ms is the minimum value. Default: 30000 (30 seconds)
     */
    @Min(1000)
    @Column(name = "conn_timeout_ms")
    private long connectionTimeoutMs = 30000;
    /**
     * This property controls the maximum amount of time that a connection is
     * allowed to sit idle in the pool. Whether a connection is retired as idle
     * or not is subject to a maximum variation of +30 seconds, and average
     * variation of +15 seconds. A connection will never be retired as idle
     * before this timeout. A value of 0 means that idle connections are never
     * removed from the pool. Default: 600000 (10 minutes)
     */
    @Min(0)
    @Column(name = "idle_timeout_ms")
    private long idleTimeoutMs = 600000;
    /**
     * This property controls the maximum lifetime of a connection in the pool.
     * When a connection reaches this timeout it will be retired from the pool,
     * subject to a maximum variation of +30 seconds. An in-use connection will
     * never be retired, only when it is closed will it then be removed. We
     * strongly recommend setting this value, and it should be at least 30
     * seconds less than any database-level connection timeout. A value of 0
     * indicates no maximum lifetime (infinite lifetime), subject of course to
     * the idleTimeout setting. Default: 1800000 (30 minutes)
     */
    @Min(0)
    @Column(name = "max_lifetime_ms")
    private long maxLifetimeMs = 1800000;
    /**
     * This property controls the maximum size that the pool is allowed to
     * reach, including both idle and in-use connections. Basically this value
     * will determine the maximum number of actual connections to the database
     * backend. A reasonable value for this is best determined by your execution
     * environment. When the pool reaches this size, and no idle connections are
     * available, calls to getConnection() will block for up to
     * connectionTimeout milliseconds before timing out. Default: 10
     */
    @Min(1)
    @Column(name = "max_pool_size")
    private int maxPoolSize = 10;
    /**
     * This property controls the minimum number of idle connections that
     * the pool tries to maintain in the pool. If the idle connections dip below
     * this value, the pool will make a best effort to add additional
     * connections quickly and efficiently. However, for maximum performance and
     * responsiveness to spike demands, we recommend not setting this value and
     * instead allowing the pool to act as a fixed size connection pool.
     * Default: same as maximumPoolSize
     */
    @Min(1)
    @Column(name = "min_idle")
    private int minIdle = maxPoolSize;

    /**
     * This property controls the amount of time that a connection can be out of the pool
     * before a message is logged indicating a possible connection leak.
     * A value of 0 means leak detection is disabled.
     * Lowest acceptable value for enabling leak detection is 2000 (2 secs). Default: 0
     */
    @Min(0)
    @Column(name = "leak_detection_threshold")
    private long leakDetectionThreshold = 0;

    @OneToMany(mappedBy = "datasource", cascade = {REFRESH, MERGE})
    private List<Mapping> mappings = new ArrayList<>();

    /**
     * Creates an instance of the DataSource.
     *
     * @param name           name of the pool, this is used in retrieving metrics from the store.
     * @param user           user
     * @param password       password
     * @param metricRegistry global metrics registry
     * @return instance of a the DataSource
     */
    public DataSource createInstance(String name, String user, String password, MetricRegistry metricRegistry) {
        val config = new HikariConfig();

        // name of the pool
        config.setPoolName(name);

        // metrics
        config.setMetricRegistry(metricRegistry);

        // core settings
        config.setDriverClassName(driver.getClassName());
        config.setJdbcUrl(jdbc);
        config.setConnectionTestQuery(driver.getTestQuery());
        config.setUsername(user);
        config.setPassword(password);

        // other settings
        config.setAutoCommit(false);
        config.setConnectionTimeout(connectionTimeoutMs);
        config.setIdleTimeout(idleTimeoutMs);
        config.setMaxLifetime(maxLifetimeMs);
        config.setMaximumPoolSize(maxPoolSize);
        config.setMinimumIdle(minIdle);
        config.setLeakDetectionThreshold(leakDetectionThreshold);

        return new HikariDataSource(config);
    }

    /**
     * Returns a single connection.
     *
     * @param user     database user
     * @param password database password
     * @return connection
     */
    public Connection getConnection(String user, String password) throws ClassNotFoundException, SQLException {
        Class.forName(driver.getClassName());
        DriverManager.setLoginTimeout(TIME_OUT);
        return DriverManager.getConnection(jdbc, user, password);
    }

    /**
     * Returns the database meta data for the given data source.
     *
     * @param user     database username
     * @param password database password
     * @return meta data
     */
    public DatabaseMetaData getMetaData(String user, String password) throws ClassNotFoundException, SQLException {
        return getConnection(user, password).getMetaData();
    }

    /**
     * Tests the database credentials.
     *
     * @param user     user name
     * @param password password
     * @return true on success
     * @throws ClassNotFoundException on missing driver
     * @throws SQLException           on invalid connection
     */
    public boolean test(String user, String password) throws ClassNotFoundException, SQLException {
        try (Connection conn = getConnection(user, password)) {
            try {// try is valid
                conn.isValid(TIME_OUT);
            } catch (SQLException e) {// else run test query
                conn.prepareStatement(driver.getTestQuery()).execute();
            }
            return true;
        }
    }

    /**
     * @return simple name for the data source mapping
     */
    public String getSimpleName() {
        return name + ":" + driver + " [ " + jdbc + " ]";
    }

    @Override
    public DatasourceMapping copy() {
        val dm = new DatasourceMapping();
        dm.setName(name + " - copy");
        dm.setJdbc(jdbc);
        dm.setDriver(driver);
        dm.setConnectionTimeoutMs(connectionTimeoutMs);
        dm.setIdleTimeoutMs(idleTimeoutMs);
        dm.setLeakDetectionThreshold(leakDetectionThreshold);
        dm.setMaxLifetimeMs(maxLifetimeMs);
        dm.setMaxPoolSize(maxPoolSize);
        dm.setMinIdle(minIdle);
        return dm;
    }
}
