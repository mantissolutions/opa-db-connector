/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.models;

import lombok.Getter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * OPA version enumeration.
 */
@Getter
public enum OPAVersion {

    /**
     * OPA Connector framework version 12.1.
     */
    OPA_12_1("12.1",
            "com.oracle.opa.connector._12_1.data.types:com.oracle.opa.connector._12_1.metadata.types",
            "connector_121.wsdl", false),

    /**
     * OPA Connector framework version 12.2.
     */
    OPA_12_2("12.2",
            "com.oracle.opa.connector._12_2.data.types:com.oracle.opa.connector._12_2.metadata.types",
            "connector_122.wsdl", false),

    /**
     * OPA Connector framework version 12.2.2.
     */
    OPA_12_2_2("12.2.2",
            "com.oracle.opa.connector._12_2.data.types:com.oracle.opa.connector._12_2_2.metadata.types",
            "connector_1222.wsdl", true),

    /**
     * OPA connector framework version 12.2.5.
     */
    OPA_12_2_5("12.2.5",
            "com.oracle.opa.connector._12_2.data.types:com.oracle.opa.connector._12_2_5.metadata.types",
            "connector_1225.wsdl", true),

    /**
     * OPA connector framework version 12.2.9.
     */
    OPA_12_2_9("12.2.9",
            "com.oracle.opa.connector._12_2_9.data.types:com.oracle.opa.connector._12_2_9.metadata.types",
            "connector_1229.wsdl", true);

    private final String version;
    private final String contextPath;
    private final String wsdlPath;
    private final boolean supportsEnums;
    private JAXBContext ctx;

    /**
     * @param version       OPA version
     * @param contextPath   JAXB context path
     * @param wsdlPath      WSDL template location
     * @param supportsEnums flag to indicate that the version supports enumerations
     */
    OPAVersion(String version, String contextPath, String wsdlPath, boolean supportsEnums) {
        this.version = version;
        this.contextPath = contextPath;
        this.wsdlPath = wsdlPath;
        this.supportsEnums = supportsEnums;
    }

    @Override
    public String toString() {
        return version;
    }

    /**
     * Returns the JAXBContext.
     *
     * @return JAXBContext
     */
    private JAXBContext getCtx() {
        if (ctx == null) {
            // create context
            try {
                ctx = JAXBContext.newInstance(contextPath);
            } catch (JAXBException e) {
                throw new RuntimeException("Failed to create JAXB Context", e);
            }
        }

        return ctx;
    }

    /**
     * Creates a new JAXB marshaller.
     *
     * @return marshaller
     * @throws JAXBException on JAXB error
     */
    public Marshaller createMarshaller() throws JAXBException {
        return getCtx().createMarshaller();
    }

    /**
     * Creates a new JAXB unmarshaller.
     *
     * @return unmarshaller
     * @throws JAXBException on JAXB error
     */
    public Unmarshaller createUnmarshaller() throws JAXBException {
        return getCtx().createUnmarshaller();
    }
}
