package com.mantis.odc.models.base;

import lombok.experimental.UtilityClass;
import lombok.val;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * JPA helpers.
 */
@UtilityClass
public class JPAUtility {

    /**
     * Merges a model and parent together. Needed for JPA reference consistency.
     *
     * @param model   current child model
     * @param parent  new parent to set
     * @param pGetter parent getter
     * @param cGetter children getter
     * @param <T>     model type
     * @param <P>     parent type
     */
    public <T, P> void mergeIntoParent(T model, P parent, Function<T, P> pGetter, Function<P, List<T>> cGetter) {
        val currentParent = pGetter.apply(model);

        // no opp
        if (currentParent == parent) return;

        // if a parent already exists and is not a clone (JPA clones the object, so no modifications can occur to it)
        if (currentParent != null && !currentParent.equals(parent)) {
            val cpChildren = cGetter.apply(currentParent);
            // find the current parent and remove it
            if (cpChildren.contains(model)) cpChildren.remove(model);
        }

        // add to new parent
        if (parent != null) {
            val npChildren = cGetter.apply(parent);
            if (!npChildren.contains(model)) npChildren.add(model);
        }
    }

    /**
     * Adds a child to a parent.
     *
     * @param model   parent model
     * @param child   child model to add
     * @param cGetter children getter
     * @param pSetter parent setter
     * @param <T>     parent model type
     * @param <C>     child model type
     */
    public <T, C> void addChild(T model, C child, Function<T, List<C>> cGetter, BiConsumer<C, T> pSetter) {
        val children = cGetter.apply(model);

        // if the parent does not contain the parent.
        if (!children.contains(child)) {
            // set parent to child
            pSetter.accept(child, model);
            // add child to parent
            children.add(child);
        }
    }

    /**
     * Removes a child from a parent.
     *
     * @param model   parent model
     * @param child   child model to add
     * @param cGetter children getter
     * @param pSetter parent setter
     * @param <T>     parent model type
     * @param <C>     child model type
     */
    public <T, C> void removeChild(T model, C child, Function<T, List<C>> cGetter, BiConsumer<C, T> pSetter) {
        val children = cGetter.apply(model);
        val it = children.iterator();

        // set remove parent from child
        pSetter.accept(child, model);

        // find the child and remove it from the parent
        while (it.hasNext()) if (it.next().equals(child)) it.remove();
    }
}
