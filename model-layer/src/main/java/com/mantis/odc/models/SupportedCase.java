/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models;

/**
 * List of supported case types for JDBC drivers.
 */
public enum SupportedCase {
    UPPER, LOWER, UNCHANGED
}
