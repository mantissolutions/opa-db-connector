/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.models;

import lombok.Getter;
import lombok.val;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Links types.
 */
@Getter
public enum LinkType {

    /**
     * One to one cardinality. Can be used in containment relationships.
     */
    OneToOne("OneToOne", "C_11", "One to One"),

    /**
     * One to many cardinality. Can be used in containment relationships.
     */
    OneToMany("OneToMany", "C_1M", "One to Many"),

    /**
     * Many to one cardinality.
     */
    ManyToOne("ManyToOne", "C_M1", "Many to One"),

    /**
     * Many to many cardinality.
     */
    ManyToMany("ManyToMany", "C_MN", "Many to Many");

    private final String XMLName;
    private final String value;
    private final String display;

    /**
     * @param xmlName xml name
     * @param value   link value
     * @param display link display value
     */
    LinkType(String xmlName, String value, String display) {
        XMLName = xmlName;
        this.value = value;
        this.display = display;
    }

    /**
     * @return list of link types.
     */
    public static Collection<? extends LinkType> asList() {
        val values = new ArrayList<LinkType>();
        Collections.addAll(values, LinkType.values());
        return values;
    }
}
