/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.models;

import com.mantis.odc.models.base.Duplicatable;
import com.mantis.odc.models.base.JPAUtility;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.AccessType.FIELD;
import static javax.persistence.AccessType.PROPERTY;

/**
 * Base Field mapping type.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"table", "binds"})
@EqualsAndHashCode(of = {"id", "fieldId"})
@Table(name = "odc_field_mappings")
@Access(value = FIELD)
public class FieldMapping implements Duplicatable<FieldMapping> {

    @Id
    @SequenceGenerator(name = "field_mappings_seq", sequenceName = "odc_field_mappings_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "field_mappings_seq")
    @Column(name = "fm_id", nullable = false)
    private BigDecimal id;
    @Embedded
    private OPAMappingId fieldId = new OPAMappingId();
    @Enumerated(EnumType.STRING)
    @Column(name = "fm_type")
    private DataType type;
    @Transient
    private EnumerationMapping enumeration;
    @Column(name = "fm_primary_key", nullable = false)
    private boolean primaryKey = false;
    @Column(name = "fm_auto_increment", nullable = false)
    private boolean autoIncrement = false;
    @Column(name = "fm_primary_sequence")
    private String sequence;
    @Column(name = "fm_foreign_key", nullable = false)
    private boolean foreignKey = false;
    @Column(name = "fm_required", nullable = false)
    private boolean required = false;
    @Column(name = "fm_input", nullable = false)
    private boolean input = true;
    @Column(name = "fm_output", nullable = false)
    private boolean output = true;
    @ManyToOne
    @JoinColumn(name = "tm_id", nullable = false)
    private TableMapping table;
    @OneToMany(mappedBy = "field")
    private List<BindMapping> binds = new ArrayList<>();

    /**
     * @return enumeration
     */
    @ManyToOne
    @JoinColumn(name = "em_id")
    @Access(value = PROPERTY)
    public EnumerationMapping getEnumeration() {
        return enumeration;
    }

    /**
     * Sets the enumeration.
     *
     * @param enumeration enumeration to set
     */
    public void setEnumeration(EnumerationMapping enumeration) {
        if (enumeration == null) this.enumeration = null;
        else {
            JPAUtility.mergeIntoParent(this, enumeration, FieldMapping::getEnumeration, EnumerationMapping::getFields);
            this.enumeration = enumeration;
        }
    }

    public void removeEnumeration() {
        if (enumeration != null)
            JPAUtility.removeChild(enumeration, this, EnumerationMapping::getFields, (f, e) -> enumeration = null);
    }

    @Override
    public FieldMapping copy() {
        val fm = new FieldMapping();
        fm.setFieldId(fieldId);
        fm.setType(type);
        fm.setEnumeration(enumeration);
        fm.setPrimaryKey(primaryKey);
        fm.setAutoIncrement(autoIncrement);
        fm.setSequence(sequence);
        fm.setForeignKey(foreignKey);
        fm.setRequired(required);
        fm.setInput(input);
        fm.setOutput(output);
        return fm;
    }

    /**
     * Returns the enumeration mapping if set otherwise returns the data type.
     *
     * @return enum or data type
     */
    public Object getTypeOrEnum() {
        return enumeration != null ? enumeration : type;
    }

    /**
     * Sets the enumeration or data type.
     *
     * @param typeOrEnum enumeration or data type
     */
    public void setTypeOrEnum(Object typeOrEnum) {
        if (typeOrEnum instanceof EnumerationMapping) {
            setEnumeration((EnumerationMapping) typeOrEnum);
            type = enumeration.getType().getDataType();
        } else if (typeOrEnum instanceof DataType) {
            type = (DataType) typeOrEnum;
            removeEnumeration();
        }
    }
}
