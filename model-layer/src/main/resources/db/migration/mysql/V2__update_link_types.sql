-- alter bind mapping table
---------------------------
-- create table id
ALTER TABLE odc_bind_mappings ADD COLUMN tm_id BIGINT;
ALTER TABLE odc_bind_mappings ADD FOREIGN KEY (tm_id) REFERENCES odc_table_mappings (tm_id) ON DELETE SET NULL;
ALTER TABLE odc_bind_mappings DROP COLUMN table_opa_name;
ALTER TABLE odc_bind_mappings DROP COLUMN table_source_name;

-- create field id
ALTER TABLE odc_bind_mappings ADD COLUMN fm_id BIGINT;
ALTER TABLE odc_bind_mappings ADD FOREIGN KEY (fm_id) REFERENCES odc_field_mappings (fm_id) ON DELETE SET NULL;
ALTER TABLE odc_bind_mappings DROP COLUMN field_opa_name;
ALTER TABLE odc_bind_mappings DROP COLUMN field_source_name;

-- alter field mapping table
----------------------------
ALTER TABLE odc_field_mappings ADD COLUMN em_id BIGINT;
ALTER TABLE odc_field_mappings ADD FOREIGN KEY (em_id) REFERENCES odc_enumeration_mappings (em_id) ON DELETE SET NULL;

-- alter junction mapping table
-------------------------------
-- from table id
ALTER TABLE odc_j_table_mappings ADD COLUMN jtm_from_table_id BIGINT;
ALTER TABLE odc_j_table_mappings ADD FOREIGN KEY (jtm_from_table_id) REFERENCES odc_table_mappings (tm_id) ON DELETE SET NULL;
ALTER TABLE odc_j_table_mappings DROP COLUMN to_table_opa_name;
ALTER TABLE odc_j_table_mappings DROP COLUMN to_table_source_name;

-- to table id
ALTER TABLE odc_j_table_mappings ADD COLUMN jtm_to_table_id BIGINT;
ALTER TABLE odc_j_table_mappings ADD FOREIGN KEY (jtm_to_table_id) REFERENCES odc_table_mappings (tm_id) ON DELETE SET NULL;
ALTER TABLE odc_j_table_mappings DROP COLUMN from_table_opa_name;
ALTER TABLE odc_j_table_mappings DROP COLUMN from_table_source_name;

-- alter table mapping table
----------------------------
ALTER TABLE odc_table_mappings ADD COLUMN tm_parent_id BIGINT;
ALTER TABLE odc_table_mappings ADD FOREIGN KEY (tm_parent_id) REFERENCES odc_table_mappings (tm_id) ON DELETE SET NULL;
ALTER TABLE odc_table_mappings DROP COLUMN parent_opa_name;
ALTER TABLE odc_table_mappings DROP COLUMN parent_source_name;

-- alter enumeration mapping table
----------------------------------
ALTER TABLE odc_enumeration_mappings ADD COLUMN em_child_id BIGINT;
ALTER TABLE odc_enumeration_mappings ADD FOREIGN KEY (em_child_id) REFERENCES odc_enumeration_mappings (em_id) ON DELETE SET NULL;
ALTER TABLE odc_enumeration_mappings DROP COLUMN em_child_name;