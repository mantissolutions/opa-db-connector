CREATE TABLE odc_enumeration_mappings(
    em_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    em_name VARCHAR(250) NOT NULL,
    em_type VARCHAR(50) NOT NULL,
    em_sql VARCHAR(250) NOT NULL,
    em_child_name VARCHAR(250),
    m_id BIGINT,
    FOREIGN KEY (m_id) REFERENCES odc_mappings (m_id) ON DELETE CASCADE
);