ALTER TABLE odc_mappings MODIFY COLUMN am_id BIGINT;
ALTER TABLE odc_mappings MODIFY COLUMN ch_id BIGINT;
ALTER TABLE odc_mappings DROP COLUMN m_token;
ALTER TABLE odc_table_mappings MODIFY COLUMN parent_source_name VARCHAR(250);