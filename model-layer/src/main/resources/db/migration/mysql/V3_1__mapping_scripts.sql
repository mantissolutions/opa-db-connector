ALTER TABLE odc_mappings ADD COLUMN m_pre_load_script LONGTEXT;
ALTER TABLE odc_mappings ADD COLUMN m_post_load_script LONGTEXT;
ALTER TABLE odc_mappings ADD COLUMN m_pre_save_script LONGTEXT;
ALTER TABLE odc_mappings ADD COLUMN m_post_save_script LONGTEXT;

ALTER TABLE odc_batch_mappings ADD COLUMN m_pre_script LONGTEXT;
ALTER TABLE odc_batch_mappings ADD COLUMN m_post_script LONGTEXT;