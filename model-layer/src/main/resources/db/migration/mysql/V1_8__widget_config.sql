CREATE TABLE odc_widget_config (
    widget_id VARCHAR(250) NOT NULL PRIMARY KEY,
    widget_x INT NOT NULL,
    widget_y INT NOT NULL,
    widget_width INT NOT NULL,
    widget_height INT NOT NULL,
    widget_settings VARCHAR(4000),
    user_username VARCHAR(250) NOT NULL,
    FOREIGN KEY (user_username) REFERENCES odc_users (user_username) ON DELETE CASCADE
);