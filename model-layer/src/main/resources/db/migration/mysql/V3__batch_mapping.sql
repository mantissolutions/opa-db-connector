CREATE TABLE odc_batch_mappings(
    bam_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    bam_name VARCHAR(250),
    bam_hub_url VARCHAR(250),
    bam_deployment_name VARCHAR(250),
    bam_deployment_version INT,
    bam_deploy_on_startup BOOLEAN,
    bam_root_table_id BIGINT,
    m_id BIGINT,
    FOREIGN KEY (bam_root_table_id) REFERENCES odc_table_mappings (tm_id) ON DELETE CASCADE,
    FOREIGN KEY (m_id) REFERENCES odc_mappings (m_id) ON DELETE CASCADE
);