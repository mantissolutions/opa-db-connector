CREATE TABLE odc_checkpoint_mappings (
    ch_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    ch_table_name VARCHAR(250) NOT NULL,
    ch_bind_token VARCHAR(250) NOT NULL,
    ch_id_field VARCHAR (250)  NOT NULL,
    ch_data_field VARCHAR (250)  NOT NULL
);

ALTER TABLE odc_mappings ADD COLUMN m_supports_checkpoints BOOLEAN NOT NULL;
ALTER TABLE odc_mappings ADD COLUMN ch_id BIGINT NOT NULL;
ALTER TABLE odc_mappings ADD FOREIGN KEY (ch_id) REFERENCES odc_checkpoint_mappings (ch_id) ON DELETE CASCADE;