-- Copyright (c) 2015. Mantis Innovations

CREATE TABLE odc_users (
    user_username VARCHAR(250) NOT NULL PRIMARY KEY,
    user_password VARCHAR(250) NOT NULL,
    user_password_salt VARCHAR(250) NOT NULL,
    user_roles VARCHAR(250)
);

CREATE TABLE odc_datasources (
    ds_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    ds_name VARCHAR(250) NOT NULL,
    ds_jdbc VARCHAR(250) NOT NULL,
    ds_driver VARCHAR(50) NOT NULL,
    is_auto_commit BOOLEAN,
    conn_timeout_ms BIGINT,
    idle_timeout_ms BIGINT,
    max_lifetime_ms BIGINT,
    max_pool_size INT,
    min_idle INT
);

CREATE TABLE odc_attachment_mappings (
    am_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    am_table_name VARCHAR(250) NOT NULL,
    am_table_field VARCHAR (250),
    am_key_field VARCHAR (250),
    am_name_field VARCHAR(250) NOT NULL,
    am_file_name_field VARCHAR(250) NOT NULL,
    am_description_field VARCHAR(250) NOT NULL,
    am_file_field VARCHAR(250) NOT NULL
);

CREATE TABLE odc_mappings (
    m_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    m_name VARCHAR(250) NOT NULL,
    ds_id BIGINT,
    m_version VARCHAR(50) NOT NULL,
    m_token VARCHAR(50) NOT NULL,
    m_bind_value VARCHAR(250) NOT NULL,
    m_deploy_on_startup BOOLEAN NOT NULL,
    m_supports_attachments BOOLEAN NOT NULL,
    am_id BIGINT NOT NULL,
    FOREIGN KEY (ds_id) REFERENCES odc_datasources (ds_id) ON DELETE SET NULL,
    FOREIGN KEY (am_id) REFERENCES odc_attachment_mappings (am_id) ON DELETE CASCADE
);

CREATE TABLE odc_table_mappings (
    tm_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    opa_name VARCHAR(250),
    source_name VARCHAR(250) NOT NULL,
    tm_link_name VARCHAR(250),
    tm_one_to_one BOOLEAN NOT NULL,
    tm_inferred BOOLEAN NOT NULL,
    parent_opa_name VARCHAR(250),
    parent_source_name VARCHAR(250) NOT NULL,
    m_id BIGINT,
    FOREIGN KEY (m_id) REFERENCES odc_mappings (m_id) ON DELETE CASCADE
);

CREATE TABLE odc_field_mappings (
    fm_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    opa_name VARCHAR(250),
    source_name VARCHAR(250) NOT NULL,
    fm_type VARCHAR(50),
    fm_primary_key BOOLEAN NOT NULL,
    fm_primary_sequence VARCHAR(250),
    fm_foreign_key BOOLEAN NOT NULL,
    fm_required BOOLEAN NOT NULL,
    fm_input BOOLEAN NOT NULL,
    fm_output BOOLEAN NOT NULL,
    tm_id BIGINT NOT NULL,
    FOREIGN KEY (tm_id) REFERENCES odc_table_mappings (tm_id) ON DELETE CASCADE
);

CREATE TABLE odc_bind_mappings (
    bm_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    bm_token_name VARCHAR (250) NOT NULL,
    bm_required BOOLEAN NOT NULL,
    bm_condition VARCHAR (50) NOT NULL,
    bm_operator VARCHAR (50) NOT NULL,
    table_opa_name VARCHAR(250),
    table_source_name VARCHAR(250) NOT NULL,
    field_opa_name VARCHAR(250),
    field_source_name VARCHAR(250) NOT NULL,
    m_id BIGINT NOT NULL,
    FOREIGN KEY (m_id) REFERENCES odc_mappings (m_id) ON DELETE CASCADE
);

CREATE TABLE odc_j_table_mappings (
    jtm_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    opa_name VARCHAR(250),
    source_name VARCHAR(250) NOT NULL,
    jtm_link_name VARCHAR(250),
    jtm_link_type VARCHAR(50),
    jtm_from_key_name VARCHAR(250) NOT NULL,
    jtm_to_key_name VARCHAR(250) NOT NULL,
    from_table_opa_name VARCHAR(250),
    from_table_source_name VARCHAR(250) NOT NULL,
    to_table_opa_name VARCHAR(250),
    to_table_source_name VARCHAR(250) NOT NULL,
    m_id BIGINT NOT NULL,
    FOREIGN KEY (m_id) REFERENCES odc_mappings (m_id) ON DELETE CASCADE
);

CREATE TABLE odc_operations (
    op_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    op_date TIMESTAMP NOT NULL,
    op_type INT NOT NULL,
    m_id BIGINT NOT NULL,
    op_inbound_packet_size BIGINT NOT NULL,
    op_outbound_packet_size BIGINT NOT NULL,
    op_start_request BIGINT NOT NULL,
    op_end_request BIGINT NOT NULL,
    op_start_db BIGINT NOT NULL,
    op_end_db BIGINT NOT NULL,
    FOREIGN KEY (m_id) REFERENCES odc_mappings (m_id) ON DELETE CASCADE
);
