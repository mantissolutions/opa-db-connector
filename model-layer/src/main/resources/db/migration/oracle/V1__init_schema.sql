-- Copyright (c) 2015. Mantis Innovations
CREATE TABLE odc_users (
    user_username VARCHAR2(250) NOT NULL PRIMARY KEY,
    user_password VARCHAR2(250) NOT NULL,
    user_password_salt VARCHAR2(250) NOT NULL,
    user_roles VARCHAR2(250)
);

CREATE TABLE odc_datasources (
    ds_id NUMBER NOT NULL PRIMARY KEY,
    ds_name VARCHAR2(250) NOT NULL,
    ds_jdbc VARCHAR2(250) NOT NULL,
    ds_driver VARCHAR2(50) NOT NULL,
    is_auto_commit NUMBER(1),
    conn_timeout_ms NUMBER,
    idle_timeout_ms NUMBER,
    max_lifetime_ms NUMBER,
    max_pool_size NUMBER,
    min_idle NUMBER
);

CREATE TABLE odc_attachment_mappings (
    am_id NUMBER NOT NULL PRIMARY KEY,
    am_table_name VARCHAR2(250) NOT NULL,
    am_table_field VARCHAR2 (250),
    am_key_field VARCHAR2 (250),
    am_name_field VARCHAR2(250) NOT NULL,
    am_file_name_field VARCHAR2(250) NOT NULL,
    am_description_field VARCHAR2(250) NOT NULL,
    am_file_field VARCHAR2(250) NOT NULL
);

CREATE TABLE odc_mappings (
    m_id NUMBER NOT NULL PRIMARY KEY,
    m_name VARCHAR2(250) NOT NULL,
    ds_id NUMBER,
    m_version VARCHAR2(50) NOT NULL,
    m_token VARCHAR2(50) NOT NULL,
    m_bind_value VARCHAR2(250) NOT NULL,
    m_deploy_on_startup NUMBER(1) NOT NULL,
    m_supports_attachments NUMBER(1) NOT NULL,
    am_id NUMBER NOT NULL,
    FOREIGN KEY (ds_id) REFERENCES odc_datasources (ds_id) ON DELETE SET NULL,
    FOREIGN KEY (am_id) REFERENCES odc_attachment_mappings (am_id) ON DELETE CASCADE
);

CREATE TABLE odc_table_mappings (
    tm_id NUMBER NOT NULL PRIMARY KEY,
    opa_name VARCHAR(250),
    source_name VARCHAR(250) NOT NULL,
    tm_link_name VARCHAR2(250),
    tm_one_to_one NUMBER(1) NOT NULL,
    tm_inferred NUMBER(1) NOT NULL,
    parent_opa_name VARCHAR(250),
    parent_source_name VARCHAR(250) NOT NULL,
    m_id NUMBER,
    FOREIGN KEY (m_id) REFERENCES odc_mappings (m_id) ON DELETE CASCADE
);

CREATE TABLE odc_field_mappings (
    fm_id NUMBER NOT NULL PRIMARY KEY,
    opa_name VARCHAR2(250),
    source_name VARCHAR2(250) NOT NULL,
    fm_type VARCHAR2(50),
    fm_primary_key NUMBER(1) NOT NULL,
    fm_primary_sequence VARCHAR2(250),
    fm_foreign_key NUMBER(1) NOT NULL,
    fm_required NUMBER(1) NOT NULL,
    fm_input NUMBER(1) NOT NULL,
    fm_output NUMBER(1) NOT NULL,
    tm_id NUMBER NOT NULL,
    FOREIGN KEY (tm_id) REFERENCES odc_table_mappings (tm_id) ON DELETE CASCADE
);

CREATE TABLE odc_bind_mappings (
    bm_id NUMBER NOT NULL PRIMARY KEY,
    bm_token_name VARCHAR2 (250) NOT NULL,
    bm_required NUMBER(1) NOT NULL,
    bm_condition VARCHAR2 (50) NOT NULL,
    bm_operator VARCHAR2 (50) NOT NULL,
    table_opa_name VARCHAR2(250),
    table_source_name VARCHAR2(250) NOT NULL,
    field_opa_name VARCHAR2(250),
    field_source_name VARCHAR2(250) NOT NULL,
    m_id NUMBER NOT NULL,
    FOREIGN KEY (m_id) REFERENCES odc_mappings (m_id) ON DELETE CASCADE
);

CREATE TABLE odc_j_table_mappings (
    jtm_id NUMBER NOT NULL PRIMARY KEY,
    opa_name VARCHAR2(250),
    source_name VARCHAR2(250) NOT NULL,
    jtm_link_name VARCHAR2(250),
    jtm_link_type VARCHAR2(50),
    jtm_from_key_name VARCHAR2(250) NOT NULL,
    jtm_to_key_name VARCHAR2(250) NOT NULL,
    from_table_opa_name VARCHAR2(250),
    from_table_source_name VARCHAR2(250) NOT NULL,
    to_table_opa_name VARCHAR2(250),
    to_table_source_name VARCHAR2(250) NOT NULL,
    m_id NUMBER NOT NULL,
    FOREIGN KEY (m_id) REFERENCES odc_mappings (m_id) ON DELETE CASCADE
);

CREATE TABLE odc_operations (
    op_id NUMBER NOT NULL PRIMARY KEY,
    op_date TIMESTAMP NOT NULL,
    op_type NUMBER NOT NULL,
    m_id NUMBER NOT NULL,
    op_inbound_packet_size NUMBER NOT NULL,
    op_outbound_packet_size NUMBER NOT NULL,
    op_start_request NUMBER NOT NULL,
    op_end_request NUMBER NOT NULL,
    op_start_db NUMBER NOT NULL,
    op_end_db NUMBER NOT NULL,
    FOREIGN KEY (m_id) REFERENCES odc_mappings (m_id) ON DELETE CASCADE
);

create sequence odc_attachment_mappings_seq;
create sequence odc_datasources_seq;
create sequence odc_field_mappings_seq;
create sequence odc_j_table_mappings_seq;
create sequence odc_mappings_seq;
create sequence odc_operations_seq;
create sequence odc_table_mappings_seq;
create sequence odc_bind_mappings_seq;
