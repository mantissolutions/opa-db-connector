UPDATE odc_field_mappings
SET fm_type = 'STRING_TYPE'
WHERE fm_type = 'STRING';

UPDATE odc_field_mappings
SET fm_type = 'DECIMAL_TYPE'
WHERE fm_type = 'DECIMAL';

UPDATE odc_field_mappings
SET fm_type = 'BOOLEAN_TYPE'
WHERE fm_type = 'BOOLEAN';

UPDATE odc_field_mappings
SET fm_type = 'DATE_TYPE'
WHERE fm_type = 'DATE';

UPDATE odc_field_mappings
SET fm_type = 'DATE_TIME_TYPE'
WHERE fm_type = 'DATETIME';

UPDATE odc_field_mappings
SET fm_type = 'TIME_TYPE'
WHERE fm_type = 'TIMEOFDAY';

UPDATE odc_field_mappings
SET fm_type = 'ATTACHMENT_TYPE'
WHERE fm_type = 'ATTACHMENT';