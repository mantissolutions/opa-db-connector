ALTER TABLE odc_mappings ADD COLUMN m_pre_load_script CLOB;
ALTER TABLE odc_mappings ADD COLUMN m_post_load_script CLOB;
ALTER TABLE odc_mappings ADD COLUMN m_pre_save_script CLOB;
ALTER TABLE odc_mappings ADD COLUMN m_post_save_script CLOB;

ALTER TABLE odc_batch_mappings ADD COLUMN m_pre_script CLOB;
ALTER TABLE odc_batch_mappings ADD COLUMN m_post_script CLOB;