CREATE TABLE odc_widget_config (
    widget_id VARCHAR2(250) NOT NULL PRIMARY KEY,
    widget_x NUMBER NOT NULL,
    widget_y NUMBER NOT NULL,
    widget_width NUMBER NOT NULL,
    widget_height NUMBER NOT NULL,
    widget_settings VARCHAR2(4000),
    user_username VARCHAR2(250) NOT NULL,
    FOREIGN KEY (user_username) REFERENCES odc_users (user_username) ON DELETE CASCADE
);