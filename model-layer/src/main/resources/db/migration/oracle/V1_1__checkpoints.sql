CREATE TABLE odc_checkpoint_mappings (
    ch_id NUMBER NOT NULL PRIMARY KEY,
    ch_table_name VARCHAR2(250) NOT NULL,
    ch_bind_token VARCHAR2(250) NOT NULL,
    ch_id_field VARCHAR2 (250)  NOT NULL,
    ch_data_field VARCHAR2 (250)  NOT NULL
);

ALTER TABLE odc_mappings ADD COLUMN m_supports_checkpoints NUMBER(1) NOT NULL;
ALTER TABLE odc_mappings ADD COLUMN ch_id NUMBER NOT NULL;
ALTER TABLE odc_mappings ADD FOREIGN KEY (ch_id) REFERENCES odc_checkpoint_mappings (ch_id) ON DELETE CASCADE;

CREATE SEQUENCE odc_checkpoint_mappings_seq;