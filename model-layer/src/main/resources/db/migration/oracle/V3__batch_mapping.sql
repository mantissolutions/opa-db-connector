CREATE TABLE odc_batch_mappings(
    bam_id NUMBER NOT NULL PRIMARY KEY,
    bam_name VARCHAR2(250),
    bam_hub_url VARCHAR2(250),
    bam_deployment_name VARCHAR2(250),
    bam_deployment_version NUMBER,
    bam_deploy_on_startup NUMBER(1),
    bam_root_table_id NUMBER,
    m_id NUMBER,
    FOREIGN KEY (bam_root_table_id) REFERENCES odc_table_mappings (tm_id) ON DELETE CASCADE,
    FOREIGN KEY (m_id) REFERENCES odc_mappings (m_id) ON DELETE CASCADE
);

create sequence odc_batch_mappings_seq;