CREATE TABLE odc_enumeration_mappings(
    em_id NUMBER NOT NULL PRIMARY KEY,
    em_name VARCHAR2(250) NOT NULL,
    em_type VARCHAR2(50) NOT NULL,
    em_sql VARCHAR2(250) NOT NULL,
    em_child_name VARCHAR2(250),
    m_id NUMBER,
    FOREIGN KEY (m_id) REFERENCES odc_mappings (m_id) ON DELETE CASCADE
);

CREATE SEQUENCE odc_enumeration_mappings_seq;