ALTER TABLE odc_mappings MODIFY am_id NUMBER;
ALTER TABLE odc_mappings MODIFY ch_id NUMBER;
ALTER TABLE odc_mappings DROP COLUMN m_token;
ALTER TABLE odc_table_mappings MODIFY COLUMN parent_source_name VARCHAR2(250);