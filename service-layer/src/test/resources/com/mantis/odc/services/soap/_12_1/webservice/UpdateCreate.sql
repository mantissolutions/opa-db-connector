CREATE TABLE catalog (catalog_id NUMBER) ;

CREATE SEQUENCE catalog_seq;

CREATE TABLE camera (camera_id NUMBER UNIQUE NOT NULL,
catalog_id NUMBER,
camera_model VARCHAR,
color_code VARCHAR,
has_portrait BOOLEAN,
has_redeye BOOLEAN,
has_timer BOOLEAN,
has_flash BOOLEAN,
price NUMBER) ;

CREATE SEQUENCE camera_seq;

CREATE TABLE customer_request (request_ID NUMBER,
request_date DATE,
first_name VARCHAR,
last_name VARCHAR,
email VARCHAR,
comments VARCHAR) ;

CREATE SEQUENCE customer_request_seq;

CREATE TABLE customer_request_att (A_TABLE_NAME VARCHAR,
request_ID NUMBER,
A_NAME VARCHAR,
A_FILE_NAME VARCHAR,
A_DESCRIPTION VARCHAR,
A_FILE BLOB) ;

CREATE TABLE customer_request_camera (request_ID NUMBER,
camera_id NUMBER) ;


CREATE TABLE ph_number (phone_number_id NUMBER,
camera_request_id NUMBER,
phone_number VARCHAR,
is_preferred BOOLEAN) ;

CREATE SEQUENCE ph_number_seq;

CREATE TABLE ph_number_type (phone_number_id NUMBER,
phone_type_id NUMBER) ;

CREATE TABLE phone_type (phone_type_id NUMBER,
catalog_id NUMBER,
type_desc VARCHAR) ;

CREATE SEQUENCE phone_type_seq;

INSERT INTO catalog (catalog_id) VALUES (1);

INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'BenchmarkPro', 'STD', 1, 1, 1, 1, 85.00);
INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'BenchmarkX', 'STD', 1, 0, 0, 1, 69.99);
INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'BrightShot-EM', 'STDBG', 0, 1, 1, 1, 79.99);
INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'ColorAnswer-NJ', 'ALL', 0, 0, 1, 0, 42);
INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'DC-100', 'STD', 1, 0, 1, 0, 28.99);
INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'NightBright-PP', 'ALL', 0, 1, 0, 1, 60);
INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'PicElite-400', 'STDBG', 1, 1, 1, 1, 140);
INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'ProperPic-55', 'ALL', 1, 1, 0, 1, 90);
INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'SimpleFlash-11', 'STD', 0, 0, 0, 1, 40);
INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'SimpleSnap-XS', 'STD', 0, 0, 0, 0, 29.99);
INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'SnapBlue-22', 'STDBG', 0, 0, 0, 0, 30);
INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'SuperSelfie-33', 'STDBG', 1, 0, 1, 0, 60);
INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'TimeLapse-50', 'STD', 0, 0, 1, 0, 49.99);
INSERT INTO camera (camera_id, catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (camera_seq.NEXTVAL, 1, 'TruePic-80', 'STD', 0, 1, 1, 1, 49.99);

INSERT INTO phone_type (phone_type_id, catalog_id, type_desc) VALUES (1, 1, 'Home');
INSERT INTO phone_type (phone_type_id, catalog_id, type_desc) VALUES (2, 1, 'Work');
INSERT INTO phone_type (phone_type_id, catalog_id, type_desc) VALUES (3, 1, 'Mobile');

INSERT INTO customer_request (request_id, first_name, email, request_date, last_name, comments) VALUES (1, 'Ben', 'ben.mazzarol@mantissolutions.com.au', CURRENT_DATE, 'Mazzarol', null);

INSERT INTO ph_number (phone_number_id, camera_request_id, phone_number, is_preferred) VALUES (1, 1, '0431974587', true);
INSERT INTO ph_number (phone_number_id, camera_request_id, phone_number, is_preferred) VALUES (2, 1, '0245965417', false);

INSERT INTO customer_request_camera (request_id, camera_id) VALUES (1, 101);
INSERT INTO customer_request_camera (request_id, camera_id) VALUES (1, 103);
INSERT INTO customer_request_camera (request_id, camera_id) VALUES (1, 114);

INSERT INTO ph_number_type (phone_number_id, phone_type_id) VALUES (1, 2);
INSERT INTO ph_number_type (phone_number_id, phone_type_id) VALUES (2, 1);