CREATE TABLE catalog (catalog_id NUMBER) ;

CREATE TABLE camera (camera_id NUMBER AUTO_INCREMENT,
catalog_id NUMBER,
camera_model VARCHAR,
color_code VARCHAR,
has_portrait BOOLEAN,
has_redeye BOOLEAN,
has_timer BOOLEAN,
has_flash BOOLEAN,
price NUMBER) ;

CREATE TABLE customer_request (request_ID NUMBER AUTO_INCREMENT,
request_date DATE,
first_name VARCHAR,
last_name VARCHAR,
email VARCHAR,
comments VARCHAR) ;

CREATE TABLE customer_request_att (A_TABLE_NAME VARCHAR ,
request_ID NUMBER,
A_NAME VARCHAR,
A_FILE_NAME VARCHAR,
A_DESCRIPTION VARCHAR,
A_FILE BLOB) ;

CREATE TABLE customer_request_camera (request_ID NUMBER AUTO_INCREMENT,
camera_id NUMBER) ;


CREATE TABLE ph_number (phone_number_id NUMBER AUTO_INCREMENT,
camera_request_id NUMBER,
phone_number VARCHAR,
is_preferred BOOLEAN) ;

CREATE TABLE ph_number_type (phone_number_id NUMBER AUTO_INCREMENT,
phone_type_id NUMBER) ;

CREATE TABLE phone_type (phone_type_id NUMBER AUTO_INCREMENT,
catalog_id NUMBER,
type_desc VARCHAR) ;

INSERT INTO catalog (catalog_id) VALUES (1);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'BenchmarkPro', 'STD', 1, 1, 1, 1, 85.00);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'BenchmarkX', 'STD', 1, 0, 0, 1, 69.99);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'BrightShot-EM', 'STDBG', 0, 1, 1, 1, 79.99);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'ColorAnswer-NJ', 'ALL', 0, 0, 1, 0, 42);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'DC-100', 'STD', 1, 0, 1, 0, 28.99);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'NightBright-PP', 'ALL', 0, 1, 0, 1, 60);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'PicElite-400', 'STDBG', 1, 1, 1, 1, 140);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'ProperPic-55', 'ALL', 1, 1, 0, 1, 90);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'SimpleFlash-11', 'STD', 0, 0, 0, 1, 40);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'SimpleSnap-XS', 'STD', 0, 0, 0, 0, 29.99);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'SnapBlue-22', 'STDBG', 0, 0, 0, 0, 30);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'SuperSelfie-33', 'STDBG', 1, 0, 1, 0, 60);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'TimeLapse-50', 'STD', 0, 0, 1, 0, 49.99);
INSERT INTO camera (catalog_id, camera_model, color_code, has_portrait, has_redeye, has_timer, has_flash, price) VALUES (1, 'TruePic-80', 'STD', 0, 1, 1, 1, 49.99);
INSERT INTO phone_type (phone_type_id, catalog_id, type_desc) VALUES (1, 1, 'Home');
INSERT INTO phone_type (phone_type_id, catalog_id, type_desc) VALUES (2, 1, 'Work');
INSERT INTO phone_type (phone_type_id, catalog_id, type_desc) VALUES (3, 1, 'Mobile');