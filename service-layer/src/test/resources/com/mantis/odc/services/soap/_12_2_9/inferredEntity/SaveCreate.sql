CREATE TABLE non_inferred (
    pk DECIMAL NOT NULL PRIMARY KEY AUTO_INCREMENT
   ,some_data VARCHAR2(200) NOT NULL
);

CREATE TABLE inferred (
    pk DECIMAL NOT NULL PRIMARY KEY AUTO_INCREMENT
   ,some_inf_data VARCHAR2(200) NOT NULL
   ,fk DECIMAL NOT NULL
   ,FOREIGN KEY (fk) REFERENCES non_inferred (pk)
);
