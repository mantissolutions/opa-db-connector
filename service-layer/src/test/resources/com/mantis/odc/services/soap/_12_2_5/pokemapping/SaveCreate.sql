create table poke_people(person_id number, first_name varchar2(4000), poke_caught number, last_name varchar2(4000), allowance_amount number, dob date);

create table poke_allowances(allowance_id number, person_id number, poke_caught number, eligible_flag number(1), allowance_amount number, date_tested date);

create sequence poke_allowance_id_seq;

insert into poke_people(person_id, first_name, last_name, poke_caught, dob) values (1,'Ash','Ketchum', 1, PARSEDATETIME('16/08/1986', 'dd/MM/yyyy'));