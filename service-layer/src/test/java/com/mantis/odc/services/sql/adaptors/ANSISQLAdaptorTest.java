/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.sql.adaptors;

import com.mantis.odc.models.ConditionalJoin;
import com.mantis.odc.models.DataType;
import com.mantis.odc.models.Operator;
import com.mantis.odc.services.models.*;
import com.mantis.odc.services.models.base.DatabaseTable;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.mantis.odc.models.ConditionalJoin.and;
import static com.mantis.odc.models.ConditionalJoin.or;
import static com.mantis.odc.models.DataType.*;
import static com.mantis.odc.models.Driver.Oracle;
import static java.util.Collections.EMPTY_LIST;
import static org.junit.Assert.assertEquals;

/**
 * Test the SQL generation of the standard ANSI adaptor.
 */
public class ANSISQLAdaptorTest {

    private ANSISQLAdaptor adaptor;

    private LoadModel loadModel;
    private LoadModel specialLoad;
    private LoadModel specialLoad2;
    private ContainmentTable childTableAuto;

    @Before
    public void setUp() {
        adaptor = new ANSISQLAdaptor(Oracle);

        ContainmentTable childTable = ContainmentTable.builder()
                .source("CHILDREN")
                .alias("t1")
                .primaryKey(PrimaryKeyField.builder()
                        .source("CHILD_ID")
                        .opaName("child id")
                        .type(DECIMAL_TYPE)
                        .sequence("CHILD_SEQ.NEXTVAL")
                        .build())
                .field(new DatabaseField("CHILD_NAME", "child name", STRING_TYPE))
                .field(new DatabaseField("CHILD_AGE", "child age", DECIMAL_TYPE))
                .build();

        ContainmentTable toyTable = ContainmentTable.builder()
                .source("TOYS")
                .alias("t2")
                .primaryKey(PrimaryKeyField.builder()
                        .source("TOY_ID")
                        .opaName("toy id")
                        .type(DECIMAL_TYPE)
                        .sequence("TOY_SEQ.NEXTVAL")
                        .build())
                .foreignKey(ForeignKeyField.builder()
                        .source("CHILD_ID")
                        .type(DECIMAL_TYPE)
                        .target("CHILDREN")
                        .targetOpaName("child")
                        .build())
                .field(new DatabaseField("TOY_NAME", "toy name", STRING_TYPE))
                .field(new DatabaseField("TOY_COST", "toy cost", STRING_TYPE))
                .field(new DatabaseField("EXP_DATE", "expiry date", DATE_TIME_TYPE))
                .field(new DatabaseField("SECOND_HAND", "second hand", BOOLEAN_TYPE))
                .build();

        ContainmentTable petsTable = ContainmentTable.builder()
                .source("PETS")
                .alias("t3")
                .primaryKey(PrimaryKeyField.builder()
                        .source("PET_ID")
                        .opaName("pet id")
                        .type(DECIMAL_TYPE)
                        .sequence("PET_SEQ.NEXTVAL")
                        .build())
                .foreignKey(ForeignKeyField.builder()
                        .source("CHILD_ID")
                        .type(DECIMAL_TYPE)
                        .target("CHILDREN")
                        .targetOpaName("child")
                        .build())
                .field(new DatabaseField("PET_NAME", "pet name", STRING_TYPE))
                .field(new DatabaseField("PET_SPECIES", "pet species", STRING_TYPE))
                .field(new DatabaseField("FEEDING_TIME", "feeding time", TIME_TYPE))
                .build();

        JunctionTable toyPetTable = JunctionTable.builder()
                .source("TOYS_PETS")
                .alias("jt1")
                .fromKey(ForeignKeyField.builder()
                        .source("TOY_ID")
                        .type(DECIMAL_TYPE)
                        .target("TOYS")
                        .targetOpaName("toy")
                        .build())
                .toKey(ForeignKeyField.builder()
                        .source("PET_ID")
                        .type(DECIMAL_TYPE)
                        .target("PETS")
                        .targetOpaName("pet")
                        .build())
                .build();

        loadModel = LoadModel.builder()
                .rootTable("CHILDREN")
                .table(childTable)
                .table(toyTable)
                .table(petsTable)
                .table(toyPetTable)
                .condition(BindCondition.builder()
                        .alias("t1")
                        .source("CHILD_NAME")
                        .join(and)
                        .operator(Operator.equals)
                        .build()
                        .toGroup())
                .condition(BindCondition.builder()
                        .alias("t1")
                        .source("CHILD_AGE")
                        .join(or)
                        .operator(Operator.greaterThan)
                        .build()
                        .toGroup())
                .build();

        specialLoad = loadModel.toBuilder()
                .table(ContainmentTable.builder()
                        .source("TEST_TABLE")
                        .alias("tt6")
                        .inferred(true)
                        .primaryKey(new PrimaryKeyField("TEST_ID", DECIMAL_TYPE, false, "TEST"))
                        .foreignKey(new ForeignKeyField("TOY_ID", DECIMAL_TYPE, "TOYS", "toy"))
                        .build())
                .build();

        childTableAuto = ContainmentTable.builder()
                .source("CHILDREN")
                .alias("t1")
                .primaryKey(PrimaryKeyField.builder()
                        .source("CHILD_ID")
                        .opaName("child id")
                        .autoIncrement(true)
                        .type(DECIMAL_TYPE)
                        .sequence("CHILD_SEQ.NEXTVAL")
                        .build())
                .field(new DatabaseField("CHILD_NAME", "child name", STRING_TYPE))
                .field(new DatabaseField("CHILD_AGE", "child age", DECIMAL_TYPE))
                .build();

        specialLoad2 = loadModel.toBuilder()
                .table(JunctionTable.builder()
                        .source("TOYS_TOYS")
                        .alias("jt2")
                        .fromKey(ForeignKeyField.builder()
                                .source("TOY_ID_1")
                                .type(DECIMAL_TYPE)
                                .target("TOYS")
                                .targetOpaName("toy")
                                .build())
                        .toKey(ForeignKeyField.builder()
                                .source("TOY_ID_2")
                                .type(DECIMAL_TYPE)
                                .target("TOYS")
                                .targetOpaName("toy")
                                .build())
                        .build())
                .build();

    }

    @Test
    public void testCreateContainmentSelect1() {
        assertEquals("SELECT t1.CHILD_ID AS \"child id\", t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE t1.CHILD_ID = :CHILD_ID",
                adaptor.createContainmentSelect(loadModel, getContainmentTable("CHILDREN"), EMPTY_LIST, true, true));
    }

    @Test
    public void testCreateContainmentSelectNoNamedBinds1() {
        assertEquals("SELECT t1.CHILD_ID AS \"child id\", t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE t1.CHILD_ID = ?",
                adaptor.createContainmentSelect(loadModel, getContainmentTable("CHILDREN"), EMPTY_LIST, false, true));
    }

    @Test
    public void testCreateContainmentSelect2() {
        assertEquals("SELECT t1.CHILD_ID AS \"child id\", t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE t1.CHILD_ID = :CHILD_ID AND t1.CHILD_NAME = :CHILD_NAME OR t1.CHILD_AGE > :CHILD_AGE",
                adaptor.createContainmentSelect(loadModel, getContainmentTable("CHILDREN"), loadModel.getConditions(), true, true));
    }

    @Test
    public void testCreateContainmentSelectNoNamedBinds2() {
        assertEquals("SELECT t1.CHILD_ID AS \"child id\", t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE t1.CHILD_ID = ? AND t1.CHILD_NAME = ? OR t1.CHILD_AGE > ?",
                adaptor.createContainmentSelect(loadModel, getContainmentTable("CHILDREN"), loadModel.getConditions(), false, true));
    }

    @Test
    public void testCreateContainmentSelect3() {
        assertEquals("SELECT t1.CHILD_ID AS \"child id\", t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE t1.CHILD_ID = :CHILD_ID AND t1.CHILD_NAME IN (:CHILD_NAME_0, :CHILD_NAME_1, :CHILD_NAME_2)",
                adaptor.createContainmentSelect(loadModel, getContainmentTable("CHILDREN"), Collections.singletonList(BindCondition.builder()
                        .alias("t1")
                        .source("CHILD_NAME")
                        .join(and)
                        .operator(Operator.in)
                        .value("ben")
                        .value("john")
                        .value("jess")
                        .build()
                        .toGroup()), true, true));
    }

    @Test
    public void testCreateContainmentSelectNoNamedBinds3() {
        assertEquals("SELECT t1.CHILD_ID AS \"child id\", t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE t1.CHILD_ID = ? AND t1.CHILD_NAME IN (?, ?, ?)",
                adaptor.createContainmentSelect(loadModel, getContainmentTable("CHILDREN"), Collections.singletonList(BindCondition.builder()
                        .alias("t1")
                        .source("CHILD_NAME")
                        .join(and)
                        .operator(Operator.in)
                        .value("ben")
                        .value("john")
                        .value("jess")
                        .build()
                        .toGroup()), false, true));
    }

    @Test
    public void testCreateContainmentSelect4() {
        assertEquals("SELECT t2.TOY_ID AS \"toy id\", t2.TOY_NAME AS \"toy name\", t2.TOY_COST AS \"toy cost\", t2.EXP_DATE AS \"expiry date\", t2.SECOND_HAND AS \"second hand\", t2.CHILD_ID AS \"CHILD_ID\" FROM TOYS t2 " +
                        "INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID) WHERE t1.CHILD_ID = :CHILD_ID",
                adaptor.createContainmentSelect(loadModel, getContainmentTable("TOYS"), EMPTY_LIST, true, true));
    }

    @Test
    public void testCreateContainmentSelectNoNamedBinds4() {
        assertEquals("SELECT t2.TOY_ID AS \"toy id\", t2.TOY_NAME AS \"toy name\", t2.TOY_COST AS \"toy cost\", t2.EXP_DATE AS \"expiry date\", t2.SECOND_HAND AS \"second hand\", t2.CHILD_ID AS \"CHILD_ID\" FROM TOYS t2 " +
                        "INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID) WHERE t1.CHILD_ID = ?",
                adaptor.createContainmentSelect(loadModel, getContainmentTable("TOYS"), EMPTY_LIST, false, true));
    }


    @Test
    public void testCreateContainmentSelect5() {
        assertEquals("SELECT t1.CHILD_ID AS \"child id\", t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE 1 = 1 AND t1.CHILD_NAME = :CHILD_NAME OR t1.CHILD_AGE > :CHILD_AGE",
                adaptor.createContainmentSelect(loadModel, getContainmentTable("CHILDREN"), loadModel.getConditions(), true, false));
    }

    @Test
    public void testCreateContainmentSelectNoNamedBinds5() {
        assertEquals("SELECT t1.CHILD_ID AS \"child id\", t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE 1 = 1 AND t1.CHILD_NAME = ? OR t1.CHILD_AGE > ?",
                adaptor.createContainmentSelect(loadModel, getContainmentTable("CHILDREN"), loadModel.getConditions(), false, false));
    }

    private ContainmentTable getContainmentTable(String source) {
        return (ContainmentTable) loadModel.getTable(source).get();
    }

    @Test
    public void testCreateDelete1() {
        assertEquals("DELETE FROM CHILDREN WHERE CHILD_ID = :CHILD_ID",
                adaptor.createDelete(loadModel, getContainmentTable("CHILDREN"), true));
    }

    @Test
    public void testCreateDeleteNoNamedBinds1() {
        assertEquals("DELETE FROM CHILDREN WHERE CHILD_ID = ?",
                adaptor.createDelete(loadModel, getContainmentTable("CHILDREN"), false));
    }

    @Test
    public void testCreateDelete2() {
        assertEquals("DELETE FROM TOYS WHERE TOY_ID = :TOY_ID",
                adaptor.createDelete(loadModel, getContainmentTable("TOYS"), true));
    }

    @Test
    public void testCreateDeleteNoNamedBinds2() {
        assertEquals("DELETE FROM TOYS WHERE TOY_ID = ?",
                adaptor.createDelete(loadModel, getContainmentTable("TOYS"), false));
    }

    @Test
    public void testCreateDelete3() {
        assertEquals("DELETE FROM TOYS_PETS" +
                        " WHERE TOY_ID IN (SELECT t2.TOY_ID" +
                        " FROM TOYS t2" +
                        " INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = :CHILD_ID)" +
                        " AND PET_ID IN (SELECT t3.PET_ID" +
                        " FROM PETS t3" +
                        " INNER JOIN CHILDREN t1 ON (t3.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = :CHILD_ID)",
                adaptor.createDelete(loadModel, getJunctionTable("TOYS_PETS"), true));
    }

    @Test
    public void testCreateDeleteNoNamedBinds3() {
        assertEquals("DELETE FROM TOYS_PETS" +
                        " WHERE TOY_ID IN (SELECT t2.TOY_ID" +
                        " FROM TOYS t2" +
                        " INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = ?)" +
                        " AND PET_ID IN (SELECT t3.PET_ID" +
                        " FROM PETS t3" +
                        " INNER JOIN CHILDREN t1 ON (t3.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = ?)",
                adaptor.createDelete(loadModel, getJunctionTable("TOYS_PETS"), false));
    }

    @Test
    public void testCreateDelete4() {
        assertEquals("DELETE FROM TEST_TABLE" +
                        " WHERE TEST_ID IN (SELECT tt6.TEST_ID" +
                        " FROM TEST_TABLE tt6" +
                        " INNER JOIN TOYS t2 ON (tt6.TOY_ID = t2.TOY_ID)" +
                        " INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = :CHILD_ID)",
                adaptor.createDelete(specialLoad, specialLoad.getTable("TEST_TABLE").get(), true));
    }

    @Test
    public void testCreateDeleteNoNamedBinds4() {
        assertEquals("DELETE FROM TEST_TABLE" +
                        " WHERE TEST_ID IN (SELECT tt6.TEST_ID" +
                        " FROM TEST_TABLE tt6" +
                        " INNER JOIN TOYS t2 ON (tt6.TOY_ID = t2.TOY_ID)" +
                        " INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = ?)",
                adaptor.createDelete(specialLoad, specialLoad.getTable("TEST_TABLE").get(), false));
    }

    private JunctionTable getJunctionTable(String source) {
        return (JunctionTable) loadModel.getTable(source).get();
    }

    @Test
    public void testCreateInsert1() {
        assertEquals("INSERT INTO CHILDREN(CHILD_ID, CHILD_NAME, CHILD_AGE) VALUES (:CHILD_ID, :CHILD_NAME, :CHILD_AGE)",
                adaptor.createInsert(getContainmentTable("CHILDREN"), getContainmentTable("CHILDREN").getInsertFields(null), true));
    }

    @Test
    public void testCreateInsertNoNamedBinds1() {
        assertEquals("INSERT INTO CHILDREN(CHILD_ID, CHILD_NAME, CHILD_AGE) VALUES (?, ?, ?)",
                adaptor.createInsert(getContainmentTable("CHILDREN"), getContainmentTable("CHILDREN").getInsertFields(null), false));
    }

    @Test
    public void testCreateInsert2() {
        assertEquals("INSERT INTO CHILDREN(CHILD_NAME, CHILD_AGE) VALUES (:CHILD_NAME, :CHILD_AGE)",
                adaptor.createInsert(childTableAuto, childTableAuto.getInsertFields(null), true));
    }

    @Test
    public void testCreateInsertNoNamedBinds2() {
        assertEquals("INSERT INTO CHILDREN(CHILD_NAME, CHILD_AGE) VALUES (?, ?)",
                adaptor.createInsert(childTableAuto, childTableAuto.getInsertFields(null), false));
    }

    @Test
    public void testCreateInsert3() {
        assertEquals("INSERT INTO TOYS(TOY_ID, CHILD_ID, TOY_NAME, TOY_COST, EXP_DATE, SECOND_HAND) VALUES (:TOY_ID, :CHILD_ID, :TOY_NAME, :TOY_COST, :EXP_DATE, :SECOND_HAND)",
                adaptor.createInsert(getContainmentTable("TOYS"), getContainmentTable("TOYS").getInsertFields(null), true));
    }

    @Test
    public void testCreateInsertNoNamedBinds3() {
        assertEquals("INSERT INTO TOYS(TOY_ID, CHILD_ID, TOY_NAME, TOY_COST, EXP_DATE, SECOND_HAND) VALUES (?, ?, ?, ?, ?, ?)",
                adaptor.createInsert(getContainmentTable("TOYS"), getContainmentTable("TOYS").getInsertFields(null), false));
    }

    @Test
    public void testCreateInsert4() {
        assertEquals("INSERT INTO TOYS_PETS(TOY_ID, PET_ID) VALUES (:TOY_ID, :PET_ID)",
                adaptor.createInsert(getJunctionTable("TOYS_PETS"), getJunctionTable("TOYS_PETS").getInsertFields(null), true));
    }

    @Test
    public void testCreateInsertNoNamedBinds4() {
        assertEquals("INSERT INTO TOYS_PETS(TOY_ID, PET_ID) VALUES (?, ?)",
                adaptor.createInsert(getJunctionTable("TOYS_PETS"), getJunctionTable("TOYS_PETS").getInsertFields(null), false));
    }

    @Test
    public void testCreateNonContainmentSelect1() {
        assertEquals("SELECT jt1.TOY_ID AS \"TOY_ID\", jt1.PET_ID AS \"PET_ID\"" +
                        " FROM TOYS_PETS jt1" +
                        " WHERE jt1.TOY_ID IN (" +
                        "SELECT t2.TOY_ID" +
                        " FROM TOYS t2" +
                        " INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = :CHILD_ID" +
                        " AND t1.CHILD_NAME = :CHILD_NAME" +
                        " OR t1.CHILD_AGE > :CHILD_AGE)" +
                        " AND jt1.PET_ID IN (" +
                        "SELECT t3.PET_ID" +
                        " FROM PETS t3 INNER JOIN CHILDREN t1 ON (t3.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = :CHILD_ID" +
                        " AND t1.CHILD_NAME = :CHILD_NAME" +
                        " OR t1.CHILD_AGE > :CHILD_AGE)",
                adaptor.createNonContainmentSelect(loadModel, getJunctionTable("TOYS_PETS"), true, true, true, true));
    }

    @Test
    public void testCreateNonContainmentSelectNoNamedBinds1() {
        assertEquals("SELECT jt1.TOY_ID AS \"TOY_ID\", jt1.PET_ID AS \"PET_ID\"" +
                        " FROM TOYS_PETS jt1" +
                        " WHERE jt1.TOY_ID IN (" +
                        "SELECT t2.TOY_ID" +
                        " FROM TOYS t2" +
                        " INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = ?" +
                        " AND t1.CHILD_NAME = ?" +
                        " OR t1.CHILD_AGE > ?)" +
                        " AND jt1.PET_ID IN (" +
                        "SELECT t3.PET_ID" +
                        " FROM PETS t3 INNER JOIN CHILDREN t1 ON (t3.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = ?" +
                        " AND t1.CHILD_NAME = ?" +
                        " OR t1.CHILD_AGE > ?)",
                adaptor.createNonContainmentSelect(loadModel, getJunctionTable("TOYS_PETS"), false, true, true, true));
    }

    @Test
    public void testCreateNonContainmentSelect2() {
        assertEquals("SELECT jt2.TOY_ID_1 AS \"TOY_ID_1\", jt2.TOY_ID_2 AS \"TOY_ID_2\"" +
                        " FROM TOYS_TOYS jt2" +
                        " WHERE jt2.TOY_ID_1 IN (" +
                        "SELECT t2.TOY_ID" +
                        " FROM TOYS t2" +
                        " INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = :CHILD_ID" +
                        " AND t1.CHILD_NAME = :CHILD_NAME" +
                        " OR t1.CHILD_AGE > :CHILD_AGE)" +
                        " AND jt2.TOY_ID_2 IN (" +
                        "SELECT t2.TOY_ID" +
                        " FROM TOYS t2" +
                        " INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = :CHILD_ID" +
                        " AND t1.CHILD_NAME = :CHILD_NAME" +
                        " OR t1.CHILD_AGE > :CHILD_AGE)",
                adaptor.createNonContainmentSelect(specialLoad2, (JunctionTable) specialLoad2.getTable("TOYS_TOYS").get(), true, true, true, true));
    }

    @Test
    public void testCreateNonContainmentSelectNoNamedBinds2() {
        assertEquals("SELECT jt2.TOY_ID_1 AS \"TOY_ID_1\", jt2.TOY_ID_2 AS \"TOY_ID_2\"" +
                        " FROM TOYS_TOYS jt2" +
                        " WHERE jt2.TOY_ID_1 IN (" +
                        "SELECT t2.TOY_ID" +
                        " FROM TOYS t2" +
                        " INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = ?" +
                        " AND t1.CHILD_NAME = ?" +
                        " OR t1.CHILD_AGE > ?)" +
                        " AND jt2.TOY_ID_2 IN (" +
                        "SELECT t2.TOY_ID" +
                        " FROM TOYS t2" +
                        " INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = ?" +
                        " AND t1.CHILD_NAME = ?" +
                        " OR t1.CHILD_AGE > ?)",
                adaptor.createNonContainmentSelect(specialLoad2, (JunctionTable) specialLoad2.getTable("TOYS_TOYS").get(), false, true, true, true));
    }

    @Test
    public void testCreateSelect1() {
        ContainmentTable table1 = getContainmentTable("CHILDREN");
        assertEquals("SELECT t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE t1.CHILD_ID = :CHILD_ID",
                adaptor.createSelect(table1, table1.getFields(), true));
    }

    @Test
    public void testCreateSelectNoNamedBinds1() {
        ContainmentTable table1 = getContainmentTable("CHILDREN");
        assertEquals("SELECT t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE t1.CHILD_ID = ?",
                adaptor.createSelect(table1, table1.getFields(), false));
    }

    @Test
    public void testCreateSelect2() {
        ContainmentTable table2 = getContainmentTable("TOYS");
        assertEquals("SELECT t2.TOY_NAME AS \"toy name\", t2.TOY_COST AS \"toy cost\", t2.SECOND_HAND AS \"second hand\" FROM TOYS t2 WHERE t2.TOY_ID = :TOY_ID",
                adaptor.createSelect(table2, table2.getFields().stream()
                        .filter(f -> !f.getSource().contains("EXP_DATE"))
                        .collect(Collectors.toList()), true));
    }

    @Test
    public void testCreateSelectNoNamedBinds2() {
        ContainmentTable table2 = getContainmentTable("TOYS");
        assertEquals("SELECT t2.TOY_NAME AS \"toy name\", t2.TOY_COST AS \"toy cost\", t2.SECOND_HAND AS \"second hand\" FROM TOYS t2 WHERE t2.TOY_ID = ?",
                adaptor.createSelect(table2, table2.getFields().stream()
                        .filter(f -> !f.getSource().contains("EXP_DATE"))
                        .collect(Collectors.toList()), false));
    }

    @Test
    public void testCreateSequenceSelect1() {
        assertEquals("SELECT CHILD_SEQ.NEXTVAL FROM DUAL",
                adaptor.createSequenceSelect(getContainmentTable("CHILDREN").getPrimaryKey().getSequence()));
    }

    @Test
    public void testCreateSequenceSelect2() {
        assertEquals("SELECT TOY_SEQ.NEXTVAL FROM DUAL",
                adaptor.createSequenceSelect(getContainmentTable("TOYS").getPrimaryKey().getSequence()));
    }

    @Test
    public void testCreateUpdate1() {
        assertEquals("UPDATE CHILDREN SET CHILD_NAME = :CHILD_NAME, CHILD_AGE = :CHILD_AGE WHERE CHILD_ID = :CHILD_ID",
                adaptor.createUpdate(getContainmentTable("CHILDREN"), getUpdateFields(getContainmentTable("CHILDREN")), true));
    }

    private List<DatabaseField> getUpdateFields(DatabaseTable table) {
        return table.getInsertFields(null).stream()
                .filter(f -> !(f instanceof PrimaryKeyField || f instanceof ForeignKeyField))
                .collect(Collectors.toList());
    }

    @Test
    public void testCreateUpdateNoNamedBinds1() {
        assertEquals("UPDATE CHILDREN SET CHILD_NAME = ?, CHILD_AGE = ? WHERE CHILD_ID = ?",
                adaptor.createUpdate(getContainmentTable("CHILDREN"), getUpdateFields(getContainmentTable("CHILDREN")), false));
    }

    @Test
    public void testCreateUpdate2() {
        assertEquals("UPDATE TOYS SET TOY_NAME = :TOY_NAME, TOY_COST = :TOY_COST, EXP_DATE = :EXP_DATE, SECOND_HAND = :SECOND_HAND WHERE TOY_ID = :TOY_ID",
                adaptor.createUpdate(getContainmentTable("TOYS"), getUpdateFields(getContainmentTable("TOYS")), true));
    }

    @Test
    public void testCreateUpdateNoNamedBinds2() {
        assertEquals("UPDATE TOYS SET TOY_NAME = ?, TOY_COST = ?, EXP_DATE = ?, SECOND_HAND = ? WHERE TOY_ID = ?",
                adaptor.createUpdate(getContainmentTable("TOYS"), getUpdateFields(getContainmentTable("TOYS")), false));
    }

    @Test
    public void testBindGroups1() {
        assertEquals("SELECT t1.CHILD_ID AS \"child id\", t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE 1 = 1 AND (t1.CHILD_NAME = :CHILD_NAME OR t1.CHILD_AGE > :CHILD_AGE)",
                adaptor.createContainmentSelect(loadModel, getContainmentTable("CHILDREN"), Collections.singletonList(
                        BindConditionGroup.builder()
                                .join(ConditionalJoin.and)
                                .child(BindConditionGroup.builder()
                                        .join(ConditionalJoin.and)
                                        .condition(BindCondition.builder()
                                                .alias("t1")
                                                .source("CHILD_NAME")
                                                .type(DataType.STRING_TYPE)
                                                .operator(Operator.equals)
                                                .join(ConditionalJoin.and)
                                                .build())
                                        .condition(BindCondition.builder()
                                                .alias("t1")
                                                .source("CHILD_AGE")
                                                .type(DataType.DECIMAL_TYPE)
                                                .operator(Operator.greaterThan)
                                                .join(ConditionalJoin.or)
                                                .build())
                                        .build())
                                .build()), true, false));

    }

    @Test
    public void testBindGroups2() {
        assertEquals("SELECT t1.CHILD_ID AS \"child id\", t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE 1 = 1 AND (t1.CHILD_NAME = :CHILD_NAME OR t1.CHILD_AGE > :CHILD_AGE) OR (t1.CHILD_NAME IS NOT NULL AND t1.CHILD_ID <= :CHILD_ID)",
                adaptor.createContainmentSelect(loadModel, getContainmentTable("CHILDREN"), Collections.singletonList(
                        BindConditionGroup.builder()
                                .join(ConditionalJoin.and)
                                .child(BindConditionGroup.builder()
                                        .join(ConditionalJoin.and)
                                        .condition(BindCondition.builder()
                                                .alias("t1")
                                                .source("CHILD_NAME")
                                                .type(DataType.STRING_TYPE)
                                                .operator(Operator.equals)
                                                .join(ConditionalJoin.and)
                                                .build())
                                        .condition(BindCondition.builder()
                                                .alias("t1")
                                                .source("CHILD_AGE")
                                                .type(DataType.DECIMAL_TYPE)
                                                .operator(Operator.greaterThan)
                                                .join(ConditionalJoin.or)
                                                .build())
                                        .build())
                                .child(BindConditionGroup.builder()
                                        .join(ConditionalJoin.or)
                                        .condition(BindCondition.builder()
                                                .alias("t1")
                                                .source("CHILD_NAME")
                                                .type(DataType.STRING_TYPE)
                                                .operator(Operator.isNotNull)
                                                .join(ConditionalJoin.and)
                                                .build())
                                        .condition(BindCondition.builder()
                                                .alias("t1")
                                                .source("CHILD_ID")
                                                .type(DataType.DECIMAL_TYPE)
                                                .operator(Operator.lessThanOrEqual)
                                                .join(ConditionalJoin.and)
                                                .build())
                                        .build())
                                .build()), true, false));

    }
}