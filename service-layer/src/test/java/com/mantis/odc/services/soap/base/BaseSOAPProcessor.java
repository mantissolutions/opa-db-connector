/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.soap.base;

import com.github.davidmoten.rx.jdbc.Database;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.google.common.io.Closeables;
import com.mantis.odc.models.*;
import com.mantis.odc.services.Service;
import com.mantis.odc.services.ServiceDatasource;
import com.mantis.odc.services.soap.SOAPProcessor;
import com.mantis.odc.services.storage.DebugStorage;
import com.mantis.odc.xml.MappingXMLTransport;
import lombok.SneakyThrows;
import lombok.val;
import org.jooq.lambda.Unchecked;
import org.xmlunit.builder.Input;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.mantis.odc.models.Driver.H2;
import static com.mantis.odc.models.ServiceAction.CheckAlive;
import static com.mantis.odc.models.ServiceAction.GetMetadata;
import static java.util.Collections.EMPTY_LIST;
import static javax.xml.soap.SOAPConstants.SOAP_1_1_PROTOCOL;
import static org.junit.Assert.*;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

/**
 * Base SOAP processor.
 */
public abstract class BaseSOAPProcessor {

    /**
     * Runs the configured test. This will populate a test database and run the SOAP processor against the
     * given test SOAP request, comparing it against the expected SOAP response.
     *
     * @param action               service action
     * @param version              OPA Version
     * @param requestPath          request path to load test request
     * @param ddlScriptPath        ddl script path to populate the test database
     * @param expectedResponsePath expected response path to load the expected response
     */
    @SneakyThrows
    protected Service test(ServiceAction action,
                           OPAVersion version,
                           String requestPath,
                           String ddlScriptPath,
                           String expectedResponsePath) {
        // get the test request
        val req = createSOAPMessage(requestPath);

        // create a service
        val service = createService(Optional.ofNullable(ddlScriptPath));

        val processor = new SOAPProcessor(new DebugStorage());

        val bos = new ByteArrayOutputStream();
        val builder = Operation.builder();
        val soapMessage = processor.process(action, builder, service, version, req);
        val op = builder.build();

        if ((action == GetMetadata && !service.getMapping().getEnumerations().isEmpty()) || (action != CheckAlive && action != GetMetadata)) {
            assertTrue(op.getStartDB().longValue() < op.getEndDB().longValue());
        }

        soapMessage.writeTo(bos);

        assertThat(bos.toString(), isSimilarTo(Input.from(getClass().getResourceAsStream(expectedResponsePath)))
                .ignoreComments()
                .ignoreWhitespace()
                .throwComparisonFailure());

        return service;
    }

    /**
     * Runs the configured test. This will populate a test database and run the SOAP processor against the
     * given test SOAP request, comparing it against the expected SOAP response. I then checks the database
     * and runs the scalar queries with expected values to confirm that the right data has been saved.
     *
     * @param action               service action
     * @param version              OPA Version
     * @param requestPath          request path to load test request
     * @param ddlScriptPath        ddl script path to populate the test database
     * @param expectedResponsePath expected response path to load the expected response
     */
    @SneakyThrows
    protected void testSave(ServiceAction action,
                            OPAVersion version,
                            String requestPath,
                            String ddlScriptPath,
                            String expectedResponsePath,
                            Map<String, Integer> databaseChecks) {
        val service = test(action, version, requestPath, ddlScriptPath, expectedResponsePath);
        val datasource = (TestServiceDatasource) service.getDatasource();
        val db = datasource.db;
        databaseChecks.forEach((sql, expected) -> {
            Integer actual = db.select(sql)
                    .get(rs -> rs.getInt(1))
                    .toBlocking()
                    .first();
            assertEquals(sql, expected, actual);
        });
    }

    /**
     * Creates a new SOAPMessage.
     *
     * @param requestPath request path
     * @return SOAP Message
     */
    @SneakyThrows
    private SOAPMessage createSOAPMessage(String requestPath) {
        val messageFactory = MessageFactory.newInstance(SOAP_1_1_PROTOCOL);
        val messageSource = new StreamSource(getClass().getResourceAsStream(requestPath));
        val message = messageFactory.createMessage();
        val soapPart = message.getSOAPPart();
        soapPart.setContent(messageSource);
        message.saveChanges();
        return message;
    }

    /**
     * Creates a new service.
     *
     * @param ddlScriptPath ddl script path
     * @return service
     */
    private Service createService(Optional<String> ddlScriptPath) {
        return Service.builder()
                .name("test_mapping")
                .mapping(getMapping())
                .datasource(new TestServiceDatasource(ddlScriptPath
                        .map(path -> getClass().getResourceAsStream(path))
                        .map(Unchecked.function(inputStream -> Arrays.asList(loadStringFromInputStream(inputStream).split(";"))))
                        .orElse(EMPTY_LIST)))
                .build();
    }

    /**
     * @return mapping to test
     */
    @SneakyThrows
    private Mapping getMapping() {
        val transport = new MappingXMLTransport();
        return transport.load(getClass().getResourceAsStream(getMappingPath()));
    }

    /**
     * @return relative path to the mapping xml file.
     */
    protected String getMappingPath() {
        return "Mapping.xml";
    }

    /**
     * @param stream input stream
     * @return string
     * @throws IOException on error
     */
    private String loadStringFromInputStream(InputStream stream) throws IOException {
        val content = CharStreams.toString(new InputStreamReader(stream, Charsets.UTF_8));
        Closeables.closeQuietly(stream);
        return content;
    }

    /**
     * Mock data source.
     */
    public static class TestServiceDatasource extends ServiceDatasource {

        private final List<String> statements;
        private Database db;

        /**
         * @param statements statements to run
         * @param extraCs    extra values to add to the connection string
         */
        public TestServiceDatasource(List<String> statements, String extraCs) {
            super(new DatasourceMapping() {{
                setName("test");
                setDriver(H2);
                setJdbc("jdbc:h2:mem:" + ((extraCs != null) ? extraCs : ""));
            }}, null);
            this.statements = statements;
        }

        /**
         * @param statements statements to run
         */
        public TestServiceDatasource(List<String> statements) {
            this(statements, null);
        }

        @SneakyThrows
        @Override
        public Database connect(String user, String password) {
            val mapping = this.getMapping();
            val conn = mapping.getConnection(user, password);
            db = Database.from(conn);
            // run in database.
            statements.forEach(statement -> db.update(statement).execute());
            return db;
        }
    }
}
