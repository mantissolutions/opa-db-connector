/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions;

import com.github.davidmoten.rx.jdbc.Database;
import com.mantis.odc.models.Mapping;
import com.mantis.odc.services.actions.base.ActionProcessor;
import com.mantis.odc.services.actions.base.BaseActionProcessorTest;
import com.mantis.odc.services.models.DatabaseEnumeration;
import com.mantis.odc.services.models.NumberEnumerationValue;
import com.mantis.odc.services.models.StringEnumerationValue;
import com.mantis.odc.services.sql.adaptors.ANSISQLAdaptor;
import com.mantis.odc.services.sql.converters.ANSISQLTypeConverter;
import com.mantis.odc.xml.MappingXMLTransport;
import lombok.val;
import org.jooq.lambda.Unchecked;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;

import static com.mantis.odc.models.Driver.H2;
import static com.mantis.odc.models.EnumerationType.NumberEnumeration;
import static com.mantis.odc.models.EnumerationType.StringEnumeration;

/**
 * Tests the enumeration processor.
 */
public class EnumerationProcessorTest extends BaseActionProcessorTest {

    private Mapping mapping;

    @Override
    protected ActionProcessor getActionProcessor(Connection conn) {
        return new EnumerationProcessor(Database.from(conn), new ANSISQLAdaptor(H2), new ANSISQLTypeConverter());
    }

    @Before
    public void setUp() throws Exception {
        val transport = new MappingXMLTransport();
        mapping = transport.load(getClass().getResourceAsStream("TestEnumerationMapping.xml"));
    }

    @Test
    public void enumerations1() {
        val enumerations = new ArrayList<DatabaseEnumeration>();
        enumerations.add(DatabaseEnumeration.builder()
                .name("test 1")
                .type(StringEnumeration)
                .uncertainValue(StringEnumerationValue.builder()
                        .description("test uncertain")
                        .childValue(new BigDecimal(1))
                        .childValue(new BigDecimal(2))
                        .childValue(new BigDecimal(3))
                        .build())
                .value(StringEnumerationValue.builder()
                        .value("test_1")
                        .description("test 1")
                        .childValue(new BigDecimal(2))
                        .build())
                .value(StringEnumerationValue.builder()
                        .value("test_2")
                        .description("test 2")
                        .childValue(new BigDecimal(1))
                        .childValue(new BigDecimal(3))
                        .build())
                .value(StringEnumerationValue.builder()
                        .value("test_3")
                        .description("test 3")
                        .childValue(new BigDecimal(2))
                        .childValue(new BigDecimal(3))
                        .build())
                .build());
        enumerations.add(DatabaseEnumeration.builder()
                .name("test 2")
                .type(NumberEnumeration)
                .uncertainValue(NumberEnumerationValue.builder()
                        .description("test 2 uncertain")
                        .build())
                .value(NumberEnumerationValue.builder()
                        .value(new BigDecimal(1))
                        .description("test 1")
                        .build())
                .value(NumberEnumerationValue.builder()
                        .value(new BigDecimal(2))
                        .description("test 2")
                        .build())
                .value(NumberEnumerationValue.builder()
                        .value(new BigDecimal(3))
                        .description("test 3")
                        .build())
                .build());
        testProcessor(Unchecked.function(p -> p.enumerations(mapping.getEnumerations())), enumerations,
                "CREATE TABLE TABLE_1(value VARCHAR(250), description VARCHAR(250))",
                "CREATE TABLE TABLE_2(value NUMBER, description VARCHAR(250), parent_value VARCHAR(250))",
                "INSERT INTO TABLE_1(description) VALUES('test uncertain')",
                "INSERT INTO TABLE_1(value, description) VALUES('test_1', 'test 1')",
                "INSERT INTO TABLE_1(value, description) VALUES('test_2', 'test 2')",
                "INSERT INTO TABLE_1(value, description) VALUES('test_3', 'test 3')",
                "INSERT INTO TABLE_2(description) VALUES('test 2 uncertain')",
                "INSERT INTO TABLE_2(value, description) VALUES(1, 'test 1')",
                "INSERT INTO TABLE_2(value, description, parent_value) VALUES(1, 'test 1', 'test_2')",
                "INSERT INTO TABLE_2(value, description) VALUES(2, 'test 2')",
                "INSERT INTO TABLE_2(value, description, parent_value) VALUES(2, 'test 2', 'test_1')",
                "INSERT INTO TABLE_2(value, description, parent_value) VALUES(2, 'test 2', 'test_3')",
                "INSERT INTO TABLE_2(value, description) VALUES(3, 'test 3')",
                "INSERT INTO TABLE_2(value, description, parent_value) VALUES(3, 'test 3', 'test_2')",
                "INSERT INTO TABLE_2(value, description, parent_value) VALUES(3, 'test 3', 'test_3')");
    }
}