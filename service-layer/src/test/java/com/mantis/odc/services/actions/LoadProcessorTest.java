/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions;

import com.github.davidmoten.rx.jdbc.Database;
import com.mantis.odc.services.actions.base.BaseActionProcessorTest;
import com.mantis.odc.services.models.*;
import com.mantis.odc.services.sql.adaptors.ANSISQLAdaptor;
import com.mantis.odc.services.sql.converters.ANSISQLTypeConverter;
import lombok.val;
import org.jooq.lambda.Unchecked;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.TimeZone;

import static com.mantis.odc.models.ConditionalJoin.and;
import static com.mantis.odc.models.DataType.DECIMAL_TYPE;
import static com.mantis.odc.models.DataType.STRING_TYPE;
import static com.mantis.odc.models.Driver.H2;
import static com.mantis.odc.models.Operator.greaterThan;

/**
 * Test the load processor.
 */
public class LoadProcessorTest extends BaseActionProcessorTest {

    @Override
    protected LoadProcessor getActionProcessor(Connection conn) {
        return new LoadProcessor(Database.from(conn), new ANSISQLAdaptor(H2), new ANSISQLTypeConverter());
    }

    private final ContainmentTable testTable = ContainmentTable.builder()
            .source("TEST_1")
            .alias("t1")
            .primaryKey(new PrimaryKeyField("ID", DECIMAL_TYPE, false, "TEST_SEQ.NEXTVAL"))
            .build();

    private final ContainmentTable testTable2 = ContainmentTable.builder()
            .source("TEST_2")
            .alias("t2")
            .primaryKey(new PrimaryKeyField("ID", DECIMAL_TYPE, false, "TEST_SEQ"))
            .foreignKey(new ForeignKeyField("TEST_1_ID", DECIMAL_TYPE, "TEST_1", null))
            .field(new DatabaseField("NAME", STRING_TYPE))
            .field(new DatabaseField("NUMB", DECIMAL_TYPE))
            .build();

    private final ContainmentTable expectedTable1 = testTable.toBuilder()
            .row(DatabaseRow.dataRowBuilder()
                    .source("TEST_1")
                    .id(new RowId(new BigDecimal(1)))
                    .build())
            .build();

    private final ContainmentTable expectedTable2 = testTable2.toBuilder()
            .row(DatabaseRow.dataRowBuilder()
                    .source("TEST_2")
                    .id(new RowId(new BigDecimal(1)))
                    .fk(new RowId(new BigDecimal(1)))
                    .item(new RowValue("NAME", STRING_TYPE, "test_name"))
                    .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(12)))
                    .build())
            .row(DatabaseRow.dataRowBuilder()
                    .source("TEST_2")
                    .id(new RowId(new BigDecimal(2)))
                    .fk(new RowId(new BigDecimal(1)))
                    .item(new RowValue("NAME", STRING_TYPE, "test_name2"))
                    .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(13)))
                    .build())
            .row(DatabaseRow.dataRowBuilder()
                    .source("TEST_2")
                    .id(new RowId(new BigDecimal(3)))
                    .fk(new RowId(new BigDecimal(1)))
                    .item(new RowValue("NAME", STRING_TYPE, "test_name3"))
                    .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(14)))
                    .build())
            .row(DatabaseRow.dataRowBuilder()
                    .source("TEST_2")
                    .id(new RowId(new BigDecimal(4)))
                    .fk(new RowId(new BigDecimal(1)))
                    .item(new RowValue("NAME", STRING_TYPE, "test_name4"))
                    .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(15)))
                    .build())
            .build();

    @Test
    public void load1() {
        val builder = LoadModel.builder()
                .rootTable("TEST_1")
                .preScript(Optional.empty())
                .postScript(Optional.empty())
                .timeZone(TimeZone.getDefault())
                .globalId(new RowId(1));

        val expected = builder.build().toBuilder()
                .table(expectedTable1)
                .table(expectedTable2)
                .build();

        testProcessor(Unchecked.function(p -> p.load(builder.build().toBuilder()
                        .table(testTable.toBuilder().build())
                        .table(testTable2.toBuilder().build())
                        .build(), true, OptionalInt.empty())),
                expected,
                "CREATE TABLE TEST_1(ID NUMBER)",
                "CREATE TABLE TEST_2(ID NUMBER, NAME TEXT, NUMB NUMBER, TEST_1_ID NUMBER)",
                "CREATE SEQUENCE TEST_SEQ",
                "INSERT INTO TEST_1 VALUES(1)",
                "INSERT INTO TEST_2 VALUES(1, 'test_name', 12, 1)",
                "INSERT INTO TEST_2 VALUES(2, 'test_name2', 13, 1)",
                "INSERT INTO TEST_2 VALUES(3, 'test_name3', 14, 1)",
                "INSERT INTO TEST_2 VALUES(4, 'test_name4', 15, 1)");
    }

    private final JunctionTable junctionTable = JunctionTable.builder()
            .source("TEST_2_TEST_2")
            .alias("jt3")
            .fromKey(new ForeignKeyField("TEST_1_KEY", DECIMAL_TYPE, "TEST_2", null))
            .toKey(new ForeignKeyField("TEST_2_KEY", DECIMAL_TYPE, "TEST_2", null))
            .build();

    @Test
    public void load2() {
        val builder = LoadModel.builder()
                .rootTable("TEST_1")
                .preScript(Optional.empty())
                .postScript(Optional.empty())
                .timeZone(TimeZone.getDefault())
                .globalId(new RowId(1));

        val expectedJunctionTable1 = junctionTable.toBuilder()
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2_TEST_2")
                        .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, new BigDecimal(2)))
                        .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, new BigDecimal(1)))
                        .build())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2_TEST_2")
                        .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, new BigDecimal(1)))
                        .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, new BigDecimal(4)))
                        .build())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2_TEST_2")
                        .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, new BigDecimal(2)))
                        .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, new BigDecimal(3)))
                        .build())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2_TEST_2")
                        .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, new BigDecimal(3)))
                        .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, new BigDecimal(4)))
                        .build())
                .build();

        val expected = builder.build().toBuilder()
                .table(expectedTable1)
                .table(expectedTable2)
                .table(expectedJunctionTable1)
                .build();

        testProcessor(Unchecked.function(p -> p.load(builder.build().toBuilder()
                        .table(testTable.toBuilder().build())
                        .table(testTable2.toBuilder().build())
                        .table(junctionTable.toBuilder().build())
                        .build(), true, OptionalInt.empty())),
                expected,
                "CREATE TABLE TEST_1(ID NUMBER)",
                "CREATE TABLE TEST_2(ID NUMBER, NAME TEXT, NUMB NUMBER, TEST_1_ID NUMBER)",
                "CREATE TABLE TEST_2_TEST_2(TEST_1_KEY NUMBER, TEST_2_KEY NUMBER)",
                "CREATE SEQUENCE TEST_SEQ",
                "INSERT INTO TEST_1 VALUES(1)",
                "INSERT INTO TEST_2 VALUES(1, 'test_name', 12, 1)",
                "INSERT INTO TEST_2 VALUES(2, 'test_name2', 13, 1)",
                "INSERT INTO TEST_2 VALUES(3, 'test_name3', 14, 1)",
                "INSERT INTO TEST_2 VALUES(4, 'test_name4', 15, 1)",
                "INSERT INTO TEST_2_TEST_2 VALUES(2,1)",
                "INSERT INTO TEST_2_TEST_2 VALUES(1,4)",
                "INSERT INTO TEST_2_TEST_2 VALUES(2,3)",
                "INSERT INTO TEST_2_TEST_2 VALUES(3,4)");
    }

    @Test
    public void load3() {
        val builder = LoadModel.builder()
                .rootTable("TEST_1")
                .preScript(Optional.empty())
                .postScript(Optional.empty())
                .timeZone(TimeZone.getDefault())
                .globalId(new RowId(1));

        BindCondition bind = BindCondition.builder()
                .source("NUMB")
                .alias("t2")
                .type(DECIMAL_TYPE)
                .join(and)
                .operator(greaterThan)
                .value(13)
                .build();

        val expected = builder.build().toBuilder()
                .table(expectedTable1)
                .table(testTable2.toBuilder()
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TEST_2")
                                .id(new RowId(new BigDecimal(3)))
                                .fk(new RowId(new BigDecimal(1)))
                                .item(new RowValue("NAME", STRING_TYPE, "test_name3"))
                                .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(14)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TEST_2")
                                .id(new RowId(new BigDecimal(4)))
                                .fk(new RowId(new BigDecimal(1)))
                                .item(new RowValue("NAME", STRING_TYPE, "test_name4"))
                                .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(15)))
                                .build())
                        .build())
                .table(junctionTable.toBuilder()
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TEST_2_TEST_2")
                                .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, new BigDecimal(4)))
                                .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, new BigDecimal(4)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TEST_2_TEST_2")
                                .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, new BigDecimal(3)))
                                .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, new BigDecimal(4)))
                                .build())
                        .build())
                .condition(bind.toGroup())
                .build();

        testProcessor(Unchecked.function(p -> p.load(builder.build().toBuilder()
                        .table(testTable.toBuilder().build())
                        .table(testTable2.toBuilder().build())
                        .table(junctionTable.toBuilder().build())
                        .condition(bind.toGroup())
                        .build(), true, OptionalInt.empty())),
                expected,
                "CREATE TABLE TEST_1(ID NUMBER)",
                "CREATE TABLE TEST_2(ID NUMBER, NAME TEXT, NUMB NUMBER, TEST_1_ID NUMBER)",
                "CREATE TABLE TEST_2_TEST_2(TEST_1_KEY NUMBER, TEST_2_KEY NUMBER)",
                "CREATE SEQUENCE TEST_SEQ",
                "INSERT INTO TEST_1 VALUES(1)",
                "INSERT INTO TEST_2 VALUES(1, 'test_name', 12, 1)",
                "INSERT INTO TEST_2 VALUES(2, 'test_name2', 13, 1)",
                "INSERT INTO TEST_2 VALUES(3, 'test_name3', 14, 1)",
                "INSERT INTO TEST_2 VALUES(4, 'test_name4', 15, 1)",
                "INSERT INTO TEST_2_TEST_2 VALUES(3,1)",
                "INSERT INTO TEST_2_TEST_2 VALUES(4,4)",
                "INSERT INTO TEST_2_TEST_2 VALUES(2,3)",
                "INSERT INTO TEST_2_TEST_2 VALUES(3,4)");
    }
}