/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.validators;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.mantis.odc.models.Mapping;
import com.mantis.odc.xml.MappingXMLTransport;
import lombok.val;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Tests the the meta data validator.
 */
public class MetaDataValidatorTest {

    private Mapping mapping;

    @Before
    public void setup() throws JAXBException {
        MappingXMLTransport transport = new MappingXMLTransport();
        mapping = transport.load(getClass().getResourceAsStream("Mapping.xml"));
    }

    @Test
    public void testValidate() throws Exception {
        // correct result
        val metaData = loadMetaData(mapping, "correct.sql");
        val validator = new MetaDataValidator(mapping.getDatasource().getDriver(), metaData, new MappingValidator());
        val result = validator.validate(mapping);
        Assert.assertTrue(result.isSuccess());
        metaData.getConnection().close();

        // incorrect result
        val metaData2 = loadMetaData(mapping, "incorrect.sql");
        val validator2 = new MetaDataValidator(mapping.getDatasource().getDriver(), metaData2, new MappingValidator());
        val result2 = validator2.validate(mapping);
        Assert.assertFalse(result2.isSuccess());
        metaData.getConnection().close();
    }

    private DatabaseMetaData loadMetaData(Mapping mapping, String scriptPath) throws SQLException, ClassNotFoundException, IOException {
        Connection conn = mapping.getDatasource().getConnection("test", "test");
        String script = CharStreams.toString(new InputStreamReader(getClass().getResourceAsStream(scriptPath), Charsets.UTF_8));
        for (String sql : script.split(";")) {
            PreparedStatement st = conn.prepareStatement(sql);
            st.execute();
        }
        return conn.getMetaData();
    }
}