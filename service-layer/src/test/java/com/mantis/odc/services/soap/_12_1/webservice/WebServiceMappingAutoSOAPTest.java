/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.soap._12_1.webservice;

import com.mantis.odc.services.soap.base.BaseSOAPProcessor;
import org.junit.Test;

import java.util.HashMap;

import static com.mantis.odc.models.OPAVersion.OPA_12_1;
import static com.mantis.odc.models.ServiceAction.Save;

/**
 * Test the webservice mapping against the 12.1 version of OPA.
 */
public class WebServiceMappingAutoSOAPTest extends BaseSOAPProcessor {

    @Override
    protected String getMappingPath() {
        return "MappingWithAutoKeys.xml";
    }

    @Test
    public void save1() {
        testSave(Save, OPA_12_1, "Save_req1.xml", "LoadAutoCreate.sql", "Save_resp1.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM catalog", 1);
            put("SELECT COUNT(*) FROM camera", 14);
            put("SELECT COUNT(*) FROM customer_request", 1);
            put("SELECT COUNT(*) FROM customer_request_att", 1);
            put("SELECT COUNT(*) FROM customer_request_camera", 3);
            put("SELECT COUNT(*) FROM ph_number", 2);
            put("SELECT COUNT(*) FROM ph_number_type", 2);
            put("SELECT COUNT(*) FROM phone_type", 3);
        }});
    }

    @Test
    public void save2() {
        testSave(Save, OPA_12_1, "Save_req2.xml", "UpdateAutoCreate.sql", "Save_resp2.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM catalog", 1);
            put("SELECT COUNT(*) FROM camera", 14);
            put("SELECT COUNT(*) FROM customer_request", 1);
            put("SELECT COUNT(*) FROM customer_request_att", 1);
            put("SELECT COUNT(*) FROM customer_request_camera", 3);
            put("SELECT COUNT(*) FROM ph_number", 2);
            put("SELECT COUNT(*) FROM ph_number_type", 2);
            put("SELECT COUNT(*) FROM phone_type", 3);
        }});
    }
}
