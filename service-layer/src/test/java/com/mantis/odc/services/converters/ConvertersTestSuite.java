/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters;

import com.mantis.odc.services.converters._12_1.OPA121ConverterTest;
import com.mantis.odc.services.converters._12_2.OPA122ConverterTest;
import com.mantis.odc.services.converters._12_2_2.OPA1222ConverterTest;
import com.mantis.odc.services.converters._12_2_5.OPA1225ConverterTest;
import com.mantis.odc.services.converters._12_2_9.OPA1229Converter;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Runs all the converter tests.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        OPA121ConverterTest.class,
        OPA122ConverterTest.class,
        OPA1222ConverterTest.class,
        OPA1225ConverterTest.class,
        OPA1229Converter.class
})
public class ConvertersTestSuite {
}
