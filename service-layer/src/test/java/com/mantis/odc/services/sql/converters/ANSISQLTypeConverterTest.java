/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.sql.converters;

import com.mantis.odc.models.DataType;
import com.mantis.odc.services.sql.converters.base.SQLTypeConverter;
import lombok.val;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.sql.rowset.serial.SerialBlob;
import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.*;
import java.util.Calendar;
import java.util.TimeZone;

import static org.junit.Assert.assertNotNull;

/**
 * Test the data type conversions.
 */
public class ANSISQLTypeConverterTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    private SQLTypeConverter converter;

    @Before
    public void setUp() {
        converter = new ANSISQLTypeConverter();
    }

    @Test
    public void convertToByteArray() throws Exception {
        String data = "OGRmMzkzNjItOGMyNy00NTM4LWI3OTgtYjdiNzk1YjRkNDQ3";

        byte[] value = converter.convertToByteArray(data);
        assertNotNull(value);

        Blob blob = new SerialBlob(converter.convertToByteArray(data));
        byte[] value2 = converter.convertToByteArray(blob);
        assertNotNull(value2);

        exception.expect(NullPointerException.class);
        converter.convertToByteArray(null);

        exception.expect(SQLException.class);
        converter.convertToByteArray(123);
        converter.convertToByteArray(new Date(System.currentTimeMillis()));
    }

    @Test
    public void convertFromSQLTimestamp() throws Exception {
        TimeZone timeZone = TimeZone.getDefault();

        XMLGregorianCalendar value = converter.convertFromSQLTimestamp(new Date(System.currentTimeMillis()), timeZone);
        assertNotNull(value);

        XMLGregorianCalendar value2 = converter.convertFromSQLTimestamp(value.toXMLFormat(), timeZone);
        assertNotNull(value2);

        XMLGregorianCalendar value3 = converter.convertFromSQLTimestamp(System.currentTimeMillis(), timeZone);
        assertNotNull(value3);

        exception.expect(NullPointerException.class);
        converter.convertFromSQLTimestamp(null, timeZone);

        exception.expect(SQLException.class);
        converter.convertFromSQLTimestamp(converter.convertToByteArray("OGRmMzkzNjItOGMyNy00NTM4LWI3OTgtYjdiNzk1YjRkNDQ3"), timeZone);
    }

    @Test
    public void convertFromSQLTime() throws Exception {
        TimeZone timeZone = TimeZone.getDefault();

        XMLGregorianCalendar value = converter.convertFromSQLTime(new Date(System.currentTimeMillis()), timeZone);
        assertNotNull(value);

        XMLGregorianCalendar value2 = converter.convertFromSQLTime(value.toXMLFormat(), timeZone);
        assertNotNull(value2);

        XMLGregorianCalendar value3 = converter.convertFromSQLTime(System.currentTimeMillis(), timeZone);
        assertNotNull(value3);

        exception.expect(NullPointerException.class);
        converter.convertFromSQLTime(null, timeZone);

        exception.expect(SQLException.class);
        converter.convertFromSQLTime(converter.convertToByteArray("OGRmMzkzNjItOGMyNy00NTM4LWI3OTgtYjdiNzk1YjRkNDQ3"), timeZone);
    }

    @Test
    public void convertFromSQLDate() throws Exception {
        TimeZone timeZone = TimeZone.getDefault();

        XMLGregorianCalendar value = converter.convertFromSQLDate(new Date(System.currentTimeMillis()), timeZone);
        assertNotNull(value);

        XMLGregorianCalendar value2 = converter.convertFromSQLDate(value.toXMLFormat(), timeZone);
        assertNotNull(value2);

        XMLGregorianCalendar value3 = converter.convertFromSQLDate(System.currentTimeMillis(), timeZone);
        assertNotNull(value3);

        exception.expect(NullPointerException.class);
        converter.convertFromSQLDate(null, timeZone);

        exception.expect(SQLException.class);
        converter.convertFromSQLDate(converter.convertToByteArray("OGRmMzkzNjItOGMyNy00NTM4LWI3OTgtYjdiNzk1YjRkNDQ3"), timeZone);
    }

    @Test
    public void convertToSQLTime() throws Exception {
        Time value = converter.convertToSQLTime(new Date(System.currentTimeMillis()));
        assertNotNull(value);

        Time value2 = converter.convertToSQLTime(value.toString());
        assertNotNull(value2);

        Time value3 = converter.convertToSQLTime(System.currentTimeMillis());
        assertNotNull(value3);

        exception.expect(NullPointerException.class);
        converter.convertToSQLTime(null);

        exception.expect(SQLException.class);
        converter.convertToSQLTime(converter.convertToByteArray("OGRmMzkzNjItOGMyNy00NTM4LWI3OTgtYjdiNzk1YjRkNDQ3"));
    }

    @Test
    public void convertToSQLDate() throws Exception {
        Date value = converter.convertToSQLDate(new Date(System.currentTimeMillis()));
        assertNotNull(value);

        Date value2 = converter.convertToSQLDate(value.toString());
        assertNotNull(value2);

        Date value3 = converter.convertToSQLDate(System.currentTimeMillis());
        assertNotNull(value3);

        exception.expect(NullPointerException.class);
        converter.convertToSQLDate(null);

        exception.expect(SQLException.class);
        converter.convertToSQLDate(converter.convertToByteArray("OGRmMzkzNjItOGMyNy00NTM4LWI3OTgtYjdiNzk1YjRkNDQ3"));
    }

    @Test
    public void convertToSQLDateTime() throws Exception {
        Timestamp value = converter.convertToSQLDateTime(new Date(System.currentTimeMillis()));
        assertNotNull(value);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        Timestamp value2 = converter.convertToSQLDateTime(DatatypeConverter.printDateTime(calendar));
        assertNotNull(value2);

        Timestamp value3 = converter.convertToSQLDateTime(System.currentTimeMillis());
        assertNotNull(value3);

        exception.expect(NullPointerException.class);
        converter.convertToSQLDateTime(null);

        exception.expect(SQLException.class);
        converter.convertToSQLDateTime(converter.convertToByteArray("OGRmMzkzNjItOGMyNy00NTM4LWI3OTgtYjdiNzk1YjRkNDQ3"));
    }

    @Test
    public void convertFromSQLType() throws Exception {
        val convert = converter.convertFromSQLType(null, DataType.STRING_TYPE, TimeZone.getDefault());
        Assert.assertNull(convert);
    }

    @Test
    public void convertToSQLType() throws Exception {
        val convert = converter.convertToSQLType(null, DataType.STRING_TYPE);
        Assert.assertNull(convert);
    }
}