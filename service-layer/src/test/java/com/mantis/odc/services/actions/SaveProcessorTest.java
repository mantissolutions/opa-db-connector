/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions;

import com.github.davidmoten.rx.jdbc.Database;
import com.mantis.odc.services.actions.base.ActionProcessor;
import com.mantis.odc.services.actions.base.BaseActionProcessorTest;
import com.mantis.odc.services.models.*;
import com.mantis.odc.services.sql.adaptors.ANSISQLAdaptor;
import com.mantis.odc.services.sql.converters.ANSISQLTypeConverter;
import lombok.val;
import org.jooq.lambda.Unchecked;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Optional;
import java.util.TimeZone;

import static com.mantis.odc.models.DataType.*;
import static com.mantis.odc.models.Driver.H2;
import static com.mantis.odc.services.models.DBActionType.*;

/**
 * Test the saveButton processor.
 */
public class SaveProcessorTest extends BaseActionProcessorTest {

    @Override
    protected ActionProcessor getActionProcessor(Connection conn) {
        return new SaveProcessor(Database.from(conn), new ANSISQLAdaptor(H2), new ANSISQLTypeConverter());
    }

    private final ContainmentTable testTable = ContainmentTable.builder()
            .source("TEST_1")
            .alias("t1")
            .primaryKey(new PrimaryKeyField("ID", DECIMAL_TYPE, false, "TEST_1_SEQ.NEXTVAL"))
            .build();

    private final ContainmentTable testTable2 = ContainmentTable.builder()
            .source("TEST_2")
            .alias("t2")
            .primaryKey(new PrimaryKeyField("ID", DECIMAL_TYPE, false, "TEST_2_SEQ.NEXTVAL"))
            .foreignKey(new ForeignKeyField("TEST_1_ID", DECIMAL_TYPE, "TEST_1", null))
            .field(new DatabaseField("NAME", STRING_TYPE))
            .field(new DatabaseField("NUMB", DECIMAL_TYPE))
            .build();

    @Test
    public void save1() {
        val builder = SaveModel.builder()
                .rootTable("TEST_1")
                .preScript(Optional.empty())
                .postScript(Optional.empty())
                .timeZone(TimeZone.getDefault());

        val expectedTable1 = testTable.toBuilder()
                .rows(new ArrayList<>())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_1")
                        .id(new RowId("#1", new BigDecimal(1)))
                        .build())
                .build();

        val expectedTable2 = testTable2.toBuilder()
                .rows(new ArrayList<>())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2")
                        .id(new RowId("#1", new BigDecimal(1)))
                        .fk(new RowId("#1", new BigDecimal(1)))
                        .item(new RowValue("NAME", STRING_TYPE, "ben"))
                        .build())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2")
                        .id(new RowId("#2", new BigDecimal(2)))
                        .fk(new RowId("#1", new BigDecimal(1)))
                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(13)))
                        .build())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2")
                        .id(new RowId("#3", new BigDecimal(3)))
                        .fk(new RowId("#1", new BigDecimal(1)))
                        .item(new RowValue("NAME", STRING_TYPE, "tim"))
                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(14)))
                        .build())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2")
                        .id(new RowId("#4", new BigDecimal(4)))
                        .build())
                .build();

        val expected = builder.build().toBuilder()
                .globalId(new RowId("#1", new BigDecimal(1)))
                .table(expectedTable1)
                .table(expectedTable2)
                .build();

        testProcessor(Unchecked.function(p -> p.save(builder.build().toBuilder()
                        .table(testTable.toBuilder()
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_1")
                                        .id(new RowId("#1", null))
                                        .action(insert)
                                        .build())
                                .build())
                        .table(testTable2.toBuilder()
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#1", null))
                                        .fk(new RowId("#1", null))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "ben"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(12)))
                                        .returnField(new DatabaseField("NAME", STRING_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#2", null))
                                        .fk(new RowId("#1", null))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "john"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(13)))
                                        .returnField(new DatabaseField("NUMB", DECIMAL_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#3", null))
                                        .fk(new RowId("#1", null))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "tim"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(14)))
                                        .returnField(new DatabaseField("NAME", STRING_TYPE))
                                        .returnField(new DatabaseField("NUMB", DECIMAL_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#4", null))
                                        .fk(new RowId("#1", null))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "sam"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(15)))
                                        .build())
                                .build())
                        .build())),
                expected,
                "CREATE TABLE TEST_1(ID NUMBER)",
                "CREATE TABLE TEST_2(ID NUMBER, NAME TEXT, NUMB NUMBER, TEST_1_ID NUMBER)",
                "CREATE SEQUENCE TEST_1_SEQ",
                "CREATE SEQUENCE TEST_2_SEQ");
    }

    private final ContainmentTable expectedTable1 = testTable.toBuilder()
            .rows(new ArrayList<>())
            .build();

    private final ContainmentTable expectedTable2 = testTable2.toBuilder()
            .rows(new ArrayList<>())
            .row(DatabaseRow.dataRowBuilder()
                    .source("TEST_2")
                    .id(new RowId(new BigDecimal(1)))
                    .fk(new RowId(new BigDecimal(1)))
                    .item(new RowValue("NAME", STRING_TYPE, "ben"))
                    .build())
            .row(DatabaseRow.dataRowBuilder()
                    .source("TEST_2")
                    .id(new RowId("#3", new BigDecimal(3)))
                    .fk(new RowId(new BigDecimal(1)))
                    .item(new RowValue("NAME", STRING_TYPE, "tim"))
                    .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(14)))
                    .build())
            .row(DatabaseRow.dataRowBuilder()
                    .source("TEST_2")
                    .id(new RowId("#4", new BigDecimal(4)))
                    .build())
            .build();

    @Test
    public void save2() {
        val builder = SaveModel.builder()
                .rootTable("TEST_1")
                .preScript(Optional.empty())
                .postScript(Optional.empty())
                .timeZone(TimeZone.getDefault());

        val expected = builder.build().toBuilder()
                .globalId(new RowId(new BigDecimal(1)))
                .table(expectedTable1)
                .table(expectedTable2)
                .build();

        testProcessor(Unchecked.function(p -> p.save(builder.build().toBuilder()
                        .globalId(new RowId(new BigDecimal(1)))
                        .table(testTable.toBuilder()
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_1")
                                        .id(new RowId(new BigDecimal(1)))
                                        .action(update)
                                        .build())
                                .build())
                        .table(testTable2.toBuilder()
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId(new BigDecimal(1)))
                                        .fk(new RowId(new BigDecimal(1)))
                                        .action(update)
                                        .item(new RowValue("NAME", STRING_TYPE, "ben"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(12)))
                                        .returnField(new DatabaseField("NAME", STRING_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId(new BigDecimal(2)))
                                        .fk(new RowId(new BigDecimal(1)))
                                        .action(delete)
                                        .item(new RowValue("NAME", STRING_TYPE, "john"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(13)))
                                        .returnField(new DatabaseField("NUMB", DECIMAL_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#3", null))
                                        .fk(new RowId(new BigDecimal(1)))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "tim"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(14)))
                                        .returnField(new DatabaseField("NAME", STRING_TYPE))
                                        .returnField(new DatabaseField("NUMB", DECIMAL_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#4", null))
                                        .fk(new RowId(new BigDecimal(1)))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "sam"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(15)))
                                        .build())
                                .build())
                        .build())),
                expected,
                "CREATE TABLE TEST_1(ID NUMBER)",
                "CREATE TABLE TEST_2(ID NUMBER, NAME TEXT, NUMB NUMBER, TEST_1_ID NUMBER)",
                "CREATE SEQUENCE TEST_1_SEQ",
                "CREATE SEQUENCE TEST_2_SEQ",
                "INSERT INTO TEST_1 VALUES(TEST_1_SEQ.NEXTVAL)",
                "INSERT INTO TEST_2 VALUES(TEST_2_SEQ.NEXTVAL, 'test_name', 12, 1)",
                "INSERT INTO TEST_2 VALUES(TEST_2_SEQ.NEXTVAL, 'test_name2', 13, 1)");

    }

    @Test
    public void save3() {
        val builder = SaveModel.builder()
                .rootTable("TEST_1")
                .preScript(Optional.empty())
                .postScript(Optional.empty())
                .timeZone(TimeZone.getDefault());

        val attachmentTable = AttachmentTable.builder()
                .source("TEST_ATTACHMENT")
                .sourceField("A_TABLE_NAME")
                .keyField("ENTITY_ID")
                .nameField("A_NAME")
                .fileNameField("A_FILE_NAME")
                .descriptionField("A_DESCRIPTION")
                .fileField("A_FILE")
                .attachmentRow(AttachmentRow.attachmentRowBuilder()
                        .id(new RowId(new BigDecimal(1)))
                        .table(new RowValue("A_TABLE_NAME", STRING_TYPE, "TEST_1"))
                        .name(new RowValue("A_NAME", STRING_TYPE, "attachment 1"))
                        .fileName(new RowValue("A_FILE_NAME", STRING_TYPE, "attachment_1_.pdf"))
                        .description(new RowValue("A_DESCRIPTION", STRING_TYPE, "some value..."))
                        .attachmentData(new RowValue("A_FILE", ATTACHMENT_TYPE, "SDFDSFDSFDSFSDFDSFDS"))
                        .build())
                .attachmentRow(AttachmentRow.attachmentRowBuilder()
                        .id(new RowId(new BigDecimal(1)))
                        .table(new RowValue("A_TABLE_NAME", STRING_TYPE, "TEST_1"))
                        .name(new RowValue("A_NAME", STRING_TYPE, "attachment 2"))
                        .fileName(new RowValue("A_FILE_NAME", STRING_TYPE, "attachment_2_.pdf"))
                        .description(new RowValue("A_DESCRIPTION", STRING_TYPE, "some value..."))
                        .attachmentData(new RowValue("A_FILE", ATTACHMENT_TYPE, "ASDASDASDASDQWREDASAS"))
                        .build())
                .attachmentRow(AttachmentRow.attachmentRowBuilder()
                        .id(new RowId(new BigDecimal(1)))
                        .table(new RowValue("A_TABLE_NAME", STRING_TYPE, "TEST_2"))
                        .name(new RowValue("A_NAME", STRING_TYPE, "attachment 3"))
                        .fileName(new RowValue("A_FILE_NAME", STRING_TYPE, "attachment_3_.pdf"))
                        .description(new RowValue("A_DESCRIPTION", STRING_TYPE, "some value..."))
                        .attachmentData(new RowValue("A_FILE", ATTACHMENT_TYPE, "123123123l1k321lk3n"))
                        .build())
                .attachmentRow(AttachmentRow.attachmentRowBuilder()
                        .id(new RowId(new BigDecimal(3)))
                        .table(new RowValue("A_TABLE_NAME", STRING_TYPE, "TEST_2"))
                        .name(new RowValue("A_NAME", STRING_TYPE, "attachment 4"))
                        .fileName(new RowValue("A_FILE_NAME", STRING_TYPE, "attachment_4_.pdf"))
                        .description(new RowValue("A_DESCRIPTION", STRING_TYPE, "some value..."))
                        .attachmentData(new RowValue("A_FILE", ATTACHMENT_TYPE, "asdasda564rq63w5f1a6f5"))
                        .build())
                .attachmentRow(AttachmentRow.attachmentRowBuilder()
                        .id(new RowId(new BigDecimal(3)))
                        .table(new RowValue("A_TABLE_NAME", STRING_TYPE, "TEST_2"))
                        .name(new RowValue("A_NAME", STRING_TYPE, "attachment 5"))
                        .fileName(new RowValue("A_FILE_NAME", STRING_TYPE, "attachment_5_.pdf"))
                        .description(new RowValue("A_DESCRIPTION", STRING_TYPE, "some value..."))
                        .attachmentData(new RowValue("A_FILE", ATTACHMENT_TYPE, "a5eghrst5hj74s65WEf"))
                        .build())
                .build();

        val expected = builder.build().toBuilder()
                .globalId(new RowId(new BigDecimal(1)))
                .table(expectedTable1)
                .table(expectedTable2)
                .build();

        testProcessor(Unchecked.function(p -> p.save(builder.build().toBuilder()
                        .globalId(new RowId(new BigDecimal(1)))
                        .table(testTable.toBuilder()
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_1")
                                        .id(new RowId(new BigDecimal(1)))
                                        .action(update)
                                        .build())
                                .build())
                        .table(testTable2.toBuilder()
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId(new BigDecimal(1)))
                                        .fk(new RowId(new BigDecimal(1)))
                                        .action(update)
                                        .item(new RowValue("NAME", STRING_TYPE, "ben"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(12)))
                                        .returnField(new DatabaseField("NAME", STRING_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId(new BigDecimal(2)))
                                        .fk(new RowId(new BigDecimal(1)))
                                        .action(delete)
                                        .item(new RowValue("NAME", STRING_TYPE, "john"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(13)))
                                        .returnField(new DatabaseField("NUMB", DECIMAL_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#3", null))
                                        .fk(new RowId(new BigDecimal(1)))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "tim"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(14)))
                                        .returnField(new DatabaseField("NAME", STRING_TYPE))
                                        .returnField(new DatabaseField("NUMB", DECIMAL_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#4", null))
                                        .fk(new RowId(new BigDecimal(1)))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "sam"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(15)))
                                        .build())
                                .build())
                        .table(attachmentTable)
                        .build())),
                expected.toBuilder()
                        .table(attachmentTable)
                        .build(),
                "CREATE TABLE TEST_1(ID NUMBER)",
                "CREATE TABLE TEST_2(ID NUMBER, NAME TEXT, NUMB NUMBER, TEST_1_ID NUMBER)",
                "CREATE TABLE TEST_ATTACHMENT(A_TABLE_NAME TEXT, ENTITY_ID NUMBER, A_NAME TEXT, A_FILE_NAME TEXT, A_DESCRIPTION TEXT, A_FILE BLOB)",
                "CREATE SEQUENCE TEST_1_SEQ",
                "CREATE SEQUENCE TEST_2_SEQ",
                "INSERT INTO TEST_1 VALUES(TEST_1_SEQ.NEXTVAL)",
                "INSERT INTO TEST_2 VALUES(TEST_2_SEQ.NEXTVAL, 'test_name', 12, 1)",
                "INSERT INTO TEST_2 VALUES(TEST_2_SEQ.NEXTVAL, 'test_name2', 13, 1)");

    }

    @Test
    public void save4() {
        val builder = SaveModel.builder()
                .rootTable("TEST_1")
                .preScript(Optional.empty())
                .postScript(Optional.empty())
                .timeZone(TimeZone.getDefault());

        val junctionTable = JunctionTable.builder()
                .source("TEST_2_TEST_2")
                .alias("jt3")
                .fromKey(new ForeignKeyField("TEST_1_KEY", DECIMAL_TYPE, "TEST_2", null))
                .toKey(new ForeignKeyField("TEST_2_KEY", DECIMAL_TYPE, "TEST_2", null))
                .supportedSource("TEST_2")
                .build();

        val expectedTable1 = testTable.toBuilder()
                .rows(new ArrayList<>())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_1")
                        .id(new RowId("#1", new BigDecimal(1)))
                        .build())
                .build();

        val expectedTable2 = testTable2.toBuilder()
                .rows(new ArrayList<>())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2")
                        .id(new RowId("#1", new BigDecimal(1)))
                        .fk(new RowId("#1", new BigDecimal(1)))
                        .item(new RowValue("NAME", STRING_TYPE, "ben"))
                        .build())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2")
                        .id(new RowId("#2", new BigDecimal(2)))
                        .fk(new RowId("#1", new BigDecimal(1)))
                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(13)))
                        .build())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2")
                        .id(new RowId("#3", new BigDecimal(3)))
                        .fk(new RowId("#1", new BigDecimal(1)))
                        .item(new RowValue("NAME", STRING_TYPE, "tim"))
                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(14)))
                        .build())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2")
                        .id(new RowId("#4", new BigDecimal(4)))
                        .build())
                .build();

        val expected = builder.build().toBuilder()
                .globalId(new RowId("#1", new BigDecimal(1)))
                .table(expectedTable1)
                .table(expectedTable2)
                .table(junctionTable.toBuilder()
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("TEST_2_TEST_2")
                                .action(insert)
                                .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, new BigDecimal(4)))
                                .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, new BigDecimal(2)))
                                .build())
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("TEST_2_TEST_2")
                                .action(insert)
                                .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, new BigDecimal(1)))
                                .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, new BigDecimal(3)))
                                .build())
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("TEST_2_TEST_2")
                                .action(insert)
                                .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, new BigDecimal(2)))
                                .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, new BigDecimal(1)))
                                .build())
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("TEST_2_TEST_2")
                                .action(insert)
                                .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, new BigDecimal(1)))
                                .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, new BigDecimal(4)))
                                .build())
                        .build())
                .build();

        testProcessor(Unchecked.function(p -> p.save(builder.build().toBuilder()
                        .table(testTable.toBuilder()
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_1")
                                        .id(new RowId("#1", null))
                                        .action(insert)
                                        .build())
                                .build())
                        .table(testTable2.toBuilder()
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#1", null))
                                        .fk(new RowId("#1", null))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "ben"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(12)))
                                        .returnField(new DatabaseField("NAME", STRING_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#2", null))
                                        .fk(new RowId("#1", null))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "john"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(13)))
                                        .returnField(new DatabaseField("NUMB", DECIMAL_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#3", null))
                                        .fk(new RowId("#1", null))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "tim"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(14)))
                                        .returnField(new DatabaseField("NAME", STRING_TYPE))
                                        .returnField(new DatabaseField("NUMB", DECIMAL_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#4", null))
                                        .fk(new RowId("#1", null))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "sam"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(15)))
                                        .build())
                                .build())
                        .table(junctionTable.toBuilder()
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2_TEST_2")
                                        .action(insert)
                                        .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, "#4"))
                                        .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, "#2"))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2_TEST_2")
                                        .action(insert)
                                        .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, "#1"))
                                        .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, "#3"))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2_TEST_2")
                                        .action(insert)
                                        .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, "#2"))
                                        .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, "#1"))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2_TEST_2")
                                        .action(insert)
                                        .item(new RowValue("TEST_1_KEY", DECIMAL_TYPE, "#1"))
                                        .item(new RowValue("TEST_2_KEY", DECIMAL_TYPE, "#4"))
                                        .build())
                                .build())
                        .build())),
                expected,
                "CREATE TABLE TEST_1(ID NUMBER)",
                "CREATE TABLE TEST_2(ID NUMBER, NAME TEXT, NUMB NUMBER, TEST_1_ID NUMBER)",
                "CREATE TABLE TEST_2_TEST_2(TEST_1_KEY NUMBER, TEST_2_KEY NUMBER)",
                "CREATE SEQUENCE TEST_1_SEQ",
                "CREATE SEQUENCE TEST_2_SEQ");
    }

    @Test
    public void save5() {
        val builder = SaveModel.builder()
                .rootTable("TEST_1")
                .preScript(Optional.empty())
                .postScript(Optional.empty())
                .timeZone(TimeZone.getDefault());

        val expectedTable1 = testTable.toBuilder()
                .primaryKey(new PrimaryKeyField("ID", DECIMAL_TYPE, true, null))
                .rows(new ArrayList<>())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_1")
                        .id(new RowId("#1", new BigDecimal(1)))
                        .build())
                .build();

        val expectedTable2 = testTable2.toBuilder()
                .primaryKey(new PrimaryKeyField("ID", DECIMAL_TYPE, true, null))
                .rows(new ArrayList<>())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2")
                        .id(new RowId("#1", new BigDecimal(1)))
                        .fk(new RowId("#1", new BigDecimal(1)))
                        .item(new RowValue("NAME", STRING_TYPE, "ben"))
                        .build())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2")
                        .id(new RowId("#2", new BigDecimal(2)))
                        .fk(new RowId("#1", new BigDecimal(1)))
                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(13)))
                        .build())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2")
                        .id(new RowId("#3", new BigDecimal(3)))
                        .fk(new RowId("#1", new BigDecimal(1)))
                        .item(new RowValue("NAME", STRING_TYPE, "tim"))
                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(14)))
                        .build())
                .row(DatabaseRow.dataRowBuilder()
                        .source("TEST_2")
                        .id(new RowId("#4", new BigDecimal(4)))
                        .build())
                .build();

        val expected = builder.build().toBuilder()
                .globalId(new RowId("#1", new BigDecimal(1)))
                .table(expectedTable1)
                .table(expectedTable2)
                .build();

        testProcessor(Unchecked.function(p -> p.save(builder.build().toBuilder()
                        .table(testTable.toBuilder()
                                .primaryKey(new PrimaryKeyField("ID", DECIMAL_TYPE, true, null))
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_1")
                                        .id(new RowId("#1", null))
                                        .action(insert)
                                        .build())
                                .build())
                        .table(testTable2.toBuilder()
                                .primaryKey(new PrimaryKeyField("ID", DECIMAL_TYPE, true, null))
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#1", null))
                                        .fk(new RowId("#1", null))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "ben"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(12)))
                                        .returnField(new DatabaseField("NAME", STRING_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#2", null))
                                        .fk(new RowId("#1", null))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "john"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(13)))
                                        .returnField(new DatabaseField("NUMB", DECIMAL_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#3", null))
                                        .fk(new RowId("#1", null))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "tim"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(14)))
                                        .returnField(new DatabaseField("NAME", STRING_TYPE))
                                        .returnField(new DatabaseField("NUMB", DECIMAL_TYPE))
                                        .build())
                                .row(DatabaseSubmitRow.submitRowBuilder()
                                        .source("TEST_2")
                                        .id(new RowId("#4", null))
                                        .fk(new RowId("#1", null))
                                        .action(insert)
                                        .item(new RowValue("NAME", STRING_TYPE, "sam"))
                                        .item(new RowValue("NUMB", DECIMAL_TYPE, new BigDecimal(15)))
                                        .build())
                                .build())
                        .build())),
                expected,
                "CREATE TABLE TEST_1(ID NUMBER AUTO_INCREMENT)",
                "CREATE TABLE TEST_2(ID NUMBER AUTO_INCREMENT, NAME TEXT, NUMB NUMBER, TEST_1_ID NUMBER)",
                "CREATE SEQUENCE TEST_1_SEQ",
                "CREATE SEQUENCE TEST_2_SEQ");
    }
}