/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.soap._12_2_2.childmapping;

import com.mantis.odc.services.soap.base.BaseSOAPProcessor;
import org.junit.Test;

import java.util.HashMap;

import static com.mantis.odc.models.OPAVersion.OPA_12_2_2;
import static com.mantis.odc.models.ServiceAction.Save;

/**
 * Test the child mapping against the 12.2.2 version of OPA.
 */
public class ChildMappingAutoSOAPTest extends BaseSOAPProcessor {

    @Override
    protected String getMappingPath() {
        return "MappingWithAutoKeys.xml";
    }

    @Test
    public void save1() {
        testSave(Save, OPA_12_2_2, "Save_req1.xml", "SaveAutoCreate.sql", "Save_resp1.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM CHILDREN", 1);
            put("SELECT COUNT(*) FROM TOYS", 2);
            put("SELECT COUNT(*) FROM PETS", 4);
            put("SELECT COUNT(*) FROM TOYS_PETS", 4);
            put("SELECT COUNT(*) FROM ATTACHMENTS", 0);
        }});
    }

    @Test
    public void save2() {
        testSave(Save, OPA_12_2_2, "Save_req2.xml", "SaveAutoCreate.sql", "Save_resp2.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM CHILDREN", 1);
            put("SELECT COUNT(*) FROM TOYS", 2);
            put("SELECT COUNT(*) FROM PETS", 4);
            put("SELECT COUNT(*) FROM TOYS_PETS", 4);
            put("SELECT COUNT(*) FROM ATTACHMENTS", 50);
        }});
    }

    @Test
    public void save3() {
        testSave(Save, OPA_12_2_2, "Save_req3.xml", "LoadAutoCreate.sql", "Save_resp3.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM CHILDREN", 1);
            put("SELECT COUNT(*) FROM TOYS", 4);
            put("SELECT COUNT(*) FROM PETS", 3);
            put("SELECT COUNT(*) FROM TOYS_PETS", 2);
            put("SELECT COUNT(*) FROM ATTACHMENTS", 0);
        }});
    }

    @Test
    public void save4() {
        testSave(Save, OPA_12_2_2, "Save_req4.xml", "LoadAutoCreate.sql", "Save_resp4.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM CHILDREN", 1);
            put("SELECT COUNT(*) FROM TOYS", 4);
            put("SELECT COUNT(*) FROM PETS", 3);
            put("SELECT COUNT(*) FROM TOYS_PETS", 2);
            put("SELECT COUNT(*) FROM ATTACHMENTS", 0);
        }});
    }

    @Test
    public void save5() {
        testSave(Save, OPA_12_2_2, "Save_req5.xml", "LoadAutoCreate.sql", "Save_resp5.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM CHILDREN", 1);
            put("SELECT COUNT(*) FROM TOYS", 4);
            put("SELECT COUNT(*) FROM PETS", 3);
            put("SELECT COUNT(*) FROM TOYS_PETS", 2);
            put("SELECT COUNT(*) FROM ATTACHMENTS", 0);
        }});
    }
}
