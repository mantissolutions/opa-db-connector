package com.mantis.odc.services.soap._12_2_9.inferredEntity;

import com.mantis.odc.services.soap.base.BaseSOAPProcessor;
import org.junit.Test;

import java.util.HashMap;

import static com.mantis.odc.models.OPAVersion.OPA_12_2_9;
import static com.mantis.odc.models.ServiceAction.Save;

/**
 * Tests inferred entities using the 12.2.9 connector framework.
 */
public class InferredEntityTests extends BaseSOAPProcessor {

    @Override
    protected String getMappingPath() {
        return "Mapping.xml";
    }

    @Test
    public void save1() {
        testSave(Save, OPA_12_2_9, "Save_req1.xml", "SaveCreate.sql", "Save_resp1.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM non_inferred", 1);
            put("SELECT COUNT(*) FROM non_inferred WHERE some_data = 'this is a test'", 1);
            put("SELECT COUNT(*) FROM inferred", 4);
            put("SELECT COUNT(*) FROM inferred WHERE fk = 1", 4);
            put("SELECT COUNT(*) FROM inferred WHERE some_inf_data = 'inf test 3'", 1);
        }});
    }
}
