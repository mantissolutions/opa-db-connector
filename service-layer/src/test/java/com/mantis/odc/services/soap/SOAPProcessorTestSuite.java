/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.soap;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Runs all the SOAP processor tests.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        com.mantis.odc.services.soap._12_1.childmapping.ChildMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2.childmapping.ChildMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2_2.childmapping.ChildMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2_5.childmapping.ChildMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2_9.childmapping.ChildMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_1.childmapping.ChildMappingAutoSOAPTest.class,
        com.mantis.odc.services.soap._12_2.childmapping.ChildMappingAutoSOAPTest.class,
        com.mantis.odc.services.soap._12_2_2.childmapping.ChildMappingAutoSOAPTest.class,
        com.mantis.odc.services.soap._12_2_5.childmapping.ChildMappingAutoSOAPTest.class,
        com.mantis.odc.services.soap._12_2_9.childmapping.ChildMappingAutoSOAPTest.class,
        com.mantis.odc.services.soap._12_1.webservice.WebServiceMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2.webservice.WebServiceMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2_2.webservice.WebServiceMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2_5.webservice.WebServiceMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2_9.webservice.WebServiceMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_1.webservice.WebServiceMappingAutoSOAPTest.class,
        com.mantis.odc.services.soap._12_2.webservice.WebServiceMappingAutoSOAPTest.class,
        com.mantis.odc.services.soap._12_2_2.webservice.WebServiceMappingAutoSOAPTest.class,
        com.mantis.odc.services.soap._12_2_5.webservice.WebServiceMappingAutoSOAPTest.class,
        com.mantis.odc.services.soap._12_2_9.webservice.WebServiceMappingAutoSOAPTest.class,
        com.mantis.odc.services.soap._12_2.sgipersistence.SGIPersistenceMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2_2.sgipersistence.SGIPersistenceMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2_5.sgipersistence.SGIPersistenceMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2_9.sgipersistence.SGIPersistenceMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2_2.pokemapping.PokeMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2_5.pokemapping.PokeMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2_9.pokemapping.PokeMappingSOAPTest.class,
        com.mantis.odc.services.soap._12_2_5.inferredEntity.InferredEntityTests.class,
        com.mantis.odc.services.soap._12_2_9.inferredEntity.InferredEntityTests.class
})
public class SOAPProcessorTestSuite {
}
