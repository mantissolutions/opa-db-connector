/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.soap._12_2_9.webservice;

import com.mantis.odc.services.soap.base.BaseSOAPProcessor;
import org.junit.Test;

import java.util.HashMap;

import static com.mantis.odc.models.OPAVersion.OPA_12_2_9;
import static com.mantis.odc.models.ServiceAction.*;

/**
 * Test the webservice mapping against the 12.2.9 version of OPA.
 */
public class WebServiceMappingSOAPTest extends BaseSOAPProcessor {

    @Test
    public void checkAlive() {
        test(CheckAlive, OPA_12_2_9, "CheckAlive_req1.xml", null, "CheckAlive_resp1.xml");
    }

    @Test
    public void getMetaData() {
        test(GetMetadata, OPA_12_2_9, "GetMetaData_req1.xml", null, "GetMetaData_resp1.xml");
    }

    @Test
    public void load1() {
        test(Load, OPA_12_2_9, "Load_req1.xml", "LoadCreate.sql", "Load_resp1.xml");
    }

    @Test
    public void save1() {
        testSave(Save, OPA_12_2_9, "Save_req1.xml", "LoadCreate.sql", "Save_resp1.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM catalog", 1);
            put("SELECT COUNT(*) FROM camera", 14);
            put("SELECT COUNT(*) FROM customer_request", 1);
            put("SELECT COUNT(*) FROM customer_request_att", 0);
            put("SELECT COUNT(*) FROM customer_request_camera", 3);
            put("SELECT COUNT(*) FROM ph_number", 2);
            put("SELECT COUNT(*) FROM ph_number_type", 2);
            put("SELECT COUNT(*) FROM phone_type", 3);
        }});
    }

    @Test
    public void save2() {
        testSave(Save, OPA_12_2_9, "Save_req2.xml", "UpdateCreate.sql", "Save_resp2.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM catalog", 1);
            put("SELECT COUNT(*) FROM camera", 14);
            put("SELECT COUNT(*) FROM customer_request", 1);
            put("SELECT COUNT(*) FROM customer_request_att", 0);
            put("SELECT COUNT(*) FROM customer_request_camera", 3);
            put("SELECT COUNT(*) FROM ph_number", 2);
            put("SELECT COUNT(*) FROM ph_number_type", 2);
            put("SELECT COUNT(*) FROM phone_type", 3);
        }});
    }
}
