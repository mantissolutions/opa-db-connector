/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.soap._12_2_2.sgipersistence;

import com.mantis.odc.services.soap.base.BaseSOAPProcessor;
import org.junit.Test;

import static com.mantis.odc.models.OPAVersion.OPA_12_2_2;
import static com.mantis.odc.models.ServiceAction.Load;

/**
 * Test the sgi persistence mapping against the 12.2.2 version of OPA.
 */
public class SGIPersistenceMappingSOAPTest extends BaseSOAPProcessor {

    @Test
    public void load1() {
        test(Load, OPA_12_2_2, "Load_req1.xml", "LoadCreate.sql", "Load_resp1.xml");
    }
}
