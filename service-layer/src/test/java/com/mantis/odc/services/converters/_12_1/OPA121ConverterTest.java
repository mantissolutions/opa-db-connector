/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters._12_1;

import com.mantis.odc.models.ConditionalJoin;
import com.mantis.odc.models.Mapping;
import com.mantis.odc.models.Operator;
import com.mantis.odc.services.converters.base.BaseConverterTest;
import com.mantis.odc.services.models.*;
import com.mantis.odc.services.sql.converters.ANSISQLTypeConverter;
import com.mantis.odc.services.sql.converters.base.SQLTypeConverter;
import com.mantis.odc.xml.MappingXMLTransport;
import com.oracle.opa.connector._12_1.metadata.types.LoadRequest;
import com.oracle.opa.connector._12_1.metadata.types.SaveRequest;
import lombok.val;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.math.BigDecimal;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

import static com.mantis.odc.models.DataType.*;
import static com.mantis.odc.models.OPAVersion.OPA_12_1;
import static com.mantis.odc.services.models.DBActionType.insert;
import static java.util.Collections.EMPTY_LIST;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the OPA 12.1 converter.
 */
public class OPA121ConverterTest extends BaseConverterTest {

    private OPA121Converter converter;
    private Mapping mapping;
    private Marshaller marshaller;
    private Unmarshaller unmarshaller;
    private SQLTypeConverter typeConverter;

    @Before
    public void setUp() throws Exception {
        converter = new OPA121Converter();
        typeConverter = new ANSISQLTypeConverter();
        val mappingXMLTransport = new MappingXMLTransport();
        mapping = mappingXMLTransport.load(getClass().getResourceAsStream("Mapping.xml"));
        marshaller = OPA_12_1.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        unmarshaller = OPA_12_1.createUnmarshaller();
    }

    @Test
    public void getCheckAlive() {
        assertNotNull(converter.getCheckAlive());
    }

    @Test
    public void getMetaData() throws Exception {
        val metaData = converter.getMetaData(mapping, EMPTY_LIST);
        compareGeneratedJAXB(marshaller, metaData, "GetMetaData_resp1.xml");
    }

    private final ContainmentTable.ContainmentTableBuilder childTable = ContainmentTable.builder()
            .source("CHILDREN").opaName("child").alias("t1")
            .primaryKey(PrimaryKeyField.builder().source("CHILD_ID").opaName("id").type(DECIMAL_TYPE).sequence("CHILD_SEQ.NEXTVAL").build())
            .field(DatabaseField.fieldBuilder().source("CHILD_NAME").opaName("name").type(STRING_TYPE).build())
            .field(DatabaseField.fieldBuilder().source("CHILD_AGE").opaName("age").type(DATE_TYPE).build());

    private final ContainmentTable.ContainmentTableBuilder toyTable = ContainmentTable.builder()
            .source("TOYS").opaName("toy").alias("t2").linkName("toys")
            .primaryKey(PrimaryKeyField.builder().source("TOY_ID").opaName("id").type(DECIMAL_TYPE).sequence("TOY_SEQ.NEXTVAL").build())
            .foreignKey(ForeignKeyField.builder().source("CHILD_ID").opaName("child id").type(DECIMAL_TYPE).target("CHILDREN").targetOpaName("child").build())
            .field(DatabaseField.fieldBuilder().source("TOY_NAME").opaName("name").type(STRING_TYPE).build())
            .field(DatabaseField.fieldBuilder().source("TOY_COST").opaName("cost").type(DECIMAL_TYPE).build())
            .field(DatabaseField.fieldBuilder().source("EXP_DATE").opaName("expDate").type(DATE_TIME_TYPE).build())
            .field(DatabaseField.fieldBuilder().source("SECOND_HAND").opaName("secondHand").type(BOOLEAN_TYPE).build());

    private final ContainmentTable.ContainmentTableBuilder petTable = ContainmentTable.builder()
            .source("PETS").opaName("pet").alias("t3").linkName("pets")
            .primaryKey(PrimaryKeyField.builder().source("PET_ID").opaName("id").type(DECIMAL_TYPE).sequence("PET_SEQ.NEXTVAL").build())
            .foreignKey(ForeignKeyField.builder().source("CHILD_ID").opaName("child id").type(DECIMAL_TYPE).target("CHILDREN").targetOpaName("child").build())
            .field(DatabaseField.fieldBuilder().source("PET_NAME").opaName("name").type(STRING_TYPE).build())
            .field(DatabaseField.fieldBuilder().source("PET_SPECIES").opaName("species").type(STRING_TYPE).build())
            .field(DatabaseField.fieldBuilder().source("FEEDING_TIME").opaName("feedingTime").type(TIME_TYPE).build());

    private final JunctionTable.JunctionTableBuilder toyPetTable = JunctionTable.builder()
            .source("TOYS_PETS").opaName("TOYS_PETS").alias("jt4").linkName("toy to pet")
            .fromKey(ForeignKeyField.builder().source("TOY_ID").opaName("TOY_ID").type(DECIMAL_TYPE).target("TOYS").targetOpaName("toy").build())
            .toKey(ForeignKeyField.builder().source("PET_ID").opaName("PET_ID").type(DECIMAL_TYPE).target("PETS").targetOpaName("pet").build())
            .supportedSource("TOYS");

    private final AttachmentTable.AttachmentTableBuilder attachmentTable = AttachmentTable.builder()
            .source("attachments")
            .opaName("attachments")
            .sourceField("a_table_name")
            .keyField("CHILD_ID")
            .nameField("a_name")
            .fileNameField("a_file_name")
            .descriptionField("a_description")
            .fileField("a_file");

    @Test
    public void toLoadModel1() throws Exception {
        val actual = converter.toLoadModel(mapping, (LoadRequest) unmarshaller.unmarshal(getClass().getResourceAsStream("Load_req1.xml")));
        val expected = LoadModel.builder()
                .locale(Locale.forLanguageTag("en-US"))
                .timeZone(TimeZone.getTimeZone("Australia/Melbourne"))
                .globalId(new RowId("1"))
                .rootTable("CHILDREN")
                .preScript(Optional.empty())
                .postScript(Optional.empty())
                .table(childTable.build())
                .table(toyTable.build())
                .table(petTable.build())
                .table(toyPetTable.build())
                .build();
        assertEquals(expected, actual);
    }

    @Test
    public void toLoadModel2() throws Exception {
        val actual = converter.toLoadModel(mapping, (LoadRequest) unmarshaller.unmarshal(getClass().getResourceAsStream("Load_req2.xml")));
        val expected = LoadModel.builder()
                .locale(Locale.forLanguageTag("en-US"))
                .timeZone(TimeZone.getTimeZone("Australia/Melbourne"))
                .globalId(new RowId("1"))
                .rootTable("CHILDREN")
                .preScript(Optional.empty())
                .postScript(Optional.empty())
                .table(childTable.build())
                .table(toyTable.build())
                .table(petTable.build())
                .table(toyPetTable.build())
                .condition(BindCondition.builder()
                        .source("TOY_NAME")
                        .alias("t2")
                        .type(STRING_TYPE)
                        .operator(Operator.equals)
                        .join(ConditionalJoin.and)
                        .value("teddy")
                        .build()
                        .toGroup())
                .build();
        assertEquals(expected, actual);
    }

    @Test
    public void fromLoadModel() throws Exception {
        val timezone = TimeZone.getTimeZone("Australia/Melbourne");
        val model = LoadModel.builder()
                .locale(Locale.forLanguageTag("en-US"))
                .timeZone(TimeZone.getTimeZone("Australia/Melbourne"))
                .globalId(new RowId("1"))
                .rootTable("CHILDREN")
                .table(childTable.build().toBuilder()
                        .row(DatabaseRow.dataRowBuilder()
                                .source("CHILDREN")
                                .id(new RowId(new BigDecimal(1)))
                                .item(value("CHILD_NAME", "name", STRING_TYPE, "Tim"))
                                .item(value("CHILD_AGE", "age", DATE_TYPE, typeConverter.convertFromSQLTimestamp(DatatypeConverter.parseDate("1986-12-05+11:00"), timezone)))
                                .build())
                        .build())
                .table(toyTable.build().toBuilder()
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS")
                                .id(new RowId(new BigDecimal(1)))
                                .fk(new RowId(new BigDecimal(1)))
                                .item(value("TOY_NAME", "name", STRING_TYPE, "Teddy"))
                                .item(value("TOY_COST", "cost", DECIMAL_TYPE, new BigDecimal(12)))
                                .item(value("EXP_DATE", "expDate", DATE_TIME_TYPE, typeConverter.convertFromSQLTimestamp(DatatypeConverter.parseDate("1997-02-15T15:50:23+11:00"), timezone)))
                                .item(value("SECOND_HAND", "secondHand", BOOLEAN_TYPE, true))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS")
                                .id(new RowId(new BigDecimal(2)))
                                .fk(new RowId(new BigDecimal(1)))
                                .item(value("TOY_NAME", "name", STRING_TYPE, "Truck"))
                                .item(value("TOY_COST", "cost", DECIMAL_TYPE, new BigDecimal(24)))
                                .item(value("EXP_DATE", "expDate", DATE_TIME_TYPE, typeConverter.convertFromSQLTimestamp(DatatypeConverter.parseDate("2330-10-04T03:10:30+10:00"), timezone)))
                                .item(value("SECOND_HAND", "secondHand", BOOLEAN_TYPE, false))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS")
                                .id(new RowId(new BigDecimal(3)))
                                .fk(new RowId(new BigDecimal(1)))
                                .item(value("TOY_NAME", "name", STRING_TYPE, "Ball"))
                                .item(value("TOY_COST", "cost", DECIMAL_TYPE, new BigDecimal(10)))
                                .item(value("EXP_DATE", "expDate", DATE_TIME_TYPE, null))
                                .item(value("SECOND_HAND", "secondHand", BOOLEAN_TYPE, true))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS")
                                .id(new RowId(new BigDecimal(4)))
                                .fk(new RowId(new BigDecimal(1)))
                                .item(value("TOY_NAME", "name", STRING_TYPE, "Bat"))
                                .item(value("TOY_COST", "cost", DECIMAL_TYPE, new BigDecimal(120)))
                                .item(value("EXP_DATE", "expDate", DATE_TIME_TYPE, null))
                                .item(value("SECOND_HAND", "secondHand", BOOLEAN_TYPE, false))
                                .build())
                        .build())
                .table(petTable.build().toBuilder()
                        .row(DatabaseRow.dataRowBuilder()
                                .source("PETS")
                                .id(new RowId(new BigDecimal(1)))
                                .fk(new RowId(new BigDecimal(1)))
                                .item(value("PET_NAME", "name", STRING_TYPE, "woffy"))
                                .item(value("PET_SPECIES", "species", STRING_TYPE, "dog"))
                                .item(value("FEEDING_TIME", "feedingTime", TIME_TYPE, typeConverter.convertFromSQLTimestamp(DatatypeConverter.parseDate("14:50:23+10:00"), timezone)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("PETS")
                                .id(new RowId(new BigDecimal(2)))
                                .fk(new RowId(new BigDecimal(1)))
                                .item(value("PET_NAME", "name", STRING_TYPE, "snowball"))
                                .item(value("PET_SPECIES", "species", STRING_TYPE, "cat"))
                                .item(value("FEEDING_TIME", "feedingTime", TIME_TYPE, typeConverter.convertFromSQLTimestamp(DatatypeConverter.parseDate("12:30:05+10:00"), timezone)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("PETS")
                                .id(new RowId(new BigDecimal(3)))
                                .fk(new RowId(new BigDecimal(1)))
                                .item(value("PET_NAME", "name", STRING_TYPE, "sharky"))
                                .item(value("PET_SPECIES", "species", STRING_TYPE, "gold fish"))
                                .item(value("FEEDING_TIME", "feedingTime", TIME_TYPE, null))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("PETS")
                                .id(new RowId(new BigDecimal(4)))
                                .fk(new RowId(new BigDecimal(1)))
                                .item(value("PET_NAME", "name", STRING_TYPE, "jelly bean"))
                                .item(value("PET_SPECIES", "species", STRING_TYPE, "guinea pig"))
                                .item(value("FEEDING_TIME", "feedingTime", TIME_TYPE, null))
                                .build())
                        .build())
                .table(toyPetTable.build().toBuilder()
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS_PETS")
                                .item(value("TOY_ID", DECIMAL_TYPE, new BigDecimal(1)))
                                .item(value("PET_ID", DECIMAL_TYPE, new BigDecimal(1)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS_PETS")
                                .item(value("TOY_ID", DECIMAL_TYPE, new BigDecimal(1)))
                                .item(value("PET_ID", DECIMAL_TYPE, new BigDecimal(3)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS_PETS")
                                .item(value("TOY_ID", DECIMAL_TYPE, new BigDecimal(2)))
                                .item(value("PET_ID", DECIMAL_TYPE, new BigDecimal(4)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS_PETS")
                                .item(value("TOY_ID", DECIMAL_TYPE, new BigDecimal(3)))
                                .item(value("PET_ID", DECIMAL_TYPE, new BigDecimal(1)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS_PETS")
                                .item(value("TOY_ID", DECIMAL_TYPE, new BigDecimal(3)))
                                .item(value("PET_ID", DECIMAL_TYPE, new BigDecimal(2)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS_PETS")
                                .item(value("TOY_ID", DECIMAL_TYPE, new BigDecimal(3)))
                                .item(value("PET_ID", DECIMAL_TYPE, new BigDecimal(3)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS_PETS")
                                .item(value("TOY_ID", DECIMAL_TYPE, new BigDecimal(3)))
                                .item(value("PET_ID", DECIMAL_TYPE, new BigDecimal(4)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS_PETS")
                                .item(value("TOY_ID", DECIMAL_TYPE, new BigDecimal(4)))
                                .item(value("PET_ID", DECIMAL_TYPE, new BigDecimal(1)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS_PETS")
                                .item(value("TOY_ID", DECIMAL_TYPE, new BigDecimal(4)))
                                .item(value("PET_ID", DECIMAL_TYPE, new BigDecimal(4)))
                                .build())
                        .build())
                .build();
        val resp = converter.fromLoadModel(model);
        compareGeneratedJAXB(marshaller, resp, "Load_resp1.xml");
    }

    @Test
    public void toSaveModel() throws Exception {
        val timezone = TimeZone.getTimeZone("Australia/Melbourne");
        val actual = converter.toSaveModel(mapping, (SaveRequest) unmarshaller.unmarshal(getClass().getResourceAsStream("Save_req1.xml")));
        val expected = SaveModel.builder()
                .globalId(new RowId("tim", null))
                .locale(Locale.forLanguageTag("en-US"))
                .timeZone(timezone)
                .rootTable("CHILDREN")
                .preScript(Optional.empty())
                .postScript(Optional.empty())
                .table(childTable.build().toBuilder()
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("CHILDREN")
                                .id(new RowId("tim", null))
                                .action(insert)
                                .item(value("CHILD_AGE", "age", DATE_TYPE, typeConverter.convertFromSQLDate(DatatypeConverter.parseDate("2015-06-09"), timezone)))
                                .item(value("CHILD_NAME", "name", STRING_TYPE, "tim"))
                                .returnField(DatabaseField.fieldBuilder()
                                        .source("CHILD_AGE").opaName("age")
                                        .type(DATE_TYPE)
                                        .build())
                                .build())
                        .build())
                .table(toyTable.build().toBuilder()
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("TOYS")
                                .id(new RowId("teddy", null))
                                .fk(new RowId("tim", null))
                                .action(insert)
                                .item(value("TOY_COST", "cost", DECIMAL_TYPE, new BigDecimal(12)))
                                .item(value("TOY_NAME", "name", STRING_TYPE, "teddy"))
                                .item(value("SECOND_HAND", "secondHand", BOOLEAN_TYPE, true))
                                .returnField(DatabaseField.fieldBuilder()
                                        .source("TOY_COST").opaName("cost")
                                        .type(DECIMAL_TYPE)
                                        .build())
                                .build())
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("TOYS")
                                .id(new RowId("truck", null))
                                .fk(new RowId("tim", null))
                                .action(insert)
                                .item(value("SECOND_HAND", "secondHand", BOOLEAN_TYPE, false))
                                .item(value("TOY_NAME", "name", STRING_TYPE, "truck"))
                                .item(value("TOY_COST", "cost", DECIMAL_TYPE, new BigDecimal(45)))
                                .build())
                        .build())
                .table(petTable.build().toBuilder()
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("PETS")
                                .id(new RowId("woffy", null))
                                .fk(new RowId("tim", null))
                                .action(insert)
                                .item(value("PET_NAME", "name", STRING_TYPE, "woffy"))
                                .item(value("PET_SPECIES", "species", STRING_TYPE, "dog"))
                                .build())
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("PETS")
                                .id(new RowId("jelly bean", null))
                                .fk(new RowId("tim", null))
                                .action(insert)
                                .item(value("PET_SPECIES", "species", STRING_TYPE, "guinea pig"))
                                .item(value("PET_NAME", "name", STRING_TYPE, "jelly bean"))
                                .returnField(DatabaseField.fieldBuilder()
                                        .source("PET_NAME").opaName("name")
                                        .type(STRING_TYPE)
                                        .build())
                                .build())
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("PETS")
                                .id(new RowId("snowball", null))
                                .fk(new RowId("tim", null))
                                .action(insert)
                                .item(value("PET_NAME", "name", STRING_TYPE, "snowball"))
                                .item(value("PET_SPECIES", "species", STRING_TYPE, "cat"))
                                .build())
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("PETS")
                                .id(new RowId("sharky", null))
                                .fk(new RowId("tim", null))
                                .action(insert)
                                .item(value("PET_SPECIES", "species", STRING_TYPE, "gold fish"))
                                .build())
                        .build())
                .table(toyPetTable.build().toBuilder()
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("TOYS_PETS")
                                .action(insert)
                                .item(value("TOY_ID", DECIMAL_TYPE, "teddy"))
                                .item(value("PET_ID", DECIMAL_TYPE, "woffy"))
                                .build())
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("TOYS_PETS")
                                .action(insert)
                                .item(value("TOY_ID", DECIMAL_TYPE, "teddy"))
                                .item(value("PET_ID", DECIMAL_TYPE, "jelly bean"))
                                .build())
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("TOYS_PETS")
                                .action(insert)
                                .item(value("TOY_ID", DECIMAL_TYPE, "truck"))
                                .item(value("PET_ID", DECIMAL_TYPE, "snowball"))
                                .build())
                        .row(DatabaseSubmitRow.submitRowBuilder()
                                .source("TOYS_PETS")
                                .action(insert)
                                .item(value("TOY_ID", DECIMAL_TYPE, "truck"))
                                .item(value("PET_ID", DECIMAL_TYPE, "sharky"))
                                .build())
                        .build())
                .table(attachmentTable.build().toBuilder()
                        .attachmentRow(AttachmentRow.attachmentRowBuilder()
                                .id(new RowId("tim", null))
                                .table(new RowValue("a_table_name", STRING_TYPE, "CHILDREN"))
                                .name(new RowValue("a_name", STRING_TYPE, "Attachment_1"))
                                .fileName(new RowValue("a_file_name", STRING_TYPE, "attachment_1.txt"))
                                .description(new RowValue("a_description", STRING_TYPE, "Some description for attachment 1"))
                                .attachmentData(new RowValue("a_file", ATTACHMENT_TYPE, "OGRmMzkzNjItOGMyNy00NTM4LWI3OTgtYjdiNzk1YjRkNDQ3"))
                                .build())
                        .attachmentRow(AttachmentRow.attachmentRowBuilder()
                                .id(new RowId("tim", null))
                                .table(new RowValue("a_table_name", STRING_TYPE, "CHILDREN"))
                                .name(new RowValue("a_name", STRING_TYPE, "Attachment_2"))
                                .fileName(new RowValue("a_file_name", STRING_TYPE, "attachment_2.txt"))
                                .description(new RowValue("a_description", STRING_TYPE, "Some description for attachment 2"))
                                .attachmentData(new RowValue("a_file", ATTACHMENT_TYPE, "NzAyMWYzNjYtNTg1My00NTA4LWExOTEtNjU2MTkwN2E2Yzcz"))
                                .build())
                        .attachmentRow(AttachmentRow.attachmentRowBuilder()
                                .id(new RowId("tim", null))
                                .table(new RowValue("a_table_name", STRING_TYPE, "CHILDREN"))
                                .name(new RowValue("a_name", STRING_TYPE, "Attachment_3"))
                                .fileName(new RowValue("a_file_name", STRING_TYPE, "attachment_3.txt"))
                                .description(new RowValue("a_description", STRING_TYPE, "Some description for attachment 3"))
                                .attachmentData(new RowValue("a_file", ATTACHMENT_TYPE, "NWQ1ZGEyOTgtYTFmMi00ZjNjLWEwODctM2E3N2RlZDBiOWVk"))
                                .build())
                        .build())
                .build();
        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    public void fromSaveModel() throws Exception {
        val timezone = TimeZone.getTimeZone("Australia/Melbourne");
        val model = SaveModel.builder()
                .table(childTable.build().toBuilder()
                        .row(DatabaseRow.dataRowBuilder()
                                .source("CHILDREN")
                                .id(new RowId("tim", new BigDecimal(1)))
                                .item(value("CHILD_AGE", "age", DATE_TYPE, typeConverter.convertFromSQLDate(DatatypeConverter.parseDate("2015-06-09"), timezone)))
                                .build())
                        .build())
                .table(toyTable.build().toBuilder()
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS")
                                .id(new RowId("teddy", new BigDecimal(1)))
                                .item(value("TOY_COST", "cost", DECIMAL_TYPE, new BigDecimal(12)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("TOYS")
                                .id(new RowId("truck", new BigDecimal(2)))
                                .build())
                        .build())
                .table(petTable.build().toBuilder()
                        .row(DatabaseRow.dataRowBuilder()
                                .source("PETS")
                                .id(new RowId("woffy", new BigDecimal(1)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("PETS")
                                .id(new RowId("jelly bean", new BigDecimal(2)))
                                .item(value("PET_NAME", "name", STRING_TYPE, "jelly bean"))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("PETS")
                                .id(new RowId("snowball", new BigDecimal(3)))
                                .build())
                        .row(DatabaseRow.dataRowBuilder()
                                .source("PETS")
                                .id(new RowId("sharky", new BigDecimal(4)))
                                .build())
                        .build())
                .build();
        val resp = converter.fromSaveModel(model);
        compareGeneratedJAXB(marshaller, resp, "Save_resp1.xml");
    }

}