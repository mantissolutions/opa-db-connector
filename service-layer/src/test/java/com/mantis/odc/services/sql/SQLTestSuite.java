/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.sql;

import com.mantis.odc.services.sql.adaptors.ANSISQLAdaptorTest;
import com.mantis.odc.services.sql.converters.ANSISQLTypeConverterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Runs all SQL tests.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        ANSISQLAdaptorTest.class,
        ANSISQLTypeConverterTest.class
})
public class SQLTestSuite {
}
