/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions;

import com.github.davidmoten.rx.jdbc.Database;
import com.mantis.odc.services.actions.base.ActionProcessor;
import com.mantis.odc.services.actions.base.BaseActionProcessorTest;
import com.mantis.odc.services.models.CheckpointModel;
import com.mantis.odc.services.models.CheckpointTable;
import com.mantis.odc.services.sql.adaptors.ANSISQLAdaptor;
import com.mantis.odc.services.sql.converters.ANSISQLTypeConverter;
import lombok.val;
import org.jooq.lambda.Unchecked;
import org.junit.Test;

import java.sql.Connection;
import java.util.UUID;

import static com.mantis.odc.models.Driver.H2;

/**
 * Test checkpoint actions.
 */
public class CheckpointProcessorTest extends BaseActionProcessorTest {

    private final CheckpointTable table = CheckpointTable.builder()
            .source("TEST_CHECKPOINT_TABLE")
            .idField("ID_FIELD")
            .dataField("DATA_FIELD")
            .build();

    @Override
    protected ActionProcessor getActionProcessor(Connection conn) {
        return new CheckpointProcessor(Database.from(conn), new ANSISQLAdaptor(H2), new ANSISQLTypeConverter());
    }

    @Test
    public void setCheckpoint1() {
        testProcessorWithNoResponse(Unchecked.consumer(p -> p.setCheckpoint(CheckpointModel.builder()
                        .checkpointTable(table)
                        .data("SDFDSFDSFDSFSDFDSFS".getBytes())
                        .build())),
                "CREATE TABLE TEST_CHECKPOINT_TABLE(ID_FIELD TEXT, DATA_FIELD BLOB)");
    }

    @Test
    public void setCheckpoint2() {
        val id = UUID.randomUUID().toString();
        testProcessorWithNoResponse(Unchecked.consumer(p -> p.setCheckpoint(CheckpointModel.builder()
                        .checkpointTable(table)
                        .id(UUID.randomUUID().toString())
                        .data("SDFDSFDSFDSFSDFDSFS".getBytes())
                        .build())),
                "CREATE TABLE TEST_CHECKPOINT_TABLE(ID_FIELD TEXT, DATA_FIELD BLOB)",
                "INSERT INTO TEST_CHECKPOINT_TABLE(ID_FIELD) VALUES('" + id + "')");
    }

    @Test
    public void getCheckpoint1() {
        val id = UUID.randomUUID().toString();
        val data = "NGI1NmM5MDAtNjhkYi00NjVlLWI0YTMtZWI4OTJkYWY3YTcw";
        testProcessorWithNoResponse(Unchecked.consumer(p -> p.getCheckpoint(CheckpointModel.builder()
                        .checkpointTable(table)
                        .id(id).build())),
                "CREATE TABLE TEST_CHECKPOINT_TABLE(ID_FIELD TEXT, DATA_FIELD BLOB)",
                "INSERT INTO TEST_CHECKPOINT_TABLE(ID_FIELD, DATA_FIELD) VALUES('" + id + "', RAWTOHEX('" + data + "'))");
    }
}