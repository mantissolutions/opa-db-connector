/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Runs all action processor tests.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        EnumerationProcessorTest.class,
        LoadProcessorTest.class,
        SaveProcessorTest.class,
        CheckpointProcessorTest.class
})
public class ActionProcessorTestSuite {
}
