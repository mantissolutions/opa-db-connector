/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters.base;

import com.mantis.odc.models.DataType;
import com.mantis.odc.services.models.RowValue;
import lombok.val;
import org.xmlunit.builder.Input;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;

import static org.junit.Assert.assertThat;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

/**
 * Base class for testing OPA converters.
 */
public class BaseConverterTest {

    protected void compareGeneratedJAXB(Marshaller m, Object metaData, String fileName) throws JAXBException {
        val bos = new ByteArrayOutputStream();
        m.marshal(metaData, bos);
        assertThat(bos.toString(), isSimilarTo(Input.from(getClass().getResourceAsStream(fileName)))
                .ignoreComments()
                .ignoreWhitespace()
                .throwComparisonFailure());
    }

    /**
     * HELPER CONSTRUCTORS
     **/

    protected RowValue value(String source, String opaName, DataType type, Object value) {
        return RowValue.builder().source(source).opaName(opaName).type(type).value(value).build();
    }

    protected RowValue value(String source, DataType type, Object value) {
        return RowValue.builder().source(source).opaName(source).type(type).value(value).build();
    }
}
