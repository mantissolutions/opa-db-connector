/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.soap._12_2_5.pokemapping;

import com.mantis.odc.services.soap.base.BaseSOAPProcessor;
import org.junit.Test;

import java.util.HashMap;

import static com.mantis.odc.models.OPAVersion.OPA_12_2_5;
import static com.mantis.odc.models.ServiceAction.Save;

/**
 * Test the child mapping against the 12.2.5 version of OPA.
 */
public class PokeMappingSOAPTest extends BaseSOAPProcessor {

    @Test
    public void save1() {
        testSave(Save, OPA_12_2_5, "Save_req1.xml", "SaveCreate.sql", "Save_resp1.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM poke_people WHERE first_name = 'Ash' " +
                    "AND allowance_amount = 50", 1);
        }});
    }
}
