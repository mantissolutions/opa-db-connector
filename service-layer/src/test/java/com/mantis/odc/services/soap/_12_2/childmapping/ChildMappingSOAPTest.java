/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.soap._12_2.childmapping;

import com.mantis.odc.services.soap.base.BaseSOAPProcessor;
import org.junit.Test;

import java.util.HashMap;

import static com.mantis.odc.models.OPAVersion.OPA_12_2;
import static com.mantis.odc.models.ServiceAction.*;

/**
 * Test the child mapping against the 12.2 version of OPA.
 */
public class ChildMappingSOAPTest extends BaseSOAPProcessor {

    @Test
    public void checkAlive() {
        test(CheckAlive, OPA_12_2, "CheckAlive_req1.xml", null, "CheckAlive_resp1.xml");
    }

    @Test
    public void getMetaData() {
        test(GetMetadata, OPA_12_2, "GetMetaData_req1.xml", null, "GetMetaData_resp1.xml");
    }

    @Test
    public void load1() {
        test(Load, OPA_12_2, "Load_req1.xml", "LoadCreate.sql", "Load_resp1.xml");
    }

    @Test
    public void load2() {
        test(Load, OPA_12_2, "Load_req2.xml", "LoadCreate.sql", "Load_resp2.xml");
    }

    @Test
    public void load3() {
        test(Load, OPA_12_2, "Load_req3.xml", "LoadCreate.sql", "Load_resp2.xml");
    }

    @Test
    public void load4() {
        test(Load, OPA_12_2, "Load_req4.xml", "LoadCreate.sql", "Load_resp3.xml");
    }

    @Test
    public void save1() {
        testSave(Save, OPA_12_2, "Save_req1.xml", "SaveCreate.sql", "Save_resp1.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM CHILDREN", 1);
            put("SELECT COUNT(*) FROM TOYS", 2);
            put("SELECT COUNT(*) FROM PETS", 4);
            put("SELECT COUNT(*) FROM TOYS_PETS", 4);
            put("SELECT COUNT(*) FROM ATTACHMENTS", 0);
        }});
    }

    @Test
    public void save2() {
        testSave(Save, OPA_12_2, "Save_req2.xml", "SaveCreate.sql", "Save_resp2.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM CHILDREN", 1);
            put("SELECT COUNT(*) FROM TOYS", 2);
            put("SELECT COUNT(*) FROM PETS", 4);
            put("SELECT COUNT(*) FROM TOYS_PETS", 4);
            put("SELECT COUNT(*) FROM ATTACHMENTS", 50);
        }});
    }

    @Test
    public void save3() {
        testSave(Save, OPA_12_2, "Save_req3.xml", "LoadCreate.sql", "Save_resp3.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM CHILDREN", 1);
            put("SELECT COUNT(*) FROM TOYS", 4);
            put("SELECT COUNT(*) FROM PETS", 3);
            put("SELECT COUNT(*) FROM TOYS_PETS", 2);
            put("SELECT COUNT(*) FROM ATTACHMENTS", 0);
        }});
    }

    @Test
    public void save4() {
        testSave(Save, OPA_12_2, "Save_req4.xml", "LoadCreate.sql", "Save_resp4.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM CHILDREN", 1);
            put("SELECT COUNT(*) FROM TOYS", 4);
            put("SELECT COUNT(*) FROM PETS", 3);
            put("SELECT COUNT(*) FROM TOYS_PETS", 2);
            put("SELECT COUNT(*) FROM ATTACHMENTS", 0);
        }});
    }

    @Test
    public void save5() {
        testSave(Save, OPA_12_2, "Save_req5.xml", "LoadCreate.sql", "Save_resp5.xml", new HashMap<String, Integer>() {{
            put("SELECT COUNT(*) FROM CHILDREN", 1);
            put("SELECT COUNT(*) FROM TOYS", 4);
            put("SELECT COUNT(*) FROM PETS", 3);
            put("SELECT COUNT(*) FROM TOYS_PETS", 2);
            put("SELECT COUNT(*) FROM ATTACHMENTS", 0);
        }});
    }
}
