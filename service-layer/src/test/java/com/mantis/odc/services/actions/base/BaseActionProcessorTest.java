/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions.base;

import com.github.davidmoten.rx.jdbc.Database;
import com.mantis.odc.models.DatasourceMapping;
import lombok.SneakyThrows;
import lombok.val;
import org.junit.Assert;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import static com.mantis.odc.models.Driver.H2;

/**
 * Base class for action processors.
 */
public abstract class BaseActionProcessorTest {

    /**
     * Returns a demo database connection.
     *
     * @return database connection
     * @throws ClassNotFoundException on missing driver
     * @throws SQLException           on connection error
     */
    protected Connection getConnection() throws ClassNotFoundException, SQLException {
        val datasource = new DatasourceMapping() {{
            setDriver(H2);
            setJdbc("jdbc:h2:mem:");
        }};
        return datasource.getConnection("test", "test");
    }

    /**
     * Tests an action processor.
     *
     * @param test     test to run
     * @param expected expected results
     * @param ddl      SQL to run before the test
     * @param <T>      expected type
     */
    @SneakyThrows
    protected <T> void testProcessor(Function<ActionProcessor, T> test, T expected, String... ddl) {
        try (val conn = getConnection()) {
            val db = Database.from(conn);
            // run in database.
            Stream.of(ddl).forEach(sql -> db.update(sql).execute());
            // run processor and compare output
            T actual = test.apply(getActionProcessor(conn));
            Assert.assertEquals(expected, actual);
        }
    }

    /**
     * Tests an action processor.
     *
     * @param test test to run
     * @param ddl  SQL to run before the test
     * @param <T>  expected type
     */
    @SneakyThrows
    protected <T> void testProcessorWithNoResponse(Consumer<ActionProcessor> test, String... ddl) {
        try (val conn = getConnection()) {
            val db = Database.from(conn);
            // run in database.
            Stream.of(ddl).forEach(sql -> db.update(sql).execute());
            // run processor and compare output
            test.accept(getActionProcessor(conn));
        }
    }

    /**
     * Returns the given action processor.
     *
     * @param conn active demo connection.
     * @return action processor
     */
    protected abstract ActionProcessor getActionProcessor(Connection conn);
}
