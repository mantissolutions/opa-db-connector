/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.xml;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.mantis.odc.models.*;
import lombok.Cleanup;
import lombok.val;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import static com.mantis.odc.models.ConditionalJoin.and;
import static com.mantis.odc.models.DataType.*;
import static com.mantis.odc.models.Driver.H2;
import static com.mantis.odc.models.EnumerationType.NumberEnumeration;
import static com.mantis.odc.models.EnumerationType.StringEnumeration;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;


/**
 * Tests XML serialisation.
 */
public class MappingXMLTransportTest {

    private Mapping example;
    private String exampleXML;
    private MappingXMLTransport transport;

    @Before
    public void setUp() throws Exception {
        // transport to test
        transport = new MappingXMLTransport();

        // load example xml
        InputStream is = getClass().getResourceAsStream("ExampleMapping.xml");
        exampleXML = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));

        // create mapping
        example = new Mapping() {{
            setName("Test Mappings");
            setDatasource(new DatasourceMapping() {{
                setName("TestDatasource");
                setDriver(H2);
                setJdbc("jdbc:h2:mem:maintest");
            }});
            setAttachment(new AttachmentMapping() {{
                setKeyField("CHILD_ID");
            }});
            setCheckpoint(new CheckpointMapping());

            // child table
            val children = new TableMapping() {{
                setTableId(new OPAMappingId("child", "CHILDREN"));
                asList(
                        new FieldMapping() {{
                            setFieldId(new OPAMappingId("id", "CHILD_ID"));
                            setType(DECIMAL_TYPE);
                            setPrimaryKey(true);
                            setSequence("CHILD_SEQ.NEXTVAL");
                        }},
                        new FieldMapping() {{
                            setFieldId(new OPAMappingId("name", "CHILD_NAME"));
                            setType(STRING_TYPE);
                        }},
                        new FieldMapping() {{
                            setFieldId(new OPAMappingId("age", "CHILD_AGE"));
                            setType(DATE_TYPE);
                        }}
                ).forEach(this::addFieldMaping);
            }};
            addTableMapping(children);

            // toy table
            val toyName = new FieldMapping() {{
                setFieldId(new OPAMappingId("name", "TOY_NAME"));
                setType(STRING_TYPE);
            }};
            val toys = new TableMapping() {{
                setTableId(new OPAMappingId("toy", "TOYS"));
                setLinkName("toys");
                setParent(children);
                asList(
                        new FieldMapping() {{
                            setFieldId(new OPAMappingId("id", "TOY_ID"));
                            setType(DECIMAL_TYPE);
                            setPrimaryKey(true);
                            setSequence("TOY_SEQ.NEXTVAL");
                        }},
                        toyName,
                        new FieldMapping() {{
                            setFieldId(new OPAMappingId("cost", "TOY_COST"));
                            setType(DECIMAL_TYPE);
                        }},
                        new FieldMapping() {{
                            setFieldId(new OPAMappingId("expDate", "EXP_DATE"));
                            setType(DATE_TIME_TYPE);
                        }},
                        new FieldMapping() {{
                            setFieldId(new OPAMappingId("secondHand", "SECOND_HAND"));
                            setType(BOOLEAN_TYPE);
                        }},
                        new FieldMapping() {{
                            setFieldId(new OPAMappingId("child id", "CHILD_ID"));
                            setType(DECIMAL_TYPE);
                            setForeignKey(true);
                        }}
                ).forEach(this::addFieldMaping);
            }};
            addTableMapping(toys);

            // pets table
            val pets = new TableMapping() {{
                setTableId(new OPAMappingId("pet", "PETS"));
                setLinkName("pets");
                setParent(children);
                asList(
                        new FieldMapping() {{
                            setFieldId(new OPAMappingId("id", "PET_ID"));
                            setType(DECIMAL_TYPE);
                            setPrimaryKey(true);
                            setSequence("PET_SEQ.NEXTVAL");
                        }},
                        new FieldMapping() {{
                            setFieldId(new OPAMappingId("name", "PET_NAME"));
                            setType(STRING_TYPE);
                        }},
                        new FieldMapping() {{
                            setFieldId(new OPAMappingId("species", "PET_SPECIES"));
                            setType(STRING_TYPE);
                        }},
                        new FieldMapping() {{
                            setFieldId(new OPAMappingId("feedingTime", "FEEDING_TIME"));
                            setType(TIME_TYPE);
                        }},
                        new FieldMapping() {{
                            setFieldId(new OPAMappingId("child id", "CHILD_ID"));
                            setType(DECIMAL_TYPE);
                            setForeignKey(true);
                        }}
                ).forEach(this::addFieldMaping);
            }};
            addTableMapping(pets);

            // toys pets junction table
            val toysPets = new JunctionTableMapping() {{
                setTableId(new OPAMappingId("TOYS_PETS"));
                setFromKeyName("TOY_ID");
                setFromTable(toys);
                setToKeyName("PET_ID");
                setToTable(pets);
            }};
            addJunctionTable(toysPets);

            // binds
            addBindMapping(new BindMapping() {{
                setTokenName("test");
                setCondition(and);
                setOperator(Operator.equals);
                setTable(toys);
                setField(toyName);
            }});

            // enumerations
            val enum2 = new EnumerationMapping() {{
                setName("test 2");
                setType(NumberEnumeration);
                setSql("SELECT d, e FROM table 2");
            }};
            asList(
                    enum2,
                    new EnumerationMapping() {{
                        setName("test 1");
                        setType(StringEnumeration);
                        setChild(enum2);
                        setSql("SELECT a, b, c FROM table");
                    }}
            ).forEach(this::addEnumerationMapping);
        }};
    }

    @Test
    public void testSave() throws Exception {
        @Cleanup ByteArrayOutputStream bos = new ByteArrayOutputStream();
        transport.save(example, bos);
        assertThat(bos.toString(), isSimilarTo(exampleXML)
                .ignoreComments()
                .ignoreWhitespace()
                .throwComparisonFailure());
    }

    @Test
    public void testLoad() throws Exception {
        @Cleanup ByteArrayInputStream bis = new ByteArrayInputStream(exampleXML.getBytes());
        Mapping mapping = transport.load(bis);
        assertEquals(example.toString(), mapping.toString());
    }
}