/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.xml;

import com.mantis.odc.jaxb.MappingType;
import com.mantis.odc.jaxb.ObjectFactory;
import com.mantis.odc.models.Mapping;
import com.mantis.odc.xml.base.XMLTransport;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.bind.JAXBElement;

/**
 * Converts Mapping domain models to and from XML.
 */
@Singleton
public class MappingXMLTransport extends XMLTransport<Mapping, MappingType> {

    private final ObjectFactory factory;
    private final MappingMapper mappingMapper;

    @Inject
    public MappingXMLTransport() {
        super(Mapping.class);
        this.factory = new ObjectFactory();
        this.mappingMapper = new MappingMapperImpl();
    }

    @Override
    protected String getJAXBPath() {
        return "com.mantis.odc.jaxb";
    }

    @Override
    public String getXSDPath() {
        return "/Mapping.xsd";
    }

    @Override
    protected JAXBElement<MappingType> toWrapper(MappingType mappingType) {
        return factory.createMapping(mappingType);
    }

    @Override
    public MappingType toJAXB(Mapping m) {
        return mappingMapper.mappingToJAXB(m);
    }

    @Override
    public Mapping fromJAXB(MappingType mappingType) {
        return mappingMapper.mappingFromJAXB(mappingType);
    }
}
