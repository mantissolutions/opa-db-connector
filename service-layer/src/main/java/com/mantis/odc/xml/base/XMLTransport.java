/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.xml.base;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Imports and exports mapping models.
 *
 * @param <MODEL>      domain model
 * @param <JAXB_MODEL> JAXB model
 */
@Slf4j
@Getter
public abstract class XMLTransport<MODEL, JAXB_MODEL> implements MessageBodyReader<MODEL>, MessageBodyWriter<MODEL> {

    private final Class<MODEL> modelClass;
    private final Marshaller marshaller;
    private final Unmarshaller unmarshaller;
    private final Schema schema;

    /**
     * Creates a new transport object.
     *
     * @param modelClass model class
     */
    protected XMLTransport(Class<MODEL> modelClass) {
        this.modelClass = modelClass;
        try {
            // create JAXB context
            JAXBContext jaxbContext = JAXBContext.newInstance(getJAXBPath());

            // create marshallers
            marshaller = jaxbContext.createMarshaller();
            unmarshaller = jaxbContext.createUnmarshaller();

            // add schema
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            schema = schemaFactory.newSchema(getClass().getResource(getXSDPath()));
            marshaller.setSchema(schema);
            unmarshaller.setSchema(schema);

            // format XML
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        } catch (Exception e) {
            log.error("Failed to create JAXB context", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * @return JAXB path
     */
    protected abstract String getJAXBPath();

    /**
     * @return xsd path
     */
    public abstract String getXSDPath();

    /**
     * Exports a mapping to the given output stream.
     *
     * @param model        model
     * @param outputStream output stream
     * @throws JAXBException on JAXB error
     */
    public void save(MODEL model, OutputStream outputStream) throws JAXBException {
        JAXB_MODEL jaxbModel = toJAXB(model);
        marshaller.marshal(toWrapper(jaxbModel), outputStream);
    }

    /**
     * Wraps the JAXB type in its final wrapper.
     *
     * @param jaxbModel JAXB model
     * @return wrapper
     */
    protected abstract JAXBElement<JAXB_MODEL> toWrapper(JAXB_MODEL jaxbModel);

    /**
     * Imports a model from the given input stream.
     *
     * @param inputStream input stream
     * @return model
     * @throws JAXBException on JAXB error
     */
    @SuppressWarnings("unchecked")
    public MODEL load(InputStream inputStream) throws JAXBException {
        Object e = unmarshaller.unmarshal(inputStream);
        if (e instanceof JAXBElement) return fromJAXB((JAXB_MODEL) ((JAXBElement) e).getValue());
        else return fromJAXB((JAXB_MODEL) e);
    }

    /**
     * Converts to the JAXB model.
     *
     * @param model model
     * @return JAXB model
     */
    public abstract JAXB_MODEL toJAXB(MODEL model);

    /**
     * Converts from the JAXB model.
     *
     * @param model JAXB model
     * @return model
     */
    public abstract MODEL fromJAXB(JAXB_MODEL model);

    /**
     * Writes the object to an XML string.
     *
     * @param model model
     * @return XML string
     */
    @SneakyThrows
    public String toXMLString(MODEL model) {
        val bos = new ByteArrayOutputStream();
        save(model, bos);
        return bos.toString();
    }

    // JERSEY METHODS //

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type.isAssignableFrom(modelClass);
    }

    @Override
    @SneakyThrows
    public MODEL readFrom(Class<MODEL> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws WebApplicationException {
        return load(entityStream);
    }

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type.isAssignableFrom(modelClass);
    }

    @Override
    public long getSize(MODEL model, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return -1;
    }

    @Override
    @SneakyThrows
    public void writeTo(MODEL model, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws WebApplicationException {
        save(model, entityStream);
    }
}
