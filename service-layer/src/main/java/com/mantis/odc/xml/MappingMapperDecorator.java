/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.xml;

import com.mantis.odc.jaxb.*;
import com.mantis.odc.models.*;
import com.mantis.odc.services.converters.exceptions.MissingMappingException;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.val;
import org.jooq.lambda.Unchecked;

import java.util.Optional;

import static com.mantis.odc.services.converters.exceptions.MissingMappingException.MappingType.*;

/**
 * Hand written mappers for mappings.
 * Complex models like tables, fields and junction tables are written here.
 */
abstract class MappingMapperDecorator implements MappingMapper {

    private final MappingMapper delegate;

    MappingMapperDecorator(MappingMapper delegate) {
        this.delegate = delegate;
    }

    @Override
    public MappingType mappingToJAXB(Mapping m) {
        MappingType t = delegate.mappingToJAXB(m);
        t.setDatasource(datasourceToJAXB(m.getDatasource()));
        if (m.isSupportsAttachments()) t.setAttachmentTable(attachmentTableToJAXB(m.getAttachment()));
        if (m.isSupportsCheckpoints()) t.setCheckpointTable(checkpointTableTypeToJAXB(m.getCheckpoint()));
        m.getEnumerations().forEach(em -> t.getEnumeration().add(enumerationMappingToJAXB(em)));
        m.getTables().forEach(tm -> t.getTable().add(tableToJAXB(tm)));
        m.getJunctionTables().forEach(tm -> t.getJunctionTable().add(junctionTableToJAXB(tm)));
        m.getBinds().forEach(bm -> t.getBind().add(bindMappingToJAXB(bm)));
        return t;
    }

    @Override
    public Mapping mappingFromJAXB(MappingType mappingType) {
        Mapping m = delegate.mappingFromJAXB(mappingType);
        // load enumerations first
        mappingType.getEnumeration().forEach(e -> enumerationMappingFromJAXB(e, m));
        // set parent child relationships
        mappingType.getEnumeration().stream()
                .filter(e -> e.getChildName() != null)
                .forEach(e -> {
                    EnumerationMapping em = m.getEnumeration(e.getName()).get();
                    em.setChild(m.getEnumeration(e.getChildName()).get());
                });

        // load tables
        mappingType.getTable().forEach(t -> m.addTableMapping(tableFromJAXB(m, t)));
        //set parent child relationships
        mappingType.getTable().stream()
                .filter(t -> t.getForeignKey() != null)
                .forEach(Unchecked.consumer(t -> {
                    OPAMappingId id = new OPAMappingId(t.getOpaName(), t.getSource());
                    TableMapping tm = m.getTable(id)
                            .orElseThrow(() -> new MissingMappingException(Table, id.getName()));

                    OPAMappingId parentId = fromForeignKey(t.getForeignKey());
                    tm.setParent(m.getTable(parentId).
                            orElseThrow(() -> new MissingMappingException(Table, parentId.getName())));
                }));
        mappingType.getJunctionTable().forEach(t -> m.addJunctionTable(junctionTableFromJAXB(m, t)));
        mappingType.getBind().forEach(b -> m.addBindMapping(bindMappingFromJAXB(m, b)));
        return m;
    }

    @SneakyThrows
    private BindMapping bindMappingFromJAXB(Mapping m, BindMappingType bmt) {
        val bm = delegate.bindMappingFromJAXB(bmt);
        val table = m.getTable(new OPAMappingId(bmt.getTableOpaName(), bmt.getTableSource()))
                .orElseThrow(() -> new MissingMappingException(Table, bmt.getFieldOpaName()));
        bm.setTable(table);
        bm.setField(table.getField(new OPAMappingId(bmt.getFieldOpaName(), bmt.getFieldSource()))
                .orElseThrow(() -> new MissingMappingException(Field, bmt.getFieldOpaName())));
        return bm;
    }

    @Override
    public DatasourceType datasourceToJAXB(DatasourceMapping datasource) {
        DatasourceType ds = delegate.datasourceToJAXB(datasource);
        ds.setConnectionPool(connectionPoolToJAXB(datasource));
        return ds;
    }

    @Override
    public AttachmentTableType attachmentTableToJAXB(AttachmentMapping attachmentMapping) {
        AttachmentTableType at = delegate.attachmentTableToJAXB(attachmentMapping);
        at.setTableField(fieldFromString(attachmentMapping.getTableField()));
        at.setKeyField(fieldFromString(attachmentMapping.getKeyField()));
        at.setNameField(fieldFromString(attachmentMapping.getNameField()));
        at.setFileNameField(fieldFromString(attachmentMapping.getFileNameField()));
        at.setDescriptionField(fieldFromString(attachmentMapping.getDescriptionField()));
        at.setFileField(fieldFromString(attachmentMapping.getFileField()));
        return at;
    }

    private FieldType fieldFromString(String name) {
        FieldType f = new FieldType();
        f.setOpaName(name);
        f.setSource(name);
        f.setInput(true);
        f.setOutput(true);
        f.setRequired(false);
        f.setType(DataTypeType.STRING_TYPE);
        return f;
    }

    @Override
    public CheckpointTableType checkpointTableTypeToJAXB(CheckpointMapping checkpointMapping) {
        CheckpointTableType ct = delegate.checkpointTableTypeToJAXB(checkpointMapping);
        ct.setIdField(fieldFromString(checkpointMapping.getIdField()));
        ct.setDataField(fieldFromString(checkpointMapping.getDataField()));
        return ct;
    }

    @SneakyThrows
    private JunctionTableMapping junctionTableFromJAXB(Mapping mapping, JunctionTableType jtt) {
        val jt = delegate.junctionTableFromJAXB(jtt);
        jt.setTableId(new OPAMappingId(jtt.getOpaName(), jtt.getSource()));
        val fk = fromForeignKey(jtt.getFromKey());
        jt.setFromTable(mapping.getTable(fk)
                .orElseThrow(() -> new MissingMappingException(Table, fk.getName())));
        val tk = fromForeignKey(jtt.getToKey());
        jt.setToTable(mapping.getTable(tk)
                .orElseThrow(() -> new MissingMappingException(Table, tk.getName())));
        return jt;
    }

    @Override
    public JunctionTableType junctionTableToJAXB(JunctionTableMapping t) {
        JunctionTableType jtt = delegate.junctionTableToJAXB(t);
        jtt.setFromKey(fieldFromJunction(t.getFromKeyName(), t.getFromTable(), t.getLinkName()));
        jtt.setToKey(fieldFromJunction(t.getToKeyName(), t.getToTable(), t.getLinkName()));
        return jtt;
    }

    private ForeignKeyType fieldFromJunction(@NonNull String keyName, @NonNull TableMapping tm, String linkName) {
        ForeignKeyType t = new ForeignKeyType();
        t.setTargetOpaName(tm.getTableId().getOpaName());
        t.setTargetSource(tm.getTableId().getSource());
        t.setLinkName(linkName);
        t.setSource(keyName);
        t.setType(dataTypeToJAXB(tm.getPrimaryKey().getType()));
        t.setInput(true);
        t.setOutput(true);
        t.setRequired(true);
        t.setOneToOne(null);
        return t;
    }

    @SneakyThrows
    private TableMapping tableFromJAXB(Mapping mapping, ContainmentTableType ctt) {
        TableMapping t = delegate.tableFromJAXB(ctt);
        t.setTableId(new OPAMappingId(ctt.getOpaName(), ctt.getSource()));
        t.addFieldMaping(fieldMappingFromJAXB(mapping, ctt.getPrimaryKey()));
        ctt.getField().stream()
                .map((fieldType) -> fieldMappingFromJAXB(mapping, fieldType))
                .forEach(t::addFieldMaping);
        if (ctt.getForeignKey() != null) t.addFieldMaping(fieldMappingFromJAXB(mapping, ctt.getForeignKey()));
        return t;
    }

    private OPAMappingId fromForeignKey(ForeignKeyType fk) {
        return new OPAMappingId(fk.getTargetOpaName(), fk.getTargetSource());
    }

    @Override
    public ContainmentTableType tableToJAXB(TableMapping tm) {
        ContainmentTableType ctt = delegate.tableToJAXB(tm);
        ctt.setPrimaryKey((PrimaryKeyType) fieldMappingToJAXB(tm.getPrimaryKey()));
        tm.getFields().stream().filter(f -> !f.isPrimaryKey() && !f.isForeignKey()).forEach(f -> ctt.getField().add(fieldMappingToJAXB(f)));
        if (tm.getForeignKey() != null) ctt.setForeignKey((ForeignKeyType) fieldMappingToJAXB(tm.getForeignKey()));
        return ctt;
    }

    @SneakyThrows
    private FieldMapping fieldMappingFromJAXB(Mapping mapping, FieldType ft) {
        FieldMapping fm = delegate.fieldMappingFromJAXB(ft);
        fm.setFieldId(new OPAMappingId(ft.getOpaName(), ft.getSource()));
        if (ft instanceof PrimaryKeyType) {
            PrimaryKeyType pk = (PrimaryKeyType) ft;
            fm.setPrimaryKey(true);
            fm.setAutoIncrement(pk.isAutoIncrement());
            fm.setSequence(pk.getSequence());
        } else if (ft instanceof ForeignKeyType) fm.setForeignKey(true);
        // add link to enumeration
        if (ft.getEnumerationId() != null)
            fm.setEnumeration(mapping.getEnumeration(ft.getEnumerationId())
                    .orElseThrow(() -> new MissingMappingException(Enumeration, ft.getEnumerationId())));
        return fm;
    }

    @Override
    public FieldType fieldMappingToJAXB(FieldMapping fm) {
        FieldType t = new FieldType();
        if (fm.isPrimaryKey()) {
            PrimaryKeyType pk = new PrimaryKeyType();
            pk.setAutoIncrement(fm.isAutoIncrement());
            pk.setSequence(fm.getSequence());
            t = pk;
        } else if (fm.isForeignKey()) {
            ForeignKeyType fk = new ForeignKeyType();
            TableMapping tm = fm.getTable();
            Optional<TableMapping> parent = tm.getParentOpt();
            fk.setLinkName(tm.getLinkName());
            parent.ifPresent(p -> {
                fk.setTargetOpaName(p.getTableId().getOpaName());
                fk.setTargetSource(p.getTableId().getSource());
            });
            fk.setOneToOne(tm.isOneToOne());
            t = fk;
        }
        if (fm.getEnumeration() != null) t.setEnumerationId(fm.getEnumeration().getName());
        return fieldMappingToJAXBFromExisting(fm, t);
    }

    @SneakyThrows
    private EnumerationMapping enumerationMappingFromJAXB(EnumerationMappingType emt, Mapping mapping) {
        val em = delegate.enumerationMappingFromJAXB(emt);
        mapping.addEnumerationMapping(em);
        return em;
    }


}
