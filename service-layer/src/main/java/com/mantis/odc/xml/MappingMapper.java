/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.xml;

import com.mantis.odc.jaxb.*;
import com.mantis.odc.jaxb.EnumerationType;
import com.mantis.odc.models.*;
import org.mapstruct.*;
import org.mapstruct.Mapping;

/**
 * Creates a mapper between mapping domain objects and JAXB mapping objects.
 * <p>
 * This is implemented using automatic annotation processors. This produces a MappingMapperImpl.
 */
@Mapper
@DecoratedWith(MappingMapperDecorator.class)
@SuppressWarnings("unused")
public interface MappingMapper {

    @Mapping(target = "datasource", ignore = true)
    MappingType mappingToJAXB(com.mantis.odc.models.Mapping mapping);

    @InheritInverseConfiguration
    @Mapping(source = "datasource", target = "datasource")
    @Mapping(source = "attachmentTable", target = "attachment")
    @Mapping(source = "checkpointTable", target = "checkpoint")
    com.mantis.odc.models.Mapping mappingFromJAXB(MappingType mappingType);

    DatasourceType datasourceToJAXB(DatasourceMapping datasource);

    @InheritInverseConfiguration
    @Mapping(source = "connectionPool.connectionTimeoutMs", target = "connectionTimeoutMs")
    @Mapping(source = "connectionPool.idleTimeoutMs", target = "idleTimeoutMs")
    @Mapping(source = "connectionPool.maxLifetimeMs", target = "maxLifetimeMs")
    @Mapping(source = "connectionPool.maxPoolSize", target = "maxPoolSize")
    @Mapping(source = "connectionPool.minIdle", target = "minIdle")
    @Mapping(source = "connectionPool.leakDetectionThreshold", target = "leakDetectionThreshold")
    DatasourceMapping datasourceFromJAXB(DatasourceType datasourceType);

    ConnectionPoolType connectionPoolToJAXB(DatasourceMapping datasource);

    @ValueMapping(source = "OPA_12_1", target = "OPA_121")
    @ValueMapping(source = "OPA_12_2", target = "OPA_122")
    @ValueMapping(source = "OPA_12_2_2", target = "OPA_1222")
    @ValueMapping(source = "OPA_12_2_5", target = "OPA_1225")
    @ValueMapping(source = "OPA_12_2_9", target = "OPA_1229")
    VersionType versionToJAXB(OPAVersion value);

    @InheritInverseConfiguration
    OPAVersion versionFromJAXB(VersionType versionType);

    @ValueMapping(source = "DB2_T4", target = "DB_2")
    @ValueMapping(source = "Derby", target = "DERBY")
    @ValueMapping(source = "H2", target = "H_2")
    @ValueMapping(source = "MySQL", target = "MY_SQL")
    @ValueMapping(source = "Oracle", target = "ORACLE")
    @ValueMapping(source = "PostgreSQL_T4", target = "POSTGRE_SQL")
    @ValueMapping(source = "SQLite", target = "SQ_LITE")
    @ValueMapping(source = "SQLServer_T4", target = "SQL_SERVER")
    DriverType driverToJAXB(Driver value);

    @InheritInverseConfiguration
    Driver driverFromJAXB(DriverType driverType);

    @Mapping(source = "tableName", target = "source")
    @Mapping(source = "tableName", target = "opaName")
    @Mapping(target = "tableField", ignore = true)
    @Mapping(target = "keyField", ignore = true)
    @Mapping(target = "nameField", ignore = true)
    @Mapping(target = "fileNameField", ignore = true)
    @Mapping(target = "descriptionField", ignore = true)
    @Mapping(target = "fileField", ignore = true)
    AttachmentTableType attachmentTableToJAXB(AttachmentMapping attachmentMapping);

    @InheritInverseConfiguration
    @Mapping(source = "tableField.source", target = "tableField")
    @Mapping(source = "keyField.source", target = "keyField")
    @Mapping(source = "nameField.source", target = "nameField")
    @Mapping(source = "fileNameField.source", target = "fileNameField")
    @Mapping(source = "descriptionField.source", target = "descriptionField")
    @Mapping(source = "fileField.source", target = "fileField")
    AttachmentMapping attachmentTableFromJAXB(AttachmentTableType attachmentTableType);

    @Mapping(source = "tableName", target = "source")
    @Mapping(source = "tableName", target = "opaName")
    @Mapping(target = "idField", ignore = true)
    @Mapping(target = "dataField", ignore = true)
    CheckpointTableType checkpointTableTypeToJAXB(CheckpointMapping checkpointMapping);

    @InheritInverseConfiguration
    @Mapping(source = "idField.source", target = "idField")
    @Mapping(source = "dataField.source", target = "dataField")
    CheckpointMapping checkpointTableFromJAXB(CheckpointTableType checkpointTableType);

    @Mapping(source = "tableId.source", target = "source")
    @Mapping(source = "tableId.opaName", target = "opaName")
    @Mapping(target = "primaryKey", ignore = true)
    @Mapping(target = "foreignKey", ignore = true)
    ContainmentTableType tableToJAXB(TableMapping tableMapping);

    @InheritInverseConfiguration
    @Mapping(source = "foreignKey.linkName", target = "linkName")
    TableMapping tableFromJAXB(ContainmentTableType ctt);

    @Mapping(source = "tableId.source", target = "source")
    @Mapping(source = "tableId.opaName", target = "opaName")
    @Mapping(source = "linkType", target = "cardinality")
    JunctionTableType junctionTableToJAXB(JunctionTableMapping junctionTableMapping);

    @InheritInverseConfiguration
    @Mapping(source = "fromKey.source", target = "fromKeyName")
    @Mapping(source = "toKey.source", target = "toKeyName")
    JunctionTableMapping junctionTableFromJAXB(JunctionTableType junctionTableType);

    @ValueMapping(source = "OneToOne", target = "ONE_TO_ONE")
    @ValueMapping(source = "OneToMany", target = "ONE_TO_MANY")
    @ValueMapping(source = "ManyToOne", target = "MANY_TO_ONE")
    @ValueMapping(source = "ManyToMany", target = "MANY_TO_MANY")
    CardinalityType cardinalityTypeToJAXB(LinkType linkType);

    @InheritInverseConfiguration
    LinkType cardinalityTypeFromJAXB(CardinalityType cardinalityType);

    @DataField
    @Mapping(source = "fieldId.source", target = "source")
    @Mapping(source = "fieldId.opaName", target = "opaName")
    FieldType fieldMappingToJAXB(FieldMapping fieldMapping);

    @Mapping(source = "fieldId.source", target = "source")
    @Mapping(source = "fieldId.opaName", target = "opaName")
    FieldType fieldMappingToJAXBFromExisting(FieldMapping fieldMapping, @MappingTarget FieldType fieldType);

    @DataField
    @InheritInverseConfiguration(name = "fieldMappingToJAXB")
    FieldMapping fieldMappingFromJAXB(FieldType fieldType);

    DataTypeType dataTypeToJAXB(DataType type);

    @InheritInverseConfiguration
    DataType dataTypeFromJAXB(DataTypeType type);

    @Mapping(source = "table.tableId.source", target = "tableSource")
    @Mapping(source = "table.tableId.opaName", target = "tableOpaName")
    @Mapping(source = "field.fieldId.source", target = "fieldSource")
    @Mapping(source = "field.fieldId.opaName", target = "fieldOpaName")
    BindMappingType bindMappingToJAXB(BindMapping bind);

    @InheritInverseConfiguration
    BindMapping bindMappingFromJAXB(BindMappingType bind);

    @ValueMapping(source = "and", target = "AND")
    @ValueMapping(source = "or", target = "OR")
    ConditionalJoinType joinTypeToJAXB(ConditionalJoin conditionalJoin);

    @InheritInverseConfiguration
    ConditionalJoin joinTypeFromJAXB(ConditionalJoinType conditionalJoinType);

    @ValueMapping(source = "equals", target = "EQUALS")
    @ValueMapping(source = "notEquals", target = "NOT_EQUALS")
    @ValueMapping(source = "greaterThan", target = "GREATER_THAN")
    @ValueMapping(source = "lessThan", target = "LESS_THAN")
    @ValueMapping(source = "greaterThanOrEqual", target = "GREATER_THAN_OR_EQUAL")
    @ValueMapping(source = "lessThanOrEqual", target = "LESS_THAN_OR_EQUAL")
    @ValueMapping(source = "like", target = "LIKE")
    @ValueMapping(source = "in", target = "IN")
    @ValueMapping(source = "isNull", target = "IS_NULL")
    @ValueMapping(source = "isNotNull", target = "IS_NOT_NULL")
    OperatorType operatorToJAXB(Operator operation);

    @InheritInverseConfiguration
    Operator operatorFromJAXB(OperatorType operatorType);

    @Mapping(source = "child.name", target = "childName")
    EnumerationMappingType enumerationMappingToJAXB(EnumerationMapping enumerationMapping);

    @InheritInverseConfiguration
    EnumerationMapping enumerationMappingFromJAXB(EnumerationMappingType enumerationMappingType);

    @ValueMapping(source = "StringEnumeration", target = "STRING_ENUMERATION")
    @ValueMapping(source = "NumberEnumeration", target = "NUMBER_ENUMERATION")
    @ValueMapping(source = "DateEnumeration", target = "DATE_ENUMERATION")
    @ValueMapping(source = "DateTimeEnumeration", target = "DATE_TIME_ENUMERATION")
    @ValueMapping(source = "TimeEnumeration", target = "TIME_ENUMERATION")
    @ValueMapping(source = "BooleanEnumeration", target = "BOOLEAN_ENUMERATION")
    EnumerationType enumerationTypeToJAXB(com.mantis.odc.models.EnumerationType type);

    @InheritInverseConfiguration
    com.mantis.odc.models.EnumerationType enumerationTypeFromJAXB(EnumerationType type);

    @interface DataField {
    }

    @interface PrimaryKey {
    }

    @interface ForeignKey {
    }
}
