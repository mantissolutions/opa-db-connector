/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions;

import com.github.davidmoten.rx.jdbc.Database;
import com.mantis.odc.models.EnumerationMapping;
import com.mantis.odc.services.actions.base.BaseDBProcessor;
import com.mantis.odc.services.models.*;
import com.mantis.odc.services.models.base.DatabaseEnumerationValue;
import com.mantis.odc.services.sql.adaptors.base.SQLAdaptor;
import com.mantis.odc.services.sql.converters.base.SQLTypeConverter;
import lombok.val;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import static com.mantis.odc.models.DataType.*;

/**
 * Retrieves enumerations from the database.
 */
class EnumerationProcessor extends BaseDBProcessor {

    EnumerationProcessor(Database db, SQLAdaptor adaptor, SQLTypeConverter typeConverter) {
        super(db, adaptor, typeConverter);
    }

    @Override
    public List<DatabaseEnumeration> enumerations(List<EnumerationMapping> enumerationMappings) {
        return enumerationMappings.stream()
                .map(this::createDatabaseEnumeration)
                .collect(Collectors.toList());
    }

    /**
     * Creates an enumeration from the given mapping.
     *
     * @param mapping enumeration mapping
     * @return database enumeration
     */
    private DatabaseEnumeration createDatabaseEnumeration(EnumerationMapping mapping) {
        val sql = mapping.getSql();
        val builder = DatabaseEnumeration.builder()
                .name(mapping.getName())
                .type(mapping.getType());

        // collect enumerations
        val enumerations = new LinkedHashMap<Integer, DatabaseEnumerationValue>();
        db.select(sql)
                .get(rs -> {
                    val value = rs.getObject(1);
                    val description = rs.getString(2);
                    val hash = (value + description).hashCode();

                    // create a new enumeration
                    if (!enumerations.containsKey(hash)) {
                        switch (mapping.getType()) {
                            case StringEnumeration:
                                enumerations.put(hash, StringEnumerationValue.builder()
                                        .value((String) typeConverter.convertFromSQLType(value, STRING_TYPE, TimeZone.getDefault()))
                                        .description(description)
                                        .build());
                                break;
                            case NumberEnumeration:
                                enumerations.put(hash, NumberEnumerationValue.builder()
                                        .value((BigDecimal) typeConverter.convertFromSQLType(value, DECIMAL_TYPE, TimeZone.getDefault()))
                                        .description(description)
                                        .build());
                                break;
                            case DateEnumeration:
                                enumerations.put(hash, DateEnumerationValue.builder()
                                        .value((XMLGregorianCalendar) typeConverter.convertFromSQLType(value, DATE_TYPE, TimeZone.getDefault()))
                                        .description(description)
                                        .build());
                                break;
                            case DateTimeEnumeration:
                                enumerations.put(hash, DateEnumerationValue.builder()
                                        .value((XMLGregorianCalendar) typeConverter.convertFromSQLType(value, DATE_TIME_TYPE, TimeZone.getDefault()))
                                        .description(description)
                                        .build());
                                break;
                            case TimeEnumeration:
                                enumerations.put(hash, DateEnumerationValue.builder()
                                        .value((XMLGregorianCalendar) typeConverter.convertFromSQLType(value, TIME_TYPE, TimeZone.getDefault()))
                                        .description(description)
                                        .build());
                                break;
                            case BooleanEnumeration:
                                enumerations.put(hash, BooleanEnumerationValue.builder()
                                        .value((Boolean) typeConverter.convertFromSQLType(value, BOOLEAN_TYPE, TimeZone.getDefault()))
                                        .description(description)
                                        .build());
                                break;
                        }
                    }

                    // load the enumeration
                    val enumValue = enumerations.get(hash);

                    // add any child value
                    if (mapping.hasChildEnumeration()) {
                        val childMapping = mapping.getChild();
                        val childValue = rs.getObject(3);
                        switch (childMapping.getType()) {
                            case StringEnumeration:
                                enumValue.child(typeConverter.convertFromSQLType(childValue, STRING_TYPE, TimeZone.getDefault()));
                                break;
                            case NumberEnumeration:
                                enumValue.child(typeConverter.convertFromSQLType(childValue, DECIMAL_TYPE, TimeZone.getDefault()));
                                break;
                            case DateEnumeration:
                                enumValue.child(typeConverter.convertFromSQLType(childValue, DATE_TYPE, TimeZone.getDefault()));
                                break;
                            case DateTimeEnumeration:
                                enumValue.child(typeConverter.convertFromSQLType(childValue, DATE_TIME_TYPE, TimeZone.getDefault()));
                                break;
                            case TimeEnumeration:
                                enumValue.child(typeConverter.convertFromSQLType(childValue, TIME_TYPE, TimeZone.getDefault()));
                                break;
                            case BooleanEnumeration:
                                enumValue.child(typeConverter.convertFromSQLType(childValue, BOOLEAN_TYPE, TimeZone.getDefault()));
                                break;
                        }
                    }

                    return enumValue;
                })
                .toBlocking()
                .subscribe();

        // add enumerations
        enumerations.forEach((integer, databaseEnumerationValue) -> {
            if (databaseEnumerationValue.getValue() == null) // uncertain enumeration
                builder.uncertainValue(databaseEnumerationValue);
            else builder.value(databaseEnumerationValue); // normal enumeration
        });

        return builder.build();
    }
}
