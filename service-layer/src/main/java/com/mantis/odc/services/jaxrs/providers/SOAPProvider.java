/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.providers;

import com.google.common.io.CountingOutputStream;
import lombok.SneakyThrows;
import lombok.val;

import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import static com.mantis.odc.services.soap.SOAPUtils.createMessage;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.MediaType.TEXT_XML;

/**
 * SOAP message provider for Jersey.
 */
@Produces({TEXT_XML, APPLICATION_XML})
public class SOAPProvider implements MessageBodyWriter<SOAPMessage>, MessageBodyReader<SOAPMessage> {

    @Inject
    public SOAPProvider() {
    }

    @Override
    public boolean isReadable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return isSOAPEnabled(aClass);
    }

    @Override
    public SOAPMessage readFrom(Class<SOAPMessage> aClass, Type type, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, String> multivaluedMap, InputStream inputStream) throws IOException, WebApplicationException {
        try {
            val messageSource = new StreamSource(inputStream);
            val message = createMessage();
            val soapPart = message.getSOAPPart();
            soapPart.setContent(messageSource);
            message.saveChanges();
            return message;
        } catch (SOAPException e) {
            throw new IOException(e);
        }
    }

    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return isSOAPEnabled(aClass);
    }

    @SneakyThrows
    @Override
    public long getSize(SOAPMessage soapMessage, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        val cos = new CountingOutputStream(new ByteArrayOutputStream());
        soapMessage.writeTo(cos);
        return cos.getCount();
    }

    @Override
    public void writeTo(SOAPMessage soapMessage, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap, OutputStream outputStream) throws IOException, WebApplicationException {
        try {
            soapMessage.writeTo(outputStream);
        } catch (SOAPException e) {
            throw new IOException(e);
        }
    }

    /**
     * Returns true if the class is of type SOAPMessage and it has a SOAP annotation.
     *
     * @param aClass class type
     * @return true if request is SOAP enabled
     */
    private boolean isSOAPEnabled(Class<?> aClass) {
        return SOAPMessage.class.isAssignableFrom(aClass);
    }
}
