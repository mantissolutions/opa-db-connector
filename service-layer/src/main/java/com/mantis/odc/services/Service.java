/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services;

import com.mantis.odc.models.Driver;
import com.mantis.odc.models.Mapping;
import lombok.Builder;
import lombok.Data;

/**
 * A service encapsulates all the required elements to execute OPA actions against a mapping.
 */
@Data
@Builder
public final class Service {

    /**
     * Name of the service.
     */
    private final String name;

    /**
     * Mapping the service operates against.
     */
    private final Mapping mapping;

    /**
     * Database the mapping relates to.
     */
    private final ServiceDatasource datasource;

    /**
     * @return database driver
     */
    public Driver getDriver() {
        return mapping.getDatasource().getDriver();
    }
}
