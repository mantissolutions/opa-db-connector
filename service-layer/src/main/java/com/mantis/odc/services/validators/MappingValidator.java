/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.validators;

import com.mantis.odc.models.*;
import com.mantis.odc.services.validators.base.BaseValidator;
import lombok.val;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashSet;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mantis.odc.services.validators.ValidationResult.*;
import static java.lang.String.format;
import static java.util.Objects.isNull;

/**
 * Validates mappings.
 */
@Singleton
public class MappingValidator extends BaseValidator<Mapping> {

    @Inject
    public MappingValidator() {
    }

    @Override
    public ValidationResult validate(Mapping mapping) {
        val builder = builder();

        hasValue(mapping.getName(), "mapping name", builder);
        hasValue(mapping.getBindToken(), "bind token", builder);
        hasValue(mapping.getDatasource(), "data source", builder);

        if (mapping.getTables().isEmpty())
            builder.child(error("Mapping must have at least one table"));
        else {
            builder.child(checkForGlobalTable(mapping.getTables()));
            builder.child(checkDuplicateValues(mapping.getTables(), "table mapping", TableMapping::getTableId));
            builder.child(checkDuplicateValues(Stream.concat(
                    mapping.getTables().stream()
                            .filter(tableMapping -> tableMapping.getLinkName() != null)
                            .map(TableMapping::getLinkName),
                    mapping.getJunctionTables().stream()
                            .filter(junctionTableMapping -> junctionTableMapping.getLinkName() != null)
                            .map(JunctionTableMapping::getLinkName)
            ).collect(Collectors.toList()), "Link name"));
            mapping.getTables().forEach(tableMapping -> builder.child(validateTableMapping(tableMapping)));
            mapping.getJunctionTables().forEach(junctionTableMapping -> builder.child(validateJunctionTableMapping(junctionTableMapping)));
        }

        mapping.getBinds().forEach(bindMapping -> builder.child(validateBindMapping(bindMapping)));

        if (mapping.isSupportsAttachments()) builder.child(validateAttachmentMapping(mapping.getAttachment()));
        if (mapping.isSupportsCheckpoints()) builder.child(validateCheckpointMapping(mapping.getCheckpoint()));

        return isValid("Mapping " + mapping.getName(), builder);
    }

    private ValidationResult checkForGlobalTable(List<TableMapping> tables) {
        return tables.stream().anyMatch(tableMapping -> tableMapping.getParent() == null) ?
                success("Mapping has at least one global table") :
                error("Mapping must have at least one global table");
    }

    private ValidationResult validateCheckpointMapping(CheckpointMapping table) {
        val builder = builder();

        hasValue(table.getBindToken(), "bind token value", builder);
        hasValue(table.getTableName(), "checkpoint table name", builder);
        hasValue(table.getIdField(), "id field name", builder);
        hasValue(table.getDataField(), "data field name", builder);

        return isValid("Checkpoint table mapping", builder);
    }

    private ValidationResult validateAttachmentMapping(AttachmentMapping table) {
        val builder = builder();

        hasValue(table.getTableName(), "attachment table name", builder);
        hasValue(table.getTableField(), "table name field name", builder);
        hasValue(table.getKeyField(), "key field name", builder);
        hasValue(table.getNameField(), "name field name", builder);
        hasValue(table.getFileNameField(), "file name field name", builder);
        hasValue(table.getDescriptionField(), "description field name", builder);
        hasValue(table.getFileField(), "file field name", builder);

        return isValid("Attachment table mapping", builder);
    }

    private ValidationResult validateBindMapping(BindMapping bind) {
        val builder = builder();

        val field = bind.getField();
        hasValue(field, "target field", builder);

        if (field != null) {
            val operator = bind.getOperator();
            // check like condition is valid
            if (operator == Operator.like && field.getType() != DataType.STRING_TYPE)
                builder.child(error("Like operators can only be used against Text fields."));

            // check correct boolean operator
            if (field.getType() == DataType.BOOLEAN_TYPE && (operator == Operator.greaterThan ||
                    operator == Operator.greaterThanOrEqual || operator == Operator.lessThan ||
                    operator == Operator.lessThanOrEqual)) {
                builder.child(error("Invalid operator selected for boolean type. " +
                        "Valid operators are Equal, Not Equal, In, Null and Not Null."));
            }
        }

        return isValid("Bind condition", builder);
    }

    private ValidationResult validateJunctionTableMapping(JunctionTableMapping table) {
        val builder = builder();
        val id = table.getTableId();

        hasValue(id.getSource(), "source", builder);
        hasValue(table.getFromKeyName(), "from key field name", builder);
        hasValue(table.getToKeyName(), "to key field name", builder);
        if (table.getFromTable() == null) builder.child(error("A from table is required."));
        if (table.getToTable() == null) builder.child(error("A to table is required."));

        return isValid("Junction table mapping " + id.getName(), builder);
    }

    private ValidationResult validateTableMapping(TableMapping table) {
        val builder = builder();
        val id = table.getTableId();

        hasValue(id.getSource(), "source", builder);
        hasValue(table.getPrimaryKey(), "primary key", builder);

        table.getParentOpt().ifPresent(parentTable -> {
            hasValue(table.getForeignKey(), "foreign key", builder);
            if (!isNull(parentTable.getPrimaryKey()) && !isNull(table.getForeignKey()))
                builder.child(hasMatchingKeyTypes(parentTable.getPrimaryKey(), table.getForeignKey()));
        });

        if (table.getFields().isEmpty())
            builder.child(error("Table mapping must have at least one field"));
        else {
            builder.child(checkDuplicateValues(table.getFields(), "field mapping", FieldMapping::getFieldId));
            table.getFields().forEach(fieldMapping -> builder.child(validateFieldMapping(fieldMapping)));
        }

        return isValid("Table mapping " + id.getName(), builder);
    }

    private ValidationResult validateFieldMapping(FieldMapping fieldMapping) {
        val builder = builder();
        val id = fieldMapping.getFieldId();

        hasValue(id.getSource(), "source", builder);
        hasValue(fieldMapping.getType(), "type", builder);

        return isValid("Field mapping " + id.getName(), builder);
    }

    private ValidationResult hasMatchingKeyTypes(FieldMapping pk, FieldMapping fk) {
        return pk.getType() == fk.getType() ?
                success("The foreign key type matches the parent tables primary key type") :
                error("The foreign key type " + fk.getType() + " does not match the parent tables primary key type " + pk.getType());
    }

    /**
     * Checks all OPA ids are unique.
     *
     * @param values    values
     * @param name      name of the item to check
     * @param extractor OPA id extractor
     * @param <T>       value type
     * @return validation result
     */
    private <T> ValidationResult checkDuplicateValues(List<T> values, String name, Function<T, OPAMappingId> extractor) {
        val set = new HashSet<OPAMappingId>();

        for (val object : values)
            if (!set.add(extractor.apply(object)))
                return error(format("%s name %s is not unique", name, extractor.apply(object).getName()));

        return success(format("All %s names are unique", name));
    }

    /**
     * Checks all OPA ids are unique.
     *
     * @param values values
     * @param name   name of the item to check
     * @return validation result
     */
    private ValidationResult checkDuplicateValues(List<String> values, String name) {
        val set = new HashSet<String>();

        for (val value : values)
            if (!set.add(value))
                return error(format("%s %s is not unique", name, value));

        return success(format("All %s are unique", name));
    }
}
