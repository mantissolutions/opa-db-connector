/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters._12_2_5;

import com.mantis.odc.models.CheckpointMapping;
import com.mantis.odc.models.FieldMapping;
import com.mantis.odc.models.Mapping;
import com.mantis.odc.models.TableMapping;
import com.mantis.odc.services.converters._12_2.OPA122Converter;
import com.mantis.odc.services.converters._12_2_2.OPA1222Converter;
import com.mantis.odc.services.converters.base.BaseOPAConverterWithoutExecuteQuery;
import com.mantis.odc.services.converters.exceptions.MissingMappingException;
import com.mantis.odc.services.models.*;
import com.oracle.opa.connector._12_2.data.types.AttachmentList;
import com.oracle.opa.connector._12_2_5.metadata.types.*;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.val;

import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mantis.odc.services.converters.exceptions.MissingMappingException.MappingType.Enumeration;
import static com.mantis.odc.utilities.ErrorUtil.errorToString;
import static java.lang.String.format;

/**
 * Converter for OPA Connector version 12.2.2.
 */
@Data
public final class OPA1225Converter extends BaseOPAConverterWithoutExecuteQuery<
        CheckAliveResponse,
        GetMetadataResponse,
        LoadRequest, LoadResponse,
        SaveRequest, SaveResponse,
        GetCheckpointRequest, GetCheckpointResponse,
        SetCheckpointRequest, SetCheckpointResponse,
        RequestFault> {

    /**
     * Provides simple converts for OPA 12.2.2.
     */
    private final OPA1225AutoConverter autoConverter_1225;

    /**
     * Child convertes.
     */
    private final OPA122Converter opa122Converter;
    private final OPA1222Converter opa1222Converter;
    private final ObjectFactory objectFactory;

    public OPA1225Converter() {
        autoConverter_1225 = new OPA1225AutoConverterImpl();
        opa122Converter = new OPA122Converter();
        opa1222Converter = new OPA1222Converter();
        objectFactory = new ObjectFactory();
    }

    @Override
    public CheckAliveResponse getCheckAlive() {
        return new CheckAliveResponse();
    }

    @Override
    public RequestFault getFault(Exception e) {
        val fault = new RequestFault();
        fault.setMessage(format("%s\nStack Trace:\n%s", e.getMessage(), errorToString(e)));
        return fault;
    }

    @Override
    public GetMetadataResponse getMetaData(Mapping mapping, List<DatabaseEnumeration> enumerations) {
        val resp = new GetMetadataResponse();
        resp.setSupportsCheckpoints(mapping.isSupportsCheckpoints());
        resp.getTable().addAll(mapping.getTables().stream()
                .map(this::createMetaTable)
                .collect(Collectors.toList()));
        if (!enumerations.isEmpty()) {
            val enums = new MetaEnumList();
            enums.getNumberEnumerationOrStringEnumerationOrDateEnumeration().addAll(enumerations.stream()
                    .map(databaseEnumeration -> createMetaEnumeration(mapping, databaseEnumeration))
                    .collect(Collectors.toList()));
            resp.setMetadataEnumerations(enums);
        }
        return resp;
    }

    /**
     * Creates an enumeration.
     *
     * @param mapping             mapping
     * @param databaseEnumeration database enumeration
     * @return enumeration
     */
    @SneakyThrows
    public Object createMetaEnumeration(Mapping mapping, DatabaseEnumeration databaseEnumeration) {
        val enumMapping = mapping.getEnumeration(databaseEnumeration.getName())
                .orElseThrow(() -> new MissingMappingException(Enumeration, databaseEnumeration.getName()));
        Class enumType;
        Class enumValueType;
        switch (databaseEnumeration.getType()) {
            case StringEnumeration:
                enumType = StringEnumeration.class;
                enumValueType = StringEnumVal.class;
                break;
            case NumberEnumeration:
                enumType = NumberEnumeration.class;
                enumValueType = NumberEnumVal.class;
                break;
            case DateEnumeration:
                enumType = DateEnumeration.class;
                enumValueType = DateEnumVal.class;
                break;
            case DateTimeEnumeration:
                enumType = DateTimeEnumeration.class;
                enumValueType = DateTimeEnumVal.class;
                break;
            case TimeEnumeration:
                enumType = TimeEnumeration.class;
                enumValueType = TimeEnumVal.class;
                break;
            case BooleanEnumeration:
                enumType = BooleanEnumeration.class;
                enumValueType = BooleanEnumVal.class;
                break;
            default:
                return null;
        }
        return opa1222Converter.createEnum(
                enumType,
                enumValueType,
                UncertainEnumVal.class,
                enumMapping,
                databaseEnumeration,
                ChildEnumValues.class,
                (serializables, type) -> {
                    if (serializables != null && !serializables.isEmpty()) {
                        val children = new ChildEnumValues();
                        children.getNumberValueOrStringValueOrDateValue()
                                .addAll(serializables.stream()
                                        .map(v -> {
                                            switch (type) {
                                                case StringEnumeration:
                                                    return objectFactory.createChildEnumValuesStringValue((String) v);
                                                case NumberEnumeration:
                                                    return objectFactory.createChildEnumValuesNumberValue((BigDecimal) v);
                                                case DateEnumeration:
                                                    return objectFactory.createChildEnumValuesDateValue((XMLGregorianCalendar) v);
                                                case DateTimeEnumeration:
                                                    return objectFactory.createChildEnumValuesDateTimeValue((XMLGregorianCalendar) v);
                                                case TimeEnumeration:
                                                    return objectFactory.createChildEnumValuesTimeValue((XMLGregorianCalendar) v);
                                                case BooleanEnumeration:
                                                default:
                                                    return objectFactory.createChildEnumValuesBooleanValue((Boolean) v);
                                            }
                                        })
                                        .collect(Collectors.toList()));
                        return children;
                    } else return null;
                });
    }

    /**
     * Creates a meta table from the table mapping.
     *
     * @param table table mapping
     * @return meta table
     */
    private MetaTable createMetaTable(TableMapping table) {
        val metaTable = new MetaTable();
        metaTable.setName(table.getTableId().getName());
        metaTable.setAcceptsAttachments(table.getMapping().isSupportsAttachments());
        metaTable.setCanBeInput(table.canBeInput());
        metaTable.setCanBeOutput(table.canBeOutput());
        metaTable.getField().addAll(table.getFields().stream()
                .filter(field -> (field.isInput() || field.isOutput()) && !field.isForeignKey())
                .map(this::createMetaField)
                .collect(Collectors.toList()));
        metaTable.getLink().addAll(Stream.concat(
                table.getChildren().stream()
                        .map(child -> createMetaLink(
                                child.getLinkName(),
                                child.getTableId().getName(),
                                (child.isOneToOne()) ? MetaLinkCardinality.C_11 : MetaLinkCardinality.C_1_M)),
                table.getJunctionTables().stream()
                        .map(junctionTable -> createMetaLink(
                                junctionTable.getLinkName(),
                                junctionTable.getTargetName(table.getTableId()),
                                MetaLinkCardinality.fromValue(junctionTable.getLinkType(table).getValue())))
        )
                .collect(Collectors.toList()));
        return metaTable;
    }

    /**
     * Creates a meta field from the field mapping.
     *
     * @param fieldMapping field mapping
     * @return meta field
     */
    private MetaField createMetaField(FieldMapping fieldMapping) {
        val metaField = new MetaField();
        metaField.setName(fieldMapping.getFieldId().getName());
        metaField.setCanBeOutput(fieldMapping.isOutput());
        metaField.setIsRequired(fieldMapping.isRequired());
        metaField.setCanBeInput(fieldMapping.isInput());
        if (fieldMapping.getEnumeration() != null)
            metaField.setEnumType(fieldMapping.getEnumeration().getName());
        else
            metaField.setType(autoConverter_1225.create1225MetaFieldDataType(fieldMapping.getType()));
        return metaField;
    }

    /**
     * Creates a meta link from the given parent and child table mappings.
     *
     * @param name        name of the relationship
     * @param target      target of the relationship
     * @param cardinality link cardinality
     * @return meta link
     */
    private MetaLink createMetaLink(String name, String target, MetaLinkCardinality cardinality) {
        val metaLink = new MetaLink();
        metaLink.setName(name);
        metaLink.setTarget(target);
        metaLink.setCardinality(cardinality);
        return metaLink;
    }

    @SneakyThrows
    @Override
    public LoadModel toLoadModel(Mapping mapping, LoadRequest loadRequest) {
        return opa122Converter.createLoadModel(mapping,
                loadRequest.getRoot(),
                loadRequest.getLanguage(),
                loadRequest.getTimezone(),
                loadRequest.getTables().getTable(),
                asMap(loadRequest.getRequestContext()));
    }

    private Map<String, String> asMap(RequestContext requestContext) {
        val map = new HashMap<String, String>();
        if (requestContext != null) for (val p : requestContext.getParameter()) map.put(p.getName(), p.getValue());
        return map;
    }

    /**
     * Finds and returns the given request param.
     *
     * @param key key to search for
     * @param ctx context.
     * @return value or none
     */
    private Optional<String> getValueFromContext(String key, RequestContext ctx) {
        return (ctx != null) ? ctx.getParameter().stream()
                .filter(p -> p.getName().equals(key))
                .map(RequestContextParam::getValue)
                .findFirst() :
                Optional.empty();
    }

    @Override
    public LoadResponse fromLoadModel(LoadModel model) {
        val resp = new LoadResponse();
        resp.setTables(opa122Converter.getLoadResponseData(model));
        return resp;
    }

    @SneakyThrows
    @Override
    public SaveModel toSaveModel(Mapping mapping, SaveRequest saveRequest) {
        return opa122Converter.createSaveModel(mapping,
                saveRequest.getRoot(),
                saveRequest.getLanguage(),
                saveRequest.getTimezone(),
                saveRequest.getTables().getTable(),
                Optional.ofNullable(saveRequest.getAttachments())
                        .map(AttachmentList::getAttachment)
                        .orElse(new ArrayList<>()),
                asMap(saveRequest.getRequestContext()));
    }

    @Override
    public SaveResponse fromSaveModel(SaveModel model) {
        val resp = new SaveResponse();
        resp.setTables(opa122Converter.getUpdateData(model));
        return resp;
    }

    @Override
    public CheckpointModel toCheckpointModelFromGetCheckpointRequest(Mapping mapping, GetCheckpointRequest req) {
        val checkpointMapping = mapping.getCheckpoint();
        return CheckpointModel.builder()
                .id(getValueFromContext(checkpointMapping.getBindToken(), req.getRequestContext()).orElse(null))
                .checkpointTable(createCheckpointTable(checkpointMapping))
                .build();
    }

    /**
     * Creates a checkpoint table from a checkpoint table mapping.
     *
     * @param checkpointMapping checkpoint table mapping
     * @return checkpoint table
     */
    public CheckpointTable createCheckpointTable(CheckpointMapping checkpointMapping) {
        return CheckpointTable.builder()
                .source(checkpointMapping.getTableName())
                .opaName(checkpointMapping.getTableName())
                .idField(checkpointMapping.getIdField())
                .dataField(checkpointMapping.getDataField())
                .build();
    }

    @Override
    public GetCheckpointResponse fromCheckpointModelToGetCheckpointResponse(CheckpointModel model) {
        val resp = new GetCheckpointResponse();
        val data = new CheckpointData();
        data.setCheckpointId(model.getId());
        data.setValue(DatatypeConverter.printBase64Binary(model.getData()));
        resp.setCheckpointData(data);
        return resp;
    }

    @Override
    public CheckpointModel toCheckpointModelFromSetCheckpointRequest(Mapping mapping, SetCheckpointRequest req) {
        val checkpointMapping = mapping.getCheckpoint();
        return CheckpointModel.builder()
                .id(req.getCheckpointData().getCheckpointId())
                .data(DatatypeConverter.parseBase64Binary(req.getCheckpointData().getValue()))
                .checkpointTable(createCheckpointTable(checkpointMapping))
                .build();
    }

    @Override
    public SetCheckpointResponse fromCheckpointModelToSetCheckpointResponse(CheckpointModel model) {
        val resp = new SetCheckpointResponse();
        resp.setCheckpointId(model.getId());
        return resp;
    }
}
