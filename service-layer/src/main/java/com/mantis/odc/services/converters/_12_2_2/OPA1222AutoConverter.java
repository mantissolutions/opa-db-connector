/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters._12_2_2;

import com.mantis.odc.models.DataType;
import com.mantis.odc.services.converters._12_2.OPA122AutoConverter;
import com.oracle.opa.connector._12_2_2.metadata.types.MetaFieldDataType;
import org.mapstruct.Mapper;
import org.mapstruct.ValueMapping;

/**
 * This is an auto converter, it handles some of the easier conversions and is used in the
 * main OPA1222Converter.
 */
@Mapper
@SuppressWarnings("unused")
public interface OPA1222AutoConverter extends OPA122AutoConverter {

    @ValueMapping(source = "STRING_TYPE", target = "STRING")
    @ValueMapping(source = "DATE_TIME_TYPE", target = "DATETIME")
    @ValueMapping(source = "BOOLEAN_TYPE", target = "BOOLEAN")
    @ValueMapping(source = "DATE_TYPE", target = "DATE")
    @ValueMapping(source = "DECIMAL_TYPE", target = "DECIMAL")
    @ValueMapping(source = "TIME_TYPE", target = "TIMEOFDAY")
    @ValueMapping(source = "ATTACHMENT_TYPE", target = "STRING")
    MetaFieldDataType create1222MetaFieldDataType(DataType type);
}
