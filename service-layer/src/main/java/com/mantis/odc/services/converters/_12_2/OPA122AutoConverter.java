/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters._12_2;

import com.mantis.odc.models.DataType;
import com.mantis.odc.models.FieldMapping;
import com.mantis.odc.services.models.DBActionType;
import com.mantis.odc.services.models.DatabaseLink;
import com.mantis.odc.services.models.RowId;
import com.oracle.opa.connector._12_2.data.types.DataLink;
import com.oracle.opa.connector._12_2.data.types.LinkRef;
import com.oracle.opa.connector._12_2.data.types.RowAction;
import com.oracle.opa.connector._12_2.metadata.types.MetaField;
import com.oracle.opa.connector._12_2.metadata.types.MetaFieldDataType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ValueMapping;

/**
 * This is an auto converter, it handles some of the easier conversions and is used in the
 * main OPA122Converter.
 */
@Mapper
@SuppressWarnings("unused")
public interface OPA122AutoConverter {

    @Mapping(source = "fieldId.name", target = "name")
    @Mapping(source = "input", target = "canBeInput")
    @Mapping(source = "output", target = "canBeOutput")
    @Mapping(source = "required", target = "isRequired")
    MetaField createMetaField(FieldMapping field);

    @ValueMapping(source = "STRING_TYPE", target = "STRING")
    @ValueMapping(source = "DATE_TIME_TYPE", target = "DATETIME")
    @ValueMapping(source = "BOOLEAN_TYPE", target = "BOOLEAN")
    @ValueMapping(source = "DATE_TYPE", target = "DATE")
    @ValueMapping(source = "DECIMAL_TYPE", target = "DECIMAL")
    @ValueMapping(source = "TIME_TYPE", target = "TIMEOFDAY")
    @ValueMapping(source = "ATTACHMENT_TYPE", target = "STRING")
    MetaFieldDataType create122MetaFieldDataType(DataType type);

    @Mapping(source = "linkName", target = "name")
    @Mapping(source = "refs", target = "ref")
    DataLink createDataLink(DatabaseLink link);

    @Mapping(source = "databaseKeyAsString", target = "id")
    LinkRef createLinkRef(RowId rowId);

    @ValueMapping(source = "CREATE", target = "insert")
    @ValueMapping(source = "UPDATE", target = "update")
    @ValueMapping(source = "DELETE", target = "delete")
    DBActionType createDBAction(RowAction action);
}
