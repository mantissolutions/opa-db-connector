/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.utils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 * Utilities for creating REST responses.
 */
public class ResponseUtils {

    public static Response error(Response.Status code, String message) {
        return Response.status(code).entity(message).build();
    }

    public static Response error(Response.Status code, Object body) {
        return Response.status(code).entity(body).build();
    }

    public static WebApplicationException throwError(Response.Status code, String message) {
        return new WebApplicationException(error(code, message));
    }

    public static WebApplicationException throwError(Response.Status code, Object body) {
        return new WebApplicationException(error(code, body));
    }

    public static Response ok() {
        return Response.ok().build();
    }
}
