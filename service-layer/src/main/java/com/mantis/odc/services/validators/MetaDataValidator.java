/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.validators;

import com.mantis.odc.models.*;
import com.mantis.odc.services.validators.base.BaseValidator;
import lombok.SneakyThrows;
import lombok.val;

import java.sql.DatabaseMetaData;
import java.sql.JDBCType;
import java.sql.SQLException;
import java.util.Optional;
import java.util.stream.Stream;

import static com.mantis.odc.models.DataType.*;
import static com.mantis.odc.models.EnumerationType.*;
import static com.mantis.odc.services.validators.ValidationResult.*;
import static java.text.MessageFormat.format;
import static java.util.stream.Collectors.joining;


/**
 * Validates a mapping against a given schema.
 */
public class MetaDataValidator extends BaseValidator<Mapping> {

    private final Driver driver;
    private final DatabaseMetaData metaData;
    private final MappingValidator mappingValidator;

    /**
     * @param driver           database driver
     * @param metaData         JDBC meta-data
     * @param mappingValidator mapping validator
     */
    public MetaDataValidator(Driver driver, DatabaseMetaData metaData, MappingValidator mappingValidator) {
        this.driver = driver;
        this.metaData = metaData;
        this.mappingValidator = mappingValidator;
    }

    /**
     * Validates a mapping against the database.
     *
     * @param mapping mapping
     * @return validation result
     */
    @Override
    @SneakyThrows
    public ValidationResult validate(Mapping mapping) {
        val builder = builder();
        val result = mappingValidator.validate(mapping);

        if (result.isSuccess()) {
            mapping.getTables().forEach(table -> builder.child(validateTable(table)));
            mapping.getJunctionTables().forEach(table -> builder.child(validateJunctionTable(table)));
            if (mapping.isSupportsAttachments()) builder.child(validateAttachmentTable(mapping.getAttachment()));
            if (mapping.isSupportsCheckpoints()) builder.child(validateCheckpointTable(mapping.getCheckpoint()));
            mapping.getEnumerations().forEach(enumerationMapping -> builder.child(validateEnumeration(enumerationMapping)));
        } else builder.child(result);

        return isValid("The database schema", builder);
    }

    /**
     * Validates the containment table against the database.
     *
     * @param table table mapping
     * @return validation result
     */
    private ValidationResult validateTable(TableMapping table) {
        val builder = builder();
        val source = table.getTableId().getSource();

        builder.child(checkTheTableExists(source));
        table.getFields().forEach(field -> builder.child(validateFieldMapping(source, field.getFieldId().getSource(), field.getType(),
                field.isRequired() || field.isPrimaryKey() || field.isForeignKey(), field.isOutput())));

        return isValid("Table " + source, builder);
    }

    /**
     * Validates the given junction table against the database.
     *
     * @param table junction table
     * @return validation result
     */
    private ValidationResult validateJunctionTable(JunctionTableMapping table) {
        val builder = builder();
        val source = table.getTableId().getSource();

        builder.child(checkTheTableExists(source));

        val fromTable = table.getFromTable();
        val toTable = table.getToTable();
        builder.child(validateFieldMapping(source, table.getFromKeyName(), fromTable.getPrimaryKey().getType(), true, true));
        builder.child(validateFieldMapping(source, table.getToKeyName(), toTable.getPrimaryKey().getType(), true, true));

        return isValid("Junction table " + source, builder);
    }

    /**
     * Validates an attachment table against the database.
     *
     * @param table attachment table
     * @return validation result
     */
    private ValidationResult validateAttachmentTable(AttachmentMapping table) {
        val builder = builder();
        val source = table.getTableName();

        builder.child(checkTheTableExists(source));
        table.getFields().stream()
                .filter(fieldSource -> !fieldSource.equals(table.getFileField())) // exclude data field
                .forEach(fieldSource -> builder.child(validateFieldMapping(source, fieldSource, STRING_TYPE, false, true)));

        builder.child(validateFieldMapping(source, table.getFileField(), ATTACHMENT_TYPE, true, true));

        return isValid("Attachment table " + source, builder);
    }

    /**
     * Validates a check point table against the database.
     *
     * @param table checkpoint table
     * @return validation result
     */
    private ValidationResult validateCheckpointTable(CheckpointMapping table) {
        val builder = builder();
        val source = table.getTableName();

        builder.child(checkTheTableExists(source));
        table.getFields().stream()
                .filter(fieldSource -> !fieldSource.equals(table.getDataField())) // exclude data field
                .forEach(fieldSource -> builder.child(validateFieldMapping(source, fieldSource, STRING_TYPE, false, true)));

        builder.child(validateFieldMapping(source, table.getDataField(), ATTACHMENT_TYPE, true, true));

        return isValid("Checkpoint table " + source, builder);
    }

    /**
     * Validates a field mapping against the database schema.
     *
     * @param tableSource table source name
     * @param fieldSource field source name
     * @param type        data type
     * @param required    required flag
     * @return validation result
     */
    private ValidationResult validateFieldMapping(String tableSource, String fieldSource, DataType type, Boolean required, Boolean output) {
        val builder = builder();

        try {
            val rs = metaData.getColumns(null, null, setCase(tableSource), setCase(fieldSource));
            if (!rs.next()) throw new SQLException();

            // check type information
            val columnType = rs.getInt(5); // DATA_TYPE
            val typeEnum = JDBCType.valueOf(columnType);
            if (type.isCoreType(typeEnum)) {
                builder.child(success(format("Field type {0} is a direct match for the JDBC type {1}.",
                        type.name(), typeEnum.name())));
            } else if (type.isSupportedType(typeEnum)) {
                builder.child(warning(format("Field type {0} can be cast from the JDBC type {1}.\n" +
                                "But this is not recommended as it could fail or corrupt the data.\n" +
                                "The core supported types are {2}.",
                        type.name(), typeEnum.name(), Stream.of(type.getCoreTypes())
                                .map(JDBCType::getName)
                                .collect(joining(", ")))));
            } else {
                builder.child(error(format("Field type {0} is not supported by the JDBC type {1}. The core supported types are {2}",
                        type.name(), typeEnum.name(), Stream.of(type.getCoreTypes())
                                .map(JDBCType::getName)
                                .collect(joining(", ")))));
            }

            // check not null
            val nullable = rs.getBoolean(11); // NULLABLE
            if (!required && output && !nullable)
                builder.child(error("The field is not required, however the field is not nullable in the database."));

            builder.child(success("Field name " + fieldSource + " exists."));
        } catch (SQLException e) {
            builder.child(error(format("No field {0} exists.", fieldSource)));
        }

        return isValid("Field Mapping " + fieldSource, builder);
    }

    /**
     * Sets the correct case for the given driver.
     *
     * @param source source to set case on
     * @return correct case for source
     */
    private String setCase(String source) {
        switch (driver.getSupportedCase()) {
            case UPPER:
                return source.toUpperCase();
            case LOWER:
                return source.toLowerCase();
            case UNCHANGED:
            default:
                return source;
        }
    }

    /**
     * Test the existence of the given table.
     *
     * @param source table source
     * @return validation result
     */
    private ValidationResult checkTheTableExists(String source) {
        try {
            val rs = metaData.getTables(null, null, setCase(source), null);
            if (!rs.next()) throw new SQLException();
            return success("Table " + source + " exists.");
        } catch (SQLException e) {
            return error("No table exists with source " + source);
        }
    }

    /**
     * Validates the given enumeration mapping.
     *
     * @param enumerationMapping enumeration mapping
     * @return validation result
     */
    private ValidationResult validateEnumeration(EnumerationMapping enumerationMapping) {
        val builder = builder();

        try {
            val preparedStatement = metaData.getConnection().prepareStatement(enumerationMapping.getSql());
            val rs = preparedStatement.executeQuery();
            val queryMetaData = rs.getMetaData();

            // check column count
            if (enumerationMapping.hasChildEnumeration() && queryMetaData.getColumnCount() < 3) {
                builder.child(error(format("The query returns {0} columns. This enumeration requires that 3 be returned. " +
                        "(value, description, child value)", queryMetaData.getColumnCount())));
            } else if (!enumerationMapping.hasChildEnumeration() && queryMetaData.getColumnCount() < 2) {
                builder.child(error(format("The query returns {0} columns. This enumeration requires that 2 be returned. " +
                        "(value, description)", queryMetaData.getColumnCount())));
            } else { // check return types
                val valueType = JDBCType.valueOf(queryMetaData.getColumnType(1));
                val descriptionType = JDBCType.valueOf(queryMetaData.getColumnType(2));
                checkEnumTypes(Optional.empty(), enumerationMapping, builder, valueType);

                // check description
                if (!STRING_TYPE.isSupportedType(descriptionType)) {
                    builder.child(error(getNonSupportedTypeErrorMessage("description", descriptionType, STRING_TYPE)));
                }

                // check child types
                if (enumerationMapping.hasChildEnumeration()) {
                    JDBCType childValue = JDBCType.valueOf(queryMetaData.getColumnType(3));
                    checkEnumTypes(Optional.of("child"), enumerationMapping, builder, childValue);
                }
            }
        } catch (Exception e) {
            builder.child(error("Failed to execute enumeration query: " + e.getMessage()));
        }

        return isValid("Enumeration Mapping " + enumerationMapping.getName(), builder);
    }

    /**
     * @param prefix             name prefix
     * @param enumerationMapping enumeration mapping
     * @param builder            result builder
     * @param valueType          value type
     */
    private void checkEnumTypes(Optional<String> prefix, EnumerationMapping enumerationMapping, ValidationResultBuilder builder, JDBCType valueType) {
        // check value type
        if (enumerationMapping.getType() == StringEnumeration && !STRING_TYPE.isSupportedType(valueType)) {
            builder.child(error(getNonSupportedTypeErrorMessage(prefix.map(p -> p + " value").orElse("value"), valueType, STRING_TYPE)));
        } else if (enumerationMapping.getType() == NumberEnumeration && !DECIMAL_TYPE.isSupportedType(valueType)) {
            builder.child(error(getNonSupportedTypeErrorMessage(prefix.map(p -> p + " value").orElse("value"), valueType, DECIMAL_TYPE)));
        } else if (enumerationMapping.getType() == DateEnumeration && !DATE_TYPE.isSupportedType(valueType)) {
            builder.child(error(getNonSupportedTypeErrorMessage(prefix.map(p -> p + " value").orElse("value"), valueType, DATE_TYPE)));
        } else if (enumerationMapping.getType() == DateTimeEnumeration && !DATE_TIME_TYPE.isSupportedType(valueType)) {
            builder.child(error(getNonSupportedTypeErrorMessage(prefix.map(p -> p + " value").orElse("value"), valueType, DATE_TIME_TYPE)));
        } else if (enumerationMapping.getType() == TimeEnumeration && !TIME_TYPE.isSupportedType(valueType)) {
            builder.child(error(getNonSupportedTypeErrorMessage(prefix.map(p -> p + " value").orElse("value"), valueType, TIME_TYPE)));
        } else if (enumerationMapping.getType() == BooleanEnumeration && !BOOLEAN_TYPE.isSupportedType(valueType)) {
            builder.child(error(getNonSupportedTypeErrorMessage(prefix.map(p -> p + " value").orElse("value"), valueType, BOOLEAN_TYPE)));
        }
    }

    /**
     * @param columnName column name
     * @param valueType  value type
     * @param type       data type
     * @return error message
     */
    private String getNonSupportedTypeErrorMessage(String columnName, JDBCType valueType, DataType type) {
        return format("The {0} column is of type {1} which is not supported, supported types are {2}",
                columnName, valueType, Stream.of(type.getSupportedTypes())
                        .map(JDBCType::getName)
                        .collect(joining(", ")));
    }
}
