package com.mantis.odc.services.jaxrs.resources;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.InputStream;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;

/**
 * This resource provides access to the support resources used by the connector.
 * These are static files that detail how its components work.
 */
@Slf4j
@Singleton
@Path("/support")
public class SupportResource {

    @Getter(lazy = true)
    private final InputStream mappingXSD = getClass().getResourceAsStream("/Mapping.xsd");

    @Getter(lazy = true)
    private final InputStream swaggerJSON = getClass().getResourceAsStream("/swagger.json");

    @Getter(lazy = true)
    private final InputStream swaggerYAML = getClass().getResourceAsStream("/swagger.yaml");

    @Inject
    public SupportResource() {
    }

    @GET
    @Path("/mapping.xml")
    @Produces(APPLICATION_XML)
    public InputStream getMappingXSDFile() {
        return getMappingXSD();
    }

    @GET
    @Path("/swagger.json")
    @Produces(APPLICATION_JSON)
    public InputStream getSwaggerJSONFile() {
        return getSwaggerJSON();
    }

    @GET
    @Path("/swagger.yaml")
    @Produces("application/x-yaml")
    public InputStream getSwaggerYAMLFile() {
        return getSwaggerYAML();
    }
}
