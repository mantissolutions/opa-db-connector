/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters.exceptions;

/**
 * Created on a missing global row on a saveButton request.
 */
public class MissingGlobalRowException extends Exception {
}
