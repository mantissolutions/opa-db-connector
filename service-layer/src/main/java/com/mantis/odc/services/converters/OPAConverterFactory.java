/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters;

import com.mantis.odc.models.OPAVersion;
import com.mantis.odc.services.converters._12_1.OPA121Converter;
import com.mantis.odc.services.converters._12_2.OPA122Converter;
import com.mantis.odc.services.converters._12_2_2.OPA1222Converter;
import com.mantis.odc.services.converters._12_2_5.OPA1225Converter;
import com.mantis.odc.services.converters._12_2_9.OPA1229Converter;
import com.mantis.odc.services.converters.base.OPAConverter;

import javax.inject.Singleton;

/**
 * Factory for OPA converters.
 */
@Singleton
public class OPAConverterFactory {

    /**
     * @param version OPA version
     * @return opa converter
     */
    public static OPAConverter getConverter(OPAVersion version) {
        switch (version) {
            case OPA_12_1:
                return new OPA121Converter();
            case OPA_12_2:
                return new OPA122Converter();
            case OPA_12_2_2:
                return new OPA1222Converter();
            case OPA_12_2_5:
                return new OPA1225Converter();
            case OPA_12_2_9:
            default:
                return new OPA1229Converter();
        }
    }
}
