/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs;

import com.google.common.collect.Sets;
import com.mantis.odc.services.di.ServiceSubComponent;
import com.mantis.odc.services.jaxrs.providers.GsonProvider;
import com.mantis.odc.services.jaxrs.providers.SOAPProvider;
import com.mantis.odc.services.jaxrs.providers.ServiceConverterProvider;
import com.mantis.odc.services.jaxrs.resources.*;
import com.mantis.odc.xml.MappingXMLTransport;
import lombok.val;
import org.secnod.shiro.jersey.AuthInjectionBinder;
import org.secnod.shiro.jersey.AuthorizationFilterFeature;
import org.secnod.shiro.jersey.SubjectFactory;

import javax.inject.Singleton;
import javax.servlet.ServletContext;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import java.util.Set;

/**
 * Main service application. New Resources need to added here.
 */
@Singleton
public class ServiceApplication extends Application {

    /**
     * Context path to the service injector.
     */
    public static final String SERVICE_INJECTOR = "service-injector";

    // security
    private final AuthorizationFilterFeature authorizationFilterFeature = new AuthorizationFilterFeature();
    private final SubjectFactory subjectFactory = new SubjectFactory();
    private final AuthInjectionBinder authInjectionBinder = new AuthInjectionBinder();

    // providers
    private final MappingXMLTransport mappingXMLTransport;
    private final SOAPProvider soapProvider;
    private final ServiceConverterProvider serviceConverterProvider;
    private final GsonProvider gsonProvider;

    // resources
    private final ServiceResource serviceResource;
    private final ServiceManagementResource serviceManagementResource;
    private final ServiceDatasourceManagementResource serviceDatasourceManagementResource;
    private final MappingsResource mappingsResource;
    private final DatasourceMappingsResource datasourceResource;
    private final SupportResource supportResource;

    public ServiceApplication(@Context ServletContext ctx) {
        val injector = (ServiceSubComponent) ctx.getAttribute(SERVICE_INJECTOR);
        mappingXMLTransport = injector.getMappingXMLTransport();
        soapProvider = injector.getSOAPProvider();
        serviceConverterProvider = injector.getServiceConverterProvider();
        gsonProvider = injector.getGsonProvider();
        serviceResource = injector.getActionResource();
        serviceManagementResource = injector.getServiceManagementResource();
        serviceDatasourceManagementResource = injector.getServiceDatasourceManagementResource();
        mappingsResource = injector.getMappingResource();
        datasourceResource = injector.getDatasourceMappingsResource();
        supportResource = injector.getSupportResource();
    }

    @Override
    public Set<Object> getSingletons() {
        return Sets.newHashSet(
                authorizationFilterFeature, subjectFactory, authInjectionBinder,
                mappingXMLTransport, soapProvider, serviceConverterProvider,
                gsonProvider, serviceResource, serviceManagementResource,
                serviceDatasourceManagementResource, mappingsResource, datasourceResource, supportResource);
    }
}
