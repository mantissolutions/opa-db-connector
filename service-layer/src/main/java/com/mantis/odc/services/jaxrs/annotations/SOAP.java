/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.annotations;


import javax.xml.soap.SOAPConstants;
import java.lang.annotation.*;

/**
 * Custom annotation to denote that SOAP is used for a given REST endpoint.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SOAP {

    /**
     * Version of SOAP.
     */
    String version() default SOAPConstants.SOAP_1_2_PROTOCOL;
}
