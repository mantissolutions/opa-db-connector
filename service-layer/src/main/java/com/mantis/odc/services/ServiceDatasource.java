/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.github.davidmoten.rx.jdbc.ConnectionProvider;
import com.github.davidmoten.rx.jdbc.Database;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.mantis.odc.models.DatasourceMapping;
import com.mantis.odc.utilities.TimeUtils;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.val;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static java.text.MessageFormat.format;
import static java.util.Collections.unmodifiableList;

/**
 * A Service data source wraps a collection of connection pools.
 * These pools are related to one data source template and contain one per unique username/password combination.
 */
@Data
public class ServiceDatasource {

    /**
     * Name of the service data source.
     */
    private final String name;

    /**
     * Data source mapping.
     */
    private final DatasourceMapping mapping;

    /**
     * Global metrics registry.
     */
    private final MetricRegistry metricRegistry;

    /**
     * Cache of active connection pools indexed by a hash of the username/password.
     */
    private final Cache<ServiceUser, DatabaseAndPool> connectionPools;

    public ServiceDatasource(DatasourceMapping mapping, MetricRegistry metricRegistry) {
        name = mapping.getName().replace(" ", "_").toLowerCase();
        this.mapping = mapping;
        this.metricRegistry = metricRegistry;
        this.connectionPools = CacheBuilder.newBuilder()
                .removalListener(notification -> {
                    DatabaseAndPool pool = (DatabaseAndPool) notification.getValue();
                    if (pool != null) pool.close();
                })
                .build();
    }

    /**
     * Creates a connection to a service data source with a given set of credentials.
     * Connections are cached for a period of 15 minuets. If a connection is invalid an exception
     * is thrown.
     *
     * @param user     username
     * @param password password
     * @return active database
     * @throws ExecutionException on failed connection
     */
    public Database connect(String user, String password) throws ExecutionException {
        int hash = (user + password).hashCode();
        return connectionPools.get(new ServiceUser(user, hash),
                () -> new DatabaseAndPool(user, password))
                .getDatabase();
    }

    /**
     * Destroys the cached connection pools.
     */
    public void close() {
        connectionPools.invalidateAll();
    }

    /**
     * @return list of active pools
     */
    public List<DatabaseAndPool> getActivePools() {
        return unmodifiableList(new ArrayList<>(connectionPools.asMap().values()));
    }

    /**
     * @return list of active users
     */
    public List<ServiceUser> getActiveUsers() {
        val users = unmodifiableList(new ArrayList<>(connectionPools.asMap().keySet()));
        users.sort(Comparator.comparing(ServiceUser::getUser));
        return users;
    }

    /**
     * Current user connected to the data source.
     */
    @Value
    public class ServiceUser {

        /**
         * Username.
         */
        private final String user;

        /**
         * Hash or username and password.
         */
        private final Integer hash;
    }

    /**
     * Groups an RX database and connection pool.
     * Because once a pool is handed to RX it cannot be returned.
     * <p>
     * This also manages access to all the metrics for the given pool.
     */
    @Value
    public class DatabaseAndPool {

        /**
         * Database username.
         */
        private final String username;

        /**
         * Name of the pool.
         */
        private final String poolName;

        /**
         * Time the pool was created.
         */
        private final long createTime;

        /**
         * Connection pool.
         */
        private final DataSource dataSource;

        private DatabaseAndPool(String user, String password) {
            username = user;
            poolName = format("{0}_{1}", user, mapping.getName().replace(" ", "_"));
            createTime = System.nanoTime();
            dataSource = mapping.createInstance(poolName, user, password, metricRegistry);
        }

        /**
         * @return Time the pool has been running (H:MM:SS)
         */
        public String getUpTime() {
            return TimeUtils.formatDuration(System.nanoTime() - createTime);
        }

        /**
         * A Timer instance collecting how long requesting threads to getConnection() are waiting for a
         * connection (or timeout exception) from the pool.
         *
         * @return how long requesting threads to getConnection() are waiting for a connection
         */
        public Timer getPoolWaitTime() {
            return metricRegistry.getTimers().get(format("{0}.pool.Wait", poolName));
        }

        /**
         * A Histogram instance collecting how long each connection is used before being returned to the pool.
         * This is the "out of pool" or "in-use" time.
         *
         * @return how long each connection is used before being returned to the pool
         */
        public Histogram getPoolUsage() {
            return metricRegistry.getHistograms().get(format("{0}.pool.Usage", poolName));
        }

        /**
         * A CachedGauge, refreshed on demand at 1 second resolution, indicating the
         * total number of connections in the pool.
         *
         * @return total number of connections in the pool
         */
        public Gauge<Integer> getTotalConnections() {
            return metricRegistry.getGauges().get(format("{0}.pool.TotalConnections", poolName));
        }

        /**
         * A CachedGauge, refreshed on demand at 1 second resolution, indicating the
         * number of idle connections in the pool.
         *
         * @return number of idle connections in the pool
         */
        public Gauge<Integer> getIdleConnections() {
            return metricRegistry.getGauges().get(format("{0}.pool.IdleConnections", poolName));
        }

        /**
         * A CachedGauge, refreshed on demand at 1 second resolution, indicating the
         * number of active (in-use) connections in the pool.
         *
         * @return number of active (in-use) connections in the pool
         */
        public Gauge<Integer> getActiveConnections() {
            return metricRegistry.getGauges().get(format("{0}.pool.ActiveConnections", poolName));
        }

        /**
         * A CachedGauge, refreshed on demand at 1 second resolution, indicating the
         * number of threads awaiting connections from the pool.
         *
         * @return number of threads awaiting connections from the pool
         */
        public Gauge<Integer> getPendingConnections() {
            return metricRegistry.getGauges().get(format("{0}.pool.PendingConnections", poolName));
        }

        void close() {
            ((HikariDataSource) dataSource).close();
        }

        /**
         * Creates a new rx database for operations against a checked out connection.
         *
         * @return rx database
         */
        @SneakyThrows
        public Database getDatabase() {
            return Database.builder()
                    .connectionProvider(new ConnectionProvider() {
                        // Connection for the duration of the database operation
                        final Connection conn = dataSource.getConnection();

                        @Override
                        public Connection get() {
                            return conn;
                        }

                        @SneakyThrows
                        @Override
                        public void close() {
                            conn.close();
                        }
                    }).build();
        }
    }
}
