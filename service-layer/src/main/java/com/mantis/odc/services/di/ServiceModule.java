/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.di;

import com.codahale.metrics.MetricRegistry;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

/**
 * Service module for providing service related objects.
 */
@Module
public class ServiceModule {

    /**
     * @return global metrics registry
     */
    @Provides
    @Singleton
    public static MetricRegistry createMetricRegistry() {
        return new MetricRegistry();
    }
}
