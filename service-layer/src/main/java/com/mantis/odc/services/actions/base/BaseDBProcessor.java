/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions.base;

import com.github.davidmoten.rx.jdbc.Database;
import com.mantis.odc.models.EnumerationMapping;
import com.mantis.odc.services.models.CheckpointModel;
import com.mantis.odc.services.models.DatabaseEnumeration;
import com.mantis.odc.services.models.LoadModel;
import com.mantis.odc.services.models.SaveModel;
import com.mantis.odc.services.sql.ScriptRunner;
import com.mantis.odc.services.sql.adaptors.base.SQLAdaptor;
import com.mantis.odc.services.sql.converters.base.SQLTypeConverter;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.sql.SQLException;
import java.util.List;
import java.util.OptionalInt;

/**
 * Base class for all processes backed by database actions.
 */
@ToString
@EqualsAndHashCode
public abstract class BaseDBProcessor implements ActionProcessor {

    protected final Database db;
    protected final ScriptRunner scriptRunner;
    protected final SQLAdaptor adaptor;
    protected final SQLTypeConverter typeConverter;

    protected BaseDBProcessor(Database db, SQLAdaptor adaptor, SQLTypeConverter typeConverter) {
        this.db = db;
        this.adaptor = adaptor;
        this.typeConverter = typeConverter;
        scriptRunner = new ScriptRunner(db.getConnectionProvider().get());
    }

    @Override
    public List<DatabaseEnumeration> enumerations(List<EnumerationMapping> enumerationMappings) {
        throw new UnsupportedOperationException();
    }

    @Override
    public LoadModel load(LoadModel loadModel, boolean fromGobal, OptionalInt limit) throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public SaveModel save(SaveModel save) throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public CheckpointModel setCheckpoint(CheckpointModel actionModel) {
        throw new UnsupportedOperationException();
    }

    @Override
    public CheckpointModel getCheckpoint(CheckpointModel actionModel) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void close() {
        db.close();
    }
}
