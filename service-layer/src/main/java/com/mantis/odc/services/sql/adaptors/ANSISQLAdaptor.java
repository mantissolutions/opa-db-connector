/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.services.sql.adaptors;

import com.mantis.odc.models.Driver;
import com.mantis.odc.services.models.*;
import com.mantis.odc.services.models.base.ActionModel;
import com.mantis.odc.services.models.base.DatabaseTable;
import com.mantis.odc.services.sql.adaptors.base.SQLAdaptor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.text.MessageFormat.format;
import static java.util.Collections.EMPTY_LIST;
import static java.util.stream.Collectors.joining;

/**
 * ANSI SQL adaptor.
 */
@Data
@Slf4j
public class ANSISQLAdaptor implements SQLAdaptor {

    /**
     * Database driver.
     */
    private final Driver driver;

    @Override
    public String createContainmentSelect(LoadModel actionModel, ContainmentTable table, List<BindConditionGroup> binds, boolean useNamedBinds, boolean fromGlobal) {
        return join("SELECT ", selectFields(table.getAlias(), table.getSelectFields(actionModel))
                , getContainmentFromClause(actionModel, table, useNamedBinds, fromGlobal)
                , (!actionModel.isDisconnected(table)) ? bindConditions(binds, useNamedBinds) : "");
    }

    /**
     * Returns the from clause joining to the root table in the relationship.
     *
     * @param actionModel   action model
     * @param table         containment table
     * @param useNamedBinds flag to indicate that named binds are to be used
     * @param fromGlobal    flag to indicate that the global table primary key condition be added
     * @return from clause with joins to root table
     */
    private String getContainmentFromClause(ActionModel actionModel, ContainmentTable table, boolean useNamedBinds, boolean fromGlobal) {
        val root = table.getRoot(actionModel);
        return join(" FROM ", table.getSource(), " ", table.getAlias(),
                innerJoinsToRoot(actionModel, table),
                (fromGlobal && !actionModel.isDisconnected(table)) ?
                        join(" WHERE ", root.getAlias(), ".", root.getPrimaryKey().getSource(), " = ", getBindString(useNamedBinds, root.getPrimaryKey()))
                        : " WHERE 1 = 1"
                // if the table is disconnected, create its non-containment condition
                , (actionModel.isDisconnected(table)) ?
                        actionModel.getRelatedJunctionTables(table).stream()
                                .map(jt ->
                                        join(" AND ", table.getAlias(), ".", root.getPrimaryKey().getSource(), " IN ("
                                                , createNonContainmentSelect((LoadModel) actionModel, jt, useNamedBinds, false, false, true)
                                                , ")"))
                                .collect(joining())
                        : "");
    }

    /**
     * Returns the binding string.
     *
     * @param useNamedBinds flag to indicate that named binds are to be used
     * @param source        named source to bind
     * @return bind string
     */
    private String getBindString(boolean useNamedBinds, String source) {
        return useNamedBinds ? ":" + source : "?";
    }

    /**
     * Returns the binding string.
     *
     * @param useNamedBinds flag to indicate that named binds are to be used
     * @param field         field to bind
     * @return bind string
     */
    private String getBindString(boolean useNamedBinds, DatabaseField field) {
        return useNamedBinds ? ":" + field.getSource() : "?";
    }

    @Override
    public String createNonContainmentSelect(LoadModel actionModel, JunctionTable table, boolean useNamedBinds, boolean fromGlobal, boolean addFromKey, boolean addToKey) {
        val conditions = getJunctionCondition(actionModel, table, true, true, useNamedBinds, fromGlobal);
        val selectFields = table.getSelectFields(actionModel);
        if (!addFromKey) selectFields.remove(0);
        if (!addToKey) selectFields.remove(1);
        return join(
                "SELECT ", selectFields(table.getAlias(), selectFields),
                " FROM ", table.getSource(), " ", table.getAlias(),
                " WHERE ", conditions.get(0),
                (conditions.size() == 2) ? " AND " + conditions.get(1) : "");
    }

    /**
     * Creates the junction conditions.
     *
     * @param actionModel   action model
     * @param table         junction table
     * @param useAlias      flag to add alias into the condition
     * @param addConditions flag to add load conditions to the inner select
     * @param useNamedBinds flag to indicate that named binds are to be used
     * @param fromGlobal    flag to indicate the primary key be excluded from the condition
     * @return junction table conditions
     */
    private List<String> getJunctionCondition(ActionModel actionModel, JunctionTable table, boolean useAlias, boolean addConditions, boolean useNamedBinds, boolean fromGlobal) {
        return table.getTargetTableMap(actionModel, fromGlobal).entrySet().stream()
                .map(entry -> join(useAlias ? table.getAlias() + "." : "", entry.getKey().getSource(), " IN ",
                        "(", createSelectPrimaryKeyToRoot(actionModel, entry.getValue(),
                                addConditions ? ((LoadModel) actionModel).getConditionsForTable(entry.getValue()) : EMPTY_LIST, useNamedBinds, fromGlobal), ")"))
                .collect(Collectors.toList());
    }

    /**
     * Creates a primary key selection, joined to the root table in the containment.
     *
     * @param actionModel   action model
     * @param table         table
     * @param binds         conditions
     * @param useNamedBinds flag to indicate that named binds are to be used
     * @param fromGlobal    flag to indicate the primary key be excluded from the condition
     * @return primary key select join to root
     */
    private String createSelectPrimaryKeyToRoot(ActionModel actionModel, ContainmentTable table, List<BindConditionGroup> binds, boolean useNamedBinds, boolean fromGlobal) {
        return join("SELECT ", table.getAlias(), ".", table.getPrimaryKey().getSource(),
                getContainmentFromClause(actionModel, table, useNamedBinds, fromGlobal),
                bindConditions(binds, useNamedBinds));
    }

    /**
     * Creates inner joins to the root table.
     *
     * @param actionModel action model
     * @param table       table
     * @return inner join conditions.
     */
    private String innerJoinsToRoot(ActionModel actionModel, ContainmentTable table) {
        ContainmentTable parent = table.getParent(actionModel).orElse(null);
        val sb = new StringBuilder();

        // climb up to root
        while (parent != null) {

            // create a join from the child table to the parent table
            sb.append(joinCondition(table.getAlias(), table.getForeignKey().get().getSource(), parent.getSource(), parent.getAlias(), parent.getPrimaryKey().getSource()));

            // get the next parent
            table = parent;
            parent = table.getParent(actionModel).orElse(null);
        }
        return sb.toString();
    }

    /**
     * Creates a join.
     *
     * @param t1Alias table 1 alias
     * @param t1Key   table 1 key
     * @param t2Name  table 2 name
     * @param t2Alias table 2 alias
     * @param t2Key   table 2 key
     * @return join
     */
    private String joinCondition(String t1Alias, String t1Key, String t2Name, String t2Alias, String t2Key) {
        return join(" ", "INNER", " JOIN ", t2Name, " ", t2Alias, " ON (", t1Alias, ".", t1Key, " = ", t2Alias, ".", t2Key, ")");
    }

    /**
     * Returns all the bind conditions.
     *
     * @param binds         binds
     * @param useNamedBinds flag to indicate that named binds are to be used
     * @return conditions
     */
    private String bindConditions(List<BindConditionGroup> binds, boolean useNamedBinds) {
        return binds.stream()
                // for each group
                .map(group -> {
                    // get the bind conditions
                    val cond = group.getConditions().stream()
                            .map(bind ->
                                    join(join((group.requiresOperator(bind) ? " " + bind.getJoin().getSqlOperator() + " " : "")
                                            , bind.getAlias(), ".", bind.getSource(), " ", bind.getOperator().getSqlOperator()),
                                            // check a bind variable is required
                                            (bind.hasValue()) ?
                                                    // check if multiple values are required
                                                    " " + ((bind.isSingleValue()) ?
                                                            getBindString(useNamedBinds, bind.getSource()) :
                                                            join("(", IntStream.range(0, bind.getValues().size())
                                                                    .mapToObj(i -> getBindString(useNamedBinds, bind.getSource() + "_" + i))
                                                                    .collect(joining(", ")), ")")) : ""))
                            .collect(joining());
                    // return the group
                    return join(group.isRequiresBrackets() ? join(" ", group.getJoin().getSqlOperator(), " (", cond, ")") : cond
                            , bindConditions(group.getChildren(), useNamedBinds));
                })
                .collect(joining());
    }

    /**
     * Prints out a select list.
     *
     * @param alias  table alias
     * @param fields fields to printErrors.
     * @return select list
     */
    private String selectFields(String alias, List<DatabaseField> fields) {
        return fields.stream()
                .map(f -> format("{0}.{1} AS \"{2}\"", alias, f.getSource(), f.getOpaName()))
                .collect(joining(", "));
    }

    @Override
    public String createDelete(ActionModel actionModel, DatabaseTable table, boolean useNamedBinds) {
        if (table instanceof ContainmentTable && ((ContainmentTable) table).isInferred()) {
            val containmentTable = (ContainmentTable) table;
            val pk = table.getPrimaryKey();
            return join(
                    "DELETE FROM ", containmentTable.getSource(),
                    " WHERE ", pk.getSource(), " IN ",
                    "(", createSelectPrimaryKeyToRoot(actionModel, containmentTable, EMPTY_LIST, useNamedBinds, true), ")");
        } else if (table instanceof JunctionTable) {
            val junctionTable = (JunctionTable) table;
            val conditions = getJunctionCondition(actionModel, junctionTable, false, false, useNamedBinds, true);
            return join(
                    "DELETE FROM ", junctionTable.getSource(),
                    " WHERE ", conditions.get(0),
                    (conditions.size() == 2) ? " AND " + conditions.get(1) : "");
        }

        return join(
                "DELETE FROM ", table.getSource(),
                " WHERE ", table.getPrimaryKey().getSource(), " = ", getBindString(useNamedBinds, table.getPrimaryKey()));
    }

    @Override
    public String createInsert(DatabaseTable table, List<DatabaseField> fields, boolean useNamedBinds) {
        return join("INSERT INTO ", table.getSource(), "(",
                fields.stream()
                        .map(DatabaseField::getSource)
                        .collect(joining(", ")),
                ") VALUES (",
                fields.stream()
                        .map(f -> getBindString(useNamedBinds, f))
                        .collect(joining(", ")),
                ")");
    }

    @Override
    public String createUpdate(DatabaseTable table, List<DatabaseField> fields, boolean useNamedBinds) {
        return join(
                "UPDATE ", table.getSource(),
                " SET ", fields.stream()
                        .filter(f -> !(f instanceof PrimaryKeyField || f instanceof ForeignKeyField))
                        .map(f -> f.getSource() + " = " + getBindString(useNamedBinds, f))
                        .collect(joining(", ")),
                " WHERE ", table.getPrimaryKey().getSource(), " = ", getBindString(useNamedBinds, table.getPrimaryKey()));
    }

    @Override
    public String createSelect(DatabaseTable table, List<DatabaseField> fields, boolean useNamedBinds) {
        return join(
                "SELECT ", selectFields(table.getAlias(), fields),
                " FROM ", table.getSource(), " ", table.getAlias(),
                " WHERE ", table.getAlias(), ".", table.getPrimaryKey().getSource(), " = ", getBindString(useNamedBinds, table.getPrimaryKey()));
    }

    @Override
    public String createSequenceSelect(String sequence) {
        return join("SELECT ", sequence, ((!driver.getSystemTable().isEmpty()) ? " FROM " + driver.getSystemTable() : ""));
    }

    /**
     * Constructs a complete string from string parts.
     *
     * @param parts string parts.
     * @return string
     */
    private String join(String... parts) {
        return Stream.of(parts).collect(joining());
    }
}
