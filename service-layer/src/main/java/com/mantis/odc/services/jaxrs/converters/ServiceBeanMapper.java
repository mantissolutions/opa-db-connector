/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.converters;

import com.codahale.metrics.Timer;
import com.mantis.odc.services.Service;
import com.mantis.odc.services.ServiceDatasource;
import com.mantis.odc.services.jaxrs.models.ServiceBean;
import com.mantis.odc.services.jaxrs.models.ServiceDatasourceBean;
import com.mantis.odc.services.jaxrs.models.ServiceDatasourcePoolBean;
import com.mantis.odc.services.jaxrs.models.metrics.TimerBean;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.stream.Collectors;

/**
 * Maps domain types to JSON bean transport types.
 */
@Singleton
public class ServiceBeanMapper {

    @Inject
    public ServiceBeanMapper() {
    }

    /**
     * Creates the service bean.
     *
     * @param service service
     * @return bean
     */
    public ServiceBean serviceToBean(Service service) {
        return ServiceBean.builder()
                .name(service.getName())
                .mappingName(service.getMapping().getName())
                .datasource(serviceDatasourceToBean(service.getDatasource()))
                .build();
    }

    /**
     * Creates a service data source bean.
     *
     * @param serviceDatasource service data source
     * @return bean
     */
    public ServiceDatasourceBean serviceDatasourceToBean(ServiceDatasource serviceDatasource) {
        return ServiceDatasourceBean.builder()
                .name(serviceDatasource.getMapping().getName())
                .url(serviceDatasource.getMapping().getJdbc())
                .driverType(serviceDatasource.getMapping().getDriver())
                .pools(serviceDatasource.getActivePools().stream()
                        .map(this::databaseAndPoolToServiceDatasourcePool)
                        .collect(Collectors.toList()))
                .build();
    }

    /**
     * Creates a service data source pool bean.
     *
     * @param databaseAndPool database pool
     * @return bean
     */
    private ServiceDatasourcePoolBean databaseAndPoolToServiceDatasourcePool(ServiceDatasource.DatabaseAndPool databaseAndPool) {
        return ServiceDatasourcePoolBean.builder()
                .username(databaseAndPool.getUsername())
                .poolName(databaseAndPool.getPoolName())
                .upTime(databaseAndPool.getUpTime())
                .poolWaitTime(createTimerBean(databaseAndPool.getPoolWaitTime()))
                .poolUsage(databaseAndPool.getPoolUsage().getCount())
                .totalConnections(databaseAndPool.getTotalConnections().getValue())
                .IdleConnections(databaseAndPool.getIdleConnections().getValue())
                .activeConnections(databaseAndPool.getActiveConnections().getValue())
                .pendingConnections(databaseAndPool.getPendingConnections().getValue())
                .build();
    }

    /**
     * Creates a timer bean.
     *
     * @param timer timer
     * @return bean
     */
    private TimerBean createTimerBean(Timer timer) {
        return TimerBean.builder()
                .count(timer.getCount())
                .fifteenMinuteRate(timer.getFifteenMinuteRate())
                .fiveMinuteRate(timer.getFiveMinuteRate())
                .meanRate(timer.getMeanRate())
                .oneMinuteRate(timer.getOneMinuteRate())
                .build();
    }
}
