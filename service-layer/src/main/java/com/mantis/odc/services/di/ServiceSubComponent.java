/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.di;

import com.mantis.odc.di.SharedModule;
import com.mantis.odc.di.SharedSubComponent;
import com.mantis.odc.services.OperationStatisticsFilter;
import com.mantis.odc.services.jaxrs.providers.GsonProvider;
import com.mantis.odc.services.jaxrs.providers.SOAPProvider;
import com.mantis.odc.services.jaxrs.providers.ServiceConverterProvider;
import com.mantis.odc.services.jaxrs.resources.*;
import com.mantis.odc.services.storage.DatasourceStorage;
import com.mantis.odc.services.storage.OperationStorage;
import com.mantis.odc.services.storage.ServiceStorage;
import com.mantis.odc.xml.MappingXMLTransport;
import dagger.Subcomponent;

/**
 * Service Sub component
 */
@Subcomponent(modules = {SharedModule.class, ServiceModule.class})
public interface ServiceSubComponent {

    /**
     * @return mapping XML transport
     */
    MappingXMLTransport getMappingXMLTransport();

    /**
     * @return SOAP provider
     */
    SOAPProvider getSOAPProvider();

    /**
     * @return service converter provider for jersey
     */
    ServiceConverterProvider getServiceConverterProvider();

    /**
     * @return service action resource
     */
    ServiceResource getActionResource();

    /**
     * @return mapping resource
     */
    MappingsResource getMappingResource();

    /**
     * @return datasource mapping resource
     */
    DatasourceMappingsResource getDatasourceMappingsResource();

    /**
     * @return gson provider
     */
    GsonProvider getGsonProvider();

    /**
     * @return service management resource
     */
    ServiceManagementResource getServiceManagementResource();

    /**
     * @return service datasource management resource
     */
    ServiceDatasourceManagementResource getServiceDatasourceManagementResource();

    /**
     * @return support resource
     */
    SupportResource getSupportResource();

    /**
     * @return operation stats filter
     */
    OperationStatisticsFilter getOperationStatisticsFilter();

    /**
     * @return datasource storage
     */
    DatasourceStorage getDatasourceStorage();

    /**
     * @return service storage
     */
    ServiceStorage getServiceStorage();

    /**
     * @return operation storage
     */
    OperationStorage getOperationStorage();

    // SUB-COMPONENTS //

    /**
     * @return shared sub component
     */
    SharedSubComponent getSharedSubComponent();
}
