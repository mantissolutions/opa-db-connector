/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters.base;

import com.mantis.odc.models.Mapping;
import com.mantis.odc.services.models.*;

import java.util.List;

/**
 * Contract for converting from a given OPA JAXB version to the ODC domain models.
 *
 * @param <CHECK_ALIVE>         check alive response JAXB type
 * @param <META_DATA>           get metadata response JAXB type
 * @param <LOAD_REQ>            load request JAXB type
 * @param <LOAD_RESP>           load response JAXB type
 * @param <SAVE_REQ>            saveButton request JAXB type
 * @param <SAVE_RESP>           saveButton response JAXB type
 * @param <GET_CHECKPOINT_REQ>  get checkpoint request JAXB type
 * @param <GET_CHECKPOINT_RESP> get checkpoint response JAXB type
 * @param <SET_CHECKPOINT_REQ>  set checkpoint request JAXB type
 * @param <SET_CHECKPOINT_RESP> set checkpoint response JAXB type
 * @param <EXECUTE_QUERY_REQ>   execute query request JAXB type
 * @param <EXECUTE_QUERY_RESP>  execute query response JAXB type
 */
public interface OPAConverter<
        CHECK_ALIVE,
        META_DATA,
        LOAD_REQ, LOAD_RESP,
        SAVE_REQ, SAVE_RESP,
        GET_CHECKPOINT_REQ, GET_CHECKPOINT_RESP,
        SET_CHECKPOINT_REQ, SET_CHECKPOINT_RESP,
        EXECUTE_QUERY_REQ, EXECUTE_QUERY_RESP,
        FAULT> {

    /**
     * @return check alive message
     */
    CHECK_ALIVE getCheckAlive();

    /**
     * @param e exception
     * @return fault message
     */
    FAULT getFault(Exception e);

    /**
     * Returns the metadata.
     *
     * @param mapping      current mapping
     * @param enumerations list of database enumerations
     * @return meta data
     */
    META_DATA getMetaData(Mapping mapping, List<DatabaseEnumeration> enumerations);

    /**
     * Converts from the load request to a load model.
     *
     * @param mapping current mapping
     * @param req     Load request
     * @return load model
     */
    LoadModel toLoadModel(Mapping mapping, LOAD_REQ req);

    /**
     * Converts to the load response from a load model.
     *
     * @param model load model
     * @return load response
     */
    LOAD_RESP fromLoadModel(LoadModel model);

    /**
     * Converts from the saveButton request to a saveButton model.
     *
     * @param mapping current mapping
     * @param req     saveButton request
     * @return saveButton model
     */
    SaveModel toSaveModel(Mapping mapping, SAVE_REQ req);

    /**
     * Converts to the saveButton response from a saveButton model.
     *
     * @param model saveButton model
     * @return saveButton response
     */
    SAVE_RESP fromSaveModel(SaveModel model);

    /**
     * Converts from the get checkpoint request to a checkpoint model.
     *
     * @param mapping current mapping
     * @param req     get checkpoint request
     * @return checkpoint model
     */
    CheckpointModel toCheckpointModelFromGetCheckpointRequest(Mapping mapping, GET_CHECKPOINT_REQ req);

    /**
     * Converts to the get checkpoint response from a checkpoint model.
     *
     * @param model checkpoint model
     * @return get checkpoint response
     */
    GET_CHECKPOINT_RESP fromCheckpointModelToGetCheckpointResponse(CheckpointModel model);

    /**
     * Converts from the set checkpoint request to a checkpoint model.
     *
     * @param mapping current mapping
     * @param req     set checkpoint request
     * @return checkpoint model
     */
    CheckpointModel toCheckpointModelFromSetCheckpointRequest(Mapping mapping, SET_CHECKPOINT_REQ req);

    /**
     * Converts to the set checkpoint response from a checkpoint model.
     *
     * @param model checkpoint model
     * @return set checkpoint response
     */
    SET_CHECKPOINT_RESP fromCheckpointModelToSetCheckpointResponse(CheckpointModel model);

    /**
     * Converts from the query request to a query model.
     *
     * @param mapping current mapping
     * @param req     query request
     * @return query model
     */
    QueryModel toQueryModelFromQueryRequest(Mapping mapping, EXECUTE_QUERY_REQ req);

    /**
     * Converts to the execute query response from a query model.
     *
     * @param model checkpoint model
     * @return execute query response
     */
    EXECUTE_QUERY_RESP fromQueryModelToQueryResponse(LoadModel model);

}
