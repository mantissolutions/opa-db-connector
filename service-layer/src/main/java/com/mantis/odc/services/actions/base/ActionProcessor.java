/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions.base;

import com.mantis.odc.models.EnumerationMapping;
import com.mantis.odc.services.models.CheckpointModel;
import com.mantis.odc.services.models.DatabaseEnumeration;
import com.mantis.odc.services.models.LoadModel;
import com.mantis.odc.services.models.SaveModel;

import java.io.Closeable;
import java.sql.SQLException;
import java.util.List;
import java.util.OptionalInt;

/**
 * This interface defines all the possible actions that ODC can process.
 */
public interface ActionProcessor extends Closeable {

    /**
     * Returns all enumerations from the given enumeration mappings.
     *
     * @param enumerationMappings enumeration mappings
     * @return enumerations
     * @throws SQLException on SQL error
     */
    List<DatabaseEnumeration> enumerations(List<EnumerationMapping> enumerationMappings) throws SQLException;

    /**
     * Loads mapped data from the database.
     *
     * @param load      action model to return data from.
     * @param fromGobal flag to indicate that the load request is contained within a global entity.
     *                  The default is true, this is always the case for load requests, but not for ExecuteQuery requests
     * @param limit     query limit for global, only applys to unbound global queries. (when fromGlobal is false)
     * @return processed action.
     * @throws SQLException on SQL error
     */
    LoadModel load(LoadModel load, boolean fromGobal, OptionalInt limit) throws SQLException;

    /**
     * Saves mapped data to the database.
     *
     * @param save saveButton model
     * @return return data.
     * @throws SQLException on SQL error
     */
    SaveModel save(SaveModel save) throws SQLException;

    /**
     * Sets a checkpoint to the database.
     *
     * @param checkpoint checkpoint
     * @return checkpoint id
     * @throws SQLException on SQL error
     */
    CheckpointModel setCheckpoint(CheckpointModel checkpoint) throws SQLException;

    /**
     * Gets a checkpoint from the database.
     *
     * @param checkpoint checkpoint action
     * @return checkpoint data
     * @throws SQLException on SQL error
     */
    CheckpointModel getCheckpoint(CheckpointModel checkpoint) throws SQLException;
}
