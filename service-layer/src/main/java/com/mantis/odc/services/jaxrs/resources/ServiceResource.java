/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.resources;

import com.mantis.odc.models.OPAVersion;
import com.mantis.odc.models.ServiceAction;
import com.mantis.odc.services.OperationStatisticsFilter;
import com.mantis.odc.services.Service;
import com.mantis.odc.services.ServiceWSDL;
import com.mantis.odc.services.soap.SOAPProcessor;
import com.mantis.odc.services.storage.ServiceStorage;
import io.swagger.annotations.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.mantis.odc.services.OperationStatisticsFilter.OPERATION_ID_PARAM;
import static java.text.MessageFormat.format;
import static javax.ws.rs.core.MediaType.*;

/**
 * This resource processes all OPA Connector requests. This is SOAP service, it operates against a header action
 * and uses a custom SOAP message handler to processes the requests and responses.
 */
@Slf4j
@Singleton
@Api(value = "Services", description = "Exposes deployed Mappings")
@Path("/services")
public class ServiceResource {

    private final ServiceStorage storage;
    private final SOAPProcessor soapProcessor;
    private final OperationStatisticsFilter operationStatisticsFilter;

    @Inject
    public ServiceResource(ServiceStorage storage, SOAPProcessor soapProcessor, OperationStatisticsFilter operationStatisticsFilter) {
        this.storage = storage;
        this.soapProcessor = soapProcessor;
        this.operationStatisticsFilter = operationStatisticsFilter;
    }

    /**
     * Returns all the deployed service end points.
     *
     * @param request request context
     * @return list of deployed service end points
     */
    @GET
    @Produces(value = APPLICATION_JSON)
    @ApiOperation(value = "List Services", notes = "Lists all deployed service endpoints.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of deployed end points.")
    })
    public Map<String, List<String>> listDeployedEndPoints(@Context HttpServletRequest request) {
        return storage.listValues().stream()
                .collect(Collectors.toMap(Service::getName, service -> Arrays.stream(OPAVersion.values())
                        .map(version -> format("{0}/{1}/{2}", request.getRequestURL().toString(), service.getName(), version.name()))
                        .collect(Collectors.toList())));
    }

    /**
     * Responds to all get requests for the given service name, with its WSDL.
     *
     * @param service related service
     * @param version OPA version
     * @param request request
     * @return WSDL
     * @throws IOException on IO error constructing the WSDL
     */
    @GET
    @Path("/{service}/{version}")
    @Produces(value = TEXT_XML)
    @ApiOperation(value = "Service WSDL",
            notes = "Returns the service WSDL. This can then be imported into the OPA HUB.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Service WSDL.")
    })
    public String getServiceWSDL(@ApiParam(value = "Name of the deployed service", required = true)
                                 @PathParam("service") Service service,
                                 @ApiParam(value = "Supported OPA version.", required = true)
                                 @PathParam("version") OPAVersion version,
                                 @Context HttpServletRequest request) {
        return ServiceWSDL.builder()
                .version(version)
                .serviceName(service.getName())
                .location(request.getRequestURL().toString())
                .build().render();
    }

    /**
     * Processes all OPA connector requests to the given service.
     *
     * @param actionString action to process
     * @param service      service to use
     * @param version      OPA version
     * @param message      message to process
     * @param request      context
     * @return soap message
     */
    @POST
    @Path("/{service}/{version}")
    @Produces(value = TEXT_XML)
    @Consumes(value = {TEXT_XML, APPLICATION_XML})
    @ApiOperation(value = "Service Action", notes = "Handles SOAP operations for a given deployed service.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "SOAP Response.", response = String.class)
    })
    public SOAPMessage handleAction(@ApiParam(value = "Action to perform", required = true,
            allowableValues = "CheckAlive, GetMetadata, Load, Save, GetCheckpoint, SetCheckpoint")
                                    @HeaderParam("SOAPAction") String actionString,
                                    @ApiParam(value = "Name of the deployed service", required = true)
                                    @PathParam("service") Service service,
                                    @ApiParam(value = "Supported OPA version.", required = true)
                                    @PathParam("version") OPAVersion version,
                                    @ApiParam(value = "SOAP Message. This must be SOAP 1.1 compliant.", required = true)
                                            SOAPMessage message,
                                    @Context HttpServletRequest request) {
        logMessage(message);
        // unfortunately OPA sends SOAP actions with extra commas in the string.
        val serviceAction = ServiceAction.valueOf(actionString.replace("\"", ""));
        val builder = operationStatisticsFilter.get((String) request.getAttribute(OPERATION_ID_PARAM));
        val response = soapProcessor.process(serviceAction, builder, service, version, message);
        logMessage(response);
        return response;
    }


    /**
     * Logs a SOAP message.
     *
     * @param soap message to log
     */
    @SneakyThrows
    private void logMessage(SOAPMessage soap) {
        if (log.isDebugEnabled()) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            soap.writeTo(stream);
            log.debug(stream.toString());
        }
    }
}
