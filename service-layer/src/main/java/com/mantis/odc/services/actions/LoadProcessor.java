/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions;

import com.github.davidmoten.rx.jdbc.Database;
import com.mantis.odc.services.actions.base.BaseDBProcessor;
import com.mantis.odc.services.models.*;
import com.mantis.odc.services.models.base.DatabaseTable;
import com.mantis.odc.services.sql.adaptors.base.SQLAdaptor;
import com.mantis.odc.services.sql.converters.base.SQLTypeConverter;
import lombok.val;
import rx.Observable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.OptionalInt;
import java.util.TimeZone;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Implements the loading elements of the action processor.
 */
class LoadProcessor extends BaseDBProcessor {

    LoadProcessor(Database db, SQLAdaptor adaptor, SQLTypeConverter typeConverter) {
        super(db, adaptor, typeConverter);
    }

    /**
     * Gets all bind conditions in a group.
     *
     * @param group bind group
     * @return bind conditions
     */
    private Stream<BindCondition> getAllBinds(BindConditionGroup group) {
        return Stream.concat(
                group.getConditions().stream()
                , group.getChildren().stream()
                        .flatMap(this::getAllBinds));
    }

    @Override
    public LoadModel load(LoadModel loadModel, boolean fromGlobal, OptionalInt limit) throws SQLException {
        val conn = db.getConnectionProvider().get();

        try {
            // set save point
            conn.setSavepoint();

            // run pre scripts
            loadModel.getPreScript().ifPresent(scriptRunner::run);

            // if there is data to be loaded, load data
            if (!fromGlobal || loadModel.getGlobalId() != null) {
                for (val table : loadModel.getTables()) {
                    // load conditions
                    val conditions = loadModel.getConditionsForTable(table);
                    val binds = conditions.stream()
                            .flatMap(this::getAllBinds)
                            .collect(Collectors.toList());
                    // get SQL
                    val sql = (table instanceof ContainmentTable) ?
                            adaptor.createContainmentSelect(loadModel, (ContainmentTable) table, conditions, true, fromGlobal) :
                            adaptor.createNonContainmentSelect(loadModel, (JunctionTable) table, true, fromGlobal, true, true);
                    populateTable(loadModel, table, sql, binds, fromGlobal, limit);
                }
            }

            // run post script
            loadModel.getPostScript().ifPresent(scriptRunner::run);
        } catch (Exception e) {
            conn.rollback();
            throw e;
        }
        return loadModel;
    }

    /**
     * Populates a table from the database.
     *
     * @param loadModel  load model
     * @param table      table model
     * @param sql        sql query
     * @param conditions custom bind conditions
     * @param fromGlobal true if the select is a global based containment select
     * @param limit      @throws SQLException on conversion error
     */
    private void populateTable(LoadModel loadModel, DatabaseTable table, String sql, List<BindCondition> conditions, boolean fromGlobal, OptionalInt limit) throws SQLException {
        val builder = db.select(sql);

        // add global key value
        if (fromGlobal) {
            val pk = loadModel.getGlobalContainmentTable().getPrimaryKey();
            val key = typeConverter.convertToSQLType(
                    loadModel.getGlobalId().getKey(),
                    pk.getType());
            builder.parameter(pk.getSource(), key);
        }

        // add custom conditions
        for (val bindCondition : conditions) {
            if (bindCondition.isSingleValue()) {
                val value = typeConverter.convertToSQLType(bindCondition.getValues().get(0), bindCondition.getType());
                builder.parameter(bindCondition.getSource(), value);
            } else for (val v : bindCondition.getValues()) {
                val value = typeConverter.convertToSQLType(v, bindCondition.getType());
                builder.parameter(bindCondition.getSource() + "_" + conditions.indexOf(bindCondition), value);
            }
        }
        loadModel.incrementSelects();
        Observable<DatabaseRow> toRun = builder.get(resultSet -> resultSetToRow(resultSet, table, table.getSelectFields(loadModel), loadModel.getTimeZone()));
        if (limit.isPresent()) toRun = toRun.limit(limit.getAsInt());
        toRun.toBlocking()
                .subscribe(table::addRow);
    }

    /**
     * Converts a result set to a row.
     *
     * @param resultSet    result set
     * @param table        table model
     * @param selectFields list of selected fields
     * @param timeZone     current time zone
     * @return data row
     * @throws SQLException on conversion error
     */
    DatabaseRow resultSetToRow(ResultSet resultSet, DatabaseTable table, List<DatabaseField> selectFields, TimeZone timeZone) throws SQLException {
        val builder = DatabaseRow.dataRowBuilder();
        builder.source(table.getSource());

        for (val dataField : selectFields) {
            val convertedValue = typeConverter.convertFromSQLType(resultSet.getObject(dataField.getOpaName()), dataField.getType(), timeZone);
            if (dataField instanceof PrimaryKeyField) builder.id(new RowId(convertedValue));
            else if (dataField instanceof ForeignKeyField && table instanceof ContainmentTable)
                builder.fk(new RowId(convertedValue));
            else builder.item(RowValue.builder()
                        .source(dataField.getSource())
                        .opaName(dataField.getOpaName())
                        .type(dataField.getType())
                        .value(convertedValue)
                        .build());
        }

        return builder.build();
    }
}
