/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters._12_2;

import com.mantis.odc.models.AttachmentMapping;
import com.mantis.odc.models.Mapping;
import com.mantis.odc.models.OPAMappingId;
import com.mantis.odc.models.TableMapping;
import com.mantis.odc.services.converters.base.BaseOPAConverterWithoutCheckpoints;
import com.mantis.odc.services.converters.base.SharedConverters;
import com.mantis.odc.services.converters.exceptions.MissingGlobalRowException;
import com.mantis.odc.services.converters.exceptions.MissingMappingException;
import com.mantis.odc.services.models.*;
import com.mantis.odc.services.models.base.ActionModel;
import com.mantis.odc.services.models.base.DatabaseTable;
import com.oracle.opa.connector._12_2.data.types.*;
import com.oracle.opa.connector._12_2.metadata.types.*;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.val;
import org.jooq.lambda.Unchecked;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mantis.odc.models.DataType.ATTACHMENT_TYPE;
import static com.mantis.odc.models.DataType.STRING_TYPE;
import static com.mantis.odc.services.converters.base.SharedConverters.*;
import static com.mantis.odc.services.converters.exceptions.MissingMappingException.MappingType.Table;
import static com.mantis.odc.services.models.DBActionType.insert;
import static com.mantis.odc.utilities.ErrorUtil.errorToString;
import static java.lang.String.format;

/**
 * Converter for OPA Connector version 12.2.
 */
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public final class OPA122Converter extends BaseOPAConverterWithoutCheckpoints<
        CheckAliveResponse,
        GetMetadataResponse,
        LoadRequest, LoadResponse,
        SaveRequest, SaveResponse,
        RequestFault> {

    private final OPA122AutoConverter autoConverter;

    public OPA122Converter() {
        autoConverter = new OPA122AutoConverterImpl();
    }

    @Override
    public CheckAliveResponse getCheckAlive() {
        return new CheckAliveResponse();
    }

    @Override
    public RequestFault getFault(Exception e) {
        val fault = new RequestFault();
        fault.setMessage(format("%s\nStack Trace:\n%s", e.getMessage(), errorToString(e)));
        return fault;
    }

    @Override
    public GetMetadataResponse getMetaData(Mapping mapping, List<DatabaseEnumeration> enumerations) {
        val resp = new GetMetadataResponse();
        resp.getTable().addAll(mapping.getTables().stream()
                .map(this::createMetaTable)
                .collect(Collectors.toList()));
        return resp;
    }

    /**
     * Creates a meta table from the table mapping.
     *
     * @param table table mapping
     * @return meta table
     */
    private MetaTable createMetaTable(TableMapping table) {
        val metaTable = new MetaTable();
        metaTable.setName(table.getTableId().getName());
        metaTable.setAcceptsAttachments(table.getMapping().isSupportsAttachments());
        metaTable.setCanBeInput(table.canBeInput());
        metaTable.setCanBeOutput(table.canBeOutput());
        metaTable.getField().addAll(table.getFields().stream()
                .filter(field -> (field.isInput() || field.isOutput()) && !field.isForeignKey())
                .map(autoConverter::createMetaField)
                .collect(Collectors.toList()));
        metaTable.getLink().addAll(Stream.concat(
                table.getChildren().stream()
                        .map(child -> createMetaLink(
                                child.getLinkName(),
                                child.getTableId().getName(),
                                (child.isOneToOne()) ? MetaLinkCardinality.C_11 : MetaLinkCardinality.C_1_M)),
                table.getJunctionTables().stream()
                        .map(junctionTable -> createMetaLink(
                                junctionTable.getLinkName(),
                                junctionTable.getTargetName(table.getTableId()),
                                MetaLinkCardinality.fromValue(junctionTable.getLinkType(table).getValue())))
        )
                .collect(Collectors.toList()));
        return metaTable;
    }

    /**
     * Creates a meta link from the given parent and child table mappings.
     *
     * @param name        name of the relationship
     * @param target      target of the relationship
     * @param cardinality link cardinality
     * @return meta link
     */
    private MetaLink createMetaLink(String name, String target, MetaLinkCardinality cardinality) {
        val metaLink = new MetaLink();
        metaLink.setName(name);
        metaLink.setTarget(target);
        metaLink.setCardinality(cardinality);
        return metaLink;
    }

    @SneakyThrows
    @Override
    public LoadModel toLoadModel(Mapping mapping, LoadRequest loadRequest) {
        return createLoadModel(mapping,
                loadRequest.getRoot(),
                loadRequest.getLanguage(),
                loadRequest.getTimezone(),
                loadRequest.getTables().getTable(),
                asMap(loadRequest.getRequestContext()));
    }

    /**
     * Creates a load model from a load request.
     *
     * @param mapping       mapping
     * @param root          root model name
     * @param language      language
     * @param timezone      timezone
     * @param tables        list of tables
     * @param requestParams map of request params
     * @return load model
     */
    @SneakyThrows
    public LoadModel createLoadModel(Mapping mapping,
                                     String root,
                                     String language,
                                     String timezone,
                                     List<Table> tables,
                                     Map<String, String> requestParams) {
        val rootTable = mapping.getTable(root).orElseThrow(() -> new MissingMappingException(Table, root));

        // get containment tables
        Stream<DatabaseTable> containmentTables = tables.stream()
                .map(Unchecked.function(table -> createContainmentTable(mapping, table.getName(), table.getField().stream()
                        .map(Field::getName)
                        .collect(Collectors.toList()))));

        // get junction tables
        Stream<DatabaseTable> junctionTables = tables.stream()
                .flatMap(table -> table.getLink().stream()).distinct()
                .flatMap(link -> mapping.getJunctionTable(link.getName())
                        .map(junctionTableMapping -> createJunctionTable(junctionTableMapping, link.getTarget()))
                        .map(Stream::of).orElse(Stream.empty()));

        return LoadModel.builder()
                .locale(Locale.forLanguageTag(language))
                .timeZone(TimeZone.getTimeZone(timezone))
                .globalId(Optional.ofNullable(requestParams.get(mapping.getBindToken())).map(RowId::new).orElse(null))
                .rootTable(rootTable.getTableId().getSource())
                .preScript(createScript(mapping.getPreLoadScript(), requestParams))
                .postScript(createScript(mapping.getPostLoadScript(), requestParams))
                .tables(Stream.concat(containmentTables, junctionTables)
                        .collect(Collectors.toList()))
                .conditions(mapping.getBinds().stream()
                        .flatMap(bindMapping -> Optional.ofNullable(requestParams.get(bindMapping.getTokenName()))
                                .map(v -> Stream.of(createBindCondition(v, bindMapping)))
                                .orElse(Stream.empty()))
                        .collect(Collectors.toList()))
                .build();
    }

    private Map<String, String> asMap(RequestContext requestContext) {
        val map = new HashMap<String, String>();
        if (requestContext != null) for (val p : requestContext.getParameter()) map.put(p.getName(), p.getValue());
        return map;
    }

    @Override
    public LoadResponse fromLoadModel(LoadModel model) {
        val resp = new LoadResponse();
        resp.setTables(getLoadResponseData(model));
        return resp;
    }

    /**
     * Returns the load response
     *
     * @param model load model
     * @return load data
     */
    public LoadData getLoadResponseData(LoadModel model) {
        val loadData = new LoadData();
        loadData.getTable().addAll(model.getContainmentTables().stream()
                .filter(table -> !table.getRows().isEmpty())
                .map(table -> createDataTable(model, table))
                .collect(Collectors.toList()));
        return loadData;
    }

    /**
     * Creates an OPA data table from a containment table.
     *
     * @param model load model
     * @param table containment table
     * @return OPA data table
     */
    private DataTable createDataTable(LoadModel model, ContainmentTable table) {
        val dataTable = new DataTable();
        dataTable.setName(table.getOpaName());
        dataTable.getRow().addAll(table.getRows().stream()
                .map(dataRow -> createDataRow(model, dataRow))
                .collect(Collectors.toList()));
        return dataTable;
    }

    /**
     * Creates an OPA data row from the given database row.
     *
     * @param model load model
     * @param row   database row
     * @return OPA data row
     */
    private DataRow createDataRow(LoadModel model, DatabaseRow row) {
        return createDataRowFromRow(model, row, new DataRow());
    }

    /**
     * Creates an OPA data row from the given database row.
     *
     * @param model   load model
     * @param row     database row
     * @param dataRow existing row
     * @return OPA data row
     */
    private DataRow createDataRowFromRow(ActionModel model, DatabaseRow row, DataRow dataRow) {
        dataRow.setId(row.getId().getKeyAsString());
        dataRow.getField().addAll(row.getData().stream()
                .map(this::createDataField)
                .collect(Collectors.toList()));
        if (!(dataRow instanceof UpdateRow)) {
            dataRow.getLink().addAll(model.getLinks(row).stream()
                    .filter(databaseLink -> !databaseLink.getRefs().isEmpty())
                    .map(autoConverter::createDataLink)
                    .collect(Collectors.toList()));
        }
        return dataRow;
    }

    /**
     * Creates a data field from a row value and row instance.
     *
     * @param rowValue row value
     * @return OPA data field
     */
    private DataField createDataField(RowValue rowValue) {
        val dataField = new DataField();
        dataField.setName(rowValue.getOpaName());
        if (rowValue.getValue() != null) switch (rowValue.getType()) {
            case STRING_TYPE:
                dataField.setTextVal((String) rowValue.getValue());
                break;
            case DATE_TIME_TYPE:
                dataField.setDatetimeVal((XMLGregorianCalendar) rowValue.getValue());
                break;
            case BOOLEAN_TYPE:
                dataField.setBooleanVal((Boolean) rowValue.getValue());
                break;
            case DATE_TYPE:
                dataField.setDateVal((XMLGregorianCalendar) rowValue.getValue());
                break;
            case DECIMAL_TYPE:
                dataField.setNumberVal((BigDecimal) rowValue.getValue());
                break;
            case TIME_TYPE:
                dataField.setTimeVal((XMLGregorianCalendar) rowValue.getValue());
                break;
        }
        else dataField.setUnknownVal(new UnknownValue());
        return dataField;
    }

    @SneakyThrows
    @Override
    public SaveModel toSaveModel(Mapping mapping, SaveRequest saveRequest) {
        return createSaveModel(mapping,
                saveRequest.getRoot(),
                saveRequest.getLanguage(),
                saveRequest.getTimezone(),
                saveRequest.getTables().getTable(),
                Optional.ofNullable(saveRequest.getAttachments())
                        .map(AttachmentList::getAttachment)
                        .orElse(new ArrayList<>()),
                asMap(saveRequest.getRequestContext()));
    }

    /**
     * Creates a save model from a save request.
     *
     * @param mapping       mapping
     * @param root          root submit table name
     * @param language      language code
     * @param timezone      timezone code
     * @param tables        list of submit tables
     * @param attachments   list of attachments
     * @param requestParams map of request params
     * @return save model
     */
    @SneakyThrows
    public SaveModel createSaveModel(Mapping mapping,
                                     String root,
                                     String language,
                                     String timezone,
                                     List<SubmitTable> tables,
                                     List<Attachment> attachments,
                                     Map<String, String> requestParams) {
        val rootTable = mapping.getTable(root).orElseThrow(() -> new MissingMappingException(Table, root));
        val globalId = Optional.ofNullable(requestParams.get(mapping.getBindToken()))
                .map(RowId::new)
                .orElse(getRootRowId(root, tables));

        // get containment tables
        Stream<DatabaseTable> containmentTables = tables.stream().map(table -> createTablesFromSubmit(mapping, table));

        // get junction tables
        val addedJunctionTables = new HashSet<String>();
        Stream<DatabaseTable> junctionTables = tables.stream()
                .flatMap(submitTable -> submitTable.getRow().stream())
                .flatMap(submitRow -> submitRow.getLink().stream())
                .flatMap(link -> (addedJunctionTables.add(link.getName())) ? mapping.getJunctionTable(link.getName())
                        .map(junctionTableMapping -> createJunctionTable(junctionTableMapping, link.getTarget()))
                        .map(Stream::of).orElse(Stream.empty()) : Stream.empty());

        // get attachments
        Stream<DatabaseTable> attachmentTable = (mapping.isSupportsAttachments()) ?
                Stream.of(createAttachmentTable(mapping, rootTable.getTableId().getSource(), globalId, tables, attachments)) :
                Stream.empty();

        // build links
        val saveModel = SaveModel.builder()
                .locale(Locale.forLanguageTag(language))
                .timeZone(TimeZone.getTimeZone(timezone))
                .globalId(globalId)
                .rootTable(rootTable.getTableId().getSource())
                .preScript(createScript(mapping.getPreSaveScript(), requestParams))
                .postScript(createScript(mapping.getPostSaveScript(), requestParams))
                .tables(Stream.concat(containmentTables, Stream.concat(junctionTables, attachmentTable))
                        .collect(Collectors.toList()))
                .build();

        // build links
        tables.stream()
                .flatMap(submitTable -> submitTable.getRow().stream())
                .forEach(Unchecked.consumer(submitRow -> {
                    String id = submitRow.getId();
                    for (DataLink link : submitRow.getLink()) {
                        Optional<JunctionTable> junctionTableOptional = saveModel.getJunctionTable(link.getName());

                        // add a row for the junction table
                        if (junctionTableOptional.isPresent()) {
                            JunctionTable junctionTable = junctionTableOptional.get();
                            // this might be a table in the current saveButton model,
                            // or a table in the wider mapping.
                            OPAMappingId tableKey = mapping.getTable(link.getTarget())
                                    .orElseThrow(() -> new MissingMappingException(Table, link.getTarget())).getTableId();

                            ForeignKeyField rowKey = junctionTable.getDestinationKey(tableKey.getSource());
                            ForeignKeyField refKey = junctionTable.getTargetKey(tableKey.getSource());
                            link.getRef().forEach(linkRef ->
                                    junctionTable.getRows().add(DatabaseSubmitRow.submitRowBuilder()
                                            .source(junctionTable.getSource())
                                            .action(insert)
                                            .item(new RowValue(rowKey.getSource(), rowKey.getOpaName(), rowKey.getType(), id))
                                            .item(new RowValue(refKey.getSource(), refKey.getOpaName(), refKey.getType(), linkRef.getId()))
                                            .build()));
                        } else { // add foreign key value
                            // this is always a containment table
                            ContainmentTable targetTable = (ContainmentTable) saveModel.getTableByOpaName(link.getTarget())
                                    .orElseThrow(() -> new MissingMappingException(Table, link.getTarget()));
                            link.getRef().forEach(linkRef -> targetTable.getRows().stream()
                                    .filter(databaseRow -> databaseRow.getId().getOpaKeyAsString().equals(linkRef.getId()))
                                    .forEach(databaseRow -> databaseRow.setFk(new RowId(id, null))));
                        }
                    }
                }));

        return saveModel;
    }

    /**
     * Returns the root row id.
     *
     * @param root   root table name
     * @param tables submit tables
     * @return root row id
     */
    @SneakyThrows
    private RowId getRootRowId(String root, List<SubmitTable> tables) {
        return tables.stream()
                .filter(submitTable -> submitTable.getName().equals(root))
                .map(submitTable -> new RowId(submitTable.getRow().get(0).getId(), null))
                .findFirst()
                .orElseThrow(MissingGlobalRowException::new);
    }

    /**
     * Creates an attachment table.
     *
     * @param mapping     mapping
     * @param rootSource  root table source
     * @param globalId    global row id
     * @param tables      list of submit tables
     * @param attachments list of attachments
     * @return attachment table
     */
    private AttachmentTable createAttachmentTable(Mapping mapping,
                                                  String rootSource,
                                                  RowId globalId,
                                                  List<SubmitTable> tables,
                                                  List<Attachment> attachments) {
        val attachmentMapping = mapping.getAttachment();

        // get global attachments
        Stream<AttachmentRow> globalAttachments = (attachments != null && !attachments.isEmpty()) ?
                attachments.stream().map(attachment -> createAttachmentRow(attachmentMapping, rootSource, attachment, globalId)) : Stream.empty();

        // get child table attachments
        Stream<AttachmentRow> childAttachments = tables.stream()
                .flatMap(submitTable -> {
                    TableMapping tableMapping = mapping.getTable(submitTable.getName()).get();
                    return submitTable.getRow().stream()
                            .filter(submitRow -> submitRow.getAttachments() != null && !submitRow.getAttachments().getAttachment().isEmpty())
                            .flatMap(submitRow -> submitRow.getAttachments().getAttachment().stream()
                                    .map(attachment -> createAttachmentRow(attachmentMapping, tableMapping.getTableId().getSource(), attachment, new RowId(submitRow.getId(), null))));
                });

        return AttachmentTable.builder()
                .source(attachmentMapping.getTableName())
                .opaName(attachmentMapping.getTableName())
                .sourceField(attachmentMapping.getTableField())
                .keyField(attachmentMapping.getKeyField())
                .nameField(attachmentMapping.getNameField())
                .fileNameField(attachmentMapping.getFileNameField())
                .descriptionField(attachmentMapping.getDescriptionField())
                .fileField(attachmentMapping.getFileField())
                .attachmentRows(Stream.concat(globalAttachments, childAttachments)
                        .collect(Collectors.toList()))
                .build();
    }

    /**
     * Creates an attachment row.
     *
     * @param attachmentMapping attachment table mapping
     * @param source            global table source
     * @param attachment        attachment
     * @param id                global id
     * @return attachment row
     */
    private AttachmentRow createAttachmentRow(AttachmentMapping attachmentMapping, String source, Attachment attachment, RowId id) {
        return AttachmentRow.attachmentRowBuilder()
                .id(id)
                .table(RowValue.builder()
                        .source(attachmentMapping.getTableField())
                        .opaName(attachmentMapping.getTableField())
                        .type(STRING_TYPE)
                        .value(source)
                        .build())
                .name(RowValue.builder()
                        .source(attachmentMapping.getNameField())
                        .opaName(attachmentMapping.getNameField())
                        .type(STRING_TYPE)
                        .value(attachment.getName())
                        .build())
                .fileName(RowValue.builder()
                        .source(attachmentMapping.getFileNameField())
                        .opaName(attachmentMapping.getFileNameField())
                        .type(STRING_TYPE)
                        .value(attachment.getFilename())
                        .build())
                .description(RowValue.builder()
                        .source(attachmentMapping.getDescriptionField())
                        .opaName(attachmentMapping.getDescriptionField())
                        .type(STRING_TYPE)
                        .value(attachment.getDescription())
                        .build())
                .attachmentData(RowValue.builder()
                        .source(attachmentMapping.getFileField())
                        .opaName(attachmentMapping.getFileField())
                        .type(ATTACHMENT_TYPE)
                        .value(attachment.getValue())
                        .build())
                .build();
    }

    /**
     * Creates a containment table from an OPA submit table.
     *
     * @param mapping mapping
     * @param table   OPA submit table
     * @return containment table
     */
    @SneakyThrows
    private ContainmentTable createTablesFromSubmit(Mapping mapping, SubmitTable table) {
        val tableMapping = mapping.getTable(table.getName())
                .orElseThrow(() -> new MissingMappingException(Table, table.getName()));
        return ContainmentTable.builder()
                .source(tableMapping.getTableId().getSource())
                .opaName(tableMapping.getTableId().getName())
                .linkName(tableMapping.getLinkName())
                .alias(tableMapping.getAlias())
                .inferred(tableMapping.isInferred())
                .primaryKey(createPrimaryKey(tableMapping.getPrimaryKey()))
                .foreignKey(tableMapping.getForeignKeyOpt()
                        .map(fk -> createForeignKey(tableMapping.getParent().getTableId(), fk))
                        .orElse(null))
                .fields(tableMapping.getFields().stream()
                        .filter(fieldMapping -> !(fieldMapping.isPrimaryKey() || fieldMapping.isForeignKey()))
                        .map(Unchecked.function(fieldMapping -> SharedConverters.createDataField(tableMapping, fieldMapping.getFieldId().getName())))
                        .collect(Collectors.toList()))
                .rows(table.getRow().stream()
                        .map(submitRow -> createDatabaseSubmitRow(tableMapping, submitRow))
                        .collect(Collectors.toList()))
                .build();
    }

    /**
     * Creates a database submit row from an OPA submit row.
     *
     * @param mapping   table mapping
     * @param submitRow OPA submit row
     * @return database submit row
     */
    private DatabaseSubmitRow createDatabaseSubmitRow(TableMapping mapping, SubmitRow submitRow) {
        return DatabaseSubmitRow.submitRowBuilder()
                .source(mapping.getTableId().getSource())
                .action(autoConverter.createDBAction(submitRow.getAction()))
                .id(new RowId(submitRow.getId(), null))
                .data(submitRow.getInputField().stream()
                        .map(dataField -> createRowValue(mapping, dataField))
                        .collect(Collectors.toList()))
                .returnFields(submitRow.getOutputField().stream()
                        .map(dataField -> createOutputField(mapping, dataField))
                        .collect(Collectors.toList()))
                .build();
    }

    /**
     * Creates a row value from the given OPA data field.
     *
     * @param mapping   table mapping
     * @param dataField OPA data field
     * @return row value
     */
    @SneakyThrows
    private RowValue createRowValue(TableMapping mapping, DataField dataField) {
        val fieldMapping = mapping.getField(dataField.getName())
                .orElseThrow(() -> new MissingMappingException(MissingMappingException.MappingType.Field, dataField.getName()));
        val builder = RowValue.builder()
                .source(fieldMapping.getFieldId().getSource())
                .opaName(fieldMapping.getFieldId().getName())
                .type(fieldMapping.getType());

        switch (fieldMapping.getType()) {
            case STRING_TYPE:
                builder.value(dataField.getTextVal());
                break;
            case DATE_TIME_TYPE:
                builder.value(dataField.getDatetimeVal());
                break;
            case BOOLEAN_TYPE:
                builder.value(dataField.isBooleanVal());
                break;
            case DATE_TYPE:
                builder.value(dataField.getDateVal());
                break;
            case DECIMAL_TYPE:
                builder.value(dataField.getNumberVal());
                break;
            case TIME_TYPE:
                builder.value(dataField.getTimeVal());
                break;
        }

        return builder.build();
    }

    /**
     * Creates an output field from the given OPA data field.
     *
     * @param mapping   table mapping
     * @param dataField data field
     * @return output field
     */
    @SneakyThrows
    private DatabaseField createOutputField(TableMapping mapping, DataField dataField) {
        val fieldMapping = mapping.getField(dataField.getName())
                .orElseThrow(() -> new MissingMappingException(MissingMappingException.MappingType.Field, dataField.getName()));
        return DatabaseField.fieldBuilder()
                .source(fieldMapping.getFieldId().getSource())
                .opaName(fieldMapping.getFieldId().getName())
                .type(fieldMapping.getType())
                .build();
    }

    @Override
    public SaveResponse fromSaveModel(SaveModel model) {
        val resp = new SaveResponse();
        resp.setTables(getUpdateData(model));
        return resp;
    }

    /**
     * Returns the update data.
     *
     * @param model save model
     * @return update data
     */
    public UpdateData getUpdateData(SaveModel model) {
        val data = new UpdateData();
        data.getTable().addAll(model.getContainmentTables().stream()
                .filter(table -> !table.isInferred() && !table.getRows().isEmpty())
                .map((table) -> createUpdateTable(model, table))
                .collect(Collectors.toList()));
        return data;
    }

    /**
     * Creates an OPA update table from containment table.
     *
     * @param model saveButton model
     * @param table containment table
     * @return OPA update table
     */
    private UpdateTable createUpdateTable(SaveModel model, ContainmentTable table) {
        val updateTable = new UpdateTable();
        updateTable.setName(table.getOpaName());
        updateTable.getRow().addAll(table.getRows().stream()
                .map((databaseRow) -> createUpdateRow(model, databaseRow))
                .collect(Collectors.toList()));
        return updateTable;
    }

    /**
     * Creates an OPA update row from the given database row.
     *
     * @param model       saveButton model
     * @param databaseRow database row
     * @return OPA update row
     */
    private UpdateRow createUpdateRow(SaveModel model, DatabaseRow databaseRow) {
        val updateRow = (UpdateRow) createDataRowFromRow(model, databaseRow, new UpdateRow());
        if (databaseRow.getId().hasNewKey()) updateRow.setOrigId(databaseRow.getId().getOpaKeyAsString());
        return updateRow;
    }
}
