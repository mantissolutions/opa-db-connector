/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters.base;

import com.mantis.odc.models.Mapping;
import com.mantis.odc.services.models.LoadModel;
import com.mantis.odc.services.models.QueryModel;

/**
 * Converter without checkpoint support.
 */
public abstract class BaseOPAConverterWithoutExecuteQuery<
        CHECK_ALIVE, META_DATA,
        LOAD_REQ, LOAD_RESP,
        SAVE_REQ, SAVE_RESP,
        GET_CHECKPOINT_REQ, GET_CHECKPOINT_RESP,
        SET_CHECKPOINT_REQ, SET_CHECKPOINT_RESP,
        FAULT> implements OPAConverter<
        CHECK_ALIVE, META_DATA,
        LOAD_REQ, LOAD_RESP,
        SAVE_REQ, SAVE_RESP,
        GET_CHECKPOINT_REQ, GET_CHECKPOINT_RESP,
        SET_CHECKPOINT_REQ, SET_CHECKPOINT_RESP,
        Object, Object,
        FAULT> {

    @Override
    public QueryModel toQueryModelFromQueryRequest(Mapping mapping, Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object fromQueryModelToQueryResponse(LoadModel model) {
        throw new UnsupportedOperationException();
    }
}
