/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.sql;

import lombok.Data;
import lombok.SneakyThrows;
import lombok.val;

import java.io.LineNumberReader;
import java.io.StringReader;
import java.sql.Connection;
import java.util.regex.Pattern;

/**
 * Runs whole scripts against a given database connection.
 */
@Data
public class ScriptRunner {

    private static final String DEFAULT_DELIMITER = ";";
    private static final Pattern DELIMITER_RGX = Pattern.compile("^\\s*(--)?\\s*delimiter\\s*=?\\s*([^\\s]+)+\\s*.*$", Pattern.CASE_INSENSITIVE);

    private final Connection connection;
    private String delimiter = DEFAULT_DELIMITER;

    /**
     * Runs a given script, autocommit is off, and it stops on the first error.
     *
     * @param script script to run
     */
    @SneakyThrows
    public void run(String script) {
        val sr = new StringReader(script);
        StringBuffer command = null;
        val lineReader = new LineNumberReader(sr);
        String line;
        while ((line = lineReader.readLine()) != null) {
            if (command == null) command = new StringBuffer();
            String trimmedLine = line.trim();

            val delimMatch = DELIMITER_RGX.matcher(trimmedLine);
            if (delimMatch.matches()) delimiter = delimMatch.group(2);
            else if (trimmedLine.endsWith(delimiter)) {
                command.append(line.substring(0, line.lastIndexOf(getDelimiter())));
                command.append(" ");
                execCommand(command);
                command = null;
            } else {
                command.append(line);
                command.append("\n");
            }
        }
        if (command != null) execCommand(command);
    }

    @SneakyThrows
    private void execCommand(StringBuffer command) {
        val st = connection.createStatement();
        st.execute(command.toString());
    }
}
