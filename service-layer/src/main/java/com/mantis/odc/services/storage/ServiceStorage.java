/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.storage;

import com.google.common.cache.CacheBuilder;
import com.mantis.odc.database.dao.MappingDAO;
import com.mantis.odc.models.DatasourceMapping;
import com.mantis.odc.models.Mapping;
import com.mantis.odc.services.Service;
import com.mantis.odc.services.storage.base.BaseStorage;
import com.mantis.odc.services.validators.MappingValidator;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Manages access to all deployed services. A service is deployed on startup or on call.
 */
@Data
@Singleton
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public final class ServiceStorage extends BaseStorage<String, Service> {

    private final MappingDAO mappingDAO;
    private final DatasourceStorage datasourceStorage;
    private final MappingValidator validator;

    /**
     * @param mappingDAO        mapping dao
     * @param datasourceStorage handle to service data sources
     * @param validator         mapping validator
     */
    @Inject
    public ServiceStorage(MappingDAO mappingDAO,
                          DatasourceStorage datasourceStorage,
                          MappingValidator validator) {
        super(CacheBuilder.newBuilder().build());

        this.datasourceStorage = datasourceStorage;
        this.validator = validator;
        this.mappingDAO = mappingDAO;

        // auto load required services.
        mappingDAO.getDeployableMappings().forEach(this::deployMapping);
    }

    /**
     * Deploys the given named mapping. Returning a new service on success, or none if the service exists
     * or deployment fails.
     *
     * @param mappingName mapping name
     * @return service or none
     */
    public Optional<Service> deployMappingByName(String mappingName) {
        return mappingDAO.getMappingByName(mappingName).flatMap(this::deployMapping);
    }

    /**
     * Deploys the given mapping.
     *
     * @param mapping mapping.
     * @return service or none
     */
    public Optional<Service> deployMapping(Mapping mapping) {
        return Optional.of(mapping)
                .filter(mappingToTest -> validator.validate(mappingToTest).isSuccess())
                .map(mappingToDeploy -> {
                    Service service = Service.builder()
                            .name(mapping.getServiceName())
                            .mapping(mapping)
                            .datasource(datasourceStorage.get(mapping.getDatasource()))
                            .build();
                    set(service.getName(), service);
                    return service;
                });
    }

    /**
     * Un-deploys the given service.
     *
     * @param name service name
     * @return true if a success
     */
    public boolean undeployService(String name) {
        if (contains(name)) {
            cache.invalidate(name);
            return true;
        } else return false;
    }

    /**
     * Returns all the services linked to the given data source mapping.
     *
     * @param datasource data source mapping
     * @return list of services
     */
    public List<Service> dependentServices(DatasourceMapping datasource) {
        return listValues().stream()
                .filter(service -> service.getDatasource().getMapping().equals(datasource))
                .collect(Collectors.toList());
    }
}
