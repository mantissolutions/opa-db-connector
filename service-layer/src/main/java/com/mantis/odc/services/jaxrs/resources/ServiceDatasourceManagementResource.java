/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.resources;

import com.mantis.odc.models.DatasourceMapping;
import com.mantis.odc.services.Service;
import com.mantis.odc.services.ServiceDatasource;
import com.mantis.odc.services.jaxrs.converters.ServiceBeanMapper;
import com.mantis.odc.services.jaxrs.models.ServiceDatasourceBean;
import com.mantis.odc.services.storage.DatasourceStorage;
import com.mantis.odc.services.storage.ServiceStorage;
import io.swagger.annotations.*;
import lombok.val;
import org.apache.shiro.authz.annotation.RequiresAuthentication;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static com.mantis.odc.services.jaxrs.utils.ResponseUtils.*;
import static com.mantis.odc.utilities.ErrorUtil.errorToString;
import static java.text.MessageFormat.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

/**
 * Manages data sources deployed to ODC.
 */
@Singleton
@RequiresAuthentication
@Path("/service-datasource-management")
@Api(value = "Services Datasource Management", description = "Methods for managing deployed service datasources.")
public class ServiceDatasourceManagementResource {

    private final DatasourceStorage datasourceStorage;
    private final ServiceStorage serviceStorage;
    private final ServiceBeanMapper mapper;

    @Inject
    public ServiceDatasourceManagementResource(DatasourceStorage datasourceStorage, ServiceStorage serviceStorage, ServiceBeanMapper mapper) {
        this.datasourceStorage = datasourceStorage;
        this.serviceStorage = serviceStorage;
        this.mapper = mapper;
    }

    /**
     * @return deployed data source names
     */
    @GET
    @Produces(value = APPLICATION_JSON)
    @ApiOperation(value = "Service Datasource Names", notes = "List of deployed service datasources.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of deployed service datasource names.")
    })
    public List<String> getServiceNames() {
        return datasourceStorage.listValues().stream()
                .map(ServiceDatasource::getName)
                .collect(Collectors.toList());
    }

    /**
     * Deploys the named mapping.
     *
     * @param datasourceName mapping name
     * @return service bean
     */
    @GET
    @Path("/datasource/{mappingName}/deploy")
    @Produces(value = APPLICATION_JSON)
    @ApiOperation(value = "Deploy Datasource", notes = "Deploys a Datasource Mapping.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Service Datasource information."),
            @ApiResponse(code = 400,
                    message = "Thrown if the Connector cant find the Datasource Mapping, or failed to deploy it.",
                    response = String.class)
    })
    public ServiceDatasourceBean deployDatasource(@ApiParam(value = "Datasource Mapping name.")
                                                  @PathParam("datasourceName") String datasourceName) {
        return datasourceStorage.deployMappingByName(datasourceName)
                .map(mapper::serviceDatasourceToBean)
                .orElseThrow(() -> throwError(BAD_REQUEST, format("Failed to deploy datasource mapping {0}. Check that the datasource mapping exists, is valid and is not already deployed.", datasourceName)));
    }

    /**
     * Un-deploys the given service.
     *
     * @param name                      service name
     * @param undeployDependentServices flag to undeploy any dependent services
     * @return ok if success
     */
    @GET
    @Path("/{name}/un-deploy")
    @ApiOperation(value = "Un-Deploy Datasource", notes = "Un-Deploys a Service Datasource.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok message.", response = String.class),
            @ApiResponse(code = 400, message = "Thrown if the Connector fails to un-deploy a service datasource.",
                    response = String.class),
            @ApiResponse(code = 404, message = "Thrown on an unknown service datasource.", response = String.class),
    })
    public Response undeployService(@ApiParam(value = "Service Datasource name.")
                                    @PathParam("name") String name,
                                    @ApiParam(value = "Un-Deploy re.")
                                    @QueryParam("undeployDependentServices") boolean undeployDependentServices) {
        val datasourceOpt = datasourceStorage.getByNameOpt(name);
        if (!datasourceOpt.isPresent())
            throw throwError(NOT_FOUND, "Unknown Service Datasource" + name);

        val datasource = datasourceOpt.get().getMapping();

        val services = serviceStorage.dependentServices(datasource);
        if (!services.isEmpty() && !undeployDependentServices) {
            throw throwError(BAD_REQUEST, "The following deployed services depended on this datasource. Undeploy them first.\n" +
                    services.stream()
                            .map(Service::getName)
                            .collect(Collectors.joining("\n")));
        } else services.forEach(service -> serviceStorage.undeployService(service.getName()));

        return datasourceStorage.undeployServiceDatasource(datasource) ? ok() : error(BAD_REQUEST, format("Failed to un-deploy service datasource {0}. Check that a service datasource with the current name is deployed.", name));
    }

    /**
     * Tests the service data source with the given username and password.
     *
     * @param name     data source name
     * @param username user name
     * @param password user password
     * @return result of test
     */
    @GET
    @Path("/{name}/test")
    @ApiOperation(value = "Test Datasource", notes = "Tests a Service Datasource.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Test result.", response = String.class),
            @ApiResponse(code = 404, message = "Thrown on an unknown service datasource.", response = String.class)
    })
    public String testDatasource(@PathParam("name") String name,
                                 @QueryParam("username") String username,
                                 @QueryParam("password") String password) {
        return datasourceStorage.getByNameOpt(name)
                .map(serviceDatasource -> {
                    DatasourceMapping mapping = serviceDatasource.getMapping();
                    try {
                        mapping.test(username, password);
                        return "Connection Successful";
                    } catch (ClassNotFoundException | SQLException e) {
                        return format("Connection Failed: {0}\n{1}", e.getMessage(), errorToString(e));
                    }
                })
                .orElseThrow(() -> throwError(NOT_FOUND, format("Unknown service datasource {0}.", name)));
    }
}
