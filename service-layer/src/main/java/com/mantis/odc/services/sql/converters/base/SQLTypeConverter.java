/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.sql.converters.base;

import com.mantis.odc.models.DataType;
import com.mantis.odc.services.sql.converters.ANSISQLTypeConverter;
import lombok.NonNull;

import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.sql.*;
import java.util.TimeZone;

/**
 * Interface which defines the conversion of data types from SQL types to java/XML types.
 *
 * @see ANSISQLTypeConverter for base implementation
 */
public interface SQLTypeConverter {

    /**
     * Converts a given object from its sql type, to its java type.
     *
     * @param sqlValue sql value to convert
     * @param type     type to convert
     * @param timeZone current timezone of the operation
     * @return converted java type
     */
    default Object convertFromSQLType(Object sqlValue, DataType type, TimeZone timeZone) throws SQLException {
        if (sqlValue == null) return null;

        switch (type) {
            case STRING_TYPE:
                return convertToString(sqlValue);
            case DATE_TIME_TYPE:
                return convertFromSQLTimestamp(sqlValue, timeZone);
            case BOOLEAN_TYPE:
                return convertToBoolean(sqlValue);
            case DATE_TYPE:
                return convertFromSQLDate(sqlValue, timeZone);
            case DECIMAL_TYPE:
                return convertToNumber(sqlValue);
            case TIME_TYPE:
                return convertFromSQLTime(sqlValue, timeZone);
            case ATTACHMENT_TYPE:
                return convertToByteArray(sqlValue);
        }
        throw new SQLException(String.format("Cannot convert from SQL type, %s, to OPA type %s",
                sqlValue.getClass().getSimpleName(), type.name()));
    }

    /**
     * Converts a string, number and date types to a gregorian calendar.
     *
     * @param sqlValue sql value
     * @param timeZone timezone to use
     * @return gregorian calendar
     * @throws SQLException on conversion error
     */
    XMLGregorianCalendar convertFromSQLTime(@NonNull Object sqlValue, TimeZone timeZone) throws SQLException;

    /**
     * Converts a string, number and date types to a gregorian calendar.
     *
     * @param sqlValue sql value
     * @param timeZone timezone to use
     * @return gregorian calendar
     * @throws SQLException on conversion error
     */
    XMLGregorianCalendar convertFromSQLTimestamp(@NonNull Object sqlValue, TimeZone timeZone) throws SQLException;

    /**
     * Converts a string, number and date types to a gregorian calendar.
     *
     * @param sqlValue sql value
     * @param timeZone timezone to use
     * @return gregorian calendar
     * @throws SQLException on conversion error
     */
    XMLGregorianCalendar convertFromSQLDate(@NonNull Object sqlValue, TimeZone timeZone) throws SQLException;

    /**
     * Converts a given object from its java type, to its SQL type.
     *
     * @param javaValue java value
     * @param type      type to convert
     * @return converted sql type
     */
    default Object convertToSQLType(Object javaValue, DataType type) throws SQLException {
        if (javaValue == null) return null;

        switch (type) {
            case STRING_TYPE:
                return convertToString(javaValue);
            case DATE_TIME_TYPE:
                return convertToSQLDateTime(javaValue);
            case BOOLEAN_TYPE:
                return convertToBoolean(javaValue);
            case DATE_TYPE:
                return convertToSQLDate(javaValue);
            case DECIMAL_TYPE:
                return convertToNumber(javaValue);
            case TIME_TYPE:
                return convertToSQLTime(javaValue);
            case ATTACHMENT_TYPE:
                return convertToByteArray(javaValue);
        }
        throw new SQLException(String.format("Cannot convert from Java type, %s, to OPA type %s",
                javaValue.getClass().getSimpleName(), type.name()));
    }

    /**
     * Converts a string, number and date to a SQL time.
     *
     * @param javaValue java value.
     * @return time
     * @throws SQLException on conversion error
     */
    Time convertToSQLTime(@NonNull Object javaValue) throws SQLException;

    /**
     * Converts a string, number and date to a SQL date.
     *
     * @param javaValue java value.
     * @return date
     * @throws SQLException on conversion error
     */
    Date convertToSQLDate(@NonNull Object javaValue) throws SQLException;

    /**
     * Converts a string, number and date to a SQL timestamp.
     *
     * @param javaValue java value.
     * @return timestamp
     * @throws SQLException on conversion error
     */
    Timestamp convertToSQLDateTime(@NonNull Object javaValue) throws SQLException;

    /* PRIMITIVE CONVERTERS */

    /**
     * @param value object
     * @return string
     * @throws SQLException on conversion error
     */
    default String convertToString(@NonNull Object value) throws SQLException {
        if (value instanceof Clob) return ((Clob) value).getSubString(1, (int) ((Clob) value).length());
        if (value instanceof String) return (String) value;
        return value.toString();
    }

    /**
     * Converts an object, either as a number or a string to a boolean.
     *
     * @param value value to convert
     * @return boolean
     * @throws SQLException on conversion error
     */
    default boolean convertToBoolean(@NonNull Object value) throws SQLException {
        if (value instanceof Boolean) return (boolean) value;
        if (value instanceof Number) return ((Number) value).intValue() == 1;
        if (value instanceof String) return Boolean.parseBoolean(convertToString(value));
        else throw new SQLException(String.format("Cannot convert %s to boolean", value.getClass().getSimpleName()));
    }

    /**
     * Converts an object, either a number or a string to a BigDecimal
     *
     * @param value value to convert
     * @return big decimal
     * @throws SQLException on conversion error
     */
    default BigDecimal convertToNumber(@NonNull Object value) throws SQLException {
        if (value instanceof BigDecimal) return (BigDecimal) value;
        if (value instanceof Number) return new BigDecimal(((Number) value).doubleValue());
        if (value instanceof String) return new BigDecimal((String) value);
        else throw new SQLException(String.format("Cannot convert %s to BigDecimal", value.getClass().getSimpleName()));
    }

    /**
     * Converts a string or blob to a byte array.
     *
     * @param sqlValue sql value
     * @return byte array
     * @throws SQLException on conversion error
     */
    default byte[] convertToByteArray(@NonNull Object sqlValue) throws SQLException {
        if (sqlValue instanceof byte[]) return (byte[]) sqlValue;
        if (sqlValue instanceof String) return DatatypeConverter.parseBase64Binary((String) sqlValue);
        else if (sqlValue instanceof Blob) return ((Blob) sqlValue).getBytes(1, (int) ((Blob) sqlValue).length());
        throw new SQLException(String.format("Failed to convert %s to byte[]", sqlValue.getClass().getSimpleName()));
    }
}
