/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters._12_2_9;

import com.mantis.odc.models.*;
import com.mantis.odc.services.converters._12_2_2.OPA1222Converter;
import com.mantis.odc.services.converters._12_2_5.OPA1225Converter;
import com.mantis.odc.services.converters.base.OPAConverter;
import com.mantis.odc.services.converters.base.SharedConverters;
import com.mantis.odc.services.converters.exceptions.MissingGlobalRowException;
import com.mantis.odc.services.converters.exceptions.MissingMappingException;
import com.mantis.odc.services.models.*;
import com.mantis.odc.services.models.base.ActionModel;
import com.mantis.odc.services.models.base.DatabaseTable;
import com.oracle.opa.connector._12_2_9.data.types.*;
import com.oracle.opa.connector._12_2_9.metadata.types.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.val;
import org.jooq.lambda.Unchecked;

import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mantis.odc.models.DataType.ATTACHMENT_TYPE;
import static com.mantis.odc.models.DataType.STRING_TYPE;
import static com.mantis.odc.services.converters.base.SharedConverters.*;
import static com.mantis.odc.services.converters.exceptions.MissingMappingException.MappingType.Enumeration;
import static com.mantis.odc.services.converters.exceptions.MissingMappingException.MappingType.Table;
import static com.mantis.odc.services.models.DBActionType.insert;
import static com.mantis.odc.utilities.ErrorUtil.errorToString;
import static java.lang.String.format;

/**
 * Converter for OPA Connector version 12.2.9.
 */
@Data
public final class OPA1229Converter implements OPAConverter<
        CheckAliveResponse,
        GetMetadataResponse,
        LoadRequest, LoadResponse,
        SaveRequest, SaveResponse,
        GetCheckpointRequest, GetCheckpointResponse,
        SetCheckpointRequest, SetCheckpointResponse,
        ExecuteQueryRequest, ExecuteQueryResponse,
        RequestFault> {

    /**
     * Provides simple converts for OPA 12.2.2.
     */
    private final OPA1229AutoConverter autoConverter;

    /**
     * Child converters.
     */
    private final OPA1222Converter opa1222Converter;
    private final OPA1225Converter opa1225Converter;
    private final com.oracle.opa.connector._12_2_9.metadata.types.ObjectFactory objectFactory;

    public OPA1229Converter() {
        autoConverter = new OPA1229AutoConverterImpl();
        opa1222Converter = new OPA1222Converter();
        opa1225Converter = new OPA1225Converter();
        objectFactory = new com.oracle.opa.connector._12_2_9.metadata.types.ObjectFactory();
    }

    @Override
    public CheckAliveResponse getCheckAlive() {
        return new CheckAliveResponse();
    }

    @Override
    public RequestFault getFault(Exception e) {
        val fault = new RequestFault();
        fault.setMessage(format("%s\nStack Trace:\n%s", e.getMessage(), errorToString(e)));
        return fault;
    }

    @Override
    public GetMetadataResponse getMetaData(Mapping mapping, List<DatabaseEnumeration> enumerations) {
        val resp = new GetMetadataResponse();
        resp.setSupportsCheckpoints(mapping.isSupportsCheckpoints());
        resp.getTable().addAll(mapping.getTables().stream()
                .map(this::createMetaTable)
                .collect(Collectors.toList()));
        if (!enumerations.isEmpty()) {
            val enums = new MetaEnumList();
            enums.getNumberEnumerationOrStringEnumerationOrDateEnumeration().addAll(enumerations.stream()
                    .map(databaseEnumeration -> createMetaEnumeration(mapping, databaseEnumeration))
                    .collect(Collectors.toList()));
            resp.setMetadataEnumerations(enums);
        }
        return resp;
    }

    /**
     * Creates an enumeration.
     *
     * @param mapping             mapping
     * @param databaseEnumeration database enumeration
     * @return enumeration
     */
    @SneakyThrows
    public Object createMetaEnumeration(Mapping mapping, DatabaseEnumeration databaseEnumeration) {
        val enumMapping = mapping.getEnumeration(databaseEnumeration.getName())
                .orElseThrow(() -> new MissingMappingException(Enumeration, databaseEnumeration.getName()));
        Class enumType;
        Class enumValueType;
        switch (databaseEnumeration.getType()) {
            case StringEnumeration:
                enumType = StringEnumeration.class;
                enumValueType = StringEnumVal.class;
                break;
            case NumberEnumeration:
                enumType = NumberEnumeration.class;
                enumValueType = NumberEnumVal.class;
                break;
            case DateEnumeration:
                enumType = DateEnumeration.class;
                enumValueType = DateEnumVal.class;
                break;
            case DateTimeEnumeration:
                enumType = DateTimeEnumeration.class;
                enumValueType = DateTimeEnumVal.class;
                break;
            case TimeEnumeration:
                enumType = TimeEnumeration.class;
                enumValueType = TimeEnumVal.class;
                break;
            case BooleanEnumeration:
                enumType = BooleanEnumeration.class;
                enumValueType = BooleanEnumVal.class;
                break;
            default:
                return null;
        }
        return opa1222Converter.createEnum(
                enumType,
                enumValueType,
                UncertainEnumVal.class,
                enumMapping,
                databaseEnumeration,
                ChildEnumValues.class,
                (serializables, type) -> {
                    if (serializables != null && !serializables.isEmpty()) {
                        val children = new ChildEnumValues();
                        children.getNumberValueOrStringValueOrDateValue()
                                .addAll(serializables.stream()
                                        .map(v -> {
                                            switch (type) {
                                                case StringEnumeration:
                                                    return objectFactory.createChildEnumValuesStringValue((String) v);
                                                case NumberEnumeration:
                                                    return objectFactory.createChildEnumValuesNumberValue((BigDecimal) v);
                                                case DateEnumeration:
                                                    return objectFactory.createChildEnumValuesDateValue((XMLGregorianCalendar) v);
                                                case DateTimeEnumeration:
                                                    return objectFactory.createChildEnumValuesDateTimeValue((XMLGregorianCalendar) v);
                                                case TimeEnumeration:
                                                    return objectFactory.createChildEnumValuesTimeValue((XMLGregorianCalendar) v);
                                                case BooleanEnumeration:
                                                default:
                                                    return objectFactory.createChildEnumValuesBooleanValue((Boolean) v);
                                            }
                                        })
                                        .collect(Collectors.toList()));
                        return children;
                    } else return null;
                });
    }

    /**
     * Creates a meta table from the table mapping.
     *
     * @param table table mapping
     * @return meta table
     */
    private MetaTable createMetaTable(TableMapping table) {
        val metaTable = new MetaTable();
        metaTable.setName(table.getTableId().getName());
        metaTable.setAcceptsAttachments(table.getMapping().isSupportsAttachments());
        metaTable.setCanBeInput(table.canBeInput());
        metaTable.setCanBeOutput(table.canBeOutput());
        metaTable.getField().addAll(table.getFields().stream()
                .filter(field -> (field.isInput() || field.isOutput()) && !field.isForeignKey())
                .map(this::createMetaField)
                .collect(Collectors.toList()));
        metaTable.getLink().addAll(Stream.concat(
                table.getChildren().stream()
                        .map(child -> createMetaLink(
                                child.getLinkName(),
                                child.getTableId().getName(),
                                (child.isOneToOne()) ? MetaLinkCardinality.C_11 : MetaLinkCardinality.C_1_M)),
                table.getJunctionTables().stream()
                        .map(junctionTable -> createMetaLink(
                                junctionTable.getLinkName(),
                                junctionTable.getTargetName(table.getTableId()),
                                MetaLinkCardinality.fromValue(junctionTable.getLinkType(table).getValue())))
        )
                .collect(Collectors.toList()));
        return metaTable;
    }

    /**
     * Creates a meta field from the field mapping.
     *
     * @param fieldMapping field mapping
     * @return meta field
     */
    private MetaField createMetaField(FieldMapping fieldMapping) {
        val metaField = new MetaField();
        metaField.setName(fieldMapping.getFieldId().getName());
        metaField.setCanBeOutput(fieldMapping.isOutput());
        metaField.setIsRequired(fieldMapping.isRequired());
        metaField.setCanBeInput(fieldMapping.isInput());
        if (fieldMapping.getEnumeration() != null)
            metaField.setEnumType(fieldMapping.getEnumeration().getName());
        else
            metaField.setType(autoConverter.createMetaFieldDataType(fieldMapping.getType()));
        return metaField;
    }

    /**
     * Creates a meta link from the given parent and child table mappings.
     *
     * @param name        name of the relationship
     * @param target      target of the relationship
     * @param cardinality link cardinality
     * @return meta link
     */
    private MetaLink createMetaLink(String name, String target, MetaLinkCardinality cardinality) {
        val metaLink = new MetaLink();
        metaLink.setName(name);
        metaLink.setTarget(target);
        metaLink.setCardinality(cardinality);
        return metaLink;
    }

    /**
     * Creates a load model from a load request.
     *
     * @param mapping       mapping
     * @param root          root model name
     * @param language      language
     * @param timezone      timezone
     * @param tables        list of tables
     * @param requestParams map of request params
     * @return load model
     */
    @SneakyThrows
    public LoadModel createLoadModel(Mapping mapping,
                                     String root,
                                     String language,
                                     String timezone,
                                     List<Table> tables,
                                     Map<String, String> requestParams) {
        val rootTable = mapping.getTable(root).orElseThrow(() -> new MissingMappingException(Table, root));

        // get containment tables
        Stream<DatabaseTable> containmentTables = tables.stream()
                .map(Unchecked.function(table -> createContainmentTable(mapping, table.getName(), table.getField().stream()
                        .map(Field::getName)
                        .collect(Collectors.toList()))));

        // get junction tables
        Stream<DatabaseTable> junctionTables = tables.stream()
                .flatMap(table -> table.getLink().stream()).distinct()
                .flatMap(link -> mapping.getJunctionTable(link.getName())
                        .map(junctionTableMapping -> createJunctionTable(junctionTableMapping, link.getTarget()))
                        .map(Stream::of).orElse(Stream.empty()));

        return LoadModel.builder()
                .locale(Locale.forLanguageTag(language))
                .timeZone(TimeZone.getTimeZone(timezone))
                .globalId(Optional.ofNullable(requestParams.get(mapping.getBindToken())).map(RowId::new).orElse(null))
                .rootTable(rootTable.getTableId().getSource())
                .preScript(createScript(mapping.getPreLoadScript(), requestParams))
                .postScript(createScript(mapping.getPostLoadScript(), requestParams))
                .tables(Stream.concat(containmentTables, junctionTables)
                        .collect(Collectors.toList()))
                .conditions(mapping.getBinds().stream()
                        .flatMap(bindMapping -> Optional.ofNullable(requestParams.get(bindMapping.getTokenName()))
                                .map(v -> Stream.of(createBindCondition(v, bindMapping)))
                                .orElse(Stream.empty()))
                        .collect(Collectors.toList()))
                .build();
    }

    @Override
    public LoadModel toLoadModel(Mapping mapping, LoadRequest loadRequest) {
        return createLoadModel(mapping,
                loadRequest.getRoot(),
                loadRequest.getLanguage(),
                loadRequest.getTimezone(),
                loadRequest.getTables().getTable(),
                asMap(loadRequest.getRequestContext()));
    }

    private Map<String, String> asMap(RequestContext requestContext) {
        val map = new HashMap<String, String>();
        if (requestContext != null) for (val p : requestContext.getParameter()) map.put(p.getName(), p.getValue());
        return map;
    }

    /**
     * Returns the load response
     *
     * @param model load model
     * @return load data
     */
    public LoadData getLoadResponseData(LoadModel model) {
        val loadData = new LoadData();
        loadData.getTable().addAll(model.getContainmentTables().stream()
                .filter(table -> !table.getRows().isEmpty())
                .map(table -> createDataTable(model, table))
                .collect(Collectors.toList()));
        return loadData;
    }

    /**
     * Creates an OPA data table from a containment table.
     *
     * @param model load model
     * @param table containment table
     * @return OPA data table
     */
    private DataTable createDataTable(LoadModel model, ContainmentTable table) {
        val dataTable = new DataTable();
        dataTable.setName(table.getOpaName());
        dataTable.getRow().addAll(table.getRows().stream()
                .map(dataRow -> createDataRow(model, dataRow))
                .collect(Collectors.toList()));
        return dataTable;
    }

    /**
     * Creates an OPA data row from the given database row.
     *
     * @param model load model
     * @param row   database row
     * @return OPA data row
     */
    private DataRow createDataRow(LoadModel model, DatabaseRow row) {
        return createDataRowFromRow(model, row, new DataRow());
    }

    /**
     * Creates an OPA data row from the given database row.
     *
     * @param model   load model
     * @param row     database row
     * @param dataRow existing row
     * @return OPA data row
     */
    private DataRow createDataRowFromRow(ActionModel model, DatabaseRow row, DataRow dataRow) {
        dataRow.setId(row.getId().getKeyAsString());
        dataRow.getField().addAll(row.getData().stream()
                .map(this::createDataField)
                .collect(Collectors.toList()));
        if (!(dataRow instanceof UpdateRow)) {
            dataRow.getLink().addAll(model.getLinks(row).stream()
                    .filter(databaseLink -> !databaseLink.getRefs().isEmpty())
                    .map(autoConverter::createDataLink)
                    .collect(Collectors.toList()));
        }
        return dataRow;
    }

    /**
     * Creates a data field from a row value and row instance.
     *
     * @param rowValue row value
     * @return OPA data field
     */
    private DataField createDataField(RowValue rowValue) {
        val dataField = new DataField();
        dataField.setName(rowValue.getOpaName());
        if (rowValue.getValue() != null) switch (rowValue.getType()) {
            case STRING_TYPE:
                dataField.setTextVal((String) rowValue.getValue());
                break;
            case DATE_TIME_TYPE:
                dataField.setDatetimeVal((XMLGregorianCalendar) rowValue.getValue());
                break;
            case BOOLEAN_TYPE:
                dataField.setBooleanVal((Boolean) rowValue.getValue());
                break;
            case DATE_TYPE:
                dataField.setDateVal((XMLGregorianCalendar) rowValue.getValue());
                break;
            case DECIMAL_TYPE:
                dataField.setNumberVal((BigDecimal) rowValue.getValue());
                break;
            case TIME_TYPE:
                dataField.setTimeVal((XMLGregorianCalendar) rowValue.getValue());
                break;
        }
        else dataField.setUnknownVal(new UnknownValue());
        return dataField;
    }

    @Override
    public LoadResponse fromLoadModel(LoadModel model) {
        val resp = new LoadResponse();
        resp.setTables(getLoadResponseData(model));
        return resp;
    }

    /**
     * Creates a save model from a save request.
     *
     * @param mapping       mapping
     * @param root          root submit table name
     * @param language      language code
     * @param timezone      timezone code
     * @param tables        list of submit tables
     * @param attachments   list of attachments
     * @param requestParams map of request params
     * @return save model
     */
    @SneakyThrows
    public SaveModel createSaveModel(Mapping mapping,
                                     String root,
                                     String language,
                                     String timezone,
                                     List<SubmitTable> tables,
                                     List<Attachment> attachments,
                                     Map<String, String> requestParams) {
        val rootTable = mapping.getTable(root).orElseThrow(() -> new MissingMappingException(Table, root));
        val globalId = Optional.ofNullable(requestParams.get(mapping.getBindToken()))
                .map(RowId::new)
                .orElse(getRootRowId(root, tables));

        // get containment tables
        Stream<DatabaseTable> containmentTables = tables.stream().map(table -> createTablesFromSubmit(mapping, table));

        // get junction tables
        val addedJunctionTables = new HashSet<String>();
        Stream<DatabaseTable> junctionTables = tables.stream()
                .flatMap(submitTable -> submitTable.getRow().stream())
                .flatMap(submitRow -> submitRow.getLink().stream())
                .flatMap(link -> (addedJunctionTables.add(link.getName())) ? mapping.getJunctionTable(link.getName())
                        .map(junctionTableMapping -> createJunctionTable(junctionTableMapping, link.getTarget()))
                        .map(Stream::of).orElse(Stream.empty()) : Stream.empty());

        // get attachments
        Stream<DatabaseTable> attachmentTable = (mapping.isSupportsAttachments()) ?
                Stream.of(createAttachmentTable(mapping, rootTable.getTableId().getSource(), globalId, tables, attachments)) :
                Stream.empty();

        // build links
        val saveModel = SaveModel.builder()
                .locale(Locale.forLanguageTag(language))
                .timeZone(TimeZone.getTimeZone(timezone))
                .globalId(globalId)
                .rootTable(rootTable.getTableId().getSource())
                .preScript(createScript(mapping.getPreSaveScript(), requestParams))
                .postScript(createScript(mapping.getPostSaveScript(), requestParams))
                .tables(Stream.concat(containmentTables, Stream.concat(junctionTables, attachmentTable))
                        .collect(Collectors.toList()))
                .build();

        // build links
        tables.stream()
                .flatMap(submitTable -> submitTable.getRow().stream())
                .forEach(Unchecked.consumer(submitRow -> {
                    String id = submitRow.getId();
                    for (DataLink link : submitRow.getLink()) {
                        Optional<JunctionTable> junctionTableOptional = saveModel.getJunctionTable(link.getName());

                        // add a row for the junction table
                        if (junctionTableOptional.isPresent()) {
                            JunctionTable junctionTable = junctionTableOptional.get();
                            // this might be a table in the current saveButton model,
                            // or a table in the wider mapping.
                            OPAMappingId tableKey = mapping.getTable(link.getTarget())
                                    .orElseThrow(() -> new MissingMappingException(Table, link.getTarget())).getTableId();

                            ForeignKeyField rowKey = junctionTable.getDestinationKey(tableKey.getSource());
                            ForeignKeyField refKey = junctionTable.getTargetKey(tableKey.getSource());
                            link.getRef().forEach(linkRef ->
                                    junctionTable.getRows().add(DatabaseSubmitRow.submitRowBuilder()
                                            .source(junctionTable.getSource())
                                            .action(insert)
                                            .item(new RowValue(rowKey.getSource(), rowKey.getOpaName(), rowKey.getType(), id))
                                            .item(new RowValue(refKey.getSource(), refKey.getOpaName(), refKey.getType(), linkRef.getId()))
                                            .build()));
                        } else { // add foreign key value
                            // this is always a containment table
                            ContainmentTable targetTable = (ContainmentTable) saveModel.getTableByOpaName(link.getTarget())
                                    .orElseThrow(() -> new MissingMappingException(Table, link.getTarget()));
                            link.getRef().forEach(linkRef -> targetTable.getRows().stream()
                                    .filter(databaseRow -> databaseRow.getId().getOpaKeyAsString().equals(linkRef.getId()))
                                    .forEach(databaseRow -> databaseRow.setFk(new RowId(id, null))));
                        }
                    }
                }));

        return saveModel;
    }

    /**
     * Returns the root row id.
     *
     * @param root   root table name
     * @param tables submit tables
     * @return root row id
     */
    @SneakyThrows
    private RowId getRootRowId(String root, List<SubmitTable> tables) {
        return tables.stream()
                .filter(submitTable -> submitTable.getName().equals(root))
                .map(submitTable -> new RowId(submitTable.getRow().get(0).getId(), null))
                .findFirst()
                .orElseThrow(MissingGlobalRowException::new);
    }

    /**
     * Creates an attachment table.
     *
     * @param mapping     mapping
     * @param rootSource  root table source
     * @param globalId    global row id
     * @param tables      list of submit tables
     * @param attachments list of attachments
     * @return attachment table
     */
    private AttachmentTable createAttachmentTable(Mapping mapping,
                                                  String rootSource,
                                                  RowId globalId,
                                                  List<SubmitTable> tables,
                                                  List<Attachment> attachments) {
        val attachmentMapping = mapping.getAttachment();

        // get global attachments
        Stream<AttachmentRow> globalAttachments = (attachments != null && !attachments.isEmpty()) ?
                attachments.stream().map(attachment -> createAttachmentRow(attachmentMapping, rootSource, attachment, globalId)) : Stream.empty();

        // get child table attachments
        Stream<AttachmentRow> childAttachments = tables.stream()
                .flatMap(submitTable -> {
                    TableMapping tableMapping = mapping.getTable(submitTable.getName()).get();
                    return submitTable.getRow().stream()
                            .filter(submitRow -> submitRow.getAttachments() != null && !submitRow.getAttachments().getAttachment().isEmpty())
                            .flatMap(submitRow -> submitRow.getAttachments().getAttachment().stream()
                                    .map(attachment -> createAttachmentRow(attachmentMapping, tableMapping.getTableId().getSource(), attachment, new RowId(submitRow.getId(), null))));
                });

        return AttachmentTable.builder()
                .source(attachmentMapping.getTableName())
                .opaName(attachmentMapping.getTableName())
                .sourceField(attachmentMapping.getTableField())
                .keyField(attachmentMapping.getKeyField())
                .nameField(attachmentMapping.getNameField())
                .fileNameField(attachmentMapping.getFileNameField())
                .descriptionField(attachmentMapping.getDescriptionField())
                .fileField(attachmentMapping.getFileField())
                .attachmentRows(Stream.concat(globalAttachments, childAttachments)
                        .collect(Collectors.toList()))
                .build();
    }

    /**
     * Creates an attachment row.
     *
     * @param attachmentMapping attachment table mapping
     * @param source            global table source
     * @param attachment        attachment
     * @param id                global id
     * @return attachment row
     */
    private AttachmentRow createAttachmentRow(AttachmentMapping attachmentMapping, String source, Attachment attachment, RowId id) {
        return AttachmentRow.attachmentRowBuilder()
                .id(id)
                .table(RowValue.builder()
                        .source(attachmentMapping.getTableField())
                        .opaName(attachmentMapping.getTableField())
                        .type(STRING_TYPE)
                        .value(source)
                        .build())
                .name(RowValue.builder()
                        .source(attachmentMapping.getNameField())
                        .opaName(attachmentMapping.getNameField())
                        .type(STRING_TYPE)
                        .value(attachment.getName())
                        .build())
                .fileName(RowValue.builder()
                        .source(attachmentMapping.getFileNameField())
                        .opaName(attachmentMapping.getFileNameField())
                        .type(STRING_TYPE)
                        .value(attachment.getFilename())
                        .build())
                .description(RowValue.builder()
                        .source(attachmentMapping.getDescriptionField())
                        .opaName(attachmentMapping.getDescriptionField())
                        .type(STRING_TYPE)
                        .value(attachment.getDescription())
                        .build())
                .attachmentData(RowValue.builder()
                        .source(attachmentMapping.getFileField())
                        .opaName(attachmentMapping.getFileField())
                        .type(ATTACHMENT_TYPE)
                        .value(attachment.getValue())
                        .build())
                .build();
    }

    /**
     * Creates a containment table from an OPA submit table.
     *
     * @param mapping mapping
     * @param table   OPA submit table
     * @return containment table
     */
    @SneakyThrows
    private ContainmentTable createTablesFromSubmit(Mapping mapping, SubmitTable table) {
        val tableMapping = mapping.getTable(table.getName())
                .orElseThrow(() -> new MissingMappingException(Table, table.getName()));
        return ContainmentTable.builder()
                .source(tableMapping.getTableId().getSource())
                .opaName(tableMapping.getTableId().getName())
                .linkName(tableMapping.getLinkName())
                .alias(tableMapping.getAlias())
                .inferred(tableMapping.isInferred())
                .primaryKey(createPrimaryKey(tableMapping.getPrimaryKey()))
                .foreignKey(tableMapping.getForeignKeyOpt()
                        .map(fk -> createForeignKey(tableMapping.getParent().getTableId(), fk))
                        .orElse(null))
                .fields(tableMapping.getFields().stream()
                        .filter(fieldMapping -> !(fieldMapping.isPrimaryKey() || fieldMapping.isForeignKey()))
                        .map(Unchecked.function(fieldMapping -> SharedConverters.createDataField(tableMapping, fieldMapping.getFieldId().getName())))
                        .collect(Collectors.toList()))
                .rows(table.getRow().stream()
                        .map(submitRow -> createDatabaseSubmitRow(tableMapping, submitRow))
                        .collect(Collectors.toList()))
                .build();
    }

    /**
     * Creates a database submit row from an OPA submit row.
     *
     * @param mapping   table mapping
     * @param submitRow OPA submit row
     * @return database submit row
     */
    private DatabaseSubmitRow createDatabaseSubmitRow(TableMapping mapping, SubmitRow submitRow) {
        return DatabaseSubmitRow.submitRowBuilder()
                .source(mapping.getTableId().getSource())
                .action(autoConverter.createDBAction(submitRow.getAction()))
                .id(new RowId(submitRow.getId(), null))
                .data(submitRow.getInputField().stream()
                        .map(dataField -> createRowValue(mapping, dataField))
                        .collect(Collectors.toList()))
                .returnFields(submitRow.getOutputField().stream()
                        .map(dataField -> createOutputField(mapping, dataField))
                        .collect(Collectors.toList()))
                .build();
    }

    /**
     * Creates a row value from the given OPA data field.
     *
     * @param mapping   table mapping
     * @param dataField OPA data field
     * @return row value
     */
    @SneakyThrows
    private RowValue createRowValue(TableMapping mapping, DataField dataField) {
        val fieldMapping = mapping.getField(dataField.getName())
                .orElseThrow(() -> new MissingMappingException(MissingMappingException.MappingType.Field, dataField.getName()));
        val builder = RowValue.builder()
                .source(fieldMapping.getFieldId().getSource())
                .opaName(fieldMapping.getFieldId().getName())
                .type(fieldMapping.getType());

        switch (fieldMapping.getType()) {
            case STRING_TYPE:
                builder.value(dataField.getTextVal());
                break;
            case DATE_TIME_TYPE:
                builder.value(dataField.getDatetimeVal());
                break;
            case BOOLEAN_TYPE:
                builder.value(dataField.isBooleanVal());
                break;
            case DATE_TYPE:
                builder.value(dataField.getDateVal());
                break;
            case DECIMAL_TYPE:
                builder.value(dataField.getNumberVal());
                break;
            case TIME_TYPE:
                builder.value(dataField.getTimeVal());
                break;
        }

        return builder.build();
    }

    /**
     * Creates an output field from the given OPA data field.
     *
     * @param mapping   table mapping
     * @param dataField data field
     * @return output field
     */
    @SneakyThrows
    private DatabaseField createOutputField(TableMapping mapping, DataField dataField) {
        val fieldMapping = mapping.getField(dataField.getName())
                .orElseThrow(() -> new MissingMappingException(MissingMappingException.MappingType.Field, dataField.getName()));
        return DatabaseField.fieldBuilder()
                .source(fieldMapping.getFieldId().getSource())
                .opaName(fieldMapping.getFieldId().getName())
                .type(fieldMapping.getType())
                .build();
    }

    @Override
    public SaveModel toSaveModel(Mapping mapping, SaveRequest saveRequest) {
        return createSaveModel(mapping,
                saveRequest.getRoot(),
                saveRequest.getLanguage(),
                saveRequest.getTimezone(),
                saveRequest.getTables().getTable(),
                new ArrayList<>(),
                asMap(saveRequest.getRequestContext()));
    }

    /**
     * Returns the update data.
     *
     * @param model save model
     * @return update data
     */
    public UpdateData getUpdateData(SaveModel model) {
        val data = new UpdateData();
        data.getTable().addAll(model.getContainmentTables().stream()
                .filter(table -> !table.isInferred() && !table.getRows().isEmpty())
                .map((table) -> createUpdateTable(model, table))
                .collect(Collectors.toList()));
        return data;
    }

    /**
     * Creates an OPA update table from containment table.
     *
     * @param model saveButton model
     * @param table containment table
     * @return OPA update table
     */
    private UpdateTable createUpdateTable(SaveModel model, ContainmentTable table) {
        val updateTable = new UpdateTable();
        updateTable.setName(table.getOpaName());
        updateTable.getRow().addAll(table.getRows().stream()
                .map((databaseRow) -> createUpdateRow(model, databaseRow))
                .collect(Collectors.toList()));
        return updateTable;
    }

    /**
     * Creates an OPA update row from the given database row.
     *
     * @param model       saveButton model
     * @param databaseRow database row
     * @return OPA update row
     */
    private UpdateRow createUpdateRow(SaveModel model, DatabaseRow databaseRow) {
        val updateRow = (UpdateRow) createDataRowFromRow(model, databaseRow, new UpdateRow());
        if (databaseRow.getId().hasNewKey()) updateRow.setOrigId(databaseRow.getId().getOpaKeyAsString());
        return updateRow;
    }

    @Override
    public SaveResponse fromSaveModel(SaveModel model) {
        val resp = new SaveResponse();
        resp.setTables(getUpdateData(model));
        return resp;
    }

    /**
     * Finds and returns the given request param.
     *
     * @param key key to search for
     * @param ctx context.
     * @return value or none
     */
    private Optional<String> getValueFromContext(String key, RequestContext ctx) {
        return (ctx != null) ? ctx.getParameter().stream()
                .filter(p -> p.getName().equals(key))
                .map(RequestContextParam::getValue)
                .findFirst() :
                Optional.empty();
    }

    @Override
    public CheckpointModel toCheckpointModelFromGetCheckpointRequest(Mapping mapping, GetCheckpointRequest req) {
        val checkpointMapping = mapping.getCheckpoint();
        return CheckpointModel.builder()
                .id(getValueFromContext(checkpointMapping.getBindToken(), req.getRequestContext()).orElse(null))
                .checkpointTable(opa1225Converter.createCheckpointTable(checkpointMapping))
                .build();
    }

    @Override
    public GetCheckpointResponse fromCheckpointModelToGetCheckpointResponse(CheckpointModel model) {
        val resp = new GetCheckpointResponse();
        val data = new CheckpointData();
        data.setCheckpointId(model.getId());
        data.setValue(DatatypeConverter.printBase64Binary(model.getData()));
        resp.setCheckpointData(data);
        return resp;
    }

    @Override
    public CheckpointModel toCheckpointModelFromSetCheckpointRequest(Mapping mapping, SetCheckpointRequest req) {
        val checkpointMapping = mapping.getCheckpoint();
        return CheckpointModel.builder()
                .id(req.getCheckpointData().getCheckpointId())
                .data(DatatypeConverter.parseBase64Binary(req.getCheckpointData().getValue()))
                .checkpointTable(opa1225Converter.createCheckpointTable(checkpointMapping))
                .build();
    }

    @Override
    public SetCheckpointResponse fromCheckpointModelToSetCheckpointResponse(CheckpointModel model) {
        val resp = new SetCheckpointResponse();
        resp.setCheckpointId(model.getId());
        return resp;
    }

    @Data
    @AllArgsConstructor
    class BindParts {
        TableMapping root;
        ConditionalJoin join;
        Object xmlType;
    }

    @SneakyThrows
    @Override
    public QueryModel toQueryModelFromQueryRequest(Mapping mapping, ExecuteQueryRequest req) {
        // get the load model
        val loadModel = createLoadModel(mapping,
                req.getRoot(),
                req.getLanguage(),
                req.getTimezone(),
                req.getTables().getTable(),
                new HashMap<>());
        return QueryModel.queryModelBuilder()
                .timeZone(loadModel.getTimeZone())
                .locale(loadModel.getLocale())
                .globalId(loadModel.getGlobalId())
                .rootTable(loadModel.getRootTable())
                .preScript(loadModel.getPreScript())
                .postScript(loadModel.getPostScript())
                .tables(loadModel.getTables())
                .condition(getRootCondition(mapping, loadModel, req))
                .limit(req.getResultLimit() != null ? OptionalInt.of(req.getResultLimit().intValue()) : OptionalInt.empty())
                .build();
    }

    /**
     * Creates the root condition group.
     *
     * @param mapping   mappings
     * @param loadModel load model
     * @param req       query request
     * @return root bind condition group
     */
    @SneakyThrows
    private BindConditionGroup getRootCondition(Mapping mapping, LoadModel loadModel, ExecuteQueryRequest req) {
        BindConditionGroup.BindConditionGroupBuilder rootGroup = BindConditionGroup.builder().join(ConditionalJoin.and);
        val rootTable = loadModel.getGlobalContainmentTable();
        val rootTableMapping = mapping.getTable(req.getRoot())
                .orElseThrow(() -> new MissingMappingException(MissingMappingException.MappingType.Table, req.getRoot()));
        if (req.getAndExpr() != null)
            rootGroup = addAndCondition(rootGroup, rootTable, ConditionalJoin.and, rootTableMapping, req.getAndExpr());
        if (req.getOrExpr() != null)
            rootGroup = addOrCondition(rootGroup, rootTable, ConditionalJoin.or, rootTableMapping, req.getOrExpr());
        if (req.getComparisonExpr() != null)
            rootGroup = addComparisonCondition(rootGroup, rootTable, ConditionalJoin.and, rootTableMapping, req.getComparisonExpr());
        if (req.getNullExpr() != null)
            rootGroup = addNullCondition(rootGroup, rootTable, ConditionalJoin.and, rootTableMapping, req.getNullExpr());
        if (req.getNotNullExpr() != null)
            rootGroup = addNotNullCondition(rootGroup, rootTable, ConditionalJoin.and, rootTableMapping, req.getNotNullExpr());
        return rootGroup.build();
    }

    /**
     * Adds a is not null condition to the group.
     *
     * @param group       group to populate
     * @param table       root table
     * @param join        join condition
     * @param mapping     table mapping
     * @param notNullExpr expression
     * @return builder
     */
    @SneakyThrows
    private BindConditionGroup.BindConditionGroupBuilder addNotNullCondition(
            BindConditionGroup.BindConditionGroupBuilder group
            , ContainmentTable table
            , ConditionalJoin join
            , TableMapping mapping
            , NotNullExpression notNullExpr) {
        // get the field mapping
        val fieldMapping = mapping.getField(notNullExpr.getField())
                .orElseThrow(() -> new MissingMappingException(MissingMappingException.MappingType.Field, notNullExpr.getField()));
        // add to the group
        return group.condition(BindCondition.builder()
                .alias(table.getAlias())
                .join(join)
                .operator(Operator.isNotNull)
                .source(fieldMapping.getFieldId().getSource())
                .type(fieldMapping.getType())
                .build());
    }

    /**
     * Adds a is null condition to the group.
     *
     * @param group    group to populate
     * @param table    root table
     * @param join     join condition
     * @param mapping  table mapping
     * @param nullExpr expression
     * @return builder
     */
    @SneakyThrows
    private BindConditionGroup.BindConditionGroupBuilder addNullCondition(
            BindConditionGroup.BindConditionGroupBuilder group
            , ContainmentTable table
            , ConditionalJoin join
            , TableMapping mapping
            , NullExpression nullExpr) {
        // get the field mapping
        val fieldMapping = mapping.getField(nullExpr.getField())
                .orElseThrow(() -> new MissingMappingException(MissingMappingException.MappingType.Field, nullExpr.getField()));
        // add to the group
        return group.condition(BindCondition.builder()
                .alias(table.getAlias())
                .join(join)
                .operator(Operator.isNull)
                .source(fieldMapping.getFieldId().getSource())
                .type(fieldMapping.getType())
                .build());
    }

    /**
     * Adds a condition to the group.
     *
     * @param group          group to populate
     * @param table          root table
     * @param join           join condition
     * @param mapping        table mapping
     * @param comparisonExpr expression
     * @return builder
     */
    @SneakyThrows
    private BindConditionGroup.BindConditionGroupBuilder addComparisonCondition(
            BindConditionGroup.BindConditionGroupBuilder group
            , ContainmentTable table
            , ConditionalJoin join
            , TableMapping mapping
            , ComparisonExpression comparisonExpr) {
        // get the field mapping
        val fieldMapping = mapping.getField(comparisonExpr.getField())
                .orElseThrow(() -> new MissingMappingException(MissingMappingException.MappingType.Field, comparisonExpr.getField()));

        // get the java value
        Object value;
        switch (fieldMapping.getType()) {
            case STRING_TYPE:
                value = comparisonExpr.getValue().getTextVal();
                break;
            case DECIMAL_TYPE:
                value = comparisonExpr.getValue().getNumberVal();
                break;
            case BOOLEAN_TYPE:
                value = comparisonExpr.getValue().isBooleanVal();
                break;
            case DATE_TYPE:
                value = comparisonExpr.getValue().getDateVal();
                break;
            case DATE_TIME_TYPE:
                value = comparisonExpr.getValue().getDatetimeVal();
                break;
            case TIME_TYPE:
                value = comparisonExpr.getValue().getTimeVal();
                break;
            default:
                throw new Exception("Unsupported value type.");
        }

        // get the operator
        Operator o;
        switch (comparisonExpr.getOperator()) {
            case EQUALS:
                o = Operator.equals;
                break;
            case NOT_EQUALS:
                o = Operator.notEquals;
                break;
            case LESS_THAN:
                o = Operator.lessThan;
                break;
            case LESS_THAN_EQUALS:
                o = Operator.lessThanOrEqual;
                break;
            case GREATER_THAN:
                o = Operator.greaterThan;
                break;
            case GREATER_THAN_EQUALS:
                o = Operator.greaterThanOrEqual;
                break;
            case STARTS_WITH:
                o = Operator.like;
                value = value + "%";
                break;
            case ENDS_WITH:
                o = Operator.like;
                value = "%" + value;
                break;
            case CONTAINS:
                o = Operator.like;
                value = "%" + value + "%";
                break;
            default:
                throw new Exception("Unknown comparison operator.");
        }
        // add to the group
        return group.condition(BindCondition.builder()
                .alias(table.getAlias())
                .join(join)
                .operator(o)
                .value(value)
                .source(fieldMapping.getFieldId().getSource())
                .type(fieldMapping.getType())
                .build());
    }

    /**
     * Adds a or child group to the group.
     *
     * @param group   group to populate
     * @param table   root table
     * @param join    join condition
     * @param mapping table mapping
     * @param orExpr  condition
     * @return builder
     */
    private BindConditionGroup.BindConditionGroupBuilder addOrCondition(
            BindConditionGroup.BindConditionGroupBuilder group
            , ContainmentTable table
            , ConditionalJoin join
            , TableMapping mapping
            , OrExpression orExpr) {
        BindConditionGroup.BindConditionGroupBuilder builder = BindConditionGroup.builder().join(join);

        /*
         * Split on the child type.
         *
         * {@link AndExpression }
         * {@link OrExpression }
         * {@link ComparisonExpression }
         * {@link NotNullExpression }
         * {@link NullExpression }
         */
        for (val con : orExpr.getAndExprOrOrExprOrComparisonExpr()) {
            if (con instanceof AndExpression) {
                builder = addAndCondition(builder, table, ConditionalJoin.or, mapping, (AndExpression) con);
            } else if (con instanceof OrExpression) {
                builder = addOrCondition(builder, table, ConditionalJoin.or, mapping, (OrExpression) con);
            } else if (con instanceof ComparisonExpression) {
                builder = addComparisonCondition(builder, table, ConditionalJoin.or, mapping, (ComparisonExpression) con);
            } else if (con instanceof NotNullExpression) {
                builder = addNotNullCondition(builder, table, ConditionalJoin.or, mapping, (NotNullExpression) con);
            } else if (con instanceof NullExpression) {
                builder = addNullCondition(builder, table, ConditionalJoin.or, mapping, (NullExpression) con);
            }
        }
        return group.child(builder.build());
    }

    /**
     * Adds a and child group to the group.
     *
     * @param group   group to populate
     * @param table   root table
     * @param join    join condition
     * @param mapping table mapping
     * @param andExpr condition
     * @return builder
     */
    private BindConditionGroup.BindConditionGroupBuilder addAndCondition(
            BindConditionGroup.BindConditionGroupBuilder group
            , ContainmentTable table
            , ConditionalJoin join
            , TableMapping mapping
            , AndExpression andExpr) {
        BindConditionGroup.BindConditionGroupBuilder builder = BindConditionGroup.builder().join(join);

        /*
         * Split on the child type.
         *
         * {@link AndExpression }
         * {@link OrExpression }
         * {@link ComparisonExpression }
         * {@link NotNullExpression }
         * {@link NullExpression }
         */
        for (val con : andExpr.getAndExprOrOrExprOrComparisonExpr()) {
            if (con instanceof AndExpression) {
                builder = addAndCondition(builder, table, ConditionalJoin.and, mapping, (AndExpression) con);
            } else if (con instanceof OrExpression) {
                builder = addOrCondition(builder, table, ConditionalJoin.and, mapping, (OrExpression) con);
            } else if (con instanceof ComparisonExpression) {
                builder = addComparisonCondition(builder, table, ConditionalJoin.and, mapping, (ComparisonExpression) con);
            } else if (con instanceof NotNullExpression) {
                builder = addNotNullCondition(builder, table, ConditionalJoin.and, mapping, (NotNullExpression) con);
            } else if (con instanceof NullExpression) {
                builder = addNullCondition(builder, table, ConditionalJoin.and, mapping, (NullExpression) con);
            }
        }
        return group.child(builder.build());
    }

    @Override
    public ExecuteQueryResponse fromQueryModelToQueryResponse(LoadModel model) {
        val resp = new ExecuteQueryResponse();
        resp.setTables(getLoadResponseData(model));
        return resp;
    }
}
