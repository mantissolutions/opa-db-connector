/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions;

import com.github.davidmoten.rx.jdbc.Database;
import com.mantis.odc.models.EnumerationMapping;
import com.mantis.odc.models.Operation;
import com.mantis.odc.services.models.CheckpointModel;
import com.mantis.odc.services.models.DatabaseEnumeration;
import com.mantis.odc.services.models.LoadModel;
import com.mantis.odc.services.models.SaveModel;
import com.mantis.odc.services.sql.adaptors.base.SQLAdaptor;
import com.mantis.odc.services.sql.converters.base.SQLTypeConverter;
import lombok.val;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.OptionalInt;

/**
 * Calculates the time between the start and end of the operation.
 * This counted as the database operation time, the action processor
 * for all intensive purposes is only interested with database processing
 * all XML serialisations and conversions are done elsewhere.
 */
public class TimingActionProcessorImpl extends ActionProcessorImpl {

    private final Operation.OperationBuilder operationBuilder;

    /**
     * @param db               database access.
     * @param adaptor          sql adaptor
     * @param typeConverter    sql type converter
     * @param operationBuilder operation builder.
     */
    public TimingActionProcessorImpl(Database db,
                                     SQLAdaptor adaptor,
                                     SQLTypeConverter typeConverter,
                                     Operation.OperationBuilder operationBuilder) {
        super(db, adaptor, typeConverter);
        this.operationBuilder = operationBuilder;
    }

    @Override
    public List<DatabaseEnumeration> enumerations(List<EnumerationMapping> enumerationMappings) throws SQLException {
        operationBuilder.startDB(time());
        val result = super.enumerations(enumerationMappings);
        operationBuilder.endDB(time());
        return result;
    }

    @Override
    public LoadModel load(LoadModel loadModel, boolean fromGobal, OptionalInt limit) throws SQLException {
        operationBuilder.startDB(time());
        val result = super.load(loadModel, fromGobal, limit);
        operationBuilder.endDB(time());
        return result;
    }

    @Override
    public SaveModel save(SaveModel save) throws SQLException {
        operationBuilder.startDB(time());
        val result = super.save(save);
        operationBuilder.endDB(time());
        return result;
    }

    @Override
    public CheckpointModel setCheckpoint(CheckpointModel model) throws SQLException {
        operationBuilder.startDB(time());
        val result = super.setCheckpoint(model);
        operationBuilder.endDB(time());
        return result;
    }

    @Override
    public CheckpointModel getCheckpoint(CheckpointModel model) throws SQLException {
        operationBuilder.startDB(time());
        val result = super.getCheckpoint(model);
        operationBuilder.endDB(time());
        return result;
    }

    /**
     * @return time
     */
    private BigDecimal time() {
        return new BigDecimal(System.nanoTime());
    }
}
