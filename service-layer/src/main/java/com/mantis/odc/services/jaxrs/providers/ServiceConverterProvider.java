/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.providers;

import com.mantis.odc.models.Operation;
import com.mantis.odc.services.OperationStatisticsFilter;
import com.mantis.odc.services.Service;
import com.mantis.odc.services.storage.ServiceStorage;

import javax.inject.Inject;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Converts service names to the correct service implementation.
 */
@Provider
@SuppressWarnings("unchecked")
public class ServiceConverterProvider implements ParamConverterProvider {

    /**
     * Service Storage
     */
    private final ServiceStorage serviceStorage;

    /**
     * Filter used to start all stats/operations.
     */
    private final OperationStatisticsFilter operationStatisticsFilter;

    @Inject
    public ServiceConverterProvider(ServiceStorage serviceStorage, OperationStatisticsFilter operationStatisticsFilter) {
        this.serviceStorage = serviceStorage;
        this.operationStatisticsFilter = operationStatisticsFilter;
    }

    @Override
    public <T> ParamConverter<T> getConverter(Class<T> aClass, Type type, Annotation[] annotations) {
        // create a provider for services.
        if (aClass.isAssignableFrom(Service.class)) {
            return (ParamConverter<T>) new ParamConverter<Service>() {
                @Override
                public Service fromString(String serviceName) {
                    return serviceStorage.get(serviceName);
                }

                @Override
                public String toString(Service service) {
                    return service.getName();
                }
            }; // provider for operation builders
        } else if (aClass.isAssignableFrom(Operation.OperationBuilder.class)) {
            return (ParamConverter<T>) new ParamConverter<Operation.OperationBuilder>() {
                @Override
                public Operation.OperationBuilder fromString(String value) {
                    return operationStatisticsFilter.get(value);
                }

                @Override
                public String toString(Operation.OperationBuilder value) {
                    return value.build().getUid();
                }
            };
        }
        return null;
    }
}
