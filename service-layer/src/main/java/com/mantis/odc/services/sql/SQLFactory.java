/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.sql;

import com.mantis.odc.models.Driver;
import com.mantis.odc.services.sql.adaptors.ANSISQLAdaptor;
import com.mantis.odc.services.sql.adaptors.base.SQLAdaptor;
import com.mantis.odc.services.sql.converters.ANSISQLTypeConverter;
import com.mantis.odc.services.sql.converters.base.SQLTypeConverter;

import javax.inject.Singleton;

/**
 * Creates the correct SQL adaptors and type converters for a given mapping
 */
@Singleton
public class SQLFactory {

    /**
     * Constructs the correct SQL adaptor for the given driver.
     *
     * @param driver database driver
     * @return SQL adaptor
     */
    public static SQLAdaptor getAdaptor(Driver driver) {
        switch (driver) {
            default:
                return new ANSISQLAdaptor(driver);
        }
    }

    /**
     * Constructs the correct SQL type converter for the given driver.
     *
     * @param driver database driver
     * @return SQL type converter
     */
    public static SQLTypeConverter getConverter(Driver driver) {
        switch (driver) {
            default:
                return new ANSISQLTypeConverter();
        }
    }
}
