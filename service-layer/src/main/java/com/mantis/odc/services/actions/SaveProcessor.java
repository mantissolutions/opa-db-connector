/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions;

import com.github.davidmoten.rx.jdbc.Database;
import com.github.davidmoten.rx.jdbc.QueryUpdate;
import com.mantis.odc.services.models.*;
import com.mantis.odc.services.models.base.DatabaseTable;
import com.mantis.odc.services.sql.adaptors.base.SQLAdaptor;
import com.mantis.odc.services.sql.converters.base.SQLTypeConverter;
import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.jooq.lambda.Unchecked;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mantis.odc.services.models.DBActionType.*;
import static java.text.MessageFormat.format;
import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.singletonList;

/**
 * Implements the saveButton elements of the action processor.
 */
@Slf4j
class SaveProcessor extends LoadProcessor {

    SaveProcessor(Database db, SQLAdaptor adaptor, SQLTypeConverter typeConverter) {
        super(db, adaptor, typeConverter);
    }

    @Override
    public SaveModel save(SaveModel save) throws SQLException {
        val conn = db.getConnectionProvider().get();
        try {
            // set save point
            conn.setSavepoint();

            // run pre script
            save.getPreScript().ifPresent(scriptRunner::run);

            // create all the sequences.
            save.getContainmentTables().stream()
                    .filter(DatabaseTable::hasSequence)
                    .flatMap(t -> t.getSubmitRows().stream())
                    .filter(r -> r.getAction() == DBActionType.insert && r.requiresSequenceValue(save))
                    .forEach(r -> generateSequence(r, save));

            /*
             * If the root containment row is an insert and that insert requires an auto key,
             * there is no data to clear from transient tables.
             */
            val globalContainmentTable = save.getGlobalContainmentTable();
            val globalRow = (DatabaseSubmitRow) globalContainmentTable.getRows().get(0);
            if (globalRow.getAction() != insert) {
                // clear junction tables and inferred tables against the current
                for (val table : save.getTransientTables()) clearTable(save, table, globalContainmentTable, globalRow);
            }

            // process batch operations
            val batches = getBatchOperations(save, save.getSubmitRows());
            batches.forEach((batch) -> processBatch(batch, save));

            // get data to return after saveButton
            for (val table : save.getContainmentTables()) updateTable(save, table);

            // run post scripts
            save.getPostScript().ifPresent(scriptRunner::run);
        } catch (Exception e) {
            conn.rollback();
            throw e;
        }
        return save;
    }

    /**
     * Generates and then sets a new sequence value. Updating the value everywhere it is currently referenced.
     *
     * @param row  current row
     * @param save saveButton model
     * @return submit row
     */
    private DatabaseSubmitRow generateSequence(DatabaseSubmitRow row, SaveModel save) {
        val pk = row.getTable(save).getPrimaryKey();
        val sql = adaptor.createSequenceSelect(pk.getSequence());
        return updateNewKeyValue(db.select(sql)
                .get(r -> typeConverter.convertFromSQLType(r.getObject(1), pk.getType(), save.getTimeZone()))
                .toBlocking()
                .first(), row, save);
    }

    /**
     * Sets a new key value generated from the database, either as a sequence or after an auto generated value is inserted.
     *
     * @param newKeyValue new value to set
     * @param row         row to add new value to
     * @param save        save model
     * @return updated row
     */
    private DatabaseSubmitRow updateNewKeyValue(Object newKeyValue, DatabaseSubmitRow row, SaveModel save) {
        // populate the database key
        row.getId().setDatabaseKey(newKeyValue);

        // update any joining key values.
        save.getDependentTables(row.getSource()).stream()
                .flatMap(t -> t.getDependentRows(row).stream())
                .forEach(r -> r.getFk().setDatabaseKey(row.getId().getKey()));

        // update any junction tables.
        save.getAllDependentJunctionTables(row.getSource()).forEach(jt -> jt.updateKeys(row));

        // update global key
        if (save.getRootTable().equals(row.getSource())) {
            if (save.getGlobalId() != null) save.getGlobalId().setDatabaseKey(row.getId().getKey());
            else save.setGlobalId(row.getId());
        }

        // update attachment table links
        save.getAttachmentTable().ifPresent(attachmentTable -> attachmentTable.updateLinks(row));

        return row;
    }

    /**
     * Clears the  table.
     *
     * @param save        save model
     * @param table       table to clear
     * @param globalTable global table
     * @param globalRow   global row
     */
    @SneakyThrows
    private void clearTable(SaveModel save, DatabaseTable table, DatabaseTable globalTable, DatabaseSubmitRow globalRow) {
        val sql = adaptor.createDelete(save, table, true);
        val pk = globalTable.getPrimaryKey();
        val key = typeConverter.convertToSQLType(globalRow.getId().getKey(), pk.getType());
        db.update(sql)
                .parameter(pk.getSource(), key)
                .execute();
    }

    /**
     * Creates batch operations for all the submitted rows.
     *
     * @param save save model
     * @param rows submitted rows
     * @return batch operations
     */
    private List<BatchOperation> getBatchOperations(SaveModel save, List<DatabaseSubmitRow> rows) {
        val batches = new LinkedHashMap<String, BatchOperation>();
        for (val row : rows) {
            val table = row.getTable(save);
            val fields = getFields(table, row);

            // ignore this is a no-op
            if (row.getAction() == update && fields.size() == 1) continue;

            val sql = getSQL(table, row, fields, save);
            val batch = batches.computeIfAbsent(sql, s -> BatchOperation.builder()
                    .sql(s)
                    .type(row.getAction())
                    .table(row.getTable(save))
                    .fields(fields)
                    .build());
            batch.addRow(row);
        }
        return new ArrayList<>(batches.values());
    }

    /**
     * Returns the SQL for the given row.
     *
     * @param table table model
     * @param row   submit row
     * @param save  save model
     * @return SQL
     */
    private String getSQL(DatabaseTable table, DatabaseSubmitRow row, List<DatabaseField> fields, SaveModel save) {
        switch (row.getAction()) {
            case insert:
                return adaptor.createInsert(table, fields, table.getPrimaryKey() != null && table.getPrimaryKey().isAutoIncrement());
            case update:
                return adaptor.createUpdate(table, fields, false);
            case delete:
                return adaptor.createDelete(save, table, false);
            default:
                return null;
        }
    }

    /**
     * Returns the fields for a given submit row.
     *
     * @param table table model
     * @param row   submit row
     * @return list of fields in the operation
     */
    private List<DatabaseField> getFields(DatabaseTable table, DatabaseSubmitRow row) {
        switch (row.getAction()) {
            case insert:
                return table.getInsertFields(row);
            case update:
                return Stream.concat(table.getInsertFields(row).stream()
                                // remove null values and keys
                                .filter(f -> !(f instanceof PrimaryKeyField || f instanceof ForeignKeyField)),
                        Stream.of(table.getPrimaryKey()))
                        .collect(Collectors.toList());
            case delete:
                return singletonList(table.getPrimaryKey());
            default:
                return EMPTY_LIST;
        }
    }

    /**
     * Runs dml operations on batch.
     *
     * @param batch batch to run
     * @param save  save model
     */
    @SneakyThrows
    private void processBatch(BatchOperation batch, SaveModel save) {
        if (!batch.isAutoKeyInsert()) { // batch operation
            val connection = db.getConnectionProvider().get();
            @Cleanup val ps = connection.prepareStatement(batch.getSql());
            // for each row
            for (val row : batch.getRows()) {
                // for each field add values
                for (int i = 0; i < batch.getFields().size(); i++) {
                    val field = batch.getFields().get(i);
                    if (field instanceof PrimaryKeyField)
                        ps.setObject(i + 1, typeConverter.convertToSQLType(row.getId().getKey(), field.getType()));
                    else if (field instanceof ForeignKeyField && !(batch.getTable() instanceof JunctionTable))
                        ps.setObject(i + 1, typeConverter.convertToSQLType(row.getFk().getKey(), field.getType()));
                    else
                        ps.setObject(i + 1, typeConverter.convertToSQLType(row.getValue(field.getSource()), field.getType()));
                }
                // add batch
                ps.addBatch();
            }

            log.debug(format("Running batch statement: {0} of size {1}.", batch.getSql(), batch.getRows().size()));

            // run batches
            ps.executeBatch();
            save.incrementBatchCount();
        } else { // non batch mode, for auto keys on insert
            for (val row : batch.getRows()) {
                val pk = batch.getTable().getPrimaryKey();
                updateNewKeyValue(populateUpdateQueryParameters(db.update(batch.getSql()), batch.getTable(), batch.getFields(), row)
                        .returnGeneratedKeys()
                        .get(r -> typeConverter.convertFromSQLType(r.getObject(1), pk.getType(), save.getTimeZone()))
                        .toBlocking()
                        .first(), row, save);
            }
        }
    }

    /**
     * Populates a a queries parameters based on the field type.
     *
     * @param builder query builder
     * @param table   data table
     * @param fields  fields
     * @param row     submit row
     * @return populated builder
     */
    @SneakyThrows
    private QueryUpdate.Builder populateUpdateQueryParameters(QueryUpdate.Builder builder, DatabaseTable table, List<DatabaseField> fields, DatabaseSubmitRow row) {
        for (val field : fields) {
            if (field instanceof PrimaryKeyField)
                builder.parameter(field.getSource(), typeConverter.convertToSQLType(row.getId().getKey(), field.getType()));
            else if (field instanceof ForeignKeyField && !(table instanceof JunctionTable))
                builder.parameter(field.getSource(), typeConverter.convertToSQLType(row.getFk().getKey(), field.getType()));
            else
                builder.parameter(field.getSource(), typeConverter.convertToSQLType(row.getValue(field.getSource()), field.getType()));
        }
        return builder;
    }

    /**
     * This loops through all the rows with data to return, and then replaces the
     * row with selected data.
     *
     * @param save  saveButton model
     * @param table data table
     */
    private void updateTable(SaveModel save, DatabaseTable table) {
        table.setRows(table.getSubmitRows().stream()
                // no return value for deleted rows, rows with no updated keys, or return values
                .filter(sr -> sr.getAction() != delete && (sr.getId().hasNewKey() || sr.hasReturningFields()))
                .map(Unchecked.function((DatabaseSubmitRow sr) -> (sr.hasReturningFields()) ?
                        // populate return values into new row
                        db.select(adaptor.createSelect(table, sr.getReturnFields(), true))
                                .parameter(table.getPrimaryKey().getSource(),
                                        typeConverter.convertToSQLType(sr.getId().getKey(), table.getPrimaryKey().getType()))
                                .get(resultSet -> resultSetToRow(resultSet, table, sr.getReturnFields(), save.getTimeZone()).toBuilder()
                                        .id(sr.getId())
                                        .fk(sr.getFk())
                                        .build())
                                // default row in the case that returning no data
                                .lastOrDefault(DatabaseRow.dataRowBuilder()
                                        .id(sr.getId())
                                        .source(sr.getSource())
                                        .build())
                                .toBlocking()
                                .first() :
                        // return only id in new row
                        DatabaseRow.dataRowBuilder()
                                .id(sr.getId())
                                .source(sr.getSource())
                                .build()))
                .collect(Collectors.toList()));
    }
}
