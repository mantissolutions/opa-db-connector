/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.storage;

import com.codahale.metrics.MetricRegistry;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.mantis.odc.database.dao.DatasourceMappingDAO;
import com.mantis.odc.models.DatasourceMapping;
import com.mantis.odc.services.ServiceDatasource;
import com.mantis.odc.services.storage.base.BaseStorage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import lombok.ToString;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;

/**
 * Singleton which stores all service data sources used in ODC.
 */
@Data
@Singleton
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class DatasourceStorage extends BaseStorage<DatasourceMapping, ServiceDatasource> {

    /**
     * Access to the database.
     */
    private final DatasourceMappingDAO dao;

    /**
     * Global metrics registry.
     */
    private final MetricRegistry metricRegistry;

    @Inject
    public DatasourceStorage(DatasourceMappingDAO datasourceMappingDAO, MetricRegistry metricRegistry) {
        super(CacheBuilder.newBuilder()
                // close all service data sources on removal
                .removalListener(notification -> {
                    ServiceDatasource serviceDatasource = (ServiceDatasource) notification.getValue();
                    if (serviceDatasource != null) serviceDatasource.close();
                })
                // create a new service data source if it is not cached
                .build(new CacheLoader<DatasourceMapping, ServiceDatasource>() {
                    @Override
                    public ServiceDatasource load(DatasourceMapping key) {
                        return new ServiceDatasource(key, metricRegistry);
                    }
                }));
        this.dao = datasourceMappingDAO;
        this.metricRegistry = metricRegistry;
    }

    @SneakyThrows
    @Override
    public ServiceDatasource get(DatasourceMapping datasourceMapping) {
        return cache.get(datasourceMapping, () -> new ServiceDatasource(datasourceMapping, metricRegistry));
    }

    /**
     * Deploys the given mapping.
     *
     * @param datasourceName data source mapping name
     */
    public Optional<ServiceDatasource> deployMappingByName(String datasourceName) {
        return dao.getDatasourceMappingByName(datasourceName).flatMap(this::deployMapping);
    }

    /**
     * Deploys the given mapping.
     *
     * @param mapping data source mapping
     */
    public Optional<ServiceDatasource> deployMapping(DatasourceMapping mapping) {
        return Optional.of(mapping).map(mappingToDeploy -> {
            ServiceDatasource serviceDatasource = new ServiceDatasource(mapping, metricRegistry);
            set(mapping, serviceDatasource);
            return serviceDatasource;
        });
    }

    /**
     * Un-deploys the given mapping.
     *
     * @param mapping data source mapping
     * @return true onf success false on failure
     */
    public boolean undeployServiceDatasource(DatasourceMapping mapping) {
        if (contains(mapping)) {
            cache.invalidate(mapping);
            return true;
        } else return false;
    }

    /**
     * Returns the service data source by its name.
     *
     * @param name name of the service data source
     * @return service data source or null
     */
    @Nullable
    public ServiceDatasource getByName(String name) {
        return getByNameOpt(name).orElse(null);
    }

    /**
     * Returns the service data source by its name.
     *
     * @param name name of the service data source
     * @return service data source or none
     */
    public Optional<ServiceDatasource> getByNameOpt(String name) {
        return cache.asMap().values().stream()
                .filter(serviceDatasource -> serviceDatasource.getName().equals(name))
                .findFirst();
    }
}
