/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.soap;

import lombok.SneakyThrows;
import lombok.val;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPool;

import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPMessage;

import static javax.xml.soap.SOAPConstants.SOAP_1_1_PROTOCOL;
import static javax.xml.soap.SOAPConstants.SOAP_RECEIVER_FAULT;

/**
 * Utilities for working with SOAP messages.
 */
public class SOAPUtils {

    /**
     * Message factory pool factory.
     */
    private final static PooledObjectFactory<MessageFactory> POOL_FACTORY = new BasePooledObjectFactory<MessageFactory>() {
        @Override
        public MessageFactory create() throws Exception {
            return MessageFactory.newInstance(SOAP_1_1_PROTOCOL);
        }

        @SneakyThrows
        @Override
        public PooledObject<MessageFactory> wrap(MessageFactory messageFactory) {
            return new DefaultPooledObject<>(create());
        }
    };

    /**
     * Message factory.
     */
    private final static GenericObjectPool<MessageFactory> POOL = new GenericObjectPool<>(POOL_FACTORY);

    /**
     * Creates a new SOAP message.
     *
     * @return soap message
     */
    @SneakyThrows
    public static SOAPMessage createMessage() {
        val messageFactory = POOL.borrowObject();
        val message = messageFactory.createMessage();
        POOL.returnObject(messageFactory);
        return message;
    }

    /**
     * Creates a new SOAPMessage with the given JAXB body.
     *
     * @param marshaller marshaller
     * @param body       JAXB object
     * @return SOAP message
     */
    @SneakyThrows
    public static SOAPMessage createSOAPMessage(Marshaller marshaller, Object body) {
        val message = createMessage();
        marshaller.marshal(body, message.getSOAPBody());
        return message;
    }

    /**
     * Creates a SOAP fault message.
     *
     * @param marshaller marshaller
     * @param body       JAXB object
     * @return SOAP message
     */
    @SneakyThrows
    public static SOAPMessage createSOAPFault(Marshaller marshaller, Exception e, Object body) {
        val message = createMessage();
        val fault = message.getSOAPBody().addFault();
        fault.setFaultCode(SOAP_RECEIVER_FAULT);
        fault.setFaultString("" + e.getMessage());
        val detail = fault.addDetail();
        marshaller.marshal(body, detail);
        return message;
    }

    /**
     * Creates a secure SOAP message.
     *
     * @param username username
     * @param password password
     * @return SOAP message
     */
    @SneakyThrows
    public static SOAPMessage createSecureSOAPMessage(String username, String password, Marshaller marshaller, Object body) {
        val ns = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
        val message = createMessage();
        val header = message.getSOAPHeader();
        val security = header.addHeaderElement(new QName(ns, "Security"));
        security.setMustUnderstand(true);
        val token = security.addChildElement(new QName(ns, "UsernameToken"));
        val usernameElement = token.addChildElement(new QName(ns, "Username"));
        usernameElement.setTextContent(username);
        val passwordElement = token.addChildElement(new QName(ns, "Password"));
        passwordElement.setTextContent(password);
        marshaller.marshal(body, message.getSOAPBody());
        return message;
    }
}
