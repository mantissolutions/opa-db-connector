/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.services.sql.adaptors.base;


import com.mantis.odc.services.models.*;
import com.mantis.odc.services.models.base.ActionModel;
import com.mantis.odc.services.models.base.DatabaseTable;

import java.util.List;

/**
 * SQL Adaptor interface.
 */
public interface SQLAdaptor {

    /**
     * Creates the select SQL from the root table.
     *
     * @param actionModel   action model
     * @param table         table model
     * @param binds         custom bind values
     * @param useNamedBinds flag to indicate that named binds are to be used
     * @param fromGlobal    flag to indicate that the global tables primary key condition be added to the query
     * @return SQL string
     */
    String createContainmentSelect(LoadModel actionModel, ContainmentTable table, List<BindConditionGroup> binds, boolean useNamedBinds, boolean fromGlobal);

    /**
     * Creates the select SQL from a junction table.
     *
     * @param actionModel   action model
     * @param table         table model
     * @param useNamedBinds flag to indicate that named binds are to be used
     * @param fromGlobal    flag to indicate that the global tables primary key condition be added to the query
     * @param addFromKey    flag to indicate that the from key be selected
     * @param addToKey      flag to indicate that the to key be selected
     * @return SQL string
     */
    String createNonContainmentSelect(LoadModel actionModel, JunctionTable table, boolean useNamedBinds, boolean fromGlobal, boolean addFromKey, boolean addToKey);

    /**
     * Creates the delete SQL from the table.
     *
     * @param actionModel   action model
     * @param table         table model
     * @param useNamedBinds flag to indicate that named binds are to be used
     * @return SQL string
     */
    String createDelete(ActionModel actionModel, DatabaseTable table, boolean useNamedBinds);

    /**
     * Creates the insert SQL into the table.
     *
     * @param table         table model
     * @param fields        database fields
     * @param useNamedBinds flag to indicate that named binds are to be used
     * @return SQL string
     */
    String createInsert(DatabaseTable table, List<DatabaseField> fields, boolean useNamedBinds);

    /**
     * Creates the update SQL.
     *
     * @param table         table model
     * @param fields        database fields
     * @param useNamedBinds flag to indicate that named binds are to be used
     * @return SQL string
     */
    String createUpdate(DatabaseTable table, List<DatabaseField> fields, boolean useNamedBinds);

    /**
     * Creates a single select statement.
     *
     * @param table         table.
     * @param fields        to select data for
     * @param useNamedBinds flag to indicate that named binds are to be used
     * @return SQL string
     */
    String createSelect(DatabaseTable table, List<DatabaseField> fields, boolean useNamedBinds);

    /**
     * Creates a sequence select query.
     *
     * @param sequence sequence.
     * @return SQL string
     */
    String createSequenceSelect(String sequence);
}
