/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.resources;

import com.mantis.odc.database.dao.MappingDAO;
import com.mantis.odc.jaxb.MappingType;
import com.mantis.odc.models.Mapping;
import com.mantis.odc.services.storage.ServiceStorage;
import com.mantis.odc.services.validators.MappingValidator;
import io.swagger.annotations.*;
import lombok.val;
import org.apache.shiro.authz.annotation.RequiresAuthentication;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static com.mantis.odc.services.jaxrs.utils.ResponseUtils.ok;
import static com.mantis.odc.services.jaxrs.utils.ResponseUtils.throwError;
import static java.text.MessageFormat.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

/**
 * Manages mappings deployed to the ODC server.
 */
@Singleton
@RequiresAuthentication
@Path("/mappings")
@Api(value = "Mapping Management", description = "Methods for managing Mappings.")
public class MappingsResource {

    private final MappingDAO dao;
    private final ServiceStorage storage;
    private final MappingValidator validator;

    @Inject
    public MappingsResource(MappingDAO mappingDAO, ServiceStorage storage, MappingValidator validator) {
        this.dao = mappingDAO;
        this.storage = storage;
        this.validator = validator;
    }

    /**
     * Returns all the valid mappings stored in the server.
     *
     * @param deployed is the mapping deployed
     * @return lists all mappings
     */
    @GET
    @Produces(APPLICATION_JSON)
    @ApiOperation(value = "List Mappings", notes = "List of Mappings.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of Mappings.")
    })
    public List<String> listMappings(@ApiParam(value = "Deployed flag. If set only deployed Mappings are returned.")
                                     @DefaultValue(value = "false")
                                     @QueryParam("deployed") boolean deployed) {
        return dao.list().stream()
                .filter(mapping -> !deployed || storage.contains(mapping.getServiceName()))
                .filter(mapping -> validator.validate(mapping).isSuccess())
                .map(Mapping::getName)
                .collect(Collectors.toList());
    }

    /**
     * Returns the named mapping.
     *
     * @param name mapping name
     * @return mapping
     */
    @GET
    @Path("/{name}")
    @Produces(APPLICATION_XML)
    @ApiOperation(value = "Get Mapping", notes = "Returns the Mapping.", response = MappingType.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Mapping XML.", response = MappingType.class),
            @ApiResponse(code = 404, message = "Thrown if the Connector cant find the Mapping.",
                    response = String.class)
    })
    public Mapping getMapping(@ApiParam(value = "Mapping name.", required = true)
                              @PathParam("name") String name) {
        return dao.getMappingByName(name)
                .orElseThrow(() -> throwError(NOT_FOUND, "Unknown mapping " + name));
    }

    /**
     * Creates a new mapping.
     *
     * @param mapping mapping
     * @return ok on success
     */
    @POST
    @Consumes(APPLICATION_XML)
    @ApiOperation(value = "Create Mapping", notes = "Creates a new mapping.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok message.", response = String.class),
            @ApiResponse(code = 400, message = "Thrown if the Connector files to create the Mapping.",
                    response = String.class)
    })
    public Response createMapping(@ApiParam(value = "Mapping to create", required = true) Mapping mapping) {
        validateMapping(mapping);
        if (dao.getMappingByName(mapping.getName()).isPresent())
            throw throwError(BAD_REQUEST, format("A mapping with the name {0} already exists.", mapping.getName()));
        dao.create(mapping);
        return ok();
    }

    /**
     * @param mapping mapping to validate
     * @return mapping
     */
    private Mapping validateMapping(Mapping mapping) {
        val validation = validator.validate(mapping);
        if (!validation.isSuccess())
            throw throwError(BAD_REQUEST, "The mapping is not valid.\n" + validation.printErrors());
        return mapping;
    }

    /**
     * Removes the named mapping from the database.
     *
     * @param name mapping name
     * @return ok on success
     */
    @DELETE
    @Path("/{name}")
    @ApiOperation(value = "Remove Mapping", notes = "Removes an existing mapping.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok message.", response = String.class),
            @ApiResponse(code = 404, message = "Thrown if the Connector cant find the Mapping.",
                    response = String.class)
    })
    public Response removeMapping(@ApiParam(value = "Mapping name.", required = true)
                                  @PathParam("name") String name) {
        dao.delete(dao.getMappingByName(name)
                .map(Mapping::getId)
                .orElseThrow(() -> throwError(NOT_FOUND, "Unknown mapping " + name)));
        return ok();
    }
}
