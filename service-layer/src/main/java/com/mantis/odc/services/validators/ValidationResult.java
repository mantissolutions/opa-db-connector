/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.services.validators;

import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Validation result type.
 */
@Data
@Builder(toBuilder = true)
public final class ValidationResult {

    /**
     * Flag to indicate validation state.
     */
    private final boolean isSuccess;

    /**
     * Warning flag.
     */
    private final boolean isWarning;

    /**
     * Validation message.
     */
    protected final String message;

    /**
     * Child validations.
     */
    @Singular
    protected final List<ValidationResult> children;

    /**
     * @return true if the validation is a success.
     */
    public boolean isSuccess() {
        return isSuccess && children.stream().allMatch(ValidationResult::isSuccess);
    }

    /**
     * @param message message
     * @return error
     */
    public static ValidationResult error(String message) {
        return builder()
                .isSuccess(false)
                .isWarning(false)
                .message(message)
                .build();
    }

    /**
     * @param message message
     * @return success
     */
    public static ValidationResult warning(String message) {
        return builder()
                .isSuccess(true)
                .isWarning(true)
                .message(message)
                .build();
    }

    /**
     * @param message message
     * @return success
     */
    public static ValidationResult success(String message) {
        return builder()
                .isSuccess(true)
                .isWarning(false)
                .message(message)
                .build();
    }

    /**
     * @return returns all nodes in the validation tree
     */
    public Stream<ValidationResult> listRecursive() {
        return Stream.concat(Stream.of(this), children.stream().flatMap(ValidationResult::listRecursive));
    }

    /**
     * @return prints all the failed messages.
     */
    public String printErrors() {
        return (!isSuccess()) ? listRecursive()
                .filter(validationResult -> !validationResult.isSuccess())
                .map(ValidationResult::getMessage)
                .collect(Collectors.joining("\n")) : "";
    }
}