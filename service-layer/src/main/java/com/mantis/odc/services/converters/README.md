OPA to ODC Converters
=====================

Converters are mappings between a particular version of the OPA Connector 
framework in JAXB and the ODC service action domain models.

All new version of OPA must have a corresponding mapping registered here.
That mapping will in most cases be a copy and paste of a previous mapping.
However, to keep the core ODC operation separated from changes to the OPA
connector contract, these mapping classes are required.

The database operations such as Loading, Saving and Checkpoints, map to 
the models in the com.mantis.odc.services.model package.

The meta-data operations map to the com.mantis.odc.models package.

While the check alive action has no mapping and uses the correct JAXB class.

A OPA version has its own sub package here and entry in the 
com.mantis.odc.models.OPAVersion enum.

All mapping classes must implement the OPAConverter interface.