/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters.base;

import com.mantis.odc.models.Mapping;
import com.mantis.odc.services.models.CheckpointModel;
import com.mantis.odc.services.models.LoadModel;
import com.mantis.odc.services.models.QueryModel;

/**
 * Converter without checkpoint support.
 */
public abstract class BaseOPAConverterWithoutCheckpoints<
        CHECK_ALIVE, META_DATA,
        LOAD_REQ, LOAD_RESP,
        SAVE_REQ, SAVE_RESP,
        FAULT> implements OPAConverter<
        CHECK_ALIVE, META_DATA,
        LOAD_REQ, LOAD_RESP,
        SAVE_REQ, SAVE_RESP,
        Object, Object, Object, Object,
        Object, Object,
        FAULT> {

    /* UNSUPPORTED OPERATIONS */
    @Override
    public Object fromCheckpointModelToGetCheckpointResponse(CheckpointModel model) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object fromCheckpointModelToSetCheckpointResponse(CheckpointModel model) {
        throw new UnsupportedOperationException();
    }

    @Override
    public CheckpointModel toCheckpointModelFromSetCheckpointRequest(Mapping mapping, Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public CheckpointModel toCheckpointModelFromGetCheckpointRequest(Mapping mapping, Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public QueryModel toQueryModelFromQueryRequest(Mapping mapping, Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object fromQueryModelToQueryResponse(LoadModel model) {
        throw new UnsupportedOperationException();
    }
}
