/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions;

import com.github.davidmoten.rx.jdbc.Database;
import com.mantis.odc.models.EnumerationMapping;
import com.mantis.odc.services.actions.base.ActionProcessor;
import com.mantis.odc.services.models.CheckpointModel;
import com.mantis.odc.services.models.DatabaseEnumeration;
import com.mantis.odc.services.models.LoadModel;
import com.mantis.odc.services.models.SaveModel;
import com.mantis.odc.services.sql.adaptors.base.SQLAdaptor;
import com.mantis.odc.services.sql.converters.base.SQLTypeConverter;
import lombok.SneakyThrows;

import java.sql.SQLException;
import java.util.List;
import java.util.OptionalInt;

/**
 * Implements the action processor.
 */
public class ActionProcessorImpl implements ActionProcessor {

    private final Database db;
    private final EnumerationProcessor enumerationProcessor;
    private final SaveProcessor saveLoadProcessor;
    private final CheckpointProcessor checkpointProcessor;

    /**
     * @param db            database access.
     * @param adaptor       sql adaptor
     * @param typeConverter sql type converter
     */
    public ActionProcessorImpl(Database db, SQLAdaptor adaptor, SQLTypeConverter typeConverter) {
        this.db = db;
        saveLoadProcessor = new SaveProcessor(db, adaptor, typeConverter);
        checkpointProcessor = new CheckpointProcessor(db, adaptor, typeConverter);
        enumerationProcessor = new EnumerationProcessor(db, adaptor, typeConverter);
    }

    @Override
    public List<DatabaseEnumeration> enumerations(List<EnumerationMapping> enumerationMappings) throws SQLException {
        return enumerationProcessor.enumerations(enumerationMappings);
    }

    @Override
    public LoadModel load(LoadModel loadModel, boolean fromGobal, OptionalInt limit) throws SQLException {
        return saveLoadProcessor.load(loadModel, fromGobal, limit);
    }

    @Override
    public SaveModel save(SaveModel save) throws SQLException {
        return saveLoadProcessor.save(save);
    }

    @Override
    public CheckpointModel setCheckpoint(CheckpointModel model) throws SQLException {
        return checkpointProcessor.setCheckpoint(model);
    }

    @Override
    public CheckpointModel getCheckpoint(CheckpointModel model) throws SQLException {
        return checkpointProcessor.getCheckpoint(model);
    }

    @SneakyThrows
    @Override
    public void close() {
        db.close();
    }
}
