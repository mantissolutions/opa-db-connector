/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services;

import com.google.common.cache.CacheBuilder;
import com.google.common.io.CountingInputStream;
import com.google.common.io.CountingOutputStream;
import com.mantis.odc.models.Operation;
import com.mantis.odc.services.storage.OperationStorage;
import com.mantis.odc.services.storage.base.BaseStorage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static java.math.BigDecimal.ZERO;
import static java.util.concurrent.TimeUnit.MINUTES;

/**
 * Implements a stats filter for all service action requests.
 * This location registers operations and finalises them. This doubles as a temporary cache for operations.
 */
@Data
@Singleton
@EqualsAndHashCode(callSuper = true)
public class OperationStatisticsFilter extends BaseStorage<String, Operation.OperationBuilder> implements Filter {

    public final static String OPERATION_ID_PARAM = "odc-operation-uid";

    /**
     * Storage for operations.
     */
    private final OperationStorage operationStorage;

    @Inject
    public OperationStatisticsFilter(OperationStorage operationStorage) {
        super(CacheBuilder.newBuilder()
                // for dropped connections, clean up any orphaned builders
                .expireAfterWrite(10, MINUTES)
                .build());
        this.operationStorage = operationStorage;
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (((HttpServletRequest) request).getMethod().equalsIgnoreCase("POST")) {
            val uid = UUID.randomUUID().toString();
            val builder = new Operation().toBuilder()
                    .uid(uid)
                    .date(new Date(System.currentTimeMillis()))
                    .startRequest(new BigDecimal(System.nanoTime()));

            // add to request scope
            request.setAttribute(OPERATION_ID_PARAM, uid);

            // store in cache
            cache.put(uid, builder);

            // run operation
            val csReq = new CountingServletRequest((HttpServletRequest) request);
            val csResp = new CountingServletResponse((HttpServletResponse) response);
            chain.doFilter(csReq, csResp);

            // collect final size and finish operation
            if (((HttpServletResponse) response).getStatus() == 200) {
                builder
                        .inboundPacketSize(Optional.ofNullable(csReq.getCountingInputStream())
                                .map(cis -> new BigDecimal(cis.getCount()))
                                .orElse(ZERO))
                        .outboundPacketSize(Optional.ofNullable(csResp.getCountingOutputStream())
                                .map(cos -> new BigDecimal(cos.getCount()))
                                .orElse(ZERO))
                        .endRequest(new BigDecimal(System.nanoTime()));

                operationStorage.addOperation(builder.build());
                cache.invalidate(uid);
            }
        } else chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

    /**
     * Wraps the request and returns a counting input stream.
     */
    @Data
    private class CountingServletRequest extends HttpServletRequestWrapper {

        CountingServletRequest(HttpServletRequest request) {
            super(request);
        }

        /**
         * Counting input stream.
         */
        public CountingInputStream countingInputStream;

        /**
         * Enhanced wrapped input stream.
         */
        public ServletInputStream enhancedServletInputStream;

        @Override
        public ServletInputStream getInputStream() throws IOException {
            if (enhancedServletInputStream == null) {
                val ois = super.getInputStream();
                countingInputStream = new CountingInputStream(ois);
                enhancedServletInputStream = new ServletInputStream() {
                    @Override
                    public boolean isFinished() {
                        return ois.isFinished();
                    }

                    @Override
                    public boolean isReady() {
                        return ois.isReady();
                    }

                    @Override
                    public void setReadListener(ReadListener readListener) {
                        ois.setReadListener(readListener);
                    }

                    @Override
                    public int read() throws IOException {
                        return countingInputStream.read();
                    }
                };
            }
            return enhancedServletInputStream;
        }
    }

    /**
     * Wraps the response and returns the counting output stream.
     */
    @Data
    private class CountingServletResponse extends HttpServletResponseWrapper {

        CountingServletResponse(HttpServletResponse response) {
            super(response);
        }

        /**
         * Counting output stream.
         */
        private CountingOutputStream countingOutputStream;

        /**
         * Enhanced wrapped output stream.
         */
        private ServletOutputStream enhancedServletOutputStream;

        @Override
        public ServletOutputStream getOutputStream() throws IOException {
            if (enhancedServletOutputStream == null) {
                val oos = super.getOutputStream();
                countingOutputStream = new CountingOutputStream(oos);
                enhancedServletOutputStream = new ServletOutputStream() {
                    @Override
                    public boolean isReady() {
                        return oos.isReady();
                    }

                    @Override
                    public void setWriteListener(WriteListener writeListener) {
                        oos.setWriteListener(writeListener);
                    }

                    @Override
                    public void write(int b) throws IOException {
                        countingOutputStream.write(b);
                    }
                };
            }
            return enhancedServletOutputStream;
        }
    }
}
