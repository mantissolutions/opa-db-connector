/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.sql.converters;

import com.mantis.odc.services.sql.converters.base.SQLTypeConverter;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.val;

import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import static javax.xml.datatype.DatatypeConstants.FIELD_UNDEFINED;

/**
 * ANSI standard type converter.
 */
public class ANSISQLTypeConverter implements SQLTypeConverter {

    private final DatatypeFactory factory;

    @SneakyThrows
    public ANSISQLTypeConverter() {
        factory = DatatypeFactory.newInstance();
    }

    @Override
    public XMLGregorianCalendar convertFromSQLTimestamp(
            @NonNull Object sqlValue, TimeZone timeZone) throws SQLException {
        return (sqlValue instanceof String) ?
                toXMLGregorianCalendar(timeZone, DatatypeConverter.parseTime((String) sqlValue)) :
                convertToXMLGregorianCalendar(sqlValue, timeZone);
    }

    @Override
    public XMLGregorianCalendar convertFromSQLTime(@NonNull Object sqlValue, TimeZone timeZone) throws SQLException {
        XMLGregorianCalendar calendar = (sqlValue instanceof String) ?
                toXMLGregorianCalendar(timeZone, DatatypeConverter.parseTime((String) sqlValue)) :
                convertToXMLGregorianCalendar(sqlValue, timeZone);

        calendar.setYear(FIELD_UNDEFINED);
        calendar.setMonth(FIELD_UNDEFINED);
        calendar.setDay(FIELD_UNDEFINED);

        return calendar;
    }

    @Override
    public XMLGregorianCalendar convertFromSQLDate(@NonNull Object sqlValue, TimeZone timeZone) throws SQLException {
        XMLGregorianCalendar calendar = (sqlValue instanceof String) ?
                toXMLGregorianCalendar(timeZone, DatatypeConverter.parseDate((String) sqlValue)) :
                convertToXMLGregorianCalendar(sqlValue, timeZone);

        calendar.setHour(FIELD_UNDEFINED);
        calendar.setMinute(FIELD_UNDEFINED);
        calendar.setSecond(FIELD_UNDEFINED);
        calendar.setMillisecond(FIELD_UNDEFINED);

        return calendar;
    }

    /**
     * Converts a number or date type to an XMLGregorianCalendar.
     *
     * @param sqlValue sql value.
     * @param timeZone timezone
     * @return xml gregorian calendar
     * @throws SQLException on conversion error
     */
    private XMLGregorianCalendar convertToXMLGregorianCalendar(Object sqlValue, TimeZone timeZone) throws SQLException {
        if (sqlValue instanceof XMLGregorianCalendar) return (XMLGregorianCalendar) sqlValue;
        if (sqlValue instanceof Calendar) {
            return toXMLGregorianCalendar(timeZone, (Calendar) sqlValue);
        }
        if (sqlValue instanceof Number) {
            val calendar = Calendar.getInstance();
            calendar.setTimeInMillis(((Number) sqlValue).longValue());
            return toXMLGregorianCalendar(timeZone, calendar);
        } else if (sqlValue instanceof java.util.Date) {
            val calendar = Calendar.getInstance();
            calendar.setTime((java.util.Date) sqlValue);
            return toXMLGregorianCalendar(timeZone, calendar);
        }
        throw new SQLException(String.format("Failed to convert %s to XMLGregorianCalendar", sqlValue.getClass().getSimpleName()));
    }

    /**
     * @param timeZone time zone
     * @param calendar calendar
     * @return xml gregorian calendar
     */
    private XMLGregorianCalendar toXMLGregorianCalendar(TimeZone timeZone, Calendar calendar) {
        val gregCalendar = new GregorianCalendar();
        gregCalendar.setTimeZone(timeZone);
        gregCalendar.setTimeInMillis(calendar.getTimeInMillis());
        val xmlCalendar = factory.newXMLGregorianCalendar(gregCalendar);
        xmlCalendar.setFractionalSecond(null);
        return xmlCalendar;
    }

    @Override
    public Time convertToSQLTime(@NonNull Object javaValue) throws SQLException {
        if (javaValue instanceof String)
            return new Time(DatatypeConverter.parseTime((String) javaValue).getTimeInMillis());
        else return new Time(convertToTimeFromEpoch(javaValue));
    }

    @Override
    public Date convertToSQLDate(@NonNull Object javaValue) throws SQLException {
        if (javaValue instanceof String)
            return new Date(DatatypeConverter.parseDate((String) javaValue).getTimeInMillis());
        return new Date(convertToTimeFromEpoch(javaValue));
    }

    @Override
    public Timestamp convertToSQLDateTime(@NonNull Object javaValue) throws SQLException {
        if (javaValue instanceof String)
            return new Timestamp(DatatypeConverter.parseDateTime((String) javaValue).getTimeInMillis());
        return new Timestamp(convertToTimeFromEpoch(javaValue));
    }

    /**
     * Converts a string, number and date into a number from epoch.
     *
     * @param javaValue java value
     * @return number from epoch
     * @throws SQLException on conversion error
     */
    private long convertToTimeFromEpoch(Object javaValue) throws SQLException {
        if (javaValue instanceof java.util.Date) return ((java.util.Date) javaValue).getTime();
        else if (javaValue instanceof Calendar) return ((Calendar) javaValue).getTimeInMillis();
        else if (javaValue instanceof XMLGregorianCalendar)
            return ((XMLGregorianCalendar) javaValue).toGregorianCalendar().getTimeInMillis();
        else if (javaValue instanceof Number) return ((Number) javaValue).longValue();
        throw new SQLException(String.format("Failed to convert %s to long", javaValue.getClass().getSimpleName()));
    }
}
