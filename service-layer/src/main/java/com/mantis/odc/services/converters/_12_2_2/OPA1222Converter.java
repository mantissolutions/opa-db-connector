/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters._12_2_2;

import com.mantis.odc.models.*;
import com.mantis.odc.services.converters._12_2.OPA122Converter;
import com.mantis.odc.services.converters.base.BaseOPAConverterWithoutExecuteQuery;
import com.mantis.odc.services.converters.exceptions.MissingMappingException;
import com.mantis.odc.services.models.*;
import com.mantis.odc.services.models.base.DatabaseEnumerationValue;
import com.oracle.opa.connector._12_2.data.types.AttachmentList;
import com.oracle.opa.connector._12_2_2.metadata.types.*;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.val;
import org.jooq.lambda.Unchecked;
import org.jooq.lambda.function.Function2;

import javax.xml.bind.DatatypeConverter;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mantis.odc.services.converters.exceptions.MissingMappingException.MappingType.Enumeration;
import static com.mantis.odc.utilities.ErrorUtil.errorToString;
import static java.lang.String.format;

/**
 * Converter for OPA Connector version 12.2.2.
 */
@Data
public final class OPA1222Converter extends BaseOPAConverterWithoutExecuteQuery<
        CheckAliveResponse,
        GetMetadataResponse,
        LoadRequest, LoadResponse,
        SaveRequest, SaveResponse,
        GetCheckpointRequest, GetCheckpointResponse,
        SetCheckpointRequest, SetCheckpointResponse,
        RequestFault> {

    private final OPA1222AutoConverter autoConverter_1222;

    private final OPA122Converter opa122Converter;

    public OPA1222Converter() {
        autoConverter_1222 = new OPA1222AutoConverterImpl();
        opa122Converter = new OPA122Converter();
    }

    @Override
    public CheckAliveResponse getCheckAlive() {
        return new CheckAliveResponse();
    }

    @Override
    public RequestFault getFault(Exception e) {
        val fault = new RequestFault();
        fault.setMessage(format("%s\nStack Trace:\n%s", e.getMessage(), errorToString(e)));
        return fault;
    }

    @Override
    public GetMetadataResponse getMetaData(Mapping mapping, List<DatabaseEnumeration> enumerations) {
        val resp = new GetMetadataResponse();
        resp.setSupportsCheckpoints(mapping.isSupportsCheckpoints());
        resp.getTable().addAll(mapping.getTables().stream()
                .map(this::createMetaTable)
                .collect(Collectors.toList()));
        if (!enumerations.isEmpty()) {
            val enums = new MetaEnumList();
            enums.getNumberEnumerationOrStringEnumeration().addAll(enumerations.stream()
                    .map(databaseEnumeration -> createMetaEnumeration(mapping, databaseEnumeration))
                    .collect(Collectors.toList()));
            resp.setMetadataEnumerations(enums);
        }
        return resp;
    }

    /**
     * Creates an enumeration.
     *
     * @param mapping             mapping
     * @param databaseEnumeration database enumeration
     * @return enumeration
     */
    @SneakyThrows
    private Object createMetaEnumeration(Mapping mapping, DatabaseEnumeration databaseEnumeration) {
        val enumMapping = mapping.getEnumeration(databaseEnumeration.getName())
                .orElseThrow(() -> new MissingMappingException(Enumeration, databaseEnumeration.getName()));
        Class enumType;
        Class enumValueType;
        switch (databaseEnumeration.getType()) {
            case StringEnumeration:
                enumType = StringEnumeration.class;
                enumValueType = StringEnumVal.class;
                break;
            case NumberEnumeration:
                enumType = NumberEnumeration.class;
                enumValueType = NumberEnumVal.class;
                break;
            // Unsupported in 12.2.2
            case DateEnumeration:
            case DateTimeEnumeration:
            case TimeEnumeration:
            case BooleanEnumeration:
            default:
                return null;
        }
        return createEnum(enumType,
                enumValueType,
                UncertainEnumVal.class,
                enumMapping,
                databaseEnumeration,
                ChildEnumValues.class,
                (serializables, type) -> {
                    if (serializables != null && !serializables.isEmpty()) {
                        val children = new ChildEnumValues();
                        children.getNumberValueOrStringValue().addAll(serializables.stream()
                                .filter(o -> o instanceof Serializable)
                                .map(o -> (Serializable) o)
                                .collect(Collectors.toList()));
                        return children;
                    } else return null;
                });
    }

    /**
     * Creates an enumeration.
     *
     * @param <E>                 enum type
     * @param enumType            enum type
     * @param mapping             enumeration mapping
     * @param databaseEnumeration database enumeration
     * @param childEnumListType   child list type
     * @param childCreator        child list creator
     * @return enum
     */
    @SneakyThrows
    public <E, V, U, CT> E createEnum(Class<E> enumType,
                                      Class<V> enumValueType,
                                      Class<U> uncertainEnumValueType,
                                      EnumerationMapping mapping,
                                      DatabaseEnumeration databaseEnumeration,
                                      Class<CT> childEnumListType,
                                      Function2<List<Object>, EnumerationType, CT> childCreator) {
        val enumModel = enumType.newInstance();
        // set the id
        enumType.getMethod("setId", String.class).invoke(enumModel, databaseEnumeration.getName());
        // set description
        enumType.getMethod("setDescription", String.class).invoke(enumModel, mapping.getDescription());
        // set child values
        if (mapping.hasChildEnumeration()) {
            enumType.getMethod("setChildEnumId", String.class).invoke(enumModel, mapping.getChild().getName());
        }
        // set uncertain value
        databaseEnumeration.getUncertainValueOpt().ifPresent(
                Unchecked.consumer(databaseEnumerationValue ->
                        enumType.getMethod("setUncertainValue", uncertainEnumValueType)
                                .invoke(enumModel, createEnumValue(
                                        uncertainEnumValueType,
                                        mapping.hasChildEnumeration() ? mapping.getChild().getType() : null,
                                        databaseEnumeration.getUncertainValue(),
                                        childEnumListType,
                                        childCreator))));
        // set child values
        List values = (List) enumType.getMethod("getEnumValue").invoke(enumModel);
        values.addAll(databaseEnumeration.getValues().stream()
                .map(value -> createEnumValue(enumValueType,
                        mapping.hasChildEnumeration() ? mapping.getChild().getType() : null,
                        value,
                        childEnumListType,
                        childCreator))
                .collect(Collectors.toList()));
        return enumModel;
    }

    /**
     * Creates a enum value.
     *
     * @param enumValueType     enum value type
     * @param childType         child type
     * @param value             database enum value
     * @param childEnumListType child list type
     * @param childCreator      child list creator
     * @param <V>               enum value type
     * @param <CT>              child list type
     * @return enum value
     */
    @SneakyThrows
    private <V, CT> V createEnumValue(Class<V> enumValueType,
                                      EnumerationType childType,
                                      DatabaseEnumerationValue value,
                                      Class<CT> childEnumListType,
                                      Function2<List<Object>, EnumerationType, CT> childCreator) {
        val enumValueModel = enumValueType.newInstance();
        // set value
        if (value.getValue() != null) {
            val valMethod = Stream.of(enumValueType.getMethods())
                    .filter(m -> m.getName().equals("setValue"))
                    .findFirst()
                    .orElseThrow(() -> new Exception("Failed to find set value method."));
            valMethod.invoke(enumValueModel, value.getValue());
        }
        // set description
        if (value.getDescription() != null) {
            enumValueType.getMethod("setDescription", String.class).invoke(enumValueModel, value.getDescription());
        }
        // set child values
        val childValues = childCreator.apply(value.getChildValues(), childType);
        if (childValues != null) {
            enumValueType.getMethod("setChildValues", childEnumListType).invoke(enumValueModel, childValues);
        }
        return enumValueModel;
    }

    /**
     * Creates a meta table from the table mapping.
     *
     * @param table table mapping
     * @return meta table
     */
    private MetaTable createMetaTable(TableMapping table) {
        val metaTable = new MetaTable();
        metaTable.setName(table.getTableId().getName());
        metaTable.setAcceptsAttachments(table.getMapping().isSupportsAttachments());
        metaTable.setCanBeInput(table.canBeInput());
        metaTable.setCanBeOutput(table.canBeOutput());
        metaTable.getField().addAll(table.getFields().stream()
                .filter(field -> (field.isInput() || field.isOutput()) && !field.isForeignKey())
                .map(this::createMetaField)
                .collect(Collectors.toList()));
        metaTable.getLink().addAll(Stream.concat(
                table.getChildren().stream()
                        .map(child -> createMetaLink(
                                child.getLinkName(),
                                child.getTableId().getName(),
                                (child.isOneToOne()) ? MetaLinkCardinality.C_11 : MetaLinkCardinality.C_1_M)),
                table.getJunctionTables().stream()
                        .map(junctionTable -> createMetaLink(
                                junctionTable.getLinkName(),
                                junctionTable.getTargetName(table.getTableId()),
                                MetaLinkCardinality.fromValue(junctionTable.getLinkType(table).getValue())))
        )
                .collect(Collectors.toList()));
        return metaTable;
    }

    /**
     * Creates a meta field from the field mapping.
     *
     * @param fieldMapping field mapping
     * @return meta field
     */
    private MetaField createMetaField(FieldMapping fieldMapping) {
        val metaField = new MetaField();
        metaField.setName(fieldMapping.getFieldId().getName());
        metaField.setCanBeOutput(fieldMapping.isOutput());
        metaField.setIsRequired(fieldMapping.isRequired());
        metaField.setCanBeInput(fieldMapping.isInput());
        if (fieldMapping.getEnumeration() != null)
            metaField.setEnumType(fieldMapping.getEnumeration().getName());
        else
            metaField.setType(autoConverter_1222.create1222MetaFieldDataType(fieldMapping.getType()));
        return metaField;
    }

    /**
     * Creates a meta link from the given parent and child table mappings.
     *
     * @param name        name of the relationship
     * @param target      target of the relationship
     * @param cardinality link cardinality
     * @return meta link
     */
    private MetaLink createMetaLink(String name, String target, MetaLinkCardinality cardinality) {
        val metaLink = new MetaLink();
        metaLink.setName(name);
        metaLink.setTarget(target);
        metaLink.setCardinality(cardinality);
        return metaLink;
    }

    @SneakyThrows
    @Override
    public LoadModel toLoadModel(Mapping mapping, LoadRequest loadRequest) {
        return opa122Converter.createLoadModel(mapping,
                loadRequest.getRoot(),
                loadRequest.getLanguage(),
                loadRequest.getTimezone(),
                loadRequest.getTables().getTable(),
                asMap(loadRequest.getRequestContext()));
    }

    private Map<String, String> asMap(RequestContext requestContext) {
        val map = new HashMap<String, String>();
        if (requestContext != null) for (val p : requestContext.getParameter()) map.put(p.getName(), p.getValue());
        return map;
    }

    /**
     * Finds and returns the given request param.
     *
     * @param key key to search for
     * @param ctx context.
     * @return value or none
     */
    private Optional<String> getValueFromContext(String key, RequestContext ctx) {
        return (ctx != null) ? ctx.getParameter().stream()
                .filter(p -> p.getName().equals(key))
                .map(RequestContextParam::getValue)
                .findFirst() :
                Optional.empty();
    }

    @Override
    public LoadResponse fromLoadModel(LoadModel model) {
        val resp = new LoadResponse();
        resp.setTables(opa122Converter.getLoadResponseData(model));
        return resp;
    }

    @SneakyThrows
    @Override
    public SaveModel toSaveModel(Mapping mapping, SaveRequest saveRequest) {
        return opa122Converter.createSaveModel(mapping,
                saveRequest.getRoot(),
                saveRequest.getLanguage(),
                saveRequest.getTimezone(),
                saveRequest.getTables().getTable(),
                Optional.ofNullable(saveRequest.getAttachments())
                        .map(AttachmentList::getAttachment)
                        .orElse(new ArrayList<>()),
                asMap(saveRequest.getRequestContext()));
    }

    @Override
    public SaveResponse fromSaveModel(SaveModel model) {
        val resp = new SaveResponse();
        resp.setTables(opa122Converter.getUpdateData(model));
        return resp;
    }

    @Override
    public CheckpointModel toCheckpointModelFromGetCheckpointRequest(Mapping mapping, GetCheckpointRequest req) {
        val checkpointMapping = mapping.getCheckpoint();
        return CheckpointModel.builder()
                .id(getValueFromContext(checkpointMapping.getBindToken(), req.getRequestContext()).orElse(null))
                .checkpointTable(createCheckpointTable(checkpointMapping))
                .build();
    }

    /**
     * Creates a checkpoint table from a checkpoint table mapping.
     *
     * @param checkpointMapping checkpoint table mapping
     * @return checkpoint table
     */
    private CheckpointTable createCheckpointTable(CheckpointMapping checkpointMapping) {
        return CheckpointTable.builder()
                .source(checkpointMapping.getTableName())
                .opaName(checkpointMapping.getTableName())
                .idField(checkpointMapping.getIdField())
                .dataField(checkpointMapping.getDataField())
                .build();
    }

    @Override
    public GetCheckpointResponse fromCheckpointModelToGetCheckpointResponse(CheckpointModel model) {
        val resp = new GetCheckpointResponse();
        val data = new CheckpointData();
        data.setCheckpointId(model.getId());
        data.setValue(DatatypeConverter.printBase64Binary(model.getData()));
        resp.setCheckpointData(data);
        return resp;
    }

    @Override
    public CheckpointModel toCheckpointModelFromSetCheckpointRequest(Mapping mapping, SetCheckpointRequest req) {
        val checkpointMapping = mapping.getCheckpoint();
        return CheckpointModel.builder()
                .id(req.getCheckpointData().getCheckpointId())
                .data(DatatypeConverter.parseBase64Binary(req.getCheckpointData().getValue()))
                .checkpointTable(createCheckpointTable(checkpointMapping))
                .build();
    }

    @Override
    public SetCheckpointResponse fromCheckpointModelToSetCheckpointResponse(CheckpointModel model) {
        val resp = new SetCheckpointResponse();
        resp.setCheckpointId(model.getId());
        return resp;
    }
}
