/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.validators.base;

import com.mantis.odc.services.validators.ValidationResult;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.mantis.odc.services.validators.ValidationResult.error;
import static com.mantis.odc.services.validators.ValidationResult.success;
import static java.lang.String.format;
import static java.util.Objects.isNull;

/**
 * Base validation class.
 *
 * @param <T> type to validate.
 */
public abstract class BaseValidator<T> {

    /**
     * Validates the given model.
     *
     * @param model model to validate
     * @return validation result.
     */
    public abstract ValidationResult validate(T model);

    /**
     * Adds an error if the value is null or an empty string.
     *
     * @param value   value to test
     * @param name    field name
     * @param builder validation builder
     * @param <A>     type to test
     */
    protected <A> void hasValue(A value, String name, ValidationResult.ValidationResultBuilder builder) {
        if ((value instanceof String && isNullOrEmpty((String) value)) || isNull(value))
            builder.child(error(format("A %s is required.", name)));
        else builder.child(success(name + " has been set."));
    }

    /**
     * Finalises a grouping of validation results.
     *
     * @param name    name of the validation
     * @param builder build to finalise.
     * @return validation result
     */
    protected ValidationResult isValid(String name, ValidationResult.ValidationResultBuilder builder) {
        return (builder.build().getChildren().stream().allMatch(ValidationResult::isSuccess)) ?
                builder.isSuccess(true).message(name + " is valid.").build() :
                builder.isSuccess(false).message(name + " is invalid.").build();
    }

}
