/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.storage;

import com.google.common.cache.CacheBuilder;
import com.mantis.odc.config.AppConfig;
import com.mantis.odc.services.models.debug.base.DebugEvent;
import com.mantis.odc.services.storage.base.BaseStorage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

/**
 * Storage for all debug elements captured in operation.
 */
@Data
@Singleton
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class DebugStorage extends BaseStorage<String, List<DebugEvent>> {

    @Inject
    public DebugStorage(AppConfig config) {
        super(CacheBuilder
                .newBuilder()
                .maximumSize(config.getMaxDebugMessages())
                .build());
    }

    public DebugStorage() {
        super(CacheBuilder.newBuilder().build());
    }

    /**
     * Clears messages for a given service name.
     *
     * @param serviceName service name
     */
    public void clearMessages(String serviceName) {
        cache.invalidate(serviceName);
    }

    /**
     * Adds a debug event to the given service.
     *
     * @param serviceName service name
     * @param event       debug event
     */
    public void addDebugMessage(String serviceName, DebugEvent event) {
        if (!contains(serviceName)) set(serviceName, new ArrayList<>());
        getOpt(serviceName).ifPresent(debugEvents -> debugEvents.add(event));
    }
}
