/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.actions;

import com.github.davidmoten.rx.jdbc.Database;
import com.mantis.odc.services.actions.base.BaseDBProcessor;
import com.mantis.odc.services.models.CheckpointModel;
import com.mantis.odc.services.sql.adaptors.base.SQLAdaptor;
import com.mantis.odc.services.sql.converters.base.SQLTypeConverter;
import lombok.val;

/**
 * Processes checkpoint actions.
 */
class CheckpointProcessor extends BaseDBProcessor {

    CheckpointProcessor(Database db, SQLAdaptor adaptor, SQLTypeConverter typeConverter) {
        super(db, adaptor, typeConverter);
    }

    @Override
    public CheckpointModel setCheckpoint(CheckpointModel model) {
        val table = model.getCheckpointTable();
        db.update(model.isNewCheckpoint() ?
                adaptor.createInsert(table, table.getInsertFields(), true) :
                adaptor.createUpdate(table, table.getInsertFields(), true))
                .parameter(table.getIdField(), model.getId())
                .parameter(table.getDataField(), model.getData())
                .execute();
        return model;
    }

    @Override
    public CheckpointModel getCheckpoint(CheckpointModel model) {
        val table = model.getCheckpointTable();
        model.setData(db.select(adaptor.createSelect(table, table.getSelectFields(null), true))
                .parameter(table.getPrimaryKey().getSource(), model.getId())
                .get(rs -> typeConverter.convertToByteArray(rs.getObject(1)))
                .toBlocking()
                .first());
        return model;
    }
}
