/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.sql.adaptors;

import com.mantis.odc.models.OPAVersion;
import com.mantis.odc.models.ServiceAction;
import com.mantis.odc.services.models.*;
import com.mantis.odc.services.models.base.ActionModel;
import com.mantis.odc.services.models.base.DatabaseTable;
import com.mantis.odc.services.models.debug.SQLEvent;
import com.mantis.odc.services.sql.adaptors.base.SQLAdaptor;
import com.mantis.odc.services.storage.DebugStorage;
import lombok.Data;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Wraps a SQL adaptor and logs all produced SQL.
 */
@Data
public class DebugSQLAdaptorWrapper implements SQLAdaptor {

    private final String serviceName;
    private final OPAVersion version;
    private final ServiceAction action;
    private final DebugStorage debugStorage;
    private final SQLAdaptor adaptor;

    private final Set<String> cache = new HashSet<>();

    @Override
    public String createContainmentSelect(LoadModel actionModel, ContainmentTable table, List<BindConditionGroup> binds, boolean useNamedBinds, boolean fromGlobal) {
        return debug(adaptor.createContainmentSelect(actionModel, table, binds, useNamedBinds, fromGlobal), false);
    }

    @Override
    public String createNonContainmentSelect(LoadModel actionModel, JunctionTable table, boolean useNamedBinds, boolean fromGlobal, boolean addFromKey, boolean addToKey) {
        return debug(adaptor.createNonContainmentSelect(actionModel, table, useNamedBinds, fromGlobal, true, true), false);
    }

    @Override
    public String createDelete(ActionModel actionModel, DatabaseTable table, boolean useNamedBinds) {
        return debug(adaptor.createDelete(actionModel, table, useNamedBinds), true);
    }

    @Override
    public String createInsert(DatabaseTable table, List<DatabaseField> fields, boolean useNamedBinds) {
        return debug(adaptor.createInsert(table, fields, useNamedBinds), true);
    }

    @Override
    public String createUpdate(DatabaseTable table, List<DatabaseField> fields, boolean useNamedBinds) {
        return debug(adaptor.createUpdate(table, fields, useNamedBinds), true);
    }

    @Override
    public String createSelect(DatabaseTable table, List<DatabaseField> fields, boolean useNamedBinds) {
        return debug(adaptor.createSelect(table, fields, useNamedBinds), false);
    }

    @Override
    public String createSequenceSelect(String sequence) {
        return debug(adaptor.createSequenceSelect(sequence), false);
    }

    /**
     * Adds a sql debug message.
     *
     * @param sql    SQL to debug
     * @param unique only unique SQL
     * @return SQL
     */
    private String debug(String sql, boolean unique) {
        if (!unique || cache.add(sql))
            debugStorage.addDebugMessage(serviceName, new SQLEvent(version, action, sql));
        return sql;
    }
}
