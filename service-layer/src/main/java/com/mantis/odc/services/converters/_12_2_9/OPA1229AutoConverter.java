/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters._12_2_9;

import com.mantis.odc.models.DataType;
import com.mantis.odc.models.FieldMapping;
import com.mantis.odc.services.models.*;
import com.mantis.odc.services.models.base.DatabaseEnumerationValue;
import com.oracle.opa.connector._12_2_9.data.types.DataLink;
import com.oracle.opa.connector._12_2_9.data.types.LinkRef;
import com.oracle.opa.connector._12_2_9.data.types.RowAction;
import com.oracle.opa.connector._12_2_9.metadata.types.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ValueMapping;

/**
 * This is an auto converter for OPA 12.2.9.
 */
@Mapper
@SuppressWarnings("unused")
public interface OPA1229AutoConverter {

    @Mapping(source = "fieldId.name", target = "name")
    @Mapping(source = "input", target = "canBeInput")
    @Mapping(source = "output", target = "canBeOutput")
    @Mapping(source = "required", target = "isRequired")
    MetaField createMetaField(FieldMapping field);

    @Mapping(source = "linkName", target = "name")
    @Mapping(source = "refs", target = "ref")
    DataLink createDataLink(DatabaseLink link);

    @Mapping(source = "databaseKeyAsString", target = "id")
    LinkRef createLinkRef(RowId rowId);

    @Mapping(target = "childValues", ignore = true)
    UncertainEnumVal createUncertainEnumVal(DatabaseEnumerationValue uncertainValue);

    @Mapping(target = "childValues", ignore = true)
    StringEnumVal createStringEnumValue(StringEnumerationValue stringEnumerationValue);

    @Mapping(target = "childValues", ignore = true)
    NumberEnumVal createNumberEnumValue(NumberEnumerationValue numberEnumerationValue);

    @ValueMapping(source = "CREATE", target = "insert")
    @ValueMapping(source = "UPDATE", target = "update")
    @ValueMapping(source = "DELETE", target = "delete")
    DBActionType createDBAction(RowAction action);

    @ValueMapping(source = "STRING_TYPE", target = "STRING")
    @ValueMapping(source = "DATE_TIME_TYPE", target = "DATETIME")
    @ValueMapping(source = "BOOLEAN_TYPE", target = "BOOLEAN")
    @ValueMapping(source = "DATE_TYPE", target = "DATE")
    @ValueMapping(source = "DECIMAL_TYPE", target = "DECIMAL")
    @ValueMapping(source = "TIME_TYPE", target = "TIMEOFDAY")
    @ValueMapping(source = "ATTACHMENT_TYPE", target = "STRING")
    MetaFieldDataType createMetaFieldDataType(DataType type);
}
