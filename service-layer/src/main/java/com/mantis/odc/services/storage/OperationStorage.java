/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.storage;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.mantis.odc.database.AppDB;
import com.mantis.odc.database.base.DAO;
import com.mantis.odc.models.Operation;
import com.mantis.odc.services.storage.base.BaseStorage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.val;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Stores all operation statistics. An operation is generated on an action call.
 * This is stored in memory as a buffer and then pulled into the database on regular
 * intervals. Calls to the cache for operations which have been removed, lead to that operation
 * being pulled from the database into the cache.
 */
@Data
@Singleton
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public final class OperationStorage extends BaseStorage<String, Operation> {

    /**
     * Maintenance cleanup thread.
     */
    private final ScheduledExecutorService maintenanceThread;

    @Inject
    public OperationStorage(AppDB appDB) {
        super(CacheBuilder.newBuilder()
                // clear out every 30 seconds
                .expireAfterWrite(30, SECONDS)
                // on removal from the cache, store in the database.
                .removalListener(new OperationRemovalListener(appDB))
                .build());
        maintenanceThread = Executors.newScheduledThreadPool(1);
        maintenanceThread.scheduleAtFixedRate(cache::cleanUp, 30, 30, SECONDS);
    }

    /**
     * Adds an operation.
     *
     * @param operation operation
     */
    public void addOperation(Operation operation) {
        cache.put(operation.getUid(), operation);
    }

    /**
     * Runs on operation removal from the cache.
     */
    private static class OperationRemovalListener implements RemovalListener<String, Operation> {
        /**
         * Operations in the database.
         */
        private final DAO<BigDecimal, Operation> operationDAO;

        private OperationRemovalListener(AppDB appDB) {
            operationDAO = appDB.getDAO(Operation.class);
        }

        @Override
        public void onRemoval(RemovalNotification<String, Operation> removalNotification) {
            Operation operation = removalNotification.getValue();

            // value has not been persisted to the database
            if (operation != null && operation.getId() == null)
                operationDAO.create(operation);
        }
    }

    /**
     * Loads operations from the database.
     */
    private static class OperationCacheLoader extends CacheLoader<String, Operation> {

        /**
         * Operations in the database.
         */
        private final DAO<BigDecimal, Operation> operationDAO;

        private OperationCacheLoader(AppDB appDB) {
            operationDAO = appDB.getDAO(Operation.class);
        }

        @Override
        public Operation load(String key) {
            return loadOperationByUID(key);
        }

        /**
         * Loads an operation by UID.
         *
         * @param key UID
         * @return operation
         */
        private Operation loadOperationByUID(String key) {
            val params = new HashMap<String, Object>();
            params.put("uid", key);
            return operationDAO.scalarQuery("SELECT o FROM Operation o WHERE o.uid = :uid", params);
        }
    }
}
