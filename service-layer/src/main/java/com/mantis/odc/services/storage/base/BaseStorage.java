/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.storage.base;

import com.google.common.cache.Cache;
import lombok.Data;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Base thread safe storage class.
 */
@Data
public abstract class BaseStorage<KEY, VALUE> {

    /**
     * Underlying store.
     */
    protected final Cache<KEY, VALUE> cache;

    /**
     * @param key key value.
     * @return value or null
     */
    public VALUE get(KEY key) {
        return cache.getIfPresent(key);
    }

    /**
     * @param key key value.
     * @return value or none
     */
    public Optional<VALUE> getOpt(KEY key) {
        return Optional.ofNullable(cache.getIfPresent(key));
    }

    /**
     * @param key   key to set
     * @param value value
     * @return storage
     */
    public BaseStorage<KEY, VALUE> set(KEY key, VALUE value) {
        cache.put(key, value);
        return this;
    }

    /**
     * @param key key to check
     * @return true if the values exists
     */
    public boolean contains(KEY key) {
        return cache.asMap().containsKey(key);
    }

    /**
     * @return all keys
     */
    public Set<KEY> listKeys() {
        return cache.asMap().keySet();
    }

    /**
     * @return all values.
     */
    public Collection<VALUE> listValues() {
        return cache.asMap().values();
    }

    /**
     * @return all deployed values.
     */
    public Set<Map.Entry<KEY, VALUE>> listPairs() {
        return cache.asMap().entrySet();
    }

    /**
     * Close the storage.
     */
    public void close() {
        cache.invalidateAll();
        cache.cleanUp();
    }
}
