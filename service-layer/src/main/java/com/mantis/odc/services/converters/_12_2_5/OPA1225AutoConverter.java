/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters._12_2_5;

import com.mantis.odc.models.DataType;
import com.mantis.odc.services.converters._12_2.OPA122AutoConverter;
import com.mantis.odc.services.models.NumberEnumerationValue;
import com.mantis.odc.services.models.StringEnumerationValue;
import com.mantis.odc.services.models.base.DatabaseEnumerationValue;
import com.oracle.opa.connector._12_2_5.metadata.types.MetaFieldDataType;
import com.oracle.opa.connector._12_2_5.metadata.types.NumberEnumVal;
import com.oracle.opa.connector._12_2_5.metadata.types.StringEnumVal;
import com.oracle.opa.connector._12_2_5.metadata.types.UncertainEnumVal;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ValueMapping;

/**
 * This is an auto converter, it handles some of the easier conversions and is used in the
 * main OPA1222Converter.
 */
@Mapper
@SuppressWarnings("unused")
public interface OPA1225AutoConverter extends OPA122AutoConverter {

    @ValueMapping(source = "STRING_TYPE", target = "STRING")
    @ValueMapping(source = "DATE_TIME_TYPE", target = "DATETIME")
    @ValueMapping(source = "BOOLEAN_TYPE", target = "BOOLEAN")
    @ValueMapping(source = "DATE_TYPE", target = "DATE")
    @ValueMapping(source = "DECIMAL_TYPE", target = "DECIMAL")
    @ValueMapping(source = "TIME_TYPE", target = "TIMEOFDAY")
    @ValueMapping(source = "ATTACHMENT_TYPE", target = "STRING")
    MetaFieldDataType create1225MetaFieldDataType(DataType type);

    @Mapping(target = "childValues", ignore = true)
    UncertainEnumVal createUncertainEnumVal(DatabaseEnumerationValue uncertainValue);

    @Mapping(target = "childValues", ignore = true)
    StringEnumVal createStringEnumValue(StringEnumerationValue stringEnumerationValue);

    @Mapping(target = "childValues", ignore = true)
    NumberEnumVal createNumberEnumValue(NumberEnumerationValue numberEnumerationValue);
}
