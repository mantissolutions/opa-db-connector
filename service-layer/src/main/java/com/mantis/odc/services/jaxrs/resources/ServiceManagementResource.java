/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.resources;

import com.mantis.odc.services.Service;
import com.mantis.odc.services.jaxrs.converters.ServiceBeanMapper;
import com.mantis.odc.services.jaxrs.models.ServiceBean;
import com.mantis.odc.services.storage.ServiceStorage;
import io.swagger.annotations.*;
import org.apache.shiro.authz.annotation.RequiresAuthentication;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static com.mantis.odc.services.jaxrs.utils.ResponseUtils.*;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

/**
 * Manages service deployed to ODC.
 */
@Singleton
@RequiresAuthentication
@Path("/service-management")
@Api(value = "Services Management", description = "Methods for managing deployed services.")
public class ServiceManagementResource {

    private final ServiceStorage serviceStorage;
    private final ServiceBeanMapper mapper;

    @Inject
    public ServiceManagementResource(ServiceStorage serviceStorage, ServiceBeanMapper mapper) {
        this.serviceStorage = serviceStorage;
        this.mapper = mapper;
    }

    /**
     * @return deployed service names
     */
    @GET
    @Produces(value = APPLICATION_JSON)
    @ApiOperation(value = "Service Names", notes = "Lists all deployed service names.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Deployed service names.")
    })
    public List<String> getServiceNames() {
        return serviceStorage.listValues().stream()
                .map(Service::getName)
                .collect(Collectors.toList());
    }

    /**
     * Returns the named service.
     *
     * @param name service name
     * @return service information
     */
    @GET
    @Path("/{name}")
    @Produces(value = APPLICATION_JSON)
    @ApiOperation(value = "Get Service", notes = "Returns all information about a deployed service.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Deployed Service Information."),
            @ApiResponse(code = 404, message = "Thrown on an unknown service.", response = String.class)
    })
    public ServiceBean getService(@ApiParam(value = "Service Name", required = true)
                                  @PathParam("name") String name) {
        return serviceStorage.getOpt(name)
                .map(mapper::serviceToBean)
                .orElseThrow(() -> throwError(NOT_FOUND, "Unknown Service " + name));
    }

    /**
     * Deploys the named mapping.
     *
     * @param mappingName mapping name
     * @return service bean
     */
    @GET
    @Path("/mapping/{mappingName}/deployMapping")
    @Produces(value = APPLICATION_JSON)
    @ApiOperation(value = "Deploy Mapping", notes = "Deploys a given mapping.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Deployed Service Information."),
            @ApiResponse(code = 400, message = "Thrown on an unknown mapping or invalid mapping.",
                    response = String.class)
    })
    public ServiceBean deployMapping(@ApiParam(value = "Mapping Name", required = true)
                                     @PathParam("mappingName") String mappingName) {
        return serviceStorage.deployMappingByName(mappingName)
                .map(mapper::serviceToBean)
                .orElseThrow(() -> throwError(BAD_REQUEST, "Failed to deployMapping mapping " + mappingName + ". " +
                        "Check that the mapping exists, is valid and is not already deployed."));
    }

    /**
     * Un-deploys the given service.
     *
     * @param name service name
     * @return ok if success
     */
    @GET
    @Path("/{name}/un-deployMapping")
    @ApiOperation(value = "Un-Deploy Mapping", notes = "Un-Deploys a given mapping.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok message.", response = String.class),
            @ApiResponse(code = 400, message = "Thrown if the Connector fails to un-deploy a service.",
                    response = String.class),
            @ApiResponse(code = 404, message = "Thrown on an unknown service.", response = String.class),
    })
    public Response undeployService(@PathParam("name") String name) {
        if (!serviceStorage.contains(name))
            throw throwError(NOT_FOUND, "Unknown Service " + name);
        return serviceStorage.undeployService(name) ? ok() : error(BAD_REQUEST, "Failed to un-deployMapping service " + name + ". " +
                "Check that a service with the current name is deployed.");
    }
}
