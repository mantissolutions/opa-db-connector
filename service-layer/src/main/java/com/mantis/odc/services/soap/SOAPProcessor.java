/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.soap;

import com.mantis.odc.models.OPAVersion;
import com.mantis.odc.models.Operation;
import com.mantis.odc.models.ServiceAction;
import com.mantis.odc.services.Service;
import com.mantis.odc.services.actions.TimingActionProcessorImpl;
import com.mantis.odc.services.converters.OPAConverterFactory;
import com.mantis.odc.services.models.debug.ErrorEvent;
import com.mantis.odc.services.models.debug.MessageEvent;
import com.mantis.odc.services.models.debug.base.DebugEvent;
import com.mantis.odc.services.sql.SQLFactory;
import com.mantis.odc.services.sql.adaptors.DebugSQLAdaptorWrapper;
import com.mantis.odc.services.sql.adaptors.base.SQLAdaptor;
import com.mantis.odc.services.storage.DebugStorage;
import lombok.Cleanup;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.w3c.dom.Node;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.WebApplicationException;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPMessage;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.mantis.odc.models.ServiceAction.CheckAlive;
import static com.mantis.odc.models.ServiceAction.GetMetadata;
import static com.mantis.odc.services.soap.SOAPUtils.createSOAPFault;
import static com.mantis.odc.services.soap.SOAPUtils.createSOAPMessage;
import static java.util.Collections.EMPTY_LIST;

/**
 * Processes SOAP actions.
 */
@Slf4j
@Singleton
public class SOAPProcessor {

    private final DebugStorage debugStorage;

    @Inject
    public SOAPProcessor(DebugStorage debugStorage) {
        this.debugStorage = debugStorage;
    }

    /**
     * Processes a SOAP message for a given service action.
     *
     * @param action  service action
     * @param builder operation builder for stats
     * @param service service
     * @param version OPA version
     * @param message soap message  @return JAXB model
     */
    @SneakyThrows
    public SOAPMessage process(ServiceAction action, Operation.OperationBuilder builder, Service service, OPAVersion version, SOAPMessage message) {
        SOAPMessage returnMessage = null;
        val mapping = service.getMapping();
        val converter = OPAConverterFactory.getConverter(version);

        // debug message for inbound message
        debug(service, new MessageEvent(version, action, true, message));

        // transport, errors here are ignored
        val marshaller = version.createMarshaller();
        val unmarshaller = version.createUnmarshaller();

        try { // all exceptions lead to a SOAP fault

            // set operation details
            builder.mapping(mapping).type(action);

            // return check alive. No database connection is required for this.
            if (action == CheckAlive) {
                returnMessage = createSOAPMessage(marshaller, converter.getCheckAlive());
                debug(service, new MessageEvent(version, action, false, returnMessage));
                return returnMessage;
            } else if (action == GetMetadata && !version.isSupportsEnums() && mapping.getEnumerations().isEmpty()) {
                returnMessage = createSOAPMessage(marshaller, converter.getMetaData(mapping, EMPTY_LIST));
                debug(service, new MessageEvent(version, action, false, returnMessage));
                return returnMessage;
            }

            // get soap message body
            Object body = getSoapBody(unmarshaller, message);

            // get database information
            val cred = getDatabaseCredentials(message);
            val db = service.getDatasource().connect(cred.getUsername(), cred.getPassword());
            val typeConverter = SQLFactory.getConverter(service.getDriver());

            // create adaptor
            SQLAdaptor adaptor = SQLFactory.getAdaptor(service.getDriver());
            if (mapping.isDebugMode())
                adaptor = new DebugSQLAdaptorWrapper(service.getName(), version, action, debugStorage, adaptor);

            // create an action processor
            @Cleanup val processor = new TimingActionProcessorImpl(db, adaptor, typeConverter, builder);

            // process action
            switch (action) {
                case GetMetadata:
                    returnMessage = createSOAPMessage(marshaller, converter.getMetaData(mapping, processor.enumerations(mapping.getEnumerations())));
                    break;
                case Load:
                    returnMessage = createSOAPMessage(marshaller, converter.fromLoadModel(processor.load(converter.toLoadModel(mapping, body), true, OptionalInt.empty())));
                    break;
                case Save:
                    returnMessage = createSOAPMessage(marshaller, converter.fromSaveModel(processor.save(converter.toSaveModel(mapping, body))));
                    break;
                case GetCheckpoint:
                    returnMessage = createSOAPMessage(marshaller, converter.fromCheckpointModelToGetCheckpointResponse(processor.getCheckpoint(converter.toCheckpointModelFromGetCheckpointRequest(mapping, body))));
                    break;
                case SetCheckpoint:
                    returnMessage = createSOAPMessage(marshaller, converter.fromCheckpointModelToSetCheckpointResponse(processor.setCheckpoint(converter.toCheckpointModelFromSetCheckpointRequest(mapping, body))));
                    break;
                case ExecuteQuery:
                    val model = converter.toQueryModelFromQueryRequest(mapping, body);
                    returnMessage = createSOAPMessage(marshaller, converter.fromQueryModelToQueryResponse(processor.load(model, false, model.getLimit())));
                    break;
            }

            // debug outbound message
            debug(service, new MessageEvent(version, action, false, returnMessage));

            // run a final commit
            db.getConnectionProvider().get().commit();

            return returnMessage;
        } catch (Exception e) { // catch any errors and return them as faults.
            log.error("Failed to process action. {}", e);
            val fault = createSOAPFault(marshaller, e, converter.getFault(e));
            debug(service, new ErrorEvent(version, action, e.getMessage(), e));
            return fault;
        }
    }

    /**
     * Adds a debug message.
     *
     * @param service service
     * @param event   debug event
     */
    private void debug(Service service, DebugEvent event) {
        if (service.getMapping().isDebugMode()) {
            debugStorage.addDebugMessage(service.getName(), event);
        }
    }

    /**
     * Returns the database credentials.
     *
     * @param message soap message.
     * @return credentials
     */
    @SneakyThrows
    private Credentials getDatabaseCredentials(SOAPMessage message) {
        return new Credentials(
                getElementValue("Username", message.getSOAPHeader())
                        .orElseThrow(() -> new WebApplicationException("Missing username.")),
                getElementValue("Password", message.getSOAPHeader())
                        .orElseThrow(() -> new WebApplicationException("Missing password.")));
    }

    /**
     * @param unmarshaller unmarshaller
     * @param message      soap message
     * @return JAXB object
     */
    @SneakyThrows
    private Object getSoapBody(Unmarshaller unmarshaller, SOAPMessage message) {
        return unmarshaller.unmarshal(elementStream(message.getSOAPBody()).findFirst()
                .orElseThrow(() -> new WebApplicationException("Missing SOAP body.")));
    }

    /**
     * Returns the element value. This is a recursive search.
     *
     * @param tagName tag name
     * @param parent  parent to search
     * @return string value
     */
    private Optional<String> getElementValue(String tagName, SOAPElement parent) {
        return recursiveElementStream(parent)
                .filter(soapElement -> soapElement.getLocalName().equals(tagName))
                .map(Node::getTextContent)
                .findFirst();
    }

    /**
     * @param parent parent element
     * @return recursive stream of elements inclusive of the root element
     */
    private Stream<SOAPElement> recursiveElementStream(SOAPElement parent) {
        return Stream.concat(Stream.of(parent), elementStream(parent).flatMap(this::recursiveElementStream));
    }

    /**
     * @param parent parent SOAP element
     * @return stream of child elements
     */
    private Stream<SOAPElement> elementStream(SOAPElement parent) {
        Iterable<Node> iterable = parent::getChildElements;
        return StreamSupport.stream(iterable.spliterator(), false)
                .filter(node -> node instanceof SOAPElement)
                .map(node -> (SOAPElement) node);
    }

    /**
     * Login credentials.
     */
    @Data
    private class Credentials {
        private final String username;
        private final String password;
    }

}
