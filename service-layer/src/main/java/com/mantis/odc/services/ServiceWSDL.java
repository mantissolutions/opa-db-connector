/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.services;

import com.mantis.odc.models.OPAVersion;
import lombok.Builder;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.val;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.InputStreamReader;
import java.io.StringWriter;

/**
 * Base WSDL type.
 */
@Data
public class ServiceWSDL {

    private final VelocityContext context;
    private final String result;

    /**
     * @param version     OPA version
     * @param serviceName service name
     * @param location    service location
     */
    @Builder
    @SneakyThrows
    public ServiceWSDL(OPAVersion version, String serviceName, String location) {
        // context
        context = new VelocityContext();
        context.put("serviceName", serviceName);
        context.put("serviceLocation", location);

        // string writer
        val w = new StringWriter();
        val r = new InputStreamReader(getClass().getResourceAsStream("/" + version.getWsdlPath()));

        Velocity.evaluate(context, w, "", r);
        result = w.toString();
    }

    public String render() {
        return result;
    }
}
