/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.resources;

import com.mantis.odc.database.dao.DatasourceMappingDAO;
import com.mantis.odc.jaxb.DatasourceType;
import com.mantis.odc.models.DatasourceMapping;
import com.mantis.odc.services.storage.DatasourceStorage;
import io.swagger.annotations.*;
import org.apache.shiro.authz.annotation.RequiresAuthentication;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static com.mantis.odc.services.jaxrs.utils.ResponseUtils.ok;
import static com.mantis.odc.services.jaxrs.utils.ResponseUtils.throwError;
import static java.text.MessageFormat.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

/**
 * Manages data source mappings deployed to the ODC server.
 */
@Singleton
@RequiresAuthentication
@Path("/datasources")
@Api(value = "Datasource Mapping Management", description = "Methods for managing Datasource Mappings.")
public class DatasourceMappingsResource {

    private final DatasourceMappingDAO dao;
    private final DatasourceStorage storage;

    @Inject
    public DatasourceMappingsResource(DatasourceMappingDAO dao, DatasourceStorage storage) {
        this.dao = dao;
        this.storage = storage;
    }

    /**
     * Returns all the valid datasource mappings stored in the server.
     *
     * @param deployed is the datasource mapping deployed
     * @return lists all datasource mappings
     */
    @GET
    @Produces(APPLICATION_JSON)
    @ApiOperation(value = "List Datasource Mappings", notes = "List of Datasource Mappings.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of Mappings.")
    })
    public List<String> listDatasourceMappings(
            @ApiParam(value = "Deployed flag. If set only deployed Datasource Mappings are returned.")
            @DefaultValue(value = "false")
            @QueryParam("deployed") boolean deployed) {
        return dao.list().stream()
                .filter(mapping -> !deployed || storage.contains(mapping))
                .map(DatasourceMapping::getName)
                .collect(Collectors.toList());
    }

    /**
     * Returns the named datasource mapping.
     *
     * @param name datasource mapping name
     * @return datasource mapping
     */
    @GET
    @Path("/{name}")
    @Produces(APPLICATION_XML)
    @ApiOperation(value = "Get Datasource Mapping", notes = "Returns the Datasource Mapping.",
            response = DatasourceType.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Datasource Mapping XML.", response = DatasourceType.class),
            @ApiResponse(code = 404, message = "Thrown if the Connector cant find the Datasource Mapping.",
                    response = String.class)
    })
    public DatasourceMapping getDatasourceMapping(@ApiParam(value = "Datasource Mapping name.", required = true)
                                                  @PathParam("name") String name) {
        return dao.getDatasourceMappingByName(name)
                .orElseThrow(() -> throwError(NOT_FOUND, "Unknown datasource mapping " + name));
    }

    /**
     * Creates a new mapping.
     *
     * @param mapping mapping
     * @return ok on success
     */
    @POST
    @Consumes(APPLICATION_XML)
    @ApiOperation(value = "Create Datasource Mapping", notes = "Creates a new Datasource mapping.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok message.", response = String.class),
            @ApiResponse(code = 400, message = "Thrown if the Connector fails to create the Datasource Mapping.",
                    response = String.class)
    })
    public Response createDatasourceMapping(
            @ApiParam(value = "Datasource Mapping to create", required = true) DatasourceMapping mapping) {
        if (dao.getDatasourceMappingByName(mapping.getName()).isPresent())
            throw throwError(BAD_REQUEST, format("A datasource mapping with the name {0} already exists.", mapping.getName()));
        dao.create(mapping);
        return ok();
    }

    /**
     * Removes the named datasource mapping from the database.
     *
     * @param name datasource mapping name
     * @return ok on success
     */
    @DELETE
    @Path("/{name}")
    @ApiOperation(value = "Remove Datasource Mapping", notes = "Removes an existing Datasource mapping.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok message.", response = String.class),
            @ApiResponse(code = 404, message = "Thrown if the Connector cant find the Datasource Mapping.",
                    response = String.class)
    })
    public Response removeDatasourceMapping(@ApiParam(value = "Datasource Mapping name.", required = true)
                                            @PathParam("name") String name) {
        dao.delete(dao.getDatasourceMappingByName(name)
                .map(DatasourceMapping::getId)
                .orElseThrow(() -> throwError(NOT_FOUND, "Unknown datasource mapping " + name)));
        return ok();
    }
}
