/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters.base;

import com.mantis.odc.models.*;
import com.mantis.odc.services.converters.exceptions.MissingMappingException;
import com.mantis.odc.services.models.*;
import lombok.SneakyThrows;
import lombok.val;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.jooq.lambda.Unchecked;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.mantis.odc.services.converters.exceptions.MissingMappingException.MappingType.Field;
import static com.mantis.odc.services.converters.exceptions.MissingMappingException.MappingType.Table;

/**
 * Converters that can be shared across OPA versions.
 */
public class SharedConverters {

    /**
     * Creates a containment table.
     *
     * @param mapping    mapping
     * @param tableName  OPA table name
     * @param fieldNames list of field names to include
     * @return containment table
     * @throws MissingMappingException on missing table mapping
     */
    public static ContainmentTable createContainmentTable(Mapping mapping, String tableName, List<String> fieldNames) throws MissingMappingException {
        val tableMapping = mapping.getTable(tableName).orElseThrow(() -> new MissingMappingException(Table, tableName));
        return createContainmentTable(tableMapping, fieldNames);
    }

    /**
     * Creates a containment table.
     *
     * @param tableMapping table mapping
     * @return containment table
     */
    public static ContainmentTable createContainmentTable(TableMapping tableMapping, List<String> fieldNames) {
        val builder = ContainmentTable.builder()
                .source(tableMapping.getTableId().getSource())
                .opaName(tableMapping.getTableId().getName())
                .linkName(tableMapping.getLinkName())
                .alias(tableMapping.getAlias())
                .inferred(tableMapping.isInferred())
                .primaryKey(createPrimaryKey(tableMapping.getPrimaryKey()))
                .foreignKey(tableMapping.getForeignKeyOpt()
                        .map(fk -> createForeignKey(tableMapping.getParent().getTableId(), fk))
                        .orElse(null));

        if (fieldNames != null) builder.fields(fieldNames.stream()
                .map(Unchecked.function(field -> createDataField(tableMapping, field)))
                .collect(Collectors.toList()));
        else builder.fields(tableMapping.getFields().stream()
                .filter(fm -> !(fm.isPrimaryKey() || fm.isForeignKey()))
                .map(SharedConverters::createDataField)
                .collect(Collectors.toList()));

        return builder.build();
    }

    /**
     * Creates a primary key field.
     *
     * @param primaryKey primary key mapping.
     * @return primary key field
     */
    public static PrimaryKeyField createPrimaryKey(FieldMapping primaryKey) {
        return PrimaryKeyField.builder()
                .source(primaryKey.getFieldId().getSource())
                .opaName(primaryKey.getFieldId().getName())
                .sequence(primaryKey.getSequence())
                .autoIncrement(primaryKey.isAutoIncrement())
                .type(primaryKey.getType())
                .build();
    }

    /**
     * Creates a foreign key field.
     *
     * @param target     target table.
     * @param foreignKey foreign key mapping
     * @return foreign key
     */
    public static ForeignKeyField createForeignKey(OPAMappingId target, FieldMapping foreignKey) {
        return ForeignKeyField.builder()
                .source(foreignKey.getFieldId().getSource())
                .opaName(foreignKey.getFieldId().getName())
                .type(foreignKey.getType())
                .target(target.getSource())
                .targetOpaName(target.getName())
                .build();
    }

    /**
     * Creates a data field from the OPA field.
     *
     * @param tableMapping table mapping
     * @param fieldName    field name
     * @return data field
     * @throws MissingMappingException on missing field mapping
     */
    public static DatabaseField createDataField(TableMapping tableMapping, String fieldName) throws MissingMappingException {
        val fieldMapping = tableMapping.getField(fieldName).orElseThrow(() -> new MissingMappingException(Field, fieldName));
        return createDataField(fieldMapping);
    }

    /**
     * Creates a data field from the OPA field.
     *
     * @param fieldMapping field mapping
     * @return data field
     */
    public static DatabaseField createDataField(FieldMapping fieldMapping) {
        return DatabaseField.fieldBuilder()
                .source(fieldMapping.getFieldId().getSource())
                .opaName(fieldMapping.getFieldId().getName())
                .type(fieldMapping.getType())
                .build();
    }

    /**
     * Creates a junction table from the junction table mapping.
     *
     * @param mapping    junction table mapping
     * @param targetName opa table name for the target table
     * @return junction table
     */
    public static JunctionTable createJunctionTable(JunctionTableMapping mapping, String targetName) {
        val fromTable = mapping.getFromTable();
        val toTable = mapping.getToTable();
        return JunctionTable.builder()
                .source(mapping.getTableId().getSource())
                .opaName(mapping.getTableId().getName())
                .linkName(mapping.getLinkName())
                .alias(mapping.getAlias())
                .fromKey(ForeignKeyField.builder()
                        .source(mapping.getFromKeyName())
                        .opaName(mapping.getFromKeyName())
                        .type(fromTable.getPrimaryKey().getType())
                        .target(fromTable.getTableId().getSource())
                        .targetOpaName(fromTable.getTableId().getName())
                        .build())
                .toKey(ForeignKeyField.builder()
                        .source(mapping.getToKeyName())
                        .opaName(mapping.getToKeyName())
                        .type(toTable.getPrimaryKey().getType())
                        .target(toTable.getTableId().getSource())
                        .targetOpaName(toTable.getTableId().getName())
                        .build())
                .supportedSource(mapping.getMatchingSource(targetName))
                .build();
    }

    /**
     * Creates a bind condition from a value and bind mapping.
     *
     * @param value       value to set
     * @param bindMapping bind mapping
     * @return bind condition
     */
    @SneakyThrows
    public static BindConditionGroup createBindCondition(String value, BindMapping bindMapping) {
        val fieldMapping = bindMapping.getField();
        val builder = BindCondition.builder()
                .source(fieldMapping.getFieldId().getSource())
                .type(fieldMapping.getType())
                .alias(bindMapping.getAlias())
                .join(bindMapping.getCondition())
                .operator(bindMapping.getOperator());
        if (bindMapping.getOperator() == Operator.in) builder.values(Arrays.asList(value.split(",")));
        else builder.value(value);
        return builder
                .build()
                .toGroup();
    }

    /**
     * Creates a new script template merged with the OPA context.
     *
     * @param script script to run
     * @param params context params to bind to the template
     * @return merged script
     */
    public static Optional<String> createScript(String script, Map<String, String> params) {
        if (script != null) {
            val sr = new StringWriter();
            Velocity.evaluate(new VelocityContext(params), sr, "", script);
            return Optional.ofNullable(sr.toString());
        } else return Optional.empty();
    }
}
