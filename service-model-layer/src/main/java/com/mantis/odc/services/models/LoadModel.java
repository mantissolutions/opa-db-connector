/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.services.models.base.ActionModel;
import com.mantis.odc.services.models.base.DatabaseTable;
import lombok.*;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;
import java.util.stream.Collectors;

/**
 * Model used to load data from the database. This has bind mappings.
 */
@ToString(callSuper = true, exclude = "executedSelects")
@EqualsAndHashCode(callSuper = true, exclude = "executedSelects")
public class LoadModel extends ActionModel {

    /**
     * Number of selects executed.
     */
    @Getter
    private long executedSelects;

    /**
     * Bind conditions used to restrict load results.
     */
    @Getter
    private final List<BindConditionGroup> conditions;

    @Builder(toBuilder = true)
    public LoadModel(TimeZone timeZone,
                     Locale locale,
                     RowId globalId,
                     String rootTable,
                     Optional<String> preScript,
                     Optional<String> postScript,
                     @Singular List<DatabaseTable> tables,
                     @Singular List<BindConditionGroup> conditions) {
        super(timeZone, locale, globalId, rootTable, preScript, postScript, tables);
        this.conditions = conditions;
    }

    /**
     * Returns the parent conditions.
     *
     * @param table parent table
     * @return bind conditions
     */
    private List<BindCondition> getParentConditions(DatabaseTable table) {
        return getConditionsForTable(table).stream()
                .map(BindConditionGroup::fromGroup)
                .collect(Collectors.toList());
    }

    /**
     * Returns all the relevant bind conditions for the given table.
     *
     * @param table data table
     * @return list of bind conditions.
     */
    public List<BindConditionGroup> getConditionsForTable(DatabaseTable table) {
        List<BindCondition> conditions = this.conditions.stream()
                // get all bind mappings across all groups
                .flatMap(bg -> bg.getConditions().stream())
                .filter(c -> c.getAlias().equals(table.getAlias()))
                .collect(Collectors.toList());

        if (table instanceof ContainmentTable) {
            ContainmentTable containmentTable = (ContainmentTable) table;
            containmentTable.getParent(this).ifPresent(pt -> conditions.addAll(getParentConditions(pt)));
        } else if (table instanceof JunctionTable) {
            JunctionTable junctionTable = (JunctionTable) table;
            junctionTable.getFromTable(this).ifPresent(ft -> conditions.addAll(getParentConditions(ft)));
            junctionTable.getToTable(this).ifPresent(tt -> conditions.addAll(getParentConditions(tt)));
        }
        // wrap in groups
        return conditions.stream()
                .map(BindCondition::toGroup)
                .collect(Collectors.toList());
    }

    /**
     * Count for the number of selects.
     */
    public void incrementSelects() {
        executedSelects++;
    }
}
