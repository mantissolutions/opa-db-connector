/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import lombok.*;

import java.util.List;

/**
 * Submit row.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class DatabaseSubmitRow extends DatabaseRow {

    /**
     * Action to undertake.
     */
    private final DBActionType action;

    /**
     * Fields to return back after update or select.
     */
    private final List<DatabaseField> returnFields;

    @Builder(builderMethodName = "submitRowBuilder")
    public DatabaseSubmitRow(String source,
                             RowId id,
                             RowId fk,
                             DBActionType action,
                             @Singular(value = "item") List<RowValue> data,
                             @Singular List<DatabaseField> returnFields) {
        super(source, id, fk, data);
        this.action = action;
        this.returnFields = returnFields;
    }

    /**
     * @return true if the submit row has returning fields.
     */
    public boolean hasReturningFields() {
        return !returnFields.isEmpty();
    }

    /**
     * @return true if the database key has not been set.
     */
    public boolean requiresSequenceValue(SaveModel saveModel) {
        return id.getDatabaseKey() == null && !getTable(saveModel).getPrimaryKey().isAutoIncrement();
    }
}
