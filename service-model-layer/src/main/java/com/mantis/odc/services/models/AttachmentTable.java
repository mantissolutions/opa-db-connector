/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.services.models.base.DatabaseTable;
import lombok.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.mantis.odc.models.DataType.ATTACHMENT_TYPE;
import static com.mantis.odc.models.DataType.STRING_TYPE;

/**
 * Models an attachment table.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class AttachmentTable extends DatabaseTable {

    /**
     * Source field name.
     */
    private final String sourceField;

    /**
     * key field name.
     */
    private final String keyField;

    /**
     * Name field name.
     */
    private final String nameField;

    /**
     * File name field name
     */
    private final String fileNameField;

    /**
     * Attachment description field name.
     */
    private final String descriptionField;

    /**
     * Attachment data field name.
     */
    private final String fileField;

    /**
     * Attachment rows.
     */
    private final List<AttachmentRow> attachmentRows;

    @Builder(toBuilder = true)
    public AttachmentTable(String source,
                           String opaName,
                           String sourceField,
                           String keyField,
                           String nameField,
                           String fileNameField,
                           String descriptionField,
                           String fileField,
                           @Singular List<AttachmentRow> attachmentRows) {
        super(source, opaName, "at1", PrimaryKeyField.builder().source(keyField).type(STRING_TYPE).build(),
                Arrays.asList(
                        new DatabaseField(sourceField, STRING_TYPE),
                        new DatabaseField(nameField, STRING_TYPE),
                        new DatabaseField(fileNameField, STRING_TYPE),
                        new DatabaseField(descriptionField, STRING_TYPE),
                        new DatabaseField(fileField, ATTACHMENT_TYPE)), (List) attachmentRows);
        this.sourceField = sourceField;
        this.keyField = keyField;
        this.nameField = nameField;
        this.fileNameField = fileNameField;
        this.descriptionField = descriptionField;
        this.fileField = fileField;
        this.attachmentRows = attachmentRows;
    }

    /**
     * Updates any key links in the current attachment table.
     *
     * @param row row to update from
     */
    public void updateLinks(DatabaseSubmitRow row) {
        for (val ar : getAttachmentRows())
            if (ar.getTable().equals(row.source) && ar.getId().getOpaKey().equals(row.getId().getOpaKey()))
                ar.getId().setDatabaseKey(row.getId().getDatabaseKey());
    }

    @Override
    public List<DatabaseSubmitRow> getSubmitRows() {
        return attachmentRows.stream()
                .map(attachmentRow -> (DatabaseSubmitRow) attachmentRow)
                .collect(Collectors.toList());
    }
}
