/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.services.models.base.ActionModel;
import com.mantis.odc.services.models.base.DatabaseTable;
import lombok.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Containment table type. Has a primary key, and an optional foreign key.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ContainmentTable extends DatabaseTable {

    /**
     * OPA link name.
     */
    private final String linkName;

    /**
     * Id the table is inferred.
     */
    private final boolean inferred;

    /**
     * Optional foreign key field.
     */
    private final ForeignKeyField foreignKey;

    @Builder(toBuilder = true)
    public ContainmentTable(String source,
                            String opaName,
                            String alias,
                            String linkName,
                            boolean inferred,
                            PrimaryKeyField primaryKey,
                            ForeignKeyField foreignKey,
                            @Singular List<DatabaseField> fields,
                            @Singular List<DatabaseRow> rows) {
        super(source, opaName, alias, primaryKey, fields, rows);
        this.linkName = linkName;
        this.inferred = inferred;
        this.foreignKey = foreignKey;
    }

    /**
     * @return foreign key
     */
    public Optional<ForeignKeyField> getForeignKey() {
        return Optional.ofNullable(foreignKey);
    }

    /**
     * Returns the parent containment table.
     *
     * @param actionModel action model
     * @return containment table
     */
    public Optional<ContainmentTable> getParent(ActionModel actionModel) {
        return getForeignKey()
                .map(ForeignKeyField::getTarget)
                .flatMap(actionModel::getTable)
                .map(t -> (ContainmentTable) t);
    }

    /**
     * Returns all the parent tables of the current table. This ascends to the root table.
     *
     * @param actionModel action model
     * @return all parent tables
     */
    public Stream<ContainmentTable> getParents(ActionModel actionModel) {
        return Stream.concat(getParent(actionModel).map(Stream::of).orElse(Stream.empty()),
                getParent(actionModel).map(parent -> parent.getParents(actionModel)).orElse(Stream.empty()));
    }

    /**
     * Returns the root containment table.
     *
     * @param actionModel action model
     * @return root table.
     */
    public ContainmentTable getRoot(ActionModel actionModel) {
        return getParent(actionModel)
                .map(p -> p.getRoot(actionModel))
                .orElse(this);
    }

    @Override
    public List<DatabaseField> getSelectFields(ActionModel actionModel) {
        val fields = super.getSelectFields(actionModel);
        // check the table is not the current global containment table
        if (!actionModel.getRootTable().equals(source) && !actionModel.isDisconnected(this))
            getForeignKey().ifPresent(fields::add);
        return fields;
    }

    @Override
    public List<DatabaseField> getInsertFields(DatabaseSubmitRow row) {
        val fields = super.getInsertFields(row);
        getForeignKey().ifPresent(fk -> fields.add(1, fk));
        return fields;
    }

    /**
     * @param row parent row.
     * @return list of dependent rows
     */
    public List<DatabaseRow> getDependentRows(DatabaseRow row) {
        return getRows().stream()
                .filter(r -> r.getFk().getOpaKey().equals(row.getId().getOpaKey()))
                .collect(Collectors.toList());
    }

    /**
     * Returns all the child ids for a given parent id value.
     *
     * @param id parent id
     * @return matching child ids in the current table.
     */
    public List<RowId> getRefs(RowId id) {
        return getRows().stream()
                .filter(databaseRow -> databaseRow.getFk() != null && databaseRow.getFk().equals(id))
                .map(DatabaseRow::getId)
                .collect(Collectors.toList());
    }
}
