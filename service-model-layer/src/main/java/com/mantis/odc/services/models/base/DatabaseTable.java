/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models.base;

import com.google.common.base.Strings;
import com.mantis.odc.services.models.DatabaseField;
import com.mantis.odc.services.models.DatabaseRow;
import com.mantis.odc.services.models.DatabaseSubmitRow;
import com.mantis.odc.services.models.PrimaryKeyField;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.mantis.odc.utilities.MapUtil.createMapByType;

/**
 * Base table model for ODC data operations.
 * A table has fields and rows. More specialised types exist for containment and junction tables.
 */
@Data
@ToString(exclude = {"sourceByField", "submitRows"})
@EqualsAndHashCode(exclude = {"sourceByField", "submitRows"})
@AllArgsConstructor
public abstract class DatabaseTable {

    /**
     * Table name.
     */
    protected final String source;

    /**
     * OPA name.
     */
    protected final String opaName;

    /**
     * Table alias.
     */
    protected final String alias;

    /**
     * Primary key field.
     */
    protected final PrimaryKeyField primaryKey;

    /**
     * Table Fields.
     */
    protected final List<DatabaseField> fields;

    /**
     * Any returned rows from the table.
     */
    protected List<DatabaseRow> rows;

    /**
     * Map lookup of source names to database fields.
     */
    @Getter(lazy = true)
    private final Map<String, DatabaseField> sourceByField = createMapByType(fields, DatabaseField::getSource);

    /**
     * Database submit rows.
     */
    @Getter(lazy = true)
    private final List<DatabaseSubmitRow> submitRows = getRows().stream()
            .filter(r -> r instanceof DatabaseSubmitRow)
            .map(r -> (DatabaseSubmitRow) r)
            .collect(Collectors.toList());

    /**
     * @return table rows
     */
    public List<DatabaseRow> getRows() {
        if (!(rows instanceof ArrayList))
            this.rows = new ArrayList<>(rows);
        return rows;
    }

    /**
     * Returns the field with the given source.
     *
     * @param source field source
     * @return data field
     */
    public DatabaseField getField(String source) {
        return getSourceByField().get(source);
    }

    /**
     * @param actionModel current action model
     * @return list of the select fields
     */
    public List<DatabaseField> getSelectFields(ActionModel actionModel) {
        val fields = new ArrayList<DatabaseField>();
        fields.add(primaryKey);
        fields.addAll(getFields());
        return fields;
    }

    /**
     * @param row submit row
     * @return list of fields to be used in an insert statement
     */
    public List<DatabaseField> getInsertFields(DatabaseSubmitRow row) {
        val fields = new ArrayList<DatabaseField>();
        if (!primaryKey.isAutoIncrement()) fields.add(primaryKey);
        if (row != null) fields.addAll(this.fields.stream()
                .filter(f -> row.getValue(f.getSource()) != null)
                .collect(Collectors.toList()));
        else fields.addAll(this.fields);
        return fields;
    }

    /**
     * Adds a new data row.
     *
     * @param dataRow data row
     */
    public void addRow(DatabaseRow dataRow) {
        if (!(rows instanceof ArrayList)) rows = new ArrayList<>(rows);
        rows.add(dataRow);
    }

    /**
     * @return returns true if the primary key has a sequence.
     */
    public boolean hasSequence() {
        return !Strings.isNullOrEmpty(getPrimaryKey().getSequence());
    }
}
