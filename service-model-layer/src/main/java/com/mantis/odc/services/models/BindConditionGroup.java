package com.mantis.odc.services.models;


import com.mantis.odc.models.ConditionalJoin;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Singular;

import java.util.List;

/**
 * Groups bind conditions together.
 */
@Data
@Builder(toBuilder = true)
public class BindConditionGroup {

    /**
     * Type of condition.
     */
    private final ConditionalJoin join;

    /**
     * List of child groups.
     */
    @Singular
    private final List<BindConditionGroup> children;

    /**
     * List of conditions.
     */
    @Singular
    private final List<BindCondition> conditions;

    /**
     * Returns true if the group requires brackets. This is the case for conditions that have child groups or more than on condition.
     */
    @Getter(lazy = true)
    private final boolean requiresBrackets = children.isEmpty() && conditions.size() > 1;

    /**
     * Returns a bind condition if it is the only one in the group. Otherwise it errors
     *
     * @return bind condition
     */
    public BindCondition fromGroup() {
        if (isRequiresBrackets() || conditions.size() != 1) throw new UnsupportedOperationException();
        return conditions.get(0);
    }

    /**
     * Returns true if it requires an operator.
     *
     * @param condition bind condition
     * @return true if operator is required
     */
    public boolean requiresOperator(BindCondition condition) {
        if (conditions.isEmpty()) throw new UnsupportedOperationException();
        return !isRequiresBrackets() || (isRequiresBrackets() && conditions.get(0) != condition);
    }
}
