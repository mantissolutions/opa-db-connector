/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.models.DataType;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Primary key field. Has a sequence name
 * which can be used to populate a new row id from the database.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PrimaryKeyField extends DatabaseField {

    /**
     * Auto increment flag.
     */
    private final boolean autoIncrement;

    /**
     * Sequence name.
     */
    private final String sequence;

    @Builder(toBuilder = true)
    public PrimaryKeyField(String source, String opaName, DataType type, boolean autoIncrement, String sequence) {
        super(source, opaName, type);
        this.autoIncrement = autoIncrement;
        this.sequence = sequence;
    }

    public PrimaryKeyField(String source, DataType type, boolean autoIncrement, String sequence) {
        super(source, type);
        this.autoIncrement = autoIncrement;
        this.sequence = sequence;
    }
}
