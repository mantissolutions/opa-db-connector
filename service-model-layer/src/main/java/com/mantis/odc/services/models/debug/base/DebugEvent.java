/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models.debug.base;

import com.mantis.odc.models.OPAVersion;
import com.mantis.odc.models.ServiceAction;
import com.mantis.odc.services.models.debug.DebugEventType;
import lombok.Data;

import static java.time.Instant.ofEpochMilli;
import static java.time.ZoneId.systemDefault;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

/**
 * Basic event captured in debug mode.
 */
@Data
public abstract class DebugEvent {

    /**
     * Time the event was created.
     */
    private final long createdTime = System.currentTimeMillis();

    /**
     * OPA version.
     */
    private final OPAVersion opaVersion;

    /**
     * SOAP action.
     */
    private final ServiceAction serviceAction;

    /**
     * Debug event type.
     */
    private final DebugEventType type;

    /**
     * Event.
     */
    private final String message;

    /**
     * @return formatted creation date
     */
    public String getFormattedCreateTime() {
        return ISO_DATE_TIME.format(ofEpochMilli(createdTime)
                .atZone(systemDefault())
                .toLocalDateTime());
    }

    /**
     * @return string OPA version
     */
    public String getVersionString() {
        return opaVersion.getVersion();
    }

    /**
     * @return string service action
     */
    public String getServiceActionString() {
        return serviceAction.getActionName();
    }
}
