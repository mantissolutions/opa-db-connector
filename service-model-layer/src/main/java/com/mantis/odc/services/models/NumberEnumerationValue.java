/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.services.models.base.DatabaseEnumerationValue;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * String enumeration value.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class NumberEnumerationValue extends DatabaseEnumerationValue {

    /**
     * String value.
     */
    private final BigDecimal value;

    @Builder(toBuilder = true)
    public NumberEnumerationValue(BigDecimal value, String description, @Singular List<Object> childValues) {
        super(description, childValues);
        this.value = value;
    }
}
