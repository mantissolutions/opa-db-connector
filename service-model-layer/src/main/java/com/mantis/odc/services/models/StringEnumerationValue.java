/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.services.models.base.DatabaseEnumerationValue;
import lombok.*;

import java.util.List;

/**
 * String enumeration value.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class StringEnumerationValue extends DatabaseEnumerationValue {

    /**
     * String value.
     */
    private final String value;

    @Builder(toBuilder = true)
    public StringEnumerationValue(String value, String description, @Singular List<Object> childValues) {
        super(description, childValues);
        this.value = value;
    }
}
