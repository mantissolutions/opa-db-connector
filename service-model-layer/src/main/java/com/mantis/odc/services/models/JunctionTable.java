/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.services.models.base.ActionModel;
import com.mantis.odc.services.models.base.DatabaseTable;
import lombok.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A junction table has two foreign keys.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class JunctionTable extends DatabaseTable {

    /**
     * OPA link name.
     */
    private final String linkName;

    /**
     * From key size.
     */
    private final ForeignKeyField fromKey;

    /**
     * To key side.
     */
    private final ForeignKeyField toKey;

    /**
     * This list has at most two values and defines the supported targets for this junction table.
     * An OPA load request does not have to request the returning of keys for both directions in a junction table,
     * this enables the load request to configure which side of the relationship to return, or to request both be returned.
     */
    private final List<String> supportedSources;

    @Builder(toBuilder = true)
    public JunctionTable(String source,
                         String opaName,
                         String alias,
                         String linkName,
                         @Singular List<DatabaseRow> rows,
                         ForeignKeyField fromKey,
                         ForeignKeyField toKey,
                         @Singular List<String> supportedSources) {
        super(source, opaName, alias, null, null, rows);
        this.linkName = linkName;
        this.fromKey = fromKey;
        this.toKey = toKey;
        this.supportedSources = supportedSources;
    }

    /**
     * @param actionModel action model
     * @return list of the select fields.
     */
    public List<DatabaseField> getSelectFields(ActionModel actionModel) {
        val fields = new ArrayList<DatabaseField>();
        // return them as instances of data field. See SaveProcessor class for reason.
        fields.add(new DatabaseField(fromKey.getSource(), fromKey.getType()));
        fields.add(new DatabaseField(toKey.getSource(), toKey.getType()));
        return fields;
    }

    @Override
    public List<DatabaseField> getInsertFields(DatabaseSubmitRow row) {
        val fields = new ArrayList<DatabaseField>();
        fields.add(fromKey);
        fields.add(toKey);
        return fields;
    }

    /**
     * Returns the to table.
     *
     * @param actionModel action model
     * @return from table
     */
    public Optional<ContainmentTable> getFromTable(ActionModel actionModel) {
        return actionModel.getTable(fromKey.getTarget())
                .map(t -> (ContainmentTable) t);
    }

    /**
     * Returns the from table.
     *
     * @param actionModel action model
     * @return to table
     */
    public Optional<ContainmentTable> getToTable(ActionModel actionModel) {
        return actionModel.getTable(toKey.getTarget())
                .map(t -> (ContainmentTable) t);
    }

    /**
     * Returns the target tables available in the current action model.
     *
     * @param actionModel action model
     * @return list of available tables
     */
    public List<ContainmentTable> getTargetTables(ActionModel actionModel) {
        return Stream.of(getFromTable(actionModel), getToTable(actionModel))
                .flatMap(containmentTable -> containmentTable.map(Stream::of).orElse(Stream.empty()))
                .collect(Collectors.toList());
    }

    /**
     * Returns the present targets in a map from the current junction table.
     *
     * @param actionModel action model
     * @param fromGlobal  flag to indicate a load request from global, otherwise it is an adhoc request for data.
     *                    These only require one side of the non containment link.
     * @return table foreign key map
     */
    public Map<ForeignKeyField, ContainmentTable> getTargetTableMap(ActionModel actionModel, boolean fromGlobal) {
        val map = new LinkedHashMap<ForeignKeyField, ContainmentTable>();
        getFromTable(actionModel).ifPresent(fromTable -> map.put(getFromKey(), fromTable));
        if (fromGlobal) getToTable(actionModel).ifPresent(toTable -> map.put(getToKey(), toTable));
        return map;
    }

    /**
     * Returns the sourced table from the junction table.
     *
     * @param actionModel action model
     * @param source      table source
     * @return containment table
     */
    public Optional<ContainmentTable> getTable(ActionModel actionModel, String source) {
        return getTargetTables(actionModel).stream()
                .filter(table -> table.getSource().equals(source))
                .findFirst();
    }

    /**
     * Returns the target key from the source name.
     *
     * @param source source
     * @return target key
     */
    public ForeignKeyField getTargetKey(String source) {
        return (fromKey.getTarget().equals(source)) ? fromKey : toKey;
    }

    /**
     * Returns the target table for a given source, side of a junction table.
     *
     * @param actionModel action model
     * @param source      table source.
     * @return target table
     */
    public Optional<DatabaseTable> getTargetTable(ActionModel actionModel, String source) {
        val targetKey = getDestinationKey(source);
        return actionModel.getTable(targetKey.getTarget());
    }

    /**
     * Returns the destination key from the opposite source name. So if the source is for the from table,
     * the to key is returned and vis versa.
     *
     * @param source source
     * @return destination key
     */
    public ForeignKeyField getDestinationKey(String source) {
        return (fromKey.getTarget().equals(source)) ? toKey : fromKey;
    }

    /**
     * Returns the key field for given target source.
     *
     * @param source table source
     * @return join key
     */
    private Stream<DatabaseField> getKeyFields(String source) {
        return Stream.of(fromKey, toKey)
                .filter(keyField -> keyField.getTarget().equals(source))
                .map(keyField -> (DatabaseField) keyField);
    }

    /**
     * Returns true if the table has the given link.
     *
     * @param source source
     * @return true if the table has a given table link
     */
    public boolean hasLink(String source) {
        return supportedSources.stream().anyMatch(s -> s.equals(source));
    }

    /**
     * Updates the related keys from the OPA key to the database key in the given row.
     * It finds the correct joining key, then updates all the rows which have a matching opa key, then
     * sets them to the generated database key.
     *
     * @param row submit row
     */
    public void updateKeys(DatabaseSubmitRow row) {
        getKeyFields(row.getSource())
                .forEach(dataField -> getRows().stream()
                        .filter(dataRow -> dataRow.getValue(dataField.getSource()).equals(row.getId().getOpaKey()))
                        .forEach(dataRow -> dataRow.setValue(dataField.getSource(), row.getId().getDatabaseKey())));
    }

    /**
     * Returns all the target ids for a given source id.
     *
     * @param source supported source table name
     * @param id     source row id
     * @return list of target id values link to the given source id
     */
    public List<RowId> getRefs(String source, RowId id) {
        /*
         * the terminology is flipped here, because in a load request,
         * it is requesting a link in the opposite direction to a saveButton request.
         */
        val targetKey = getDestinationKey(source);
        val sourceKey = getTargetKey(source);
        return getRows().stream()
                .filter(databaseRow -> databaseRow.getValue(sourceKey.getSource()).equals(id.getDatabaseKey()))
                .map(databaseRow -> new RowId(databaseRow.getValue(targetKey.getSource())))
                .collect(Collectors.toList());
    }
}
