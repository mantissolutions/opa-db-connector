/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.models.metrics;

import lombok.Builder;
import lombok.Data;

/**
 * Timer bean based on a Timer
 *
 * @see com.codahale.metrics.Timer
 */
@Data
@Builder
public class TimerBean {
    private final long count;
    private final double fifteenMinuteRate;
    private final double fiveMinuteRate;
    private final double meanRate;
    private final double oneMinuteRate;
}
