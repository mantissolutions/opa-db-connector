/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;

/**
 * Models a link between two tables.
 * This class is generated, links are actually transiently determined by table keys.
 */
@Data
public class DatabaseLink {

    /**
     * OPA link name.
     */
    private final String linkName;

    /**
     * OPA table name.
     */
    private final String target;

    /**
     * Links to the target table.
     */
    private final List<RowId> refs;

    @Builder
    public DatabaseLink(String linkName, String target, @Singular List<RowId> refs) {
        this.linkName = linkName;
        this.target = target;
        this.refs = refs;
    }
}
