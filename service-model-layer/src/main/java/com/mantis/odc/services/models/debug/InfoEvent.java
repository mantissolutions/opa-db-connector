/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models.debug;

import com.mantis.odc.models.OPAVersion;
import com.mantis.odc.models.ServiceAction;
import com.mantis.odc.services.models.debug.base.DebugEvent;

import static com.mantis.odc.services.models.debug.DebugEventType.Info;

/**
 * Info event.
 */
public class InfoEvent extends DebugEvent {
    public InfoEvent(OPAVersion opaVersion, ServiceAction serviceAction, String message) {
        super(opaVersion, serviceAction, Info, message);
    }
}
