/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models.base;

import com.mantis.odc.services.models.*;
import lombok.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mantis.odc.utilities.MapUtil.createMapByType;
import static com.mantis.odc.utilities.MapUtil.createMapOfListByType;
import static java.util.Collections.EMPTY_LIST;

/**
 * Base action model used to operate on the database.
 */
@Data
@ToString(
        exclude = {"containmentTables", "junctionTables", "tableBySource", "tableByName", "junctionTableByLinkName", "sourceToDependentTablesMap"})
@EqualsAndHashCode(
        exclude = {"containmentTables", "junctionTables", "tableBySource", "tableByName", "junctionTableByLinkName", "sourceToDependentTablesMap"})
@AllArgsConstructor
public abstract class ActionModel {

    /**
     * Operation timezone.
     */
    protected final TimeZone timeZone;

    /**
     * Operation locale.
     */
    protected final Locale locale;

    /**
     * Global row id. Used to load data for all child containment tables.
     */
    protected RowId globalId;

    /**
     * Name of the root containment table.
     */
    protected final String rootTable;

    /**
     * Script to run before the operations.
     */
    protected final Optional<String> preScript;

    /**
     * Script to run after the operation.
     */
    protected final Optional<String> postScript;

    /**
     * Tables used on the action.
     */
    protected final List<DatabaseTable> tables;

    /**
     * Containment tables.
     */
    @Getter(lazy = true)
    private final List<ContainmentTable> containmentTables = getTables().stream()
            .filter(t -> t instanceof ContainmentTable)
            .map(t -> (ContainmentTable) t)
            .collect(Collectors.toList());

    /**
     * Junction Tables.
     */
    @Getter(lazy = true)
    private final List<JunctionTable> junctionTables = getTables().stream()
            .filter(t -> t instanceof JunctionTable)
            .map(t -> (JunctionTable) t)
            .collect(Collectors.toList());

    /**
     * Table lookup by source.
     */
    @Getter(lazy = true)
    private final Map<String, DatabaseTable> tableBySource = createMapByType(tables, DatabaseTable::getSource);

    /**
     * Table lookup by name.
     */
    @Getter(lazy = true)
    private final Map<String, DatabaseTable> tableByName = createMapByType(tables, DatabaseTable::getOpaName);

    /**
     * Junction Table lookup by link name.
     */
    @Getter(lazy = true)
    private final Map<String, JunctionTable> junctionTableByLinkName = createMapByType(getJunctionTables(), JunctionTable::getLinkName);

    /**
     * Map of source names to there dependent tables.
     */
    @Getter(lazy = true)
    private final Map<String, List<ContainmentTable>> sourceToDependentTablesMap =
            createMapOfListByType(getContainmentTables(), (ct1, ct2) -> ct2.getForeignKey()
                            .filter(fk -> fk.getTarget().equals(ct1.getSource())).isPresent(),
                    ContainmentTable::getSource);

    /**
     * Returns the named table.
     *
     * @param source table name
     * @return table
     */
    public Optional<DatabaseTable> getTable(@NonNull String source) {
        return Optional.ofNullable(getTableBySource().get(source));
    }

    /**
     * Returns the named table.
     *
     * @param opaName opa name
     * @return table
     */
    public Optional<DatabaseTable> getTableByOpaName(@NonNull String opaName) {
        return Optional.ofNullable(getTableByName().get(opaName));
    }

    /**
     * @return returns the root containment table
     */
    public ContainmentTable getGlobalContainmentTable() {
        return (ContainmentTable) getTable(rootTable).get();
    }

    /**
     * Returns the junction table with the given link name.
     *
     * @param linkName link name
     * @return junction table
     */
    public Optional<JunctionTable> getJunctionTable(String linkName) {
        return Optional.ofNullable(getJunctionTableByLinkName().get(linkName));
    }

    /**
     * Returns all dependent tables.
     *
     * @param source table source.
     * @return list of all dependent tables.
     */
    public List<ContainmentTable> getDependentTables(String source) {
        return getSourceToDependentTablesMap().getOrDefault(source, EMPTY_LIST);
    }

    /**
     * Returns all the links to the row.
     *
     * @param row database row
     * @return database links
     */
    public List<DatabaseLink> getLinks(DatabaseRow row) {
        val childTables = getDependentTables(row.getSource()).stream();
        val childJunctionTables = getJunctionTables().stream().filter(t -> t.hasLink(row.getSource()));
        return Stream.concat(childTables, childJunctionTables)
                .map(table -> {
                            if (table instanceof ContainmentTable) { // containment table
                                ContainmentTable containmentTable = (ContainmentTable) table;
                                return DatabaseLink.builder()
                                        .linkName(containmentTable.getLinkName())
                                        .target(containmentTable.getOpaName())
                                        .refs(containmentTable.getRefs(row.getId()))
                                        .build();
                            } else { // junction table
                                JunctionTable junctionTable = (JunctionTable) table;
                                ForeignKeyField key = junctionTable.getDestinationKey(row.getSource());
                                return DatabaseLink.builder()
                                        .linkName(junctionTable.getLinkName())
                                        .target(key.getTargetOpaName())
                                        .refs(junctionTable.getRefs(row.getSource(), row.getId()))
                                        .build();
                            }
                        }
                )
                .collect(Collectors.toList());
    }

    /**
     * Returns all the non-containment links to the row.
     *
     * @param row database row
     * @return database links
     */
    public List<DatabaseLink> getNonContainmentLinks(DatabaseRow row) {
        return getJunctionTables().stream()
                .filter(t -> t.hasLink(row.getSource()))
                .map(junctionTable -> {
                    ForeignKeyField key = junctionTable.getDestinationKey(row.getSource());
                    return DatabaseLink.builder()
                            .linkName(junctionTable.getLinkName())
                            .target(key.getTargetOpaName())
                            .refs(junctionTable.getRefs(row.getSource(), row.getId()))
                            .build();
                })
                .collect(Collectors.toList());
    }

    /**
     * Returns true if the containment table is disconnected from the graph.
     *
     * @param containmentTable containment table
     * @return true if disconnected from the graph
     */
    public boolean isDisconnected(ContainmentTable containmentTable) {
        return  // the table has no parent
                !containmentTable.getParent(this).isPresent()
                        // but is not the root table
                        && !containmentTable.getSource().equals(getRootTable());
    }

    /**
     * Returns all the junction tables where the current table is the target.
     *
     * @param table containment table
     * @return junction tables that target this table
     */
    public List<JunctionTable> getRelatedJunctionTables(ContainmentTable table) {
        return getJunctionTables().stream()
                .filter(jt -> jt.getTargetTable(this, table.getSource()).isPresent())
                .collect(Collectors.toList());
    }
}
