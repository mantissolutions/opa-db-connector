/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models.debug;

import com.mantis.odc.models.OPAVersion;
import com.mantis.odc.models.ServiceAction;
import com.mantis.odc.services.models.debug.base.DebugEvent;
import com.mantis.odc.sql.SQLFormatter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import static com.mantis.odc.services.models.debug.DebugEventType.SQL;

/**
 * SQL event.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SQLEvent extends DebugEvent {

    /**
     * SQL statement.
     */
    private final String statement;

    public SQLEvent(OPAVersion opaVersion, ServiceAction serviceAction, String statement) {
        super(opaVersion, serviceAction, SQL, SQLFormatter.format(statement));
        this.statement = getMessage();
    }
}
