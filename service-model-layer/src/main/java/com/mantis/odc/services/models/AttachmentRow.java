/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.services.models.base.ActionModel;
import com.mantis.odc.services.models.base.DatabaseTable;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Arrays;

import static com.mantis.odc.services.models.DBActionType.insert;
import static java.util.Collections.EMPTY_LIST;

/**
 * Models an attachment row. These are always insert rows of a fixed size.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class AttachmentRow extends DatabaseSubmitRow {

    /**
     * Table name.
     */
    private final String table;

    /**
     * Attachment name value.
     */
    private final String name;

    /**
     * File name.
     */
    private final String fileName;

    /**
     * Attachment description
     */
    private final String description;

    /**
     * Attachment attachmentData value.
     */
    private final String attachmentData;

    @Builder(builderMethodName = "attachmentRowBuilder")
    public AttachmentRow(RowId id,
                         RowValue table,
                         RowValue name,
                         RowValue fileName,
                         RowValue description,
                         RowValue attachmentData) {
        super("", id, null, insert, Arrays.asList(table, name, fileName, description, attachmentData), EMPTY_LIST);
        this.table = (String) table.getValue();
        this.name = (String) name.getValue();
        this.fileName = (String) fileName.getValue();
        this.description = (String) description.getValue();
        this.attachmentData = (String) attachmentData.getValue();
    }

    @Override
    public DatabaseTable getTable(ActionModel actionModel) {
        if (actionModel instanceof SaveModel)
            return ((SaveModel) actionModel).getAttachmentTable().get();
        throw new UnsupportedOperationException();
    }
}
