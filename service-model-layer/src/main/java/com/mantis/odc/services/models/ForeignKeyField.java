/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.models.DataType;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Foreign key field contains a target source.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ForeignKeyField extends DatabaseField {

    /**
     * Target table.
     */
    private final String target;

    /**
     * Target OPA name.
     */
    private final String targetOpaName;

    @Builder(toBuilder = true)
    public ForeignKeyField(String source, String opaName, DataType type, String target, String targetOpaName) {
        super(source, opaName, type);
        this.target = target;
        this.targetOpaName = targetOpaName;
    }

    public ForeignKeyField(String source, DataType type, String target, String targetOpaName) {
        super(source, type);
        this.target = target;
        this.targetOpaName = targetOpaName;
    }
}
