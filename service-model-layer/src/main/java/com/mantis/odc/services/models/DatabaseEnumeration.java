/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.models.EnumerationType;
import com.mantis.odc.services.models.base.DatabaseEnumerationValue;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;
import java.util.Optional;

/**
 * Models enumeration data.
 */
@Data
@Builder(toBuilder = true)
public class DatabaseEnumeration {

    /**
     * Enumeration name.
     */
    private final String name;

    /**
     * Enumeration type.
     */
    private final EnumerationType type;

    /**
     * Uncertain enumeration value.
     */
    private final DatabaseEnumerationValue uncertainValue;

    /**
     * Enumeration values.
     */
    @Singular
    private final List<DatabaseEnumerationValue> values;

    /**
     * @return uncertain value.
     */
    public Optional<DatabaseEnumerationValue> getUncertainValueOpt() {
        return Optional.ofNullable(uncertainValue);
    }
}
