/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models.debug;

import com.mantis.odc.models.OPAVersion;
import com.mantis.odc.models.ServiceAction;
import com.mantis.odc.services.models.debug.base.DebugEvent;
import com.mantis.odc.utilities.ErrorUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import static com.mantis.odc.services.models.debug.DebugEventType.Error;
import static java.text.MessageFormat.format;

/**
 * Error event.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ErrorEvent extends DebugEvent {

    /**
     * Error that occurred.
     */
    private final Throwable error;

    public ErrorEvent(OPAVersion opaVersion, ServiceAction serviceAction, String message, Throwable error) {
        super(opaVersion, serviceAction, Error, createErrorMessage(message, error));
        this.error = error;
    }

    /**
     * Returns the error message.
     *
     * @param message message
     * @param error   error
     * @return message
     */
    private static String createErrorMessage(String message, Throwable error) {
        return format("Caused by: {0}\n\n{1}", message, ErrorUtil.errorToString(error));
    }
}
