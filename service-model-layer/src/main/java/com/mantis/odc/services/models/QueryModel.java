package com.mantis.odc.services.models;

import com.mantis.odc.services.models.base.DatabaseTable;
import lombok.*;

import java.util.*;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class QueryModel extends LoadModel {
    /**
     * Limit on the number of records to return.
     */
    @Getter
    private OptionalInt limit;

    @Builder(builderMethodName = "queryModelBuilder")
    public QueryModel(TimeZone timeZone,
                      Locale locale,
                      RowId globalId,
                      String rootTable,
                      Optional<String> preScript,
                      Optional<String> postScript,
                      @Singular List<DatabaseTable> tables,
                      @Singular List<BindConditionGroup> conditions,
                      OptionalInt limit) {
        super(timeZone, locale, globalId, rootTable, preScript, postScript, tables, conditions);
        this.limit = limit;
    }

    @Override
    public List<BindConditionGroup> getConditionsForTable(DatabaseTable table) {
        return getConditions();
    }
}