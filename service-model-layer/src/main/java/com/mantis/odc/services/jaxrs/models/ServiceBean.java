/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.models;

import com.mantis.odc.models.OPAVersion;
import io.gsonfire.annotations.ExposeMethodResult;
import lombok.Builder;
import lombok.Value;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * JSON transport bean for an ODC Service.
 */
@Value
@Builder
public class ServiceBean {

    /**
     * Service name.
     */
    private final String name;

    /**
     * Mapping name.
     */
    private final String mappingName;

    /**
     * Service datasource information.
     */
    private final ServiceDatasourceBean datasource;

    /**
     * Locations for the all the service versions.
     */
    @ExposeMethodResult("serviceLocations")
    public List<String> getServiceLocations() {
        return Arrays.stream(OPAVersion.values())
                .map(version -> "/service/" + name + "/" + version.name())
                .sorted()
                .collect(Collectors.toList());
    }
}
