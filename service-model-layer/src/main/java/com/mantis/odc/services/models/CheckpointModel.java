/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

/**
 * Models the interaction with checkpoints in OCD.
 */
@Data
public class CheckpointModel {

    /**
     * Checkpoint table information.
     */
    private final CheckpointTable checkpointTable;

    /**
     * Id value.
     */
    private final String id;

    /**
     * True if the value is new checkpoint.
     */
    private final boolean newCheckpoint;

    /**
     * Checkpoint data.
     */
    private byte[] data;

    @Builder(toBuilder = true)
    public CheckpointModel(CheckpointTable checkpointTable, String id, byte[] data) {
        this.checkpointTable = checkpointTable;
        this.id = (id != null) ? id : UUID.randomUUID().toString();
        newCheckpoint = id == null;
        this.data = data;
    }
}
