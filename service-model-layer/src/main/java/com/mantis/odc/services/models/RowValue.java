/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.models.DataType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Models a single column value.
 */
@Data
@Builder
@AllArgsConstructor
public class RowValue {

    /**
     * Field source name.
     */
    private final String source;

    /**
     * OPA name.
     */
    private final String opaName;

    /**
     * Row type.
     */
    private final DataType type;

    /**
     * Row value.
     */
    private Object value;

    public RowValue(String source, DataType type, Object value) {
        this(source, source, type, value);
    }

    /**
     * @return true if the row value is not null
     */
    public boolean hasData() {
        return value != null;
    }
}
