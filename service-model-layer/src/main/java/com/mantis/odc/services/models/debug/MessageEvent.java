/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models.debug;

import com.mantis.odc.models.OPAVersion;
import com.mantis.odc.models.ServiceAction;
import com.mantis.odc.services.models.debug.base.DebugEvent;
import lombok.*;

import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;

import static com.mantis.odc.services.models.debug.DebugEventType.Message;
import static javax.xml.transform.OutputKeys.INDENT;

/**
 * Message event.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class MessageEvent extends DebugEvent {

    /**
     * Is an inbound message.
     */
    private final boolean inboundMessage;

    /**
     * SOAP message.
     */
    private final SOAPMessage soapMessage;

    public MessageEvent(OPAVersion opaVersion, ServiceAction serviceAction, boolean inboundMessage, SOAPMessage soapMessage) {
        super(opaVersion, serviceAction, Message, soapToString(soapMessage));
        this.inboundMessage = inboundMessage;
        this.soapMessage = soapMessage;
    }

    /**
     * Pretty prints a soap message.
     *
     * @param message message
     * @return SOAP xml
     */
    @SneakyThrows
    private static String soapToString(SOAPMessage message) {
        val tff = TransformerFactory.newInstance();
        val tf = tff.newTransformer();
        tf.setOutputProperty(INDENT, "yes");
        tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        val sc = message.getSOAPPart().getContent();
        val streamOut = new ByteArrayOutputStream();
        val result = new StreamResult(streamOut);
        tf.transform(sc, result);
        return streamOut.toString();
    }
}
