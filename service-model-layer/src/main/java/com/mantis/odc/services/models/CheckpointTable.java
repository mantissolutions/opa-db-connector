/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.services.models.base.ActionModel;
import com.mantis.odc.services.models.base.DatabaseTable;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

import static com.mantis.odc.models.DataType.ATTACHMENT_TYPE;
import static com.mantis.odc.models.DataType.STRING_TYPE;
import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.singletonList;

/**
 * Models a checkpoint table.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class CheckpointTable extends DatabaseTable {

    /**
     * Id field name.
     */
    private final String idField;

    /**
     * Data field name.
     */
    private final String dataField;

    @Builder(toBuilder = true)
    public CheckpointTable(String source, String opaName, String idField, String dataField) {
        super(source, opaName, "cpt1", PrimaryKeyField.builder().source(idField).type(STRING_TYPE).build(), singletonList(new DatabaseField(dataField, ATTACHMENT_TYPE)), EMPTY_LIST);
        this.idField = idField;
        this.dataField = dataField;
    }

    @Override
    public List<DatabaseField> getSelectFields(ActionModel actionModel) {
        return singletonList(new DatabaseField(dataField, ATTACHMENT_TYPE));
    }

    public List<DatabaseField> getInsertFields() {
        return getInsertFields(null);
    }
}
