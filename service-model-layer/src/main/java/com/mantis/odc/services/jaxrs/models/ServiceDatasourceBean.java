/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.models;

import com.mantis.odc.models.Driver;
import lombok.Builder;
import lombok.Value;

import java.util.List;

/**
 * Service data source bean.
 */
@Value
@Builder
public class ServiceDatasourceBean {

    /**
     * Data source name.
     */
    private final String name;

    /**
     * Current driver type.
     */
    private final Driver driverType;

    /**
     * Database url.
     */
    private final String url;

    /**
     * Active pools in the data source.
     */
    private final List<ServiceDatasourcePoolBean> pools;
}
