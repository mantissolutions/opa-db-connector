/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models.debug;

/**
 * Supported debug event types.
 */
public enum DebugEventType {
    Error, Info, SQL, Message
}
