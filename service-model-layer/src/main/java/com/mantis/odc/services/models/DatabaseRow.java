/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.services.models.base.ActionModel;
import com.mantis.odc.services.models.base.DatabaseTable;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.mantis.odc.utilities.MapUtil.createMapByType;

/**
 * Base data row, it has a key which identifies it and indexed data items by field source.
 */
@Data
@AllArgsConstructor
@ToString(exclude = "sourceValueMap")
@EqualsAndHashCode(exclude = "sourceValueMap")
@Builder(toBuilder = true, builderMethodName = "dataRowBuilder")
public class DatabaseRow {

    /**
     * Table source name the row belongs.
     */
    protected final String source;

    /**
     * Row id. Is always unique within a given table.
     */
    protected final RowId id;

    /**
     * DatabaseLink to an existing row in another table.
     */
    protected RowId fk;

    /**
     * Data items indexed by field name.
     */
    @Singular(value = "item")
    protected List<RowValue> data;

    /**
     * Lookup source value map.
     */
    @Getter(lazy = true)
    private final Map<String, RowValue> sourceValueMap = createMapByType(data, RowValue::getSource);

    /**
     * @param actionModel current action model
     * @return data table
     */
    public DatabaseTable getTable(ActionModel actionModel) {
        return actionModel.getTable(source).get();
    }


    /**
     * @param source field source
     * @return data for the given field
     */
    public Object getValue(String source) {
        val row = getSourceValueMap().get(source);
        return row != null ? row.getValue() : null;
    }

    /**
     * Sets a given value by source if it exists in the row.
     *
     * @param source field source
     * @param value  value to set
     */
    public void setValue(String source, Object value) {
        val row = getSourceValueMap().computeIfAbsent(source, s -> {
            RowValue r = RowValue.builder().source(source).build();
            if (!(data instanceof ArrayList)) data = new ArrayList<>(data);
            data.add(r);
            return r;
        });
        if (row != null) row.setValue(value);
    }

    /**
     * Comparator which returns -1 if row 1 comes after row 2, 0 if they stay the same
     * and 1 if row 1 comes before row 2.
     *
     * @param o2 comparison row
     * @return -1, 0 or 1 if the comes after, equals or proceeds the comparison row
     */
    int dependsOn(ActionModel actionModel, DatabaseSubmitRow o2) {
        // they belong to the same table, no ordering occurs
        if (source.equals(o2.source)) return 0;
        val table1 = getTable(actionModel);
        val table2 = o2.getTable(actionModel);
        // cannot compare tables out of a containment relationship
        if (!(table1 instanceof ContainmentTable && table2 instanceof ContainmentTable)) return 0;
        else {
            val ct1 = (ContainmentTable) table1;
            return ct1.getParents(actionModel)
                    .map(DatabaseTable::getSource)
                    .anyMatch(childSource -> childSource.equals(o2.source)) ? 1 : -1;
        }
    }
}
