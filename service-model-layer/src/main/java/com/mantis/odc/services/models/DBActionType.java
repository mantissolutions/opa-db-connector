/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

/**
 * Models all valid actions.
 */
public enum DBActionType {
    insert,
    update,
    delete
}
