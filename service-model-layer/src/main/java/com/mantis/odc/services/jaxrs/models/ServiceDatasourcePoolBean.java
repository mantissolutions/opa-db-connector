/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.jaxrs.models;

import com.mantis.odc.services.jaxrs.models.metrics.TimerBean;
import lombok.Builder;
import lombok.Data;

/**
 * Represents an active connection pool in the
 * service data source.
 */
@Data
@Builder
public class ServiceDatasourcePoolBean {

    /**
     * Database username.
     */
    private final String username;

    /**
     * Name of the pool in ODC.
     */
    private final String poolName;

    /**
     * Up time.
     */
    private final String upTime;

    /**
     * How long requesting threads to getConnection() are waiting for a connection (or timeout exception) from the pool.
     */
    private final TimerBean poolWaitTime;

    /**
     * How long each connection is used before being returned to the pool. This is the "out of pool" or "in-use" time.
     */
    private final long poolUsage;

    /**
     * Total number of connections in the pool.
     */
    private final int totalConnections;

    /**
     * Number of idle connections in the pool.
     */
    private final int IdleConnections;

    /**
     * Number of active (in-use) connections in the pool.
     */
    private final int activeConnections;

    /**
     * Number of threads awaiting connections from the pool.
     */
    private final int pendingConnections;
}
