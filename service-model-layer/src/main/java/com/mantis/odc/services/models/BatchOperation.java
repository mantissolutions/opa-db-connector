/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.services.models.base.DatabaseTable;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

import static com.mantis.odc.services.models.DBActionType.insert;

/**
 * SQL batch operation. This is done for Save requests with pre-fetched keys.
 * Otherwise it is done ins serial manner.
 */
@Data
@Builder
@EqualsAndHashCode(of = {"sql"})
public class BatchOperation {

    /**
     * SQL statement to execute.
     */
    private final String sql;

    /**
     * Action type.
     */
    private final DBActionType type;

    /**
     * Table the batch is for.
     */
    private final DatabaseTable table;

    /**
     * Fields in the SQL.
     */
    private final List<DatabaseField> fields;

    /**
     * Rows to batch operate.
     */
    private final List<DatabaseSubmitRow> rows = new ArrayList<>();

    /**
     * @return returns true if the operation is an insert with and auto generated key
     */
    public boolean isAutoKeyInsert() {
        return table.getPrimaryKey() != null && table.getPrimaryKey().isAutoIncrement() && type == insert;
    }

    /**
     * @param row row to add
     */
    public void addRow(@NonNull DatabaseSubmitRow row) {
        rows.add(row);
    }
}
