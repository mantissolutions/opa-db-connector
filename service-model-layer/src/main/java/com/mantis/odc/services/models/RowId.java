/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Basic row type. This has an ID, which might or might not have a value.
 */
@Data
@AllArgsConstructor
public class RowId {

    /**
     * Value passed by OPA.
     */
    protected Object opaKey;

    /**
     * Value generated from the database, and passed back to OPA.
     * This can be the same as the OPA key.
     */
    protected Object databaseKey;

    /**
     * @param id database id
     */
    public RowId(Object id) {
        opaKey = id;
        databaseKey = id;
    }

    /**
     * Returns the database key as a string. This may be mode complicated in the future if the
     * default string formats are not correct for a given type.
     *
     * @return returns the database key as a string
     */
    public String getDatabaseKeyAsString() {
        return databaseKey != null ? databaseKey.toString() : null;
    }

    /**
     * Returns the opa key as a string. This may be mode complicated in the future if the
     * default string formats are not correct for a given type.
     *
     * @return returns the opa key as a string
     */
    public String getOpaKeyAsString() {
        return opaKey != null ? opaKey.toString() : null;
    }

    /**
     * Returns true if the database key differs from the opa key.
     *
     * @return true if database key is different from the opa key
     */
    public boolean hasNewKey() {
        return databaseKey != null && !databaseKey.equals(opaKey);
    }

    /**
     * Returns the database key if it exists, otherwise it returns the OPA key.
     *
     * @return key as a string.
     */
    public Object getKey() {
        return (databaseKey != null) ? getDatabaseKey() : getOpaKey();
    }

    /**
     * Returns the database key as a string if it exists, otherwise it returns the OPA key as a string.
     *
     * @return key as a string.
     */
    public String getKeyAsString() {
        return (databaseKey != null) ? getDatabaseKeyAsString() : getOpaKeyAsString();
    }
}
