/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.models.ConditionalJoin;
import com.mantis.odc.models.DataType;
import com.mantis.odc.models.Operator;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;

/**
 * Simple bind conditions.
 */
@Data
@Builder(toBuilder = true)
public class BindCondition {

    /**
     * Source of the condition.
     */
    private final String source;

    /**
     * Field type.
     */
    private final DataType type;

    /**
     * Table alias.
     */
    private final String alias;

    /**
     * Type of condition.
     */
    private final ConditionalJoin join;

    /**
     * Operator type.
     */
    private final Operator operator;

    /**
     * Values to compare.
     */
    @Singular
    private final List<Object> values;

    /**
     * Returns true if the condition requires a bind value.
     *
     * @return true if requires a bind variable
     */
    public boolean hasValue() {
        return operator.isHasValue();
    }

    /**
     * @return true if a scalar value.
     */
    public boolean isSingleValue() {
        return values.size() <= 1;
    }

    /**
     * Wraps the current bind condition in a group.
     *
     * @return bind condition in a group
     */
    public BindConditionGroup toGroup() {
        return BindConditionGroup.builder()
                .join(join)
                .condition(this)
                .build();
    }
}
