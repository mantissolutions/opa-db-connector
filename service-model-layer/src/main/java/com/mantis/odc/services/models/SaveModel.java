/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.services.converters.exceptions.MissingMappingException;
import com.mantis.odc.services.models.base.ActionModel;
import com.mantis.odc.services.models.base.DatabaseTable;
import lombok.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.mantis.odc.services.converters.exceptions.MissingMappingException.MappingType.Table;
import static com.mantis.odc.services.models.DBActionType.delete;
import static com.mantis.odc.services.models.DBActionType.update;
import static java.util.Collections.EMPTY_LIST;
import static java.util.stream.Stream.concat;

/**
 * Model used to saveButton data to the database.
 */
@ToString(callSuper = true,
        exclude = {"attachmentTableRef", "transientTables", "sourceToDependentJunctionTablesMap", "executedBatches"})
@EqualsAndHashCode(callSuper = true,
        exclude = {"attachmentTableRef", "transientTables", "sourceToDependentJunctionTablesMap", "executedBatches"})
public class SaveModel extends ActionModel {

    /**
     * Number of batches executed.
     */
    @Getter
    private long executedBatches;

    /**
     * Potential ref to attachment table mapping.
     */
    @Getter(lazy = true)
    private final AttachmentTable attachmentTableRef = getTables().stream()
            .filter(table -> table instanceof AttachmentTable)
            .map(table -> (AttachmentTable) table).findFirst().orElse(null);

    /**
     * Returns the tables which are cleared on every saveButton operation.
     */
    @Getter(lazy = true)
    private final List<DatabaseTable> transientTables = getTables().stream()
            .filter(t -> t instanceof JunctionTable || t instanceof ContainmentTable && ((ContainmentTable) t).isInferred())
            .collect(Collectors.toList());

    /**
     * Map of sources to dependent junction tables.
     */
    @Getter(lazy = true)
    private final Map<String, List<JunctionTable>> sourceToDependentJunctionTablesMap = createSourceToDependentJunctionTablesMap();

    @Builder(toBuilder = true)
    public SaveModel(TimeZone timeZone,
                     Locale locale,
                     RowId globalId,
                     String rootTable,
                     Optional<String> preScript,
                     Optional<String> postScript,
                     @Singular List<DatabaseTable> tables) {
        super(timeZone, locale, globalId, rootTable, preScript, postScript, tables);
    }

    /**
     * @return attachment table
     */
    public Optional<AttachmentTable> getAttachmentTable() {
        return Optional.ofNullable(getAttachmentTableRef());
    }

    /**
     * Returns all the submit rows ordered by deletes, inserts/updates, junction inserts, attachments.
     *
     * @return list or submit rows
     */
    public List<DatabaseSubmitRow> getSubmitRows() {
        // get deletes
        val deletes = getContainmentTables().stream()
                .flatMap(t -> t.getSubmitRows().stream())
                .filter(r -> r.getAction() == delete);

        // get insert and updates ordered by dependency
        val insertUpdates = getContainmentTables().stream()
                .flatMap(t -> t.getSubmitRows().stream())
                .filter(r -> r.getAction() != delete)
                .sorted((o1, o2) -> o1.dependsOn(this, o2));

        // get junction inserts
        Stream<DatabaseSubmitRow> junctionInserts = getJunctionTables().stream()
                .flatMap(t -> t.getSubmitRows().stream());

        // get attachments
        val attachments = getAttachmentTable()
                .map(table -> table.getSubmitRows().stream())
                .orElse(Stream.empty());

        return concat(deletes, concat(concat(insertUpdates, junctionInserts), attachments))
                .collect(Collectors.toList());
    }

    /**
     * Returns all junction tables with a key link to the given source table.
     *
     * @param source source to search for
     * @return junction tables
     */
    public List<JunctionTable> getAllDependentJunctionTables(String source) {
        return getSourceToDependentJunctionTablesMap().getOrDefault(source, EMPTY_LIST);
    }

    /**
     * @return map of source to dependent junction tables
     */
    private Map<String, List<JunctionTable>> createSourceToDependentJunctionTablesMap() {
        val map = new HashMap<String, List<JunctionTable>>();
        for (val t : tables) {
            val itemList = new ArrayList<JunctionTable>();
            for (val jt : getJunctionTables())
                if (jt.getFromKey().getTarget().equals(t.getSource()) || jt.getToKey().getTarget().equals(t.getSource()))
                    itemList.add(jt);
            map.put(t.getSource(), itemList);
        }
        return map;
    }

    /**
     * Returns a submit row for the given source and key. It is created if the key does not exist.
     *
     * @param source    table source
     * @param key       row key
     * @param parentKey parent row key
     * @return submit row
     */
    @SneakyThrows
    public DatabaseSubmitRow getSubmitRow(String source, String key, Optional<String> parentKey) {
        val table = (ContainmentTable) getTable(source).orElseThrow(() -> new MissingMappingException(Table, source));
        Optional<DatabaseSubmitRow> rowOpt = table.getRows().stream()
                .filter(databaseRow -> databaseRow instanceof DatabaseSubmitRow
                        && databaseRow.getId().getKeyAsString().equals(key))
                .map(databaseRow -> (DatabaseSubmitRow) databaseRow)
                .findFirst();

        if (!rowOpt.isPresent()) {
            val row = DatabaseSubmitRow.submitRowBuilder()
                    .source(source)
                    .id(new RowId(key))
                    .fk(parentKey.map(RowId::new).orElse(null))
                    .action(update)
                    .build();
            table.getRows().add(row);
            rowOpt = Optional.of(row);
        }

        return rowOpt.get();
    }

    /**
     * Increments the number of batches executed.
     */
    public void incrementBatchCount() {
        executedBatches++;
    }
}
