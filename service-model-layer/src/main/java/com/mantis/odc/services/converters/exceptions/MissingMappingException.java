/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.converters.exceptions;

import static java.text.MessageFormat.format;

/**
 * On a missing mapping type.
 */
public class MissingMappingException extends Exception {

    public MissingMappingException(MappingType type, String id) {
        super(format("Failed to find mapping of type {0} with the name {1}.", type, id));
    }

    /**
     * Types of mappings used in the system.
     */
    public enum MappingType {
        Table, Field, Link, Enumeration, Entity, Attribute, Relationship
    }
}
