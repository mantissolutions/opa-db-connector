/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models.base;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Models a base enumeration value.
 */
@Data
@AllArgsConstructor
public abstract class DatabaseEnumerationValue {

    /**
     * Description value.
     */
    protected final String description;

    /**
     * Child values. Can be of any type.
     */
    protected List<Object> childValues;

    /**
     * @return enumeration value
     */
    public abstract Object getValue();

    /**
     * Adds a child value.
     *
     * @param object value
     * @return current enumeration value
     */
    public DatabaseEnumerationValue child(Object object) {
        if (!(childValues instanceof HashSet)) childValues = new ArrayList<>(childValues);
        childValues.add(object);
        return this;
    }
}
