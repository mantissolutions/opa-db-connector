/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.services.models;

import com.mantis.odc.models.DataType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Base field type. Has a source, type and usage restrictions.
 */
@Data
@Builder(builderMethodName = "fieldBuilder")
@AllArgsConstructor
public class DatabaseField {

    /**
     * Field name.
     */
    protected final String source;

    /**
     * Opa Name.
     */
    protected final String opaName;

    /**
     * Field type.
     */
    protected final DataType type;

    public DatabaseField(String source, DataType dataType) {
        this(source, source, dataType);
    }

    /**
     * @return returns the OPA name or source
     */
    public String getOpaName() {
        return opaName != null ? opaName : source;
    }
}
