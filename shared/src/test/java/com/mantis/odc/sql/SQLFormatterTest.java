/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.sql;

import lombok.val;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test the SQL formatter.
 */
public class SQLFormatterTest {

    @Test
    public void format1() {
        formatTest(
                "SELECT t1.CHILD_ID AS \"child id\", t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE t1.CHILD_ID = :CHILD_ID",
                "SELECT t1.CHILD_ID AS \"child id\",\n t1.CHILD_NAME AS \"child name\",\n t1.CHILD_AGE AS \"child age\" \nFROM CHILDREN t1 \nWHERE t1.CHILD_ID = :CHILD_ID"
        );
    }

    @Test
    public void format2() {
        formatTest(
                "SELECT t1.CHILD_ID AS \"child id\", t1.CHILD_NAME AS \"child name\", t1.CHILD_AGE AS \"child age\" FROM CHILDREN t1 WHERE t1.CHILD_ID = :CHILD_ID AND t1.CHILD_NAME IN (:CHILD_NAME_0, :CHILD_NAME_1, :CHILD_NAME_2)",
                "SELECT t1.CHILD_ID AS \"child id\",\n t1.CHILD_NAME AS \"child name\",\n t1.CHILD_AGE AS \"child age\" \nFROM CHILDREN t1 \nWHERE t1.CHILD_ID = :CHILD_ID \nAND t1.CHILD_NAME IN (:CHILD_NAME_0,\n :CHILD_NAME_1,\n :CHILD_NAME_2)"
        );
    }

    @Test
    public void format3() {
        formatTest(
                "SELECT t2.TOY_ID AS \"toy id\", t2.TOY_NAME AS \"toy name\", t2.TOY_COST AS \"toy cost\", t2.EXP_DATE AS \"expiry date\", t2.SECOND_HAND AS \"second hand\", t2.CHILD_ID AS \"CHILD_ID\" FROM TOYS t2 INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID) WHERE t1.CHILD_ID = :CHILD_ID",
                "SELECT t2.TOY_ID AS \"toy id\",\n t2.TOY_NAME AS \"toy name\",\n t2.TOY_COST AS \"toy cost\",\n t2.EXP_DATE AS \"expiry date\",\n t2.SECOND_HAND AS \"second hand\",\n t2.CHILD_ID AS \"CHILD_ID\" \nFROM TOYS t2 \nINNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID) \nWHERE t1.CHILD_ID = :CHILD_ID"
        );
    }

    @Test
    public void format4() {
        formatTest(
                "DELETE FROM TOYS_PETS" +
                        " WHERE TOY_ID IN (SELECT t2.TOY_ID" +
                        " FROM TOYS t2" +
                        " INNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = :CHILD_ID)" +
                        " AND PET_ID IN (SELECT t3.PET_ID" +
                        " FROM PETS t3" +
                        " INNER JOIN CHILDREN t1 ON (t3.CHILD_ID = t1.CHILD_ID)" +
                        " WHERE t1.CHILD_ID = :CHILD_ID)",
                "DELETE \nFROM TOYS_PETS" +
                        " \nWHERE TOY_ID IN (SELECT t2.TOY_ID" +
                        " \nFROM TOYS t2" +
                        " \nINNER JOIN CHILDREN t1 ON (t2.CHILD_ID = t1.CHILD_ID)" +
                        " \nWHERE t1.CHILD_ID = :CHILD_ID)" +
                        " \nAND PET_ID IN (SELECT t3.PET_ID" +
                        " \nFROM PETS t3" +
                        " \nINNER JOIN CHILDREN t1 ON (t3.CHILD_ID = t1.CHILD_ID)" +
                        " \nWHERE t1.CHILD_ID = :CHILD_ID)"
        );
    }

    @Test
    public void format5() {
        formatTest(
                "INSERT INTO TOYS(TOY_ID, CHILD_ID, TOY_NAME, TOY_COST, EXP_DATE, SECOND_HAND) VALUES (:TOY_ID, :CHILD_ID, :TOY_NAME, :TOY_COST, :EXP_DATE, :SECOND_HAND)",
                "INSERT INTO TOYS(TOY_ID,\n CHILD_ID,\n TOY_NAME,\n TOY_COST,\n EXP_DATE,\n SECOND_HAND) VALUES (:TOY_ID,\n :CHILD_ID,\n :TOY_NAME,\n :TOY_COST,\n :EXP_DATE,\n :SECOND_HAND)"
        );
    }

    @Test
    public void format6() {
        formatTest(
                "UPDATE TOYS SET TOY_NAME = :TOY_NAME, TOY_COST = :TOY_COST, EXP_DATE = :EXP_DATE, SECOND_HAND = :SECOND_HAND WHERE TOY_ID = :TOY_ID",
                "UPDATE TOYS SET TOY_NAME = :TOY_NAME,\n TOY_COST = :TOY_COST,\n EXP_DATE = :EXP_DATE,\n SECOND_HAND = :SECOND_HAND \nWHERE TOY_ID = :TOY_ID"
        );
    }

    private void formatTest(String input, String expected) {
        val actual = SQLFormatter.format(input);
        Assert.assertEquals(expected, actual);
    }
}