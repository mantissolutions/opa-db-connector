/*
 * Copyright (c) 2016. Mantis Innovations
 */

package org.eclipse.persistence.logging.slf4j;

import lombok.val;
import org.eclipse.persistence.logging.AbstractSessionLog;
import org.eclipse.persistence.logging.SessionLog;
import org.eclipse.persistence.logging.SessionLogEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * <p>
 * This is a wrapper class for SLF4J. It is used when messages need to be logged
 * through SLF4J.
 * </p>
 * <p>
 * Use the following configuration for using SLF4J with EclipseLink
 * <code>eclipselink.logging.logger</code> and the value
 * <code>org.eclipse.persistence.logging.Slf4jSessionLogger</code>
 * </p>
 * <p>
 * Use the following categories from EclipseLink
 * (eclipselink.logging.timestamp, eclipselink.logging.thread,
 * eclipselink.logging.session, eclipselink.logging.connection
 * y eclipselink.logging.parameters).
 * <p>
 * Logging categories available are:
 * <p>
 * <ul>
 * <li>org.eclipse.persistence.logging.default
 * <li>org.eclipse.persistence.logging.sql
 * <li>org.eclipse.persistence.logging.transaction
 * <li>org.eclipse.persistence.logging.event
 * <li>org.eclipse.persistence.logging.connection
 * <li>org.eclipse.persistence.logging.query
 * <li>org.eclipse.persistence.logging.cache
 * <li>org.eclipse.persistence.logging.propagation
 * <li>org.eclipse.persistence.logging.sequencing
 * <li>org.eclipse.persistence.logging.ejb
 * <li>org.eclipse.persistence.logging.ejb_or_metadata
 * <li>org.eclipse.persistence.logging.weaver
 * <li>org.eclipse.persistence.logging.properties
 * <li>org.eclipse.persistence.logging.server
 * </ul>
 * </p>
 * <p>
 * Mapping of Java Log Level to SLF4J Log Level:
 * </p>
 * <ul>
 * <li>ALL,FINER,FINEST -> TRACE
 * <li>FINE -> DEBUG
 * <li>CONFIG,INFO -> INFO
 * <li>WARNING -> WARN
 * <li>SEVERE -> ERROR
 * </ul>
 * </p>
 * <p>
 */
public class Slf4jSessionLogger extends AbstractSessionLog {

    public static final String ECLIPSELINK_NAMESPACE = "org.eclipse.persistence.logging";
    public static final String DEFAULT_CATEGORY = "default";
    public static final String DEFAULT_ECLIPSELINK_NAMESPACE = ECLIPSELINK_NAMESPACE + "." + DEFAULT_CATEGORY;

    private static final Map<Integer, LogLevel> MAP_LEVELS = new HashMap<>();

    static {
        MAP_LEVELS.put(SessionLog.ALL, LogLevel.TRACE);
        MAP_LEVELS.put(SessionLog.FINEST, LogLevel.TRACE);
        MAP_LEVELS.put(SessionLog.FINER, LogLevel.TRACE);
        MAP_LEVELS.put(SessionLog.FINE, LogLevel.DEBUG);
        MAP_LEVELS.put(SessionLog.CONFIG, LogLevel.INFO);
        MAP_LEVELS.put(SessionLog.INFO, LogLevel.INFO);
        MAP_LEVELS.put(SessionLog.WARNING, LogLevel.WARN);
        MAP_LEVELS.put(SessionLog.SEVERE, LogLevel.ERROR);
    }

    private final Map<String, Logger> categoryLoggers = new HashMap<>();

    public Slf4jSessionLogger() {
        super();
        createCategoryLoggers();
    }

    @Override
    public void log(SessionLogEntry entry) {
        if (!shouldLog(entry.getLevel(), entry.getNameSpace())) return;

        val logger = getLogger(entry.getNameSpace());
        val logLevel = getLogLevel(entry.getLevel());

        val message = new StringBuilder();
        message.append(getSupplementDetailString(entry));
        message.append(formatMessage(entry));

        switch (logLevel) {
            case TRACE:
                logger.trace(message.toString());
                break;
            case DEBUG:
                logger.debug(message.toString());
                break;
            case INFO:
                logger.info(message.toString());
                break;
            case WARN:
                logger.warn(message.toString());
                break;
            case ERROR:
                logger.error(message.toString());
                break;
        }
    }

    @Override
    public boolean shouldLog(int level, String category) {
        val logger = getLogger(category);
        val logLevel = getLogLevel(level);

        switch (logLevel) {
            case TRACE:
                return logger.isTraceEnabled();
            case DEBUG:
                return logger.isDebugEnabled();
            case INFO:
                return logger.isInfoEnabled();
            case WARN:
                return logger.isWarnEnabled();
            case ERROR:
                return logger.isErrorEnabled();
            default:
                return true;
        }
    }

    @Override
    public boolean shouldLog(int level) {
        return shouldLog(level, "default");
    }

    /**
     * Return true if SQL logging should log visible bind parameters. If the
     * shouldDisplayData is not set, return false.
     */
    @Override
    public boolean shouldDisplayData() {
        return shouldDisplayData != null ? shouldDisplayData : false;
    }

    /**
     * Initialize loggers eagerly
     */
    private void createCategoryLoggers() {
        for (val category : SessionLog.loggerCatagories) {
            addLogger(category, ECLIPSELINK_NAMESPACE + "." + category);
        }
        // Logger default
        addLogger(DEFAULT_CATEGORY, DEFAULT_ECLIPSELINK_NAMESPACE);
    }

    /**
     * Add Logger to the categoryLoggers.
     */
    private void addLogger(String loggerCategory, String loggerNameSpace) {
        categoryLoggers.put(loggerCategory, LoggerFactory.getLogger(loggerNameSpace));
    }

    /**
     * Return the Logger for the given category
     */
    private Logger getLogger(String category) {
        if (isNullOrEmpty(category) || !this.categoryLoggers.containsKey(category)) category = DEFAULT_CATEGORY;
        return categoryLoggers.get(category);
    }

    /**
     * Return the corresponding Slf4j Level for a given EclipseLink level.
     */
    private LogLevel getLogLevel(Integer level) {
        LogLevel logLevel = MAP_LEVELS.get(level);
        if (logLevel == null) logLevel = LogLevel.OFF;
        return logLevel;
    }

    /**
     * SLF4J log levels.
     */
    enum LogLevel {
        TRACE, DEBUG, INFO, WARN, ERROR, OFF
    }
}