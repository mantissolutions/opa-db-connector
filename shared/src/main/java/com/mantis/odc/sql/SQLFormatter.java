/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.sql;

import lombok.experimental.UtilityClass;
import lombok.val;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Pretty prints SQL.
 * <p>
 * 1. Places returns before key words
 * 2. Aligns select, insert and update sections TODO
 * 2. Nests select statements. TODO
 */
@UtilityClass
public class SQLFormatter {

    private static final Map<String, Function<String, Boolean>> formatterKeys = new HashMap<String, Function<String, Boolean>>() {{
        put("newLineBefore", s -> Stream.of("SELECT", "INSERT", "UPDATE", "DELETE", "FROM", "WHERE", "AND", "OR", "(", "INNER")
                .anyMatch(s1 -> s1.equalsIgnoreCase(s)));
        put("newLineAfter", s -> Stream.of(",")
                .anyMatch(s::endsWith));
    }};

    private static final Map<String, Function<String, String>> formatters = new HashMap<String, Function<String, String>>() {{
        put("newLineBefore", s -> "\n" + s);
        put("newLineAfter", s -> s + "\n");
    }};

    /**
     * Formats a given SQL string.
     *
     * @param sql SQL to format
     * @return formatted SQL
     */
    public String format(String sql) {
        val newSQL = new ArrayList<String>();

        // tokenize the  on spaces
        val tokens = sql.split(" ");

        for (val token : tokens) {
            if (hasFormatter(token) && !newSQL.isEmpty()) {
                val formatter = getFormatter(token);
                newSQL.add(formatter.apply(token));
            } else newSQL.add(token);
        }

        return newSQL.stream().collect(Collectors.joining(" "));
    }

    /**
     * Returns the formatter for the given key.
     *
     * @param token token
     * @return formatter
     */
    private Function<String, String> getFormatter(String token) {
        return getFormatterKey(token).map(formatters::get).get();
    }

    /**
     * Returns the key for a given token.
     *
     * @param token token
     * @return formatter key or none
     */
    private Optional<String> getFormatterKey(String token) {
        for (val es : formatterKeys.entrySet()) {
            if (es.getValue().apply(token)) return Optional.of(es.getKey());
        }
        return Optional.empty();
    }

    /**
     * Returns true if the tokens has a formatter.
     *
     * @param token token to format
     * @return true if formatter exists
     */
    private boolean hasFormatter(String token) {
        return getFormatterKey(token).isPresent();
    }
}
