/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.security;

import com.mantis.odc.security.realms.JPARealm;
import org.apache.shiro.config.Ini;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.web.env.IniWebEnvironment;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Base security environment.
 */
@Singleton
public class AppEnvironment extends IniWebEnvironment {

    @Inject
    public AppEnvironment(JPARealm realm) {
        setIni(Ini.fromResourcePath("classpath:shiro.ini"));
        init();
        // set injected realm
        ((RealmSecurityManager) getSecurityManager()).setRealm(realm);
    }
}
