/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.security.realms;

import com.mantis.odc.database.AppDB;
import com.mantis.odc.database.base.DAO;
import com.mantis.odc.models.User;
import lombok.NonNull;
import lombok.val;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.codec.Hex;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Realm that pull information from a database using JPA.
 */
@Singleton
public class JPARealm extends AuthorizingRealm {

    /**
     * DOA object for users stored in the database.
     */
    private final DAO<String, User> users;

    /**
     * @param db                 application database
     * @param credentialsMatcher credentials matcher
     */
    @Inject
    public JPARealm(AppDB db, CredentialsMatcher credentialsMatcher) {
        users = db.getDAO(User.class);
        setCredentialsMatcher(credentialsMatcher);
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(@NonNull AuthenticationToken token) {
        return users.get(((UsernamePasswordToken) token).getUsername())
                .map(u -> {
                    val info = new SimpleAuthenticationInfo(
                            u.getUsername(),
                            u.getPassword().toCharArray(),
                            getName()
                    );
                    info.setCredentialsSalt(ByteSource.Util.bytes(Hex.decode(u.getPasswordSalt())));
                    return info;
                })
                .orElseThrow(() -> new AuthorizationException("PrincipalCollection method argument cannot be null."));
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(@NonNull PrincipalCollection principals) {
        return users.get((String) getAvailablePrincipal(principals))
                .map(u -> new SimpleAuthorizationInfo(u.getRoles()))
                .orElseThrow(() -> new AuthorizationException("PrincipalCollection method argument cannot be null."));
    }

}
