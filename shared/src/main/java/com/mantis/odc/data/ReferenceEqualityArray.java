/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.data;

import lombok.val;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.IntStream;

import static java.util.Collections.EMPTY_LIST;

/**
 * Uses reference instead of object equality on comparisons.
 */
public class ReferenceEqualityArray<MODEL> extends ArrayList<MODEL> {

    public ReferenceEqualityArray() {
    }

    public ReferenceEqualityArray(Collection items) {
        super(items != null ? items : EMPTY_LIST);
    }

    /**
     * Finds the given object in the array by reference.
     *
     * @param model model to search for
     * @return found model or none
     */
    public Optional<MODEL> getOpt(MODEL model) {
        return stream().filter(model1 -> model1 == model).findFirst();
    }

    /**
     * Returns the index of the given object.
     *
     * @param model model to search for
     * @return index or none
     */
    public OptionalInt getIndex(MODEL model) {
        return IntStream.range(0, size())
                .filter(i -> get(i) == model)
                .findFirst();
    }

    @Override
    public boolean remove(Object o) {
        val indexOpt = getIndex((MODEL) o);
        indexOpt.ifPresent(this::remove);
        return indexOpt.isPresent();
    }
}
