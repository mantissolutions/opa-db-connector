/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.di;

import com.google.gson.Gson;
import com.mantis.odc.config.AppConfig;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import dagger.Module;
import dagger.Provides;
import io.gsonfire.GsonFireBuilder;
import lombok.val;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;

import javax.inject.Singleton;
import java.io.File;

import static java.text.MessageFormat.format;

/**
 * Shared Module.
 */
@Module
@Singleton
public class SharedModule {

    /**
     * @return global Gson object.
     */
    @Provides
    @Singleton
    public static Gson createGson() {
        return new GsonFireBuilder()
                .enableExposeMethodResult()
                .createGson();
    }

    /**
     * @return default application configuration
     */
    @Provides
    @Singleton
    public static Config getApplicationConfig() {
        val defaults = ConfigFactory.load();
        val configFile = new File(format("{0}/application.conf", defaults.getString("homeFolder")));
        return configFile.exists() ? ConfigFactory.parseFile(configFile).withFallback(defaults) : defaults;
    }

    /**
     * @param config application config
     * @return returns the application credentials matcher.
     */
    @Provides
    public static CredentialsMatcher getCredentialsMatcher(AppConfig config) {
        val credentialsMatcher = new HashedCredentialsMatcher();
        credentialsMatcher.setHashAlgorithmName(config.getSecurity().getHashType().getName());
        credentialsMatcher.setHashIterations(config.getSecurity().getIterations());
        return credentialsMatcher;
    }
}
