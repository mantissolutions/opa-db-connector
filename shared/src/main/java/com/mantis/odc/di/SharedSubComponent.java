/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.di;

import com.mantis.odc.config.AppConfig;
import com.mantis.odc.database.AppDB;
import com.mantis.odc.security.AppEnvironment;
import dagger.Subcomponent;

import javax.inject.Singleton;

/**
 * Shared sub component.
 */
@Singleton
@Subcomponent(modules = SharedModule.class)
public interface SharedSubComponent {

    /**
     * @return returns the application configuration
     */
    AppConfig getConfig();

    /**
     * @return returns the application database
     */
    AppDB getDatabase();

    /**
     * @return application security environment
     */
    AppEnvironment getAppEnvironment();
}
