/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.models;

import lombok.*;

import javax.persistence.*;
import java.util.*;

/**
 * User Information.
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "odc_users")
public class User {

    @Id
    @Column(name = "user_username", nullable = false)
    private String username;
    @Column(name = "user_password", nullable = false)
    private String password;
    @Column(name = "user_password_salt", nullable = false)
    private String passwordSalt;
    @Column(name = "user_roles")
    private String roleNames;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<WidgetConfig> configList = new ArrayList<>();

    /**
     * @return all the given role names
     */
    public Set<String> getRoles() {
        val roles = new HashSet<String>();
        Collections.addAll(roles, roleNames.split(","));
        return roles;
    }

    /**
     * @param id widget id to remove
     */
    public void removeConfig(String id) {
        configList.removeIf(wc -> wc.getId().equals(id));
    }
}
