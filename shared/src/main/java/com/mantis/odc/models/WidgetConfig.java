/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mantis.odc.di.SharedModule;
import lombok.*;

import javax.persistence.*;
import java.util.Map;

/**
 * Config for a given widget.
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "odc_widget_config")
@ToString(exclude = "user")
@EqualsAndHashCode(exclude = "user")
public class WidgetConfig {

    @Id
    @Column(name = "widget_id", nullable = false)
    private String id;
    @Column(name = "widget_x", nullable = false)
    private int x;
    @Column(name = "widget_y", nullable = false)
    private int y;
    @Column(name = "widget_width", nullable = false)
    private int width;
    @Column(name = "widget_height", nullable = false)
    private int height;
    /**
     * JSON encoded settings.
     */
    @Column(name = "widget_settings")
    private String settings;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_username", nullable = false)
    private User user;

    private final static Gson json = SharedModule.createGson();

    /**
     * @return settings.
     */
    public Map<String, String> getConfig() {
        return json.fromJson(settings, new TypeToken<Map<String, String>>() {
        }.getType());
    }

    /**
     * @param config config to set
     */
    public void setConfig(Map<String, String> config) {
        setSettings(json.toJson(config));
    }
}
