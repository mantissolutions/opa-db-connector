/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.database.base;

import com.mantis.odc.database.AppDB;
import lombok.Cleanup;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Consumer;

/**
 * Database access Object.
 *
 * @param <ID>    Id type for the model
 * @param <MODEL> class of the model
 */
@Slf4j
public class DAO<ID, MODEL> {

    /**
     * Base model class.
     */
    @Getter
    private final Class<MODEL> modelClass;

    /**
     * JPA entity manager.
     */
    @Getter
    private final AppDB appDB;

    /**
     * @param modelClass model class
     * @param appDB      application database
     */
    public DAO(Class<MODEL> modelClass, AppDB appDB) {
        this.modelClass = modelClass;
        this.appDB = appDB;
    }

    /**
     * Saves a given model within a transaction.
     *
     * @param model model to saveButton
     * @return model
     */
    public MODEL create(final MODEL model) {
        transaction(em -> em.persist(model));
        return model;
    }

    /**
     * Updates a given model
     *
     * @param model model to update
     */
    public MODEL update(final MODEL model) {
        return transactionReturningModel(model, EntityManager::merge);
    }

    /**
     * Deletes a given model within a transaction.
     *
     * @param id model id
     */
    public void delete(final ID id) {
        transaction(em -> {
            val model = em.getReference(modelClass, id);
            em.remove(model);
        });
    }

    /**
     * Runs the given typed query and returns the models.
     *
     * @param query typed query to run
     * @return list of models
     */
    public List<MODEL> query(Query<MODEL> query) {
        @Cleanup val em = em();

        // create typed query
        TypedQuery<MODEL> typedQuery =
                em.createQuery(query.createQuery(em, em.getCriteriaBuilder()));

        // return results
        return typedQuery.getResultList();
    }

    /**
     * Runs the given SQL and returns the models.
     *
     * @param sql    SQL to execute
     * @param params list of bind parameters
     * @return list of models
     */
    public List<MODEL> query(String sql, Map<String, Object> params) {
        @Cleanup val em = em();
        val clazz = getModelClass();

        // create typed query
        val query = em.createQuery(sql, clazz);

        if (params != null) for (Map.Entry<String, Object> e : params.entrySet())
            query.setParameter(e.getKey(), e.getValue());

        // return results
        return query.getResultList();
    }

    /**
     * Results a single result or null.
     *
     * @param sql    SQL to execute
     * @param params list of bind params
     * @return single result
     */
    public MODEL scalarQuery(String sql, HashMap<String, Object> params) {
        val result = query(sql, params);
        return (!result.isEmpty()) ? result.get(0) : null;
    }

    /**
     * Returns a given model by id value.
     *
     * @param id model id value.
     * @return requested model if found
     */
    public Optional<MODEL> get(@NonNull ID id) {
        @Cleanup val em = em();
        return Optional.ofNullable(em.find(getModelClass(), id));
    }

    /**
     * Returns all models.
     *
     * @return list of models
     */
    public List<MODEL> list() {
        val clazz = getModelClass();

        return query((em, builder) -> {
            val query = builder.createQuery(clazz);
            query.from(clazz);
            return query;
        });
    }

    /**
     * Performs a unit of work under a transaction
     *
     * @param transaction transaction to run
     */
    private void transaction(Consumer<EntityManager> transaction) {
        @Cleanup val em = em();
        val et = em.getTransaction();

        try {
            if (!et.isActive()) et.begin();
            transaction.accept(em);
            et.commit();
        } catch (Exception e) {
            log.error("Error running transaction.", e);
            // or roll back on error
            if (et.isActive()) et.rollback();
            // pass exception up
            throw e;
        }
    }

    /**
     * Performs a unit of work under a transaction returning the model.
     *
     * @param model     model
     * @param operation operation to run under transaction
     * @return model
     */
    private MODEL transactionReturningModel(MODEL model, BiFunction<EntityManager, MODEL, MODEL> operation) {
        @Cleanup val em = em();
        val et = em.getTransaction();
        try {
            if (!et.isActive()) et.begin();
            val returnModel = operation.apply(em, model);
            et.commit();
            return returnModel;
        } catch (Exception e) {
            log.error("Error running transaction.", e);
            // or roll back on error
            if (et.isActive()) et.rollback();
            // pass exception up
            throw e;
        }
    }

    /**
     * Returns a fresh entity manager.
     *
     * @return entity manager
     */
    private EntityManager em() {
        return appDB.getEm();
    }

    /**
     * Defines a query to be created.
     *
     * @param <MODEL> model to query
     */
    @FunctionalInterface
    private interface Query<MODEL> {

        /**
         * Query to be created.
         *
         * @param em      entity manager
         * @param builder criteria builder
         * @return query
         */
        CriteriaQuery<MODEL> createQuery(EntityManager em, CriteriaBuilder builder);
    }
}
