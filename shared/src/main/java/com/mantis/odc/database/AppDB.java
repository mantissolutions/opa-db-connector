/*
 * Copyright (c) 2015. Mantis Innovations
 */

package com.mantis.odc.database;

import com.github.davidmoten.rx.jdbc.Database;
import com.mantis.odc.config.AppConfig;
import com.mantis.odc.config.database.DatabaseConfig;
import com.mantis.odc.database.base.DAO;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import lombok.val;
import org.eclipse.persistence.logging.slf4j.Slf4jSessionLogger;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;
import java.util.HashMap;

import static javax.persistence.FlushModeType.COMMIT;
import static org.eclipse.persistence.config.PersistenceUnitProperties.LOGGING_LOGGER;
import static org.eclipse.persistence.config.PersistenceUnitProperties.NON_JTA_DATASOURCE;


/**
 * Manages access to the database.
 */
@Getter
@Singleton
public final class AppDB {

    /**
     * Database config.
     */
    private final DatabaseConfig config;

    /**
     * Root connection pool.
     */
    private final DataSource connectionPool;

    /**
     * Used to access rx.
     */
    private final Database rxConnection;

    /**
     * Root JPA entity management factory.
     */
    private final EntityManagerFactory emf;

    @Inject
    public AppDB(AppConfig appConfig) {
        config = appConfig.getDatabase();
        connectionPool = createDataSource(config);
        emf = createEntityManagerFactory(config, connectionPool);
        rxConnection = Database.fromDataSource(connectionPool);
    }

    /**
     * @return returns a entity manager.
     */
    public EntityManager getEm() {
        val em = emf.createEntityManager();
        em.setFlushMode(COMMIT);
        return em;
    }

    /**
     * Creates a new data access object for the given class.
     *
     * @param clazz class to create the DAO for
     * @return DOA for the given class.
     */
    public <ID, MODEL> DAO<ID, MODEL> getDAO(final Class<MODEL> clazz) {
        return new DAO<>(clazz, this);
    }

    /**
     * Returns the database connection pool.
     *
     * @param c database config
     * @return data source
     */
    private static DataSource createDataSource(DatabaseConfig c) {
        val config = new HikariConfig();

        // core settings
        config.setDriverClassName(c.getType().getDriver());
        config.setJdbcUrl(c.getUrl());
        config.setUsername(c.getUsername());
        config.setPassword(c.getPassword());

        // other settings
        config.setConnectionTimeout(c.getConnectionTimeoutMs());
        config.setIdleTimeout(c.getIdleTimeoutMs());
        config.setMaxLifetime(c.getMaxLifetimeMs());
        config.setMaximumPoolSize(c.getMaxPoolSize());
        config.setMinimumIdle(c.getMinIdle());

        return new HikariDataSource(config);
    }

    /**
     * Returns the persistence unit from the application configuration.
     *
     * @param pool connection pool
     * @return persistence unit
     */
    private static EntityManagerFactory createEntityManagerFactory(DatabaseConfig config, DataSource pool) {
        val properties = new HashMap<String, Object>();
        properties.put(NON_JTA_DATASOURCE, pool);
        properties.put(LOGGING_LOGGER, Slf4jSessionLogger.class.getCanonicalName());
        val eclipseLinkConfig = config.getEclipseLinkSettings();
        eclipseLinkConfig.keySet().forEach(key -> properties.put(key, eclipseLinkConfig.get(key).unwrapped()));
        return Persistence.createEntityManagerFactory("root-pu", properties);
    }

    /**
     * Shuts down the root connection pool.
     */
    public void close() {
        ((HikariDataSource) connectionPool).close();
    }
}
