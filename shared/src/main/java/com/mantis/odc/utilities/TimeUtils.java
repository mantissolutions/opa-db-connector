/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.utilities;

import lombok.experimental.UtilityClass;
import lombok.val;

import java.time.Duration;

/**
 * Utilities for working with time.
 */
@UtilityClass
public class TimeUtils {

    /**
     * Returns the duration using hh:mm:ss
     *
     * @param duration time between
     * @return formatted duration
     */
    public String formatDuration(long duration) {
        val secondsActive = Duration.ofNanos(duration).getSeconds();
        return String.format("%d:%02d:%02d", secondsActive / 3600, (secondsActive % 3600) / 60, (secondsActive % 60));
    }
}
