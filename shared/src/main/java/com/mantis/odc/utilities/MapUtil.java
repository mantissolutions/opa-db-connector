/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.utilities;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import lombok.val;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Utilities for creating maps.
 */
@UtilityClass
public class MapUtil {

    /**
     * Creates a lookup map with the given types.
     *
     * @param items items
     * @param key   key function
     * @param <K>   key type
     * @param <T>   item type
     * @return map of K to T
     */
    public <K, T> Map<K, T> createMapByType(@NonNull List<T> items, Function<T, K> key) {
        val map = new HashMap<K, T>();
        for (val t : items) map.put(key.apply(t), t);
        return map;
    }

    /**
     * Creates a lookup map of values with the given types.
     *
     * @param items items
     * @param key   key function
     * @param value value function
     * @param <K>   key type
     * @param <T>   item type
     * @return map of K to T
     */
    public <K, T, R> Map<K, R> createMapByType(
            @NonNull List<T> items, Function<T, K> key, Function<T, R> value) {
        val map = new HashMap<K, R>();
        for (val t : items) map.put(key.apply(t), value.apply(t));
        return map;
    }

    /**
     * Creates a lookup map for lists of values against key.
     *
     * @param items  items
     * @param filter test filter
     * @param key    key function
     * @param <K>    key type
     * @param <T>    item type
     * @return map of list of T to K
     */
    public <K, T> Map<K, List<T>> createMapOfListByType(
            @NonNull List<T> items, BiFunction<T, T, Boolean> filter, Function<T, K> key) {
        val map = new HashMap<K, List<T>>();
        for (val ct : items) {
            val itemList = new ArrayList<T>();
            for (val ctd : items) if (filter.apply(ct, ctd)) itemList.add(ctd);
            map.put(key.apply(ct), itemList);
        }
        return map;
    }
}
