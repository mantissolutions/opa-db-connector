/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.utilities;

import lombok.experimental.UtilityClass;
import lombok.val;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Utilities for dealing with errors.
 */
@UtilityClass
public class ErrorUtil {

    public String errorToString(Throwable e) {
        val sr = new StringWriter();
        val writer = new PrintWriter(sr);
        e.printStackTrace(writer);
        return sr.toString();
    }
}
