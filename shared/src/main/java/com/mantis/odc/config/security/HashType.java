/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.config.security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported hash types.
 */
@Getter
@RequiredArgsConstructor
public enum HashType {
    Md5("MD5"),
    Sha1("SHA-1"),
    Sha256("SHA-256");

    private final String name;
}
