/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.config.security;

import lombok.Builder;
import lombok.Value;

/**
 * Stores security configuration information.
 */
@Value
@Builder
public class SecurityConfig {

    /**
     * Number of hash iterations to perform.
     */
    private final int iterations;

    /**
     * Type of hash to use.
     */
    private final HashType hashType;
}
