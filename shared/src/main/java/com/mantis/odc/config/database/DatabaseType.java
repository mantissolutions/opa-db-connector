/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.config.database;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Supported Database types.
 */
@Getter
@RequiredArgsConstructor
public enum DatabaseType {
    MySQL("com.mysql.jdbc.Driver", "classpath:/db/migration/mysql"),
    Oracle("oracle.jdbc.OracleDriver", "classpath:/db/migration/oracle");

    private final String driver;
    private final String migrationPath;
}
