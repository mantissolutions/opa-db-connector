/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.config;

/**
 * Supported deployment types.
 */
public enum DeploymentType {
    Connector, BatchProcessor, ConnectorWithBatchProcessor
}
