/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.config.database;

import com.typesafe.config.ConfigObject;
import lombok.Builder;
import lombok.Value;

/**
 * Database configuration for ODC.
 */
@Value
@Builder
public class DatabaseConfig {

    /**
     * Database type.
     */
    private final DatabaseType type;

    /**
     * URL of the database.
     */
    private final String url;

    /**
     * Database user.
     */
    private final String username;

    /**
     * Database password.
     */
    private final String password;

    /**
     * This property controls the maximum number of milliseconds that a client
     * (that's you) will wait for a connection from the pool. If this time is
     * exceeded without a connection becoming available, a SQLException will be
     * thrown. 1000ms is the minimum value. Default: 30000 (30 seconds)
     */
    private final long connectionTimeoutMs;

    /**
     * This property controls the maximum amount of time that a connection is
     * allowed to sit idle in the pool. Whether a connection is retired as idle
     * or not is subject to a maximum variation of +30 seconds, and average
     * variation of +15 seconds. A connection will never be retired as idle
     * before this timeout. A value of 0 means that idle connections are never
     * removed from the pool. Default: 600000 (10 minutes)
     */
    private final long idleTimeoutMs;

    /**
     * This property controls the maximum lifetime of a connection in the pool.
     * When a connection reaches this timeout it will be retired from the pool,
     * subject to a maximum variation of +30 seconds. An in-use connection will
     * never be retired, only when it is closed will it then be removed. We
     * strongly recommend setting this value, and it should be at least 30
     * seconds less than any database-level connection timeout. A value of 0
     * indicates no maximum lifetime (infinite lifetime), subject of course to
     * the idleTimeout setting. Default: 1800000 (30 minutes)
     */
    private final long maxLifetimeMs;

    /**
     * This property controls the maximum size that the pool is allowed to
     * reach, including both idle and in-use connections. Basically this value
     * will determine the maximum number of actual connections to the database
     * backend. A reasonable value for this is best determined by your execution
     * environment. When the pool reaches this size, and no idle connections are
     * available, calls to getConnection() will block for up to
     * connectionTimeout milliseconds before timing out. Default: 10
     */
    private final int maxPoolSize;

    /**
     * This property controls the minimum number of idle connections that
     * the pool tries to maintain in the pool. If the idle connections dip below
     * this value, the pool will make a best effort to add additional
     * connections quickly and efficiently. However, for maximum performance and
     * responsiveness to spike demands, we recommend not setting this value and
     * instead allowing the pool to act as a fixed size connection pool.
     * Default: same as maximumPoolSize
     */
    private final int minIdle;

    /**
     * Any settings for eclipse link.
     */
    private final ConfigObject eclipseLinkSettings;
}
