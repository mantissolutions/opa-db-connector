/*
 * Copyright (c) 2016. Mantis Innovations
 */

package com.mantis.odc.config;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.mantis.odc.config.database.DatabaseConfig;
import com.mantis.odc.config.database.DatabaseType;
import com.mantis.odc.config.security.HashType;
import com.mantis.odc.config.security.SecurityConfig;
import com.typesafe.config.Config;
import lombok.Getter;
import lombok.SneakyThrows;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.File;
import java.io.InputStreamReader;

import static java.text.MessageFormat.format;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * ODC application configuration.
 */
@Getter
@Singleton
public class AppConfig {

    /**
     * Version of the connector.
     */
    private final String applicationVersion;

    /**
     * Home folder for the configuration.
     */
    private final String homeFolder;

    /**
     * Maximum number of debug messages
     */
    private final long maxDebugMessages;

    /**
     * Database configuration.
     */
    private final DatabaseConfig database;

    /**
     * Security configuration.
     */
    private final SecurityConfig security;

    @SneakyThrows
    @Inject
    public AppConfig(Config c) {

        applicationVersion = CharStreams.toString(new InputStreamReader(
                getClass().getResourceAsStream("/version.txt"), Charsets.UTF_8));

        homeFolder = c.getString("homeFolder");

        maxDebugMessages = c.getLong("maxDebugMessages");

        // database config settings
        database = DatabaseConfig.builder()
                .type(DatabaseType.valueOf(c.getString("database.type")))
                .url(c.getString("database.url"))
                .username(c.getString("database.username"))
                .password(c.getString("database.password"))
                .connectionTimeoutMs(c.getDuration("database.connectionTimeoutMs", MILLISECONDS))
                .idleTimeoutMs(c.getDuration("database.idleTimeoutMs", MILLISECONDS))
                .maxLifetimeMs(c.getDuration("database.maxLifetimeMs", MILLISECONDS))
                .maxPoolSize(c.getInt("database.maxPoolSize"))
                .minIdle(c.getInt("database.minIdle"))
                .eclipseLinkSettings(c.getObject("database.eclipseLink"))
                .build();

        // security config
        security = SecurityConfig.builder()
                .iterations(c.getInt("security.iterations"))
                .hashType(HashType.valueOf(c.getString("security.hashType")))
                .build();
    }

    /**
     * @return logback config file
     */
    public File getLogbackConfig() {
        return new File(format("{0}/logback.xml", getHomeFolder()));
    }

}
