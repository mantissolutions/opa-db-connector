* Complete Documentation using ASCIIDOC
* Improve on REST API
* Add swagger documentation
* Integrate swagger into documentation
* Improve on Dashboard information